![RatelPlayer](resources/icons/ratel-player-128.png)
RatelPlayer
===========

General infomation
------------------

RatelPlayer is lightweight music player which is oriented to work with file system.

Application is based on GTK+ 3 and GStreamer 1.0 libraries to provide simple music player that manages media items through file manager.

In my opinion it's the most convenient method to manage and play music from big media collection. If it is manually organized, of course. Player doesn't care about such functionality. Main motivation for development was a fact that I was not totally happy with Exaile which I had used before.

Setup of environment
--------------------

There are scripts `prepare-environment-fedora.sh` and `prepare-environment-ubuntu.sh` in `scripts` folder which can be used to install necessary packages for `yum`-based and `apt`-based Linux distributives. These scripts should be run from `scripts` folder.

Building
--------

[![Build Status](https://travis-ci.org/tominokers/RatelPlayer.svg?branch=master)](https://travis-ci.org/tominokers/RatelPlayer)

**GNU/Linux**

Project uses `make` for building and it has the following targets:

 * `make all` - build application without tests;
 * `make tests` - build tests without application;
 * `make run` - build and run application;
 * `make run-tests` - build and run tests;
 * `make run-tests-with-ubsan` - run tests with undefined behaviour sanitizer;
 * `make run-tests-with-leaksan` - run tests with leak sanitizer.
 * `make run-tests-check-leaks` - build and run tests with valgrind;
 * `make run-app-check-leaks` - build and run application with valgrind;
 * `make install` - install application files in system;
 * `make uninstall` - remove application files from system;
 * `make rpm` - build rpm package of application;
 * `make dpkg` - build dpkg package of application;
 * `make doc` - build documentation of code base;
 * `make clean` - clean build resources of application.

Targets `all`, `tests`, `run`, `run-tests` can be configured with the following variables:
 * `ARCH` - determine to build 32 or 64 bit application, possible values: `x32`, `x64`.  Default value is specified by output of `arch` command.
 * `COMPILER` - determine to build appilcation with gcc or clang compilers, possible values: `gcc`, `clang`.  Default value is `gcc`.
 * `CODEGEN` - determine to build debug or release version of application, possible values: `Debug`, `Release`.  Default value is `Debug`.

Example of usage:

`make run ARCH=x32 CODEGEN=Release COMPILER=clang`

In result 32 bit release application will be build with clang compiler.

*Good to know*

 * Variable `ARCH` can be also specified for `rpm` target.
 * Filter for running tests can be applied by setting value to variable `TEST_FILTER=<filter mask>` to `run-tests` target (e.g. `make run-tests TEST_FILTER=PlaylistTests.sort_playlist_by_*`).
 * Variable `SANITIZER` can be used to compile code with sanitizers. Value of variable will be simply put as value for `-fsanitize=` compile option. There are also `make run-tests-with-ubsan` and `make run-tests-with-leaksan` commands which are proven to work well without errors in external libs of project.

Screenshots
-----------

The following screenshot shows view of player at version 0.8 on Fedora 26:

![Main dialog](resources/img/main_window.png)
