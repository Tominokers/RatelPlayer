Summary: Lightweight music player which is oriented to work with file system.
Name: RatelPlayer
Version: 0.8
Release: 1
License: GPLv2
Group: Applications/Multimedia
Source: RatelPlayer-0.8.tar.gz

%description
RatelPlayer is Music File Manager Player.

Application is based on GTK+ 3 and GStreamer 1.0 libraries to provide simple music player that manages media items through file manager like in Exaile, but smarter.

%prep
%setup

%build
make -j4 CODEGEN=Release ARCH=x64

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%license LICENSE
/usr/bin/RatelPlayer
/usr/share/applications/RatelPlayer.desktop
/usr/share/pixmaps/ratel-player.png
/usr/share/RatelPlayer/dictionaries/en.dic
/usr/share/RatelPlayer/dictionaries/ru.dic
/usr/share/RatelPlayer/dictionaries/ua.dic
