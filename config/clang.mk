#SHELL=/bin/bash

ifeq ($(CODEGEN),Debug)
	CFLAGS := -O0 -ggdb -pg
else
	CFLAGS := -O3
endif

CFLAGS += -g0
CFLAGS += -Weverything -Werror -pedantic \
			-Wno-c++98-compat \
			-Wno-padded \
			-Wno-weak-vtables \
			-Wno-c++98-compat-pedantic \
			-Wno-missing-prototypes \
			-Wno-old-style-cast \
			-Wno-unreachable-code \
			-Wno-documentation \
			-Wno-error=unused-command-line-argument \
			-fcolor-diagnostics \

CFLAGS += --system-header-prefix=glib \
			--system-header-prefix=gst \
			--system-header-prefix=sigc++ \
			--system-header-prefix=gio \
			--system-header-prefix=pango \
			--system-header-prefix=gdk \
			--system-header-prefix=gtk \
			--system-header-prefix=gtest \
			--system-header-prefix=gmock \

CFLAGS += -c -fmessage-length=0 -fPIC

TESTS_CFLAGS += -Wno-global-constructors \
			-Wno-exit-time-destructors \
			-Wno-used-but-marked-unused \

SHARED_OBJECT_CFLAGS := -pthread -shared
EXECUTABLE_CFLAGS := -pthread

ifeq ($(ARCH),x64)
	CFLAGS += -m64
	SHARED_OBJECT_CFLAGS += -m64
	EXECUTABLE_CFLAGS += -m64
endif
ifeq ($(ARCH),x32)
	CFLAGS += -m32
	SHARED_OBJECT_CFLAGS += -m32
	EXECUTABLE_CFLAGS += -m32
endif

ifneq (,$(SANITIZER))
	CFLAGS += -fsanitize=$(SANITIZER)
	EXECUTABLE_CFLAGS += -fsanitize=$(SANITIZER)
endif

CFLAGS_C := $(CFLAGS)
CFLAGS += -std=c++14

PRECOMPILED_HEADER_EXT := .pch
