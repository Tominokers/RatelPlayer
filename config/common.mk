#SHELL=/bin/bash

# define common variables

ifneq ($(COMPILER),)

ifeq ($(COMPILER),clang)
	CXX := clang++
	CC := clang
else # Use gcc by default
	CXX := g++
	CC := gcc
endif

endif

ifeq ($(MAKECMDGOALS),run-tests-with-ubsan)
	SANITIZER := undefined
endif
ifeq ($(MAKECMDGOALS),run-tests-with-leaksan)
	SANITIZER := leak
endif

# sanitizer was asked, rewrite compiler variables with clang
ifneq ($(SANITIZER),)
	CXX := clang++
	CC := clang
endif

ifeq ($(CODEGEN),)
	CODEGEN := Debug
endif

ifeq ($(ARCH),)
	ARCH := $(shell arch)

ifeq ($(ARCH),x86_64)
	ARCH := x64
endif
ifeq ($(ARCH),i686)
	ARCH := x32
endif

endif

ifeq ($(ARCH),x64)
export PKG_CONFIG_PATH=/usr/lib64/pkgconfig
endif
ifeq ($(ARCH),x32)
export PKG_CONFIG_PATH=/usr/lib/pkgconfig
endif

LINUX_DISTR := $(shell grep ^NAME= /etc/os-release | awk -F= '{print $$2}')

OUTPUT_DIR := bin/$(CODEGEN)_$(ARCH)
PROJECT_DIR := $(abspath .)
SRC_DIR := $(PROJECT_DIR)/src
DIRS_TO_CREATE :=
DEPENDICIES :=
