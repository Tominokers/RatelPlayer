#SHELL=/bin/bash

# define variables for core project
CORE_PROJ_DIR := $(PROJECT_DIR)/src/rpCore
CORE_TARGET_DIR := $(CORE_PROJ_DIR)/$(OUTPUT_DIR)
CORE_LIB := $(CORE_TARGET_DIR)/libRatelPlayerCore.a
CORE_CPP_SRCS := $(wildcard $(CORE_PROJ_DIR)/*.cpp) \
				 $(wildcard $(CORE_PROJ_DIR)/**/*.cpp) \
				 $(wildcard $(CORE_PROJ_DIR)/**/**/*.cpp)
CORE_OBJS := $(subst $(CORE_PROJ_DIR), $(CORE_TARGET_DIR), $(CORE_CPP_SRCS:.cpp=.o))
CORE_OUTDIRS := $(CORE_TARGET_DIR) $(sort $(dir $(CORE_OBJS)))
CORE_INCLUDE_DIRS := \
	-I$(SRC_DIR) \
	`pkg-config --cflags gstreamer-1.0` \
	`pkg-config --cflags glibmm-2.4`

DIRS_TO_CREATE += $(CORE_OUTDIRS)

CORE_HEADER_TO_PRECOMPILE := $(CORE_PROJ_DIR)/PrecompiledCore.hpp
PRECOMPILED_HEADER_CORE := $(CORE_HEADER_TO_PRECOMPILE)$(PRECOMPILED_HEADER_EXT)
FORECED_CORE_HEADER := -include $(CORE_HEADER_TO_PRECOMPILE)

DEPENDICIES += $(CORE_OBJS:.o=.d) $(CORE_HEADER_TO_PRECOMPILE).d
