#SHELL=/bin/bash

CPPCHECK_FLAGS := -q --enable=all --std=c++11 --error-exitcode=1
CPPCHECK_SUPPRESS := --suppress=unusedFunction --suppress=missingIncludeSystem
CPPCHECK_INCLUDES := -I src/rpCore -I src/rpObservers -I src/rpFacade -I face/GtkFace -I src/rpSettings -I tests
CPPCHECK_IGNORE := -i README.md -i LICENSE -i makefile -i thirdparty
