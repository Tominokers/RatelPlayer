#SHELL=/bin/bash

# define variables for rpfacade project
FACADE_PROJ_DIR := $(PROJECT_DIR)/src/rpFacade
FACADE_TARGET_DIR := $(FACADE_PROJ_DIR)/$(OUTPUT_DIR)
FACADE_LIB := $(FACADE_TARGET_DIR)/libRatelPlayerFacade.a
FACADE_CPP_SRCS := $(wildcard $(FACADE_PROJ_DIR)/*.cpp) \
				   $(wildcard $(FACADE_PROJ_DIR)/**/*.cpp) \
				   $(wildcard $(FACADE_PROJ_DIR)/**/**/*.cpp)
FACADE_OBJS := $(subst $(FACADE_PROJ_DIR), $(FACADE_TARGET_DIR), $(FACADE_CPP_SRCS:.cpp=.o))
FACADE_OUTDIRS := $(FACADE_TARGET_DIR) $(sort $(dir $(FACADE_OBJS)))
FACADE_INCLUDE_DIRS := \
	-I$(SRC_DIR) \
	`pkg-config --cflags gstreamer-1.0` \
	`pkg-config --cflags glibmm-2.4` \
	`pkg-config --cflags giomm-2.4`

DIRS_TO_CREATE += $(FACADE_OUTDIRS)

FACADE_HEADER_TO_PRECOMPILE := $(FACADE_PROJ_DIR)/PrecompiledFacade.hpp
PRECOMPILED_HEADER_FACADE := $(FACADE_HEADER_TO_PRECOMPILE)$(PRECOMPILED_HEADER_EXT)
FORECED_FACADE_HEADER := -include $(FACADE_HEADER_TO_PRECOMPILE)

DEPENDICIES += $(FACADE_OBJS:.o=.d) $(FACADE_HEADER_TO_PRECOMPILE).d
