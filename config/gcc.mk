#SHELL=/bin/bash

CFLAGS := -g3 -Wall -Wextra -Werror -c -fmessage-length=0 -fPIC -MMD -Wno-deprecated-declarations

ifeq ($(MAKECMDGOALS),prepare-perf)
	CFLAGS += -fno-omit-frame-pointer
endif

ifeq ($(CODEGEN),Debug)
	CFLAGS += -O0 -ggdb -p -pg
else
	CFLAGS += -O3
endif

SHARED_OBJECT_CFLAGS := -pthread -shared
EXECUTABLE_CFLAGS := -pthread

ifeq ($(ARCH),x64)
	CFLAGS += -m64
	SHARED_OBJECT_CFLAGS += -m64
	EXECUTABLE_CFLAGS += -m64
endif
ifeq ($(ARCH),x32)
	CFLAGS += -m32
	SHARED_OBJECT_CFLAGS += -m32
	EXECUTABLE_CFLAGS += -m32
endif

CFLAGS_C := $(CFLAGS)
CFLAGS += -std=c++14

PRECOMPILED_HEADER_EXT := .gch
