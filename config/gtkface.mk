#SHELL=/bin/bash

#define variables for gtk interface project
FACE_PROJ_DIR := $(PROJECT_DIR)/face
GTK_PROJ_DIR := $(FACE_PROJ_DIR)/GtkFace
GTK_TARGET_DIR := $(GTK_PROJ_DIR)/$(OUTPUT_DIR)
GTK_EXECUTABLE := $(GTK_TARGET_DIR)/RatelPlayer
GTK_CPP_SRCS := $(wildcard $(GTK_PROJ_DIR)/*.cpp) \
				$(wildcard $(GTK_PROJ_DIR)/**/*.cpp) \
				$(wildcard $(GTK_PROJ_DIR)/**/**/*.cpp) \
				$(wildcard $(GTK_PROJ_DIR)/**/**/**/*.cpp)
GTK_C_SRCS := $(GTK_PROJ_DIR)/gen/resource.c
GTK_OBJS := $(subst $(GTK_PROJ_DIR), $(GTK_TARGET_DIR), $(GTK_CPP_SRCS:.cpp=.o))
GTK_OBJS += $(subst $(GTK_PROJ_DIR), $(GTK_TARGET_DIR), $(GTK_C_SRCS:.c=.o))
GTK_GEN_DIR := $(GTK_PROJ_DIR)/gen
GTK_OUTDIRS := $(GTK_TARGET_DIR) $(sort $(dir $(GTK_OBJS)))
GTK_OUTDIRS += $(GTK_GEN_DIR)
GTK_INCLUDE_DIRS := \
	-I$(SRC_DIR) \
	-I$(FACE_PROJ_DIR) \
	-I/usr/include \
	`pkg-config --cflags gtkmm-3.0` \
	`pkg-config --cflags gstreamer-1.0` \

GTK_INCLUDE_LIB_DIRS := \
	-L$(OBSERVER_TARGET_DIR) \
	-L$(FACADE_TARGET_DIR) \
	-L$(SETTINGS_TARGET_DIR) \
	-L$(LOCALIZATION_TARGET_DIR) \
	-L$(CORE_TARGET_DIR) \

GTK_INCLUDE_LIBS := \
	-lRatelPlayerObservers \
	-lRatelPlayerFacade \
	-lRatelPlayerObservers \
	-lRatelPlayerSettings \
	-lRatelPlayerLocalization \
	-lRatelPlayerCore \
	`pkg-config --libs gtkmm-3.0` \
	`pkg-config --libs gstreamer-1.0` \
	-lgstpbutils-1.0 \

ifeq ($(CC),clang)
	GTK_INCLUDE_LIBS += -lstdc++ -lm
endif

GTK_DESKTOP_FILE := face/GtkFace/RatelPlayer.desktop
RESOURCES_DIR := $(PROJECT_DIR)/resources
ICONS_DIR := $(RESOURCES_DIR)/icons
GTK_RESOURCES := $(wildcard $(ICONS_DIR)/ratel-player-*.png)
DICTIONARIES_DIR := $(RESOURCES_DIR)/dictionaries
DICTIONARIES_RESOURCES := $(wildcard $(DICTIONARIES_DIR)/*.dic)

GTK_TEST_INSTALLED_DICTIONARIES_DIR := ~/.local/share/RatelPlayer/dictionaries
GTK_TEST_INSTALLED_DICTIONARIES := $(subst $(DICTIONARIES_DIR), $(GTK_TEST_INSTALLED_DICTIONARIES_DIR), $(DICTIONARIES_RESOURCES))


GTK_INSTALLED_EXECUTABLE := $(DESTDIR)/usr/bin/RatelPlayer

GTK_INSTALLATION_DIR := $(DESTDIR)/usr/share/RatelPlayer
GTK_INSTALLED_DESKTOP_FILE := $(DESTDIR)/usr/share/applications/RatelPlayer.desktop
GTK_INSTALLED_ICON := $(DESTDIR)/usr/share/pixmaps/ratel-player.png
GTK_INSTALLED_DICTIONARIES := $(GTK_INSTALLATION_DIR)/dictionaries

DIRS_TO_CREATE += $(GTK_OUTDIRS) $(GTK_TEST_INSTALLED_DICTIONARIES_DIR)

GTK_HEADER_TO_PRECOMPILE := $(GTK_PROJ_DIR)/PrecompiledGtkFace.hpp
PRECOMPILED_HEADER_GTK := $(GTK_HEADER_TO_PRECOMPILE)$(PRECOMPILED_HEADER_EXT)
FORECED_GTK_HEADER := -include $(GTK_HEADER_TO_PRECOMPILE)

DEPENDICIES += $(GTK_OBJS:.o=.d) $(GTK_HEADER_TO_PRECOMPILE).d

ifeq ($(CODEGEN),Debug)
	STRIP_TARGET := echo Skipping to strip
else
	STRIP_TARGET := strip -s
endif
