#SHELL=/bin/bash

# define variables for observer project
OBSERVER_PROJ_DIR := $(PROJECT_DIR)/src/rpObservers
OBSERVER_TARGET_DIR := $(OBSERVER_PROJ_DIR)/$(OUTPUT_DIR)
OBSERVER_LIB := $(OBSERVER_TARGET_DIR)/libRatelPlayerObservers.a
OBSERVER_CPP_SRCS := $(wildcard $(OBSERVER_PROJ_DIR)/*.cpp) $(wildcard $(OBSERVER_PROJ_DIR)/**/*.cpp)
OBSERVER_OBJS := $(subst $(OBSERVER_PROJ_DIR), $(OBSERVER_TARGET_DIR), $(OBSERVER_CPP_SRCS:.cpp=.o))
OBSERVER_OUTDIRS := $(OBSERVER_TARGET_DIR) $(sort $(dir $(OBSERVER_OBJS)))
OBSERVER_INCLUDE_DIRS := \
	-I$(SRC_DIR) \
	`pkg-config --cflags giomm-2.4`

DIRS_TO_CREATE += $(OBSERVER_OUTDIRS)
DEPENDICIES += $(OBSERVER_OBJS:.o=.d)
