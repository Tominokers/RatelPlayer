#SHELL=/bin/bash

#define variables for test project
TEST_PROJ_DIR := $(PROJECT_DIR)/tests
TEST_TARGET_DIR := $(TEST_PROJ_DIR)/$(OUTPUT_DIR)
TEST_EXECUTABLE := $(TEST_TARGET_DIR)/RatelPlayerTests
TEST_CPP_SRCS := $(wildcard $(TEST_PROJ_DIR)/*.cpp) \
				 $(wildcard $(TEST_PROJ_DIR)/**/*.cpp) \
				 $(wildcard $(TEST_PROJ_DIR)/**/**/*.cpp)
TEST_OBJS := $(subst $(TEST_PROJ_DIR), $(TEST_TARGET_DIR), $(TEST_CPP_SRCS:.cpp=.o))
TEST_OUTDIRS := $(TEST_TARGET_DIR) $(sort $(dir $(TEST_OBJS)))

TEST_INCLUDE_DIRS := \
	-I$(TEST_PROJ_DIR) \
	-I$(SRC_DIR) \
	-Ithirdparty/gtests/googletest/include \
	-Ithirdparty/gtests/googlemock/include \
	`pkg-config --cflags gtkmm-3.0` \
	`pkg-config --cflags gstreamer-1.0` \

TEST_INCLUDE_LIB_DIRS := -L$(CORE_TARGET_DIR) \
	-L$(OBSERVER_TARGET_DIR) \
	-L$(FACADE_TARGET_DIR) \
	-L$(SETTINGS_TARGET_DIR) \
	-L$(LOCALIZATION_TARGET_DIR) \

TEST_INCLUDE_LIBS := \
	-lRatelPlayerCore \
	-lRatelPlayerObservers \
	-lRatelPlayerFacade \
	-lRatelPlayerSettings \
	-lRatelPlayerLocalization \
	`pkg-config --libs gtkmm-3.0` \
	`pkg-config --libs gstreamer-1.0` \
	-lgstpbutils-1.0 \

TEST_GMOCK_LIB := thirdparty/gtests/googlemock/make/gmock_main.a

ifeq ($(CC),clang)
	TEST_INCLUDE_LIBS += -lstdc++ -lm
endif

DIRS_TO_CREATE += $(TEST_OUTDIRS)

TESTS_HEADER_TO_PRECOMPILE := $(TEST_PROJ_DIR)/PrecompiledTests.hpp
PRECOMPILED_HEADER_TESTS := $(TESTS_HEADER_TO_PRECOMPILE)$(PRECOMPILED_HEADER_EXT)
FORECED_TESTS_HEADER := -include $(TESTS_HEADER_TO_PRECOMPILE)

DEPENDICIES += $(TEST_OBJS:.o=.d) $(TESTS_HEADER_TO_PRECOMPILE).d
