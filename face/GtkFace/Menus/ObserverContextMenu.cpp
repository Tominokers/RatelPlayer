#include "ObserverContextMenu.h"

#include "GtkFace/Utils/HelpFunctions.h"

namespace GtkFace {
namespace Menus {

    ObserverContextMenu::ObserverContextMenu()
        : m_addItem(Utils::localize(rpLocalization::LocalizerKeys::AddMedia))
        , m_openInNewPanelItem(Utils::localize(rpLocalization::LocalizerKeys::OpenInNewPanel))
    {
        this->append(m_addItem);
        this->append(m_openInNewPanelItem);
        this->show_all();
    }

    void ObserverContextMenu::connectAddSignal(const sigc::slot<void> &slot) {
        m_addItem.signal_activate().connect(slot);
    }

    void ObserverContextMenu::connectNewPanelSignal(const sigc::slot<void> &slot) {
        m_openInNewPanelItem.signal_activate().connect(slot);
    }

    void ObserverContextMenu::setNewPanelItemVisibility(bool isVisible) {
        m_openInNewPanelItem.set_visible(isVisible);
    }

    void ObserverContextMenu::setAddItemSensitive(bool isSensitive) {
        m_addItem.set_sensitive(isSensitive);
    }

}
}
