#pragma once

#include <gtkmm/menu.h>
#include <gtkmm/menuitem.h>

namespace GtkFace {
namespace Menus {

    class ObserverContextMenu : public Gtk::Menu
    {
    public:
        ObserverContextMenu();

        void connectAddSignal(const sigc::slot<void> &slot);

        void connectNewPanelSignal(const sigc::slot<void> &slot);

        void setNewPanelItemVisibility(bool isVisible);

        void setAddItemSensitive(bool isSensitive);

    private:
        Gtk::MenuItem m_addItem;
        Gtk::MenuItem m_openInNewPanelItem;
    };

}
}
