#include "PlaylistContextMenu.h"

#include "GtkFace/Utils/HelpFunctions.h"

namespace GtkFace {
namespace Menus {

    PlaylistContextMenu::PlaylistContextMenu()
        : m_removeItem(Utils::localize(rpLocalization::LocalizerKeys::RemoveMedia))
    {
        this->append(m_removeItem);
        this->show_all();
    }

    void PlaylistContextMenu::connectRemoveSignal(const sigc::slot<void> &slot) {
        m_removeItem.signal_activate().connect(slot);
    }

}
}
