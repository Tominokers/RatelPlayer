#pragma once

#include <gtkmm/menu.h>
#include <gtkmm/menuitem.h>

namespace GtkFace {
namespace Menus {

    class PlaylistContextMenu : public Gtk::Menu
    {
    public:
        PlaylistContextMenu();

        void connectRemoveSignal(const sigc::slot<void> &slot);

    private:
        Gtk::MenuItem m_removeItem;
    };

}
}
