#include "SettingsButtonMenu.h"

#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Windows/WindowsProvider.h"

#include "rpFacade/PlayerInterface.h"

namespace GtkFace {
namespace Menus {

    SettingsButtonMenu::SettingsButtonMenu(rpFacade::PlayerInterface &playerMediator,
                                           Windows::WindowsProvider &provider)
        : m_noRepeat(m_repeatGroup, Utils::localize(rpLocalization::LocalizerKeys::NoRepeatMode))
        , m_wholePlaylist(m_repeatGroup, Utils::localize(rpLocalization::LocalizerKeys::WholePlaylistRepeat))
        , m_repeatTrack(m_repeatGroup, Utils::localize(rpLocalization::LocalizerKeys::RepeatTrack))
        , m_noShuffle(m_shuffleGroup, Utils::localize(rpLocalization::LocalizerKeys::TurnOffShuffling))
        , m_byTracks(m_shuffleGroup, Utils::localize(rpLocalization::LocalizerKeys::ShufflingByTrack))
        , m_aboutItem(Utils::localize(rpLocalization::LocalizerKeys::OpenAbout))
        , m_windowsProvider(provider)
        , m_player(playerMediator)
    {
        m_aboutItem.signal_activate().connect(sigc::mem_fun(*this, &SettingsButtonMenu::onAboutDialog));
        m_noRepeat.signal_activate().connect(sigc::mem_fun(*this, &SettingsButtonMenu::onRepeatOff));
        m_repeatTrack.signal_activate().connect(sigc::mem_fun(*this, &SettingsButtonMenu::onRepeatTrack));
        m_wholePlaylist.signal_activate().connect(sigc::mem_fun(*this, &SettingsButtonMenu::onRepeatPlaylist));
        m_noShuffle.signal_activate().connect(sigc::mem_fun(*this, &SettingsButtonMenu::onShuffleOff));
        m_byTracks.signal_activate().connect(sigc::mem_fun(*this, &SettingsButtonMenu::onShuffleByTrack));

        std::vector<Gtk::MenuItem*> items = {&m_noRepeat, &m_wholePlaylist, &m_repeatTrack,
                                             &m_separateModes, &m_noShuffle, &m_byTracks,
                                             &m_separator, &m_aboutItem};

        for (auto &item : items)
            append(*item);
        show_all();

        activateMode();
    }

    void SettingsButtonMenu::onRepeatOff() {
        m_player.setRepeatMode(rpFacade::RepeatMode::None);
    }

    void SettingsButtonMenu::onRepeatPlaylist() {
        m_player.setRepeatMode(rpFacade::RepeatMode::WholePlaylist);
    }

    void SettingsButtonMenu::onRepeatTrack() {
        m_player.setRepeatMode(rpFacade::RepeatMode::Track);
    }

    void SettingsButtonMenu::onShuffleOff() {
        m_player.setShuffleMode(rpFacade::ShuffleMode::None);
    }

    void SettingsButtonMenu::onShuffleByTrack() {
        m_player.setShuffleMode(rpFacade::ShuffleMode::ByTrack);
    }

    void SettingsButtonMenu::onAboutDialog() {
        m_windowsProvider.openAboutDialog();
    }

    void SettingsButtonMenu::activateMode() {
        switch(m_player.getRepeatMode())
        {
        case rpFacade::RepeatMode::None:
            m_noRepeat.set_active();
            break;
        case rpFacade::RepeatMode::WholePlaylist:
            m_wholePlaylist.set_active();
            break;
        case rpFacade::RepeatMode::Track:
            m_repeatTrack.set_active();
            break;
        }

        switch(m_player.getShuffleMode())
        {
        case rpFacade::ShuffleMode::None:
            m_noShuffle.set_active();
            break;
        case rpFacade::ShuffleMode::ByTrack:
            m_byTracks.set_active();
            break;
        }
    }

}
}
