#pragma once

#include <gtkmm/menu.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/radiobuttongroup.h>
#include <gtkmm/radiomenuitem.h>
#include <gtkmm/separatormenuitem.h>

namespace rpFacade {
    class PlayerInterface;
}

namespace GtkFace {
namespace Windows {
    class WindowsProvider;
}

namespace Menus {

    class SettingsButtonMenu : public Gtk::Menu
    {
    public:
        explicit SettingsButtonMenu(rpFacade::PlayerInterface &playerMediator,
                                    Windows::WindowsProvider &provider);

    private:
        void onRepeatOff();
        void onRepeatPlaylist();
        void onRepeatTrack();
        void onShuffleOff();
        void onShuffleByTrack();
        void onAboutDialog();

        void activateMode();

    private:
        Gtk::RadioButtonGroup m_repeatGroup;
        Gtk::RadioMenuItem m_noRepeat;
        Gtk::RadioMenuItem m_wholePlaylist;
        Gtk::RadioMenuItem m_repeatTrack;

        Gtk::RadioButtonGroup m_shuffleGroup;
        Gtk::RadioMenuItem m_noShuffle;
        Gtk::RadioMenuItem m_byTracks;

        Gtk::SeparatorMenuItem m_separator;
        Gtk::SeparatorMenuItem m_separateModes;

        Gtk::MenuItem m_aboutItem;

        Windows::WindowsProvider &m_windowsProvider;
        rpFacade::PlayerInterface &m_player;
    };
}
}
