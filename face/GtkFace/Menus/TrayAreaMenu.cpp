#include "TrayAreaMenu.h"

#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Windows/WindowsProvider.h"

namespace GtkFace {
namespace Menus {

    TrayAreaMenu::TrayAreaMenu(GtkFace::Windows::WindowsProvider &provider)
        : m_quitItem(Utils::localize(rpLocalization::LocalizerKeys::Quit))
    {
        m_quitItem.signal_activate().connect(sigc::mem_fun(provider,
                                                           &GtkFace::Windows::WindowsProvider::quit));

        this->append(m_quitItem);
        this->show_all();
    }
}
}
