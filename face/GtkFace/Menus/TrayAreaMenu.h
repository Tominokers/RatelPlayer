#pragma once

#include <gtkmm/menu.h>
#include <gtkmm/menuitem.h>

namespace GtkFace {
namespace Windows {
    class WindowsProvider;
}

namespace Menus {

    class TrayAreaMenu : public Gtk::Menu
    {
    public:
        explicit TrayAreaMenu(GtkFace::Windows::WindowsProvider &provider);

    private:
        Gtk::MenuItem m_quitItem;
    };
}
}
