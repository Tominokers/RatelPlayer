#include "ObserverTreeModel.h"

namespace GtkFace {
namespace Models {

    ObserverTreeModel::ObserverColumnModel::ObserverColumnModel() {
        add(m_icon);
        add(m_nameRepresenting);
        add(m_date);
    }

    Glib::RefPtr<ObserverTreeModel> ObserverTreeModel::create() {
        return Glib::RefPtr<ObserverTreeModel>(new ObserverTreeModel());
    }


    const Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>>&
    ObserverTreeModel::getIcon() const {
        return m_columns.m_icon;
    }

    const Gtk::TreeModelColumn<Glib::ustring>&
    ObserverTreeModel::getName() const {
        return m_columns.m_nameRepresenting;
    }

    const Gtk::TreeModelColumn<Glib::ustring>&
    ObserverTreeModel::getDate() const {
        return m_columns.m_date;
    }

    ObserverTreeModel::ObserverTreeModel() :
        Gtk::ListStore() {
        this->set_column_types(m_columns);
    }

}
}
