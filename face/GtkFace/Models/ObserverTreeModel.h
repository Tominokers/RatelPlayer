#pragma once

#include <gdkmm/pixbuf.h>
#include <gtkmm/liststore.h>
#include <gtkmm/treemodelcolumn.h>

namespace GtkFace {
namespace Models {

    class ObserverTreeModel final : public Gtk::ListStore
    {
    public:
        class ObserverColumnModel final : public Gtk::TreeModelColumnRecord
        {
            friend ObserverTreeModel;

        public:
            ObserverColumnModel();

        private:
            Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> m_icon;

            Gtk::TreeModelColumn<Glib::ustring> m_nameRepresenting;

            Gtk::TreeModelColumn<Glib::ustring> m_date;
        };

    public:
        static Glib::RefPtr<ObserverTreeModel> create();

        const Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>>& getIcon() const;

        const Gtk::TreeModelColumn<Glib::ustring>& getName() const;

        const Gtk::TreeModelColumn<Glib::ustring>& getDate() const;

    protected:
        ObserverTreeModel();

    private:
        ObserverColumnModel m_columns;
    };

}
}
