#include "PlaylistTreeModel.h"

#include "rpFacade/Track.h"

namespace GtkFace {
namespace Models {

    PlaylistTreeModel::PlaylistColumnModel::PlaylistColumnModel() {
        add(m_artist);
        add(m_album);
        add(m_songTitle);
        add(m_year);
        add(m_trackNumber);
        add(m_trackDuration);
    }

    Glib::RefPtr<PlaylistTreeModel> PlaylistTreeModel::create(const UpdateNameHandler &updatePlaylistName) {
        return Glib::RefPtr<PlaylistTreeModel>(new PlaylistTreeModel(updatePlaylistName));
    }

    const Gtk::TreeModelColumn<Glib::ustring>& PlaylistTreeModel::getArtist() const {
        return m_columns.m_artist;
    }

    const Gtk::TreeModelColumn<Glib::ustring>& PlaylistTreeModel::getAlbum() const {
        return m_columns.m_album;
    }

    const Gtk::TreeModelColumn<Glib::ustring>& PlaylistTreeModel::getSongTitle() const {
        return m_columns.m_songTitle;
    }

    const Gtk::TreeModelColumn<Glib::ustring>& PlaylistTreeModel::getYear() const {
        return m_columns.m_year;
    }

    const Gtk::TreeModelColumn<Glib::ustring>& PlaylistTreeModel::getTrackNumber() const {
        return m_columns.m_trackNumber;
    }

    const Gtk::TreeModelColumn<Glib::ustring>& PlaylistTreeModel::getTrackDuration() const {
        return m_columns.m_trackDuration;
    }

    std::size_t PlaylistTreeModel::getSize() const {
        return static_cast<std::size_t>(this->iter_n_root_children_vfunc());
    }

    rpFacade::PlaylistFiller& PlaylistTreeModel::getFiller() {
        return *this;
    }

    void PlaylistTreeModel::clearItems() {
        this->clear();
    }

    void PlaylistTreeModel::appendNewItem(const rpFacade::Track &track) {
        auto it = this->append();

        fillIterator(track, it);
    }

    void PlaylistTreeModel::insertNewItem(const rpFacade::Track &track, rpFacade::TrackIndex position) {
        if (position == 0)
        {
            auto it = this->prepend();

            fillIterator(track, it);
        }
        else
        {
            // The previous position is necessary to put new track in correct place.
            position = position - 1;
            auto itAfter = this->get_iter(Gtk::TreePath(std::to_string(position)));
            auto it = this->insert_after(itAfter);

            fillIterator(track, it);
        }
    }

    void PlaylistTreeModel::removeItems(const rpFacade::TrackIndexList &items) {
        for (auto it = std::rbegin(items);
             it != std::crend(items);
             ++it)
        {
            auto iterator = this->get_iter(Gtk::TreePath(std::to_string(*it)));
            this->erase(iterator);
        }
    }

    void PlaylistTreeModel::passSuggestedName(const std::string &name) {
        m_updatePlaylistName(name);
    }

    void PlaylistTreeModel::fillIterator(const rpFacade::Track &track, Gtk::TreeIter &it) {
        (*it)[this->getTrackNumber()] = track.getTrackNumber();
        (*it)[this->getSongTitle()] = track.getSongTitle();
        (*it)[this->getArtist()] = track.getArtist();
        (*it)[this->getAlbum()] = track.getAlbum();
        (*it)[this->getYear()] = track.getYear();
        (*it)[this->getTrackDuration()] = track.getTrackDuration();
    }

    PlaylistTreeModel::PlaylistTreeModel(const UpdateNameHandler &updatePlaylistName)
        : Gtk::ListStore()
        , m_updatePlaylistName(updatePlaylistName)
    {
        this->set_column_types(m_columns);
    }

}
}
