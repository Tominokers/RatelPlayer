#pragma once

#include "rpFacade/PlaylistFiller.h"

#include <gtkmm/liststore.h>
#include <gtkmm/treemodelcolumn.h>

#include <functional>

namespace GtkFace {

    using UpdateNameHandler = std::function<void(const std::string&)>;

namespace Models {

    class PlaylistTreeModel final : public Gtk::ListStore
                                  , public rpFacade::PlaylistFiller
    {
    public:
        class PlaylistColumnModel final : public Gtk::TreeModelColumnRecord
        {
            friend PlaylistTreeModel;

        public:
            PlaylistColumnModel();

        private:
            Gtk::TreeModelColumn<Glib::ustring> m_artist;

            Gtk::TreeModelColumn<Glib::ustring> m_album;

            Gtk::TreeModelColumn<Glib::ustring> m_songTitle;

            Gtk::TreeModelColumn<Glib::ustring> m_year;

            Gtk::TreeModelColumn<Glib::ustring> m_trackNumber;

            Gtk::TreeModelColumn<Glib::ustring> m_trackDuration;
        };

    public:
        static Glib::RefPtr<PlaylistTreeModel> create(const UpdateNameHandler &updatePlaylistName);

        const Gtk::TreeModelColumn<Glib::ustring>& getArtist() const;

        const Gtk::TreeModelColumn<Glib::ustring>& getAlbum() const;

        const Gtk::TreeModelColumn<Glib::ustring>& getSongTitle() const;

        const Gtk::TreeModelColumn<Glib::ustring>& getYear() const;

        const Gtk::TreeModelColumn<Glib::ustring>& getTrackNumber() const;

        const Gtk::TreeModelColumn<Glib::ustring>& getTrackDuration() const;

        std::size_t getSize() const;

        rpFacade::PlaylistFiller& getFiller();

    protected:
        explicit PlaylistTreeModel(const UpdateNameHandler &updatePlaylistName);

    private:
        // --- from rpFacade::PlaylistFiller ---
        virtual void clearItems() override;
        virtual void appendNewItem(const rpFacade::Track &track) override;
        virtual void insertNewItem(const rpFacade::Track &track, rpFacade::TrackIndex position) override;
        virtual void removeItems(const rpFacade::TrackIndexList &items) override;
        virtual void passSuggestedName(const std::string &name) override;

        void fillIterator(const rpFacade::Track &track, Gtk::TreeIter &it);

    private:
        PlaylistColumnModel m_columns;
        UpdateNameHandler m_updatePlaylistName;
    };

}
}
