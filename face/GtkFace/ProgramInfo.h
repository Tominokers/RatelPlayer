#define GTK_APPLICATION_VERSION     "0.8"
#define GTK_APPLICATION_NAME        "RatelPlayer"
#define GTK_APPLICATION_WEBSITE     "https://github.com/tominokers/RatelPlayer"
#define GTK_APPLICATION_AUTHORS     {"Tominokers https://github.com/tominokers"}

#define GTK_APPLICATION_ICON_16     "/org/gtk/ratel-player/icons/ratel-player-16.png"
#define GTK_APPLICATION_ICON_22     "/org/gtk/ratel-player/icons/ratel-player-22.png"
#define GTK_APPLICATION_ICON_24     "/org/gtk/ratel-player/icons/ratel-player-24.png"
#define GTK_APPLICATION_ICON_32     "/org/gtk/ratel-player/icons/ratel-player-32.png"
#define GTK_APPLICATION_ICON_48     "/org/gtk/ratel-player/icons/ratel-player-48.png"
#define GTK_APPLICATION_ICON_64     "/org/gtk/ratel-player/icons/ratel-player-64.png"
#define GTK_APPLICATION_ICON_128    "/org/gtk/ratel-player/icons/ratel-player-128.png"
#define GTK_APPLICATION_ICON_256    "/org/gtk/ratel-player/icons/ratel-player-256.png"
