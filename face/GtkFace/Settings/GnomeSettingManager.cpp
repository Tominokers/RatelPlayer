#include "GnomeSettingManager.h"

#include "GtkFace/Settings/GnomeSettingsStorage.h"
#include "GtkFace/Utils/HelpFunctions.h"

#include <iostream>
#include <cassert>

namespace GtkFace {
namespace Settings {

    GnomeSettingManager::GnomeSettingManager()
        : rpSettings::SettingManager(std::make_unique<GnomeSettingsStorage>(""))
        , m_settingsFileName("settings.txt")

    {
        this->openSettingFile(Utils::getSharedApplicationDirectory());

        assert(m_file.is_open());
        this->load();
    }

    GnomeSettingManager::~GnomeSettingManager() {
        m_file.close();
    }

    std::ostream& GnomeSettingManager::getDumpStream() noexcept {
        m_file.close();

        m_file.open(m_filePath, std::ofstream::out | std::ofstream::trunc);
        assert(m_file.is_open());

        return m_file;
    }

    std::istream& GnomeSettingManager::getLoadStream() noexcept {
        m_file.close();

        m_file.open(m_filePath);
        assert(m_file.is_open());

        m_file.seekg(0);

        return m_file;
    }

    void GnomeSettingManager::openSettingFile(std::string &&directory) {
        if (directory.empty())
            return;

        m_filePath = directory + m_settingsFileName;
        m_file.open(m_filePath, std::ios_base::in | std::ios_base::out);
        if (!m_file.is_open())
        {
            // Create file.
            m_file.open(m_filePath, std::ios_base::out);
        }
    }

}
}
