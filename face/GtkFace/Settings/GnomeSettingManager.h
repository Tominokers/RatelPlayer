#pragma once

#include "rpSettings/SettingManager.h"

#include <fstream>
#include <string>

namespace GtkFace {
namespace Settings {

    class GnomeSettingManager : public rpSettings::SettingManager
    {
    public:
        GnomeSettingManager();

        virtual ~GnomeSettingManager();

    private:
        virtual std::ostream& getDumpStream() noexcept override;
        virtual std::istream& getLoadStream() noexcept override;

        void openSettingFile(std::string &&directory);

    private:
        std::fstream m_file;

        std::string m_filePath;

        const std::string m_sharedFolder;
        const std::string m_settingsFileName;
    };

}
}
