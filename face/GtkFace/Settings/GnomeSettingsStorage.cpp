#include "GnomeSettingsStorage.h"

namespace GtkFace {
namespace Settings {

    GnomeSettingsStorage::GnomeSettingsStorage(const std::string &storageId)
        : rpSettings::SettingStorage(storageId)
    {
    }

    std::unique_ptr<rpSettings::SettingStorage> GnomeSettingsStorage::clone() const {
        return std::make_unique<GnomeSettingsStorage>("");
    }

    void GnomeSettingsStorage::flush(std::ostream &stream) {
        auto &id = this->getId();

        for (auto &it : m_settings)
        {
            stream << id << m_separator
                   << it.first << m_separator
                   << it.second << "\n";
        }
    }

    rpSettings::ExtractedDump GnomeSettingsStorage::extractOneFlush(const std::string &data) const noexcept {
        auto afterId = data.find(m_separator);
        auto afterKey = data.rfind(m_separator);

        std::string id = data.substr(0, afterId);
        std::string key = data.substr(afterId + 1, afterKey - afterId - 1);
        std::string value = data.substr(afterKey + 1);

        return std::make_tuple(id, key, value);
    }

    void GnomeSettingsStorage::storeImpl(const std::string &key, const std::string &value) noexcept {
        m_settings[key] = value;
    }

    std::string GnomeSettingsStorage::loadImpl(const std::string &key) noexcept {
        return m_settings[key];
    }

}
}
