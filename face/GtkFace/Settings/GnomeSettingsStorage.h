#pragma once

#include "rpSettings/SettingStorage.h"

#include <unordered_map>

namespace GtkFace {
namespace Settings {

    class GnomeSettingsStorage : public rpSettings::SettingStorage
    {
    public:
        explicit GnomeSettingsStorage(const std::string &storageId);

    private:
        virtual std::unique_ptr<rpSettings::SettingStorage> clone() const override;
        virtual void flush(std::ostream &stream) override;
        virtual rpSettings::ExtractedDump extractOneFlush(const std::string &data) const noexcept override;
        virtual void storeImpl(const std::string &key, const std::string &value) noexcept override;
        virtual std::string loadImpl(const std::string &key) noexcept override;

    private:
        std::unordered_map<std::string, std::string> m_settings;

        static const char m_separator = ':';
    };

}
}
