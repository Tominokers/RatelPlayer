#pragma once

#include <string>

namespace GtkFace {
namespace Settings {

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif

    static const std::string PlaylistDirectoryKey = "PlaylistDirectory";
    static const std::string IsApplicationMaximizedKey = "IsApplicationMaximized";
    static const std::string ApplicationXCoordinate = "ApplicationXCoordinate";
    static const std::string ApplicationYCoordinate = "ApplicationYCoordinate";
    static const std::string ApplicationWidth = "ApplicationWidth";
    static const std::string ApplicationHeight = "ApplicationHeight";
    static const std::string ApplicationPanePosition = "ApplicationPanePosition";

#ifdef __clang__
#pragma clang diagnostic pop
#pragma clang diagnostic pop
#endif

}
}
