#pragma once

#include <string>

namespace GtkFace {
namespace Settings {

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif

    static const std::string DirectoryObserverPanelId = "DirectoryObserverPanel";
    static const std::string PlaylistStorageId = "PlayerStorage";
    static const std::string ApplicationWindowStorageId = "ApplicationWindow";

#ifdef __clang__
#pragma clang diagnostic pop
#pragma clang diagnostic pop
#endif

}
}
