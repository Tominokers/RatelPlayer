#include "DragDropTargetBuilder.h"

#include <algorithm>

namespace GtkFace {
namespace Utils {

    const char* DragDropTargetBuilder::m_regularFileDnDId = "directoryobserver/regularfile";
    const char* DragDropTargetBuilder::m_playlistTrackDnDId = "playlist/track";
    const char* DragDropTargetBuilder::m_plainTextDnDId = "text/plain";

    DragDropTargetBuilder& DragDropTargetBuilder::requestRegularFile() {
        m_targets.push_back(Gtk::TargetEntry(m_regularFileDnDId,
                                             Gtk::TargetFlags::TARGET_OTHER_WIDGET));
        return *this;
    }

    DragDropTargetBuilder& DragDropTargetBuilder::requestPlaylistTrack() {
        m_targets.push_back(Gtk::TargetEntry(m_playlistTrackDnDId,
                                             Gtk::TargetFlags::TARGET_SAME_WIDGET));
        return *this;
    }

    DragDropTargetBuilder& DragDropTargetBuilder::requestOutsidePlainText() {
        m_targets.push_back(Gtk::TargetEntry(m_plainTextDnDId, Gtk::TargetFlags::TARGET_OTHER_APP));
        return *this;
    }

    void DragDropTargetBuilder::reset() {
        m_targets.clear();
    }

    const std::vector<Gtk::TargetEntry>& DragDropTargetBuilder::getTargets() const {
        return m_targets;
    }

    bool DragDropTargetBuilder::hasRegularFileTarget(const std::vector<std::string> &list) {
        return std::find(std::cbegin(list), std::cend(list), m_regularFileDnDId) != std::cend(list);
    }

    bool DragDropTargetBuilder::hasPlaylistTrackTarget(const std::vector<std::string> &list) {
        return std::find(std::cbegin(list), std::cend(list), m_playlistTrackDnDId) != std::cend(list);
    }

    bool DragDropTargetBuilder::hasOutsidePlainText(const std::vector<std::string> &list) {
        return std::find(std::cbegin(list), std::cend(list), m_plainTextDnDId) != std::cend(list);
    }

}
}
