#pragma once

#include <gtkmm/targetentry.h>

#include <string>
#include <vector>

namespace GtkFace {
namespace Utils {

    class DragDropTargetBuilder final
    {
    public:
        DragDropTargetBuilder& requestRegularFile();

        DragDropTargetBuilder& requestPlaylistTrack();

        DragDropTargetBuilder& requestOutsidePlainText();

        void reset();

        const std::vector<Gtk::TargetEntry>& getTargets() const;

        static bool hasRegularFileTarget(const std::vector<std::string> &list);

        static bool hasPlaylistTrackTarget(const std::vector<std::string> &list);

        static bool hasOutsidePlainText(const std::vector<std::string> &list);

    private:
        std::vector<Gtk::TargetEntry> m_targets;

        static const char *m_regularFileDnDId;
        static const char *m_playlistTrackDnDId;
        static const char *m_plainTextDnDId;
    };

}
}
