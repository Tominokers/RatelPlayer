#include "HelpFunctions.h"

#include <GtkFace/ProgramInfo.h>

#include "rpLocalization/Localizer.h"

#include <giomm/file.h>
#include <glibmm/miscutils.h>

#include <locale>
#include <codecvt>

namespace GtkFace {
namespace Utils {

    template<typename T>
    using converter_type = std::wstring_convert<std::codecvt_utf8<T>, T>;

    Glib::ustring localize(rpLocalization::LocalizerKeys key) {
        auto keyValue = rpLocalization::Localizer::get(key);
        return converter_type<wchar_t>().to_bytes(keyValue);
    }

    Glib::ustring getDictionaryDirectory() {
        const auto subdirectory = Glib::ustring("dictionaries/");

        auto instalationDirectory = getInstalledApplicationDirectory();
        if (!instalationDirectory.empty())
            return instalationDirectory + subdirectory;

        return getSharedApplicationDirectory() + subdirectory;
    }

    Glib::ustring getAutoPlaylistDirectory() {
        auto sharedDirectory = getSharedApplicationDirectory();
        if (sharedDirectory.empty())
            return Glib::ustring();

        auto playlistDirectory = sharedDirectory + Glib::ustring("auto_playlists/");

        if (createDirectory(playlistDirectory))
            return playlistDirectory;

        return Glib::ustring();
    }


    Glib::ustring getSharedApplicationDirectory() {
        auto home = Glib::get_home_dir();
        auto sharedFolder = Glib::ustring("/.local/share/");
        auto directoryPath = home + sharedFolder + GTK_APPLICATION_NAME + "/";

        if (createDirectory(directoryPath))
            return directoryPath;

        return Glib::ustring();
    }

    Glib::ustring getInstalledApplicationDirectory() {
        const auto installationDirectory = Glib::ustring("/usr/share/") + GTK_APPLICATION_NAME + "/";
        auto directory = Gio::File::create_for_path(installationDirectory);
        if (directory->query_exists())
            return installationDirectory;

        return Glib::ustring();
    }

    bool createDirectory(const Glib::ustring &path) {
        auto directory = Gio::File::create_for_path(path);
        bool exists = directory->query_exists();
        if (!exists)
        {
            try
            {
                exists = directory->make_directory_with_parents();
            }
            catch (Gio::Error &error)
            {
                static_cast<void>(error);
            }
        }

        return exists;
    }

}
}
