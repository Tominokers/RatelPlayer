#pragma once

#include "rpLocalization/LocalizationKeys.h"

#include <glibmm/ustring.h>

namespace GtkFace {
namespace Utils {

    Glib::ustring localize(rpLocalization::LocalizerKeys key);

    Glib::ustring getDictionaryDirectory();

    Glib::ustring getSharedApplicationDirectory();

    Glib::ustring getInstalledApplicationDirectory();

    Glib::ustring getAutoPlaylistDirectory();

    bool createDirectory(const Glib::ustring &path);

}
}
