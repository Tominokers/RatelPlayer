#include "HotKeyInformation.h"

namespace GtkFace {
namespace Utils {

    HotKeyInformation::HotKeyInformation(const Glib::RefPtr<Gtk::AccelGroup> &group,
                                         guint key,
                                         Gdk::ModifierType modifier)
        : m_accelGroup(group)
        , m_accelKey(key)
        , m_accelModifier(modifier)
    {
    }

    const Glib::RefPtr<Gtk::AccelGroup>& HotKeyInformation::getAccelGroup() const {
        return m_accelGroup;
    }

    guint HotKeyInformation::getKey() const {
        return m_accelKey;
    }

    Gdk::ModifierType HotKeyInformation::getModifier() const {
        return m_accelModifier;
    }

}
}
