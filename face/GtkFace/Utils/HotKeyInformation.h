#pragma once

#include <gtkmm/accelgroup.h>

namespace GtkFace {
namespace Utils {

    class HotKeyInformation {
    public:
        HotKeyInformation(const Glib::RefPtr<Gtk::AccelGroup> &group,
                          guint key,
                          Gdk::ModifierType modifier);

        const Glib::RefPtr<Gtk::AccelGroup>& getAccelGroup() const;

        guint getKey() const;

        Gdk::ModifierType getModifier() const;

    private:
        Glib::RefPtr<Gtk::AccelGroup> m_accelGroup;
        guint m_accelKey;
        Gdk::ModifierType m_accelModifier;
    };

}
}
