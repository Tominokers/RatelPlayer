#include "IconManager.h"

#include "GtkFace/ProgramInfo.h"

#include <gtkmm/icontheme.h>
#include <giomm/resource.h>

namespace GtkFace {
namespace Utils {

    IconManager::IconManager()
    {
        Gtk::IconLookupFlags flag = static_cast<Gtk::IconLookupFlags>(0);
        Glib::RefPtr<Gtk::IconTheme> theme = Gtk::IconTheme::get_default();
        if (auto iconInfo = theme->lookup_icon("folder", 16, flag))
            m_folderIcon = iconInfo.load_icon();
        if (auto iconInfo = theme->lookup_icon("sound", 16, flag))
            m_soundIcon = iconInfo.load_icon();
        if (auto iconInfo = theme->lookup_icon("document-properties", 16, flag))
            m_composableIcon = iconInfo.load_icon();

        try
        {
            m_applicationLogo = Gdk::Pixbuf::create_from_resource(GTK_APPLICATION_ICON_256);

            m_appIcon16 = Gdk::Pixbuf::create_from_resource(GTK_APPLICATION_ICON_16);
            m_appIcon22 = Gdk::Pixbuf::create_from_resource(GTK_APPLICATION_ICON_22);
            m_appIcon24 = Gdk::Pixbuf::create_from_resource(GTK_APPLICATION_ICON_24);
            m_appIcon32 = Gdk::Pixbuf::create_from_resource(GTK_APPLICATION_ICON_32);
            m_appIcon48 = Gdk::Pixbuf::create_from_resource(GTK_APPLICATION_ICON_48);
            m_appIcon64 = Gdk::Pixbuf::create_from_resource(GTK_APPLICATION_ICON_64);
            m_appIcon128 = Gdk::Pixbuf::create_from_resource(GTK_APPLICATION_ICON_128);
        }
        catch(Gio::ResourceError&)
        {
        }
        catch(Gdk::PixbufError&)
        {
        }

    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getFolder() const {
        return m_folderIcon;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getSound() const {
        return m_soundIcon;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getComposableDocument() const {
        return m_composableIcon;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getApplicationLogo() const {
        return m_applicationLogo;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getAppIcon16() const {
        return m_appIcon16;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getAppIcon22() const {
        return m_appIcon22;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getAppIcon24() const {
        return m_appIcon24;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getAppIcon32() const {
        return m_appIcon32;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getAppIcon48() const {
        return m_appIcon48;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getAppIcon64() const {
        return m_appIcon64;
    }

    const Glib::RefPtr<Gdk::Pixbuf>& IconManager::getAppIcon128() const {
        return m_appIcon128;
    }

}
}
