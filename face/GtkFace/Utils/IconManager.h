#pragma once

#include <gdkmm/pixbuf.h>

namespace GtkFace {
namespace Utils {

    class IconManager final
    {
    public:
        IconManager();

        const Glib::RefPtr<Gdk::Pixbuf>& getFolder() const;

        const Glib::RefPtr<Gdk::Pixbuf>& getSound() const;

        const Glib::RefPtr<Gdk::Pixbuf>& getComposableDocument() const;

        const Glib::RefPtr<Gdk::Pixbuf>& getApplicationLogo() const;

        const Glib::RefPtr<Gdk::Pixbuf>& getAppIcon16() const;
        const Glib::RefPtr<Gdk::Pixbuf>& getAppIcon22() const;
        const Glib::RefPtr<Gdk::Pixbuf>& getAppIcon24() const;
        const Glib::RefPtr<Gdk::Pixbuf>& getAppIcon32() const;
        const Glib::RefPtr<Gdk::Pixbuf>& getAppIcon48() const;
        const Glib::RefPtr<Gdk::Pixbuf>& getAppIcon64() const;
        const Glib::RefPtr<Gdk::Pixbuf>& getAppIcon128() const;

    private:
        Glib::RefPtr<Gdk::Pixbuf> m_folderIcon;
        Glib::RefPtr<Gdk::Pixbuf> m_soundIcon;
        Glib::RefPtr<Gdk::Pixbuf> m_composableIcon;

        Glib::RefPtr<Gdk::Pixbuf> m_applicationLogo;
        Glib::RefPtr<Gdk::Pixbuf> m_appIcon16;
        Glib::RefPtr<Gdk::Pixbuf> m_appIcon22;
        Glib::RefPtr<Gdk::Pixbuf> m_appIcon24;
        Glib::RefPtr<Gdk::Pixbuf> m_appIcon32;
        Glib::RefPtr<Gdk::Pixbuf> m_appIcon48;
        Glib::RefPtr<Gdk::Pixbuf> m_appIcon64;
        Glib::RefPtr<Gdk::Pixbuf> m_appIcon128;
    };

}
}
