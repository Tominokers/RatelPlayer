#pragma once

#include <gdkmm/types.h>

static const Gdk::ModifierType ALT_MODIFIER = Gdk::ModifierType::MOD1_MASK;
static const Gdk::ModifierType CTRL_MODIFIER = Gdk::ModifierType::CONTROL_MASK;
static const Gdk::ModifierType SHIFT_MODIFIER = Gdk::ModifierType::SHIFT_MASK;

static const std::size_t ARROW_UP_KEY = 65362;
static const std::size_t BACKSPACE_KEY = 65288;
static const std::size_t DELETE_KEY = 65535;

static const std::size_t T_KEY = 116;
static const std::size_t T_SHIFT_KEY = 84;
