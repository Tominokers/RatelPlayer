#include "MediaKeysListener.h"

#include <cassert>
#include <glibmm/signalproxy.h>
#include <giomm/dbuserror.h>

#include "rpFacade/PlayerInterface.h"

namespace GtkFace {
namespace Utils {

    MediaKeysListener::MediaKeysListener(rpFacade::PlayerInterface& playerInstance)
        : Gio::DBus::Proxy(Gio::DBus::BusType::BUS_TYPE_SESSION,
                           "org.gnome.SettingsDaemon",
                           "/org/gnome/SettingsDaemon/MediaKeys",
                           "org.gnome.SettingsDaemon.MediaKeys")
        , m_nextKey("\'Next\'")
        , m_prevKey("\'Previous\'")
        , m_playKey("\'Play\'")
        , m_stopKey("\'Stop\'")
        , m_player(playerInstance)
    {
        g_dbus_proxy_call (this->gobj(),
                           "GrabMediaPlayerKeys",
                           g_variant_new ("(su)", "RatelPlayer", 0),
                           G_DBUS_CALL_FLAGS_NONE,
                           -1,
                           NULL,
                           NULL,
                           NULL);
    }

    void MediaKeysListener::on_signal(const Glib::ustring &sender_name,
                                      const Glib::ustring &signal_name,
                                      const Glib::VariantContainerBase &parameters) {
        static_cast<void>(sender_name);
        static_cast<void>(signal_name);

        assert(parameters.get_n_children() == 2);

        Glib::VariantBase child;
        parameters.get_child(child, 1);
        auto key = child.print();

        if (key == m_nextKey.c_str())
            m_player.next();
        else if (key == m_prevKey.c_str())
            m_player.previous();
        else if (key == m_playKey.c_str())
            m_player.play();
        else if (key == m_stopKey.c_str())
            m_player.stop();
    }

}
}
