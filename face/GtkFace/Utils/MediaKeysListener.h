#pragma once

#include <giomm/dbusproxy.h>

#include <string>

namespace rpFacade {
    class PlayerInterface;
}

namespace GtkFace {
namespace Utils {

    class MediaKeysListener final : public Gio::DBus::Proxy
    {
    public:
        explicit MediaKeysListener(rpFacade::PlayerInterface& playerInstance);

    private:
        virtual void on_signal(const Glib::ustring &sender_name,
                               const Glib::ustring &signal_name,
                               const Glib::VariantContainerBase &parameters) override;

    private:
        const std::string m_nextKey;
        const std::string m_prevKey;
        const std::string m_playKey;
        const std::string m_stopKey;

        rpFacade::PlayerInterface &m_player;
    };

}
}
