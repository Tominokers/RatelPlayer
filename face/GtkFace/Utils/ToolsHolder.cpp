#include "ToolsHolder.h"

namespace GtkFace {
namespace Utils {

    ToolsHolder::ToolsHolder(Windows::AppWindow &app,
                             rpSettings::SettingManager &settings)
        : m_iconManager()
        , m_windowsProvider(app, m_iconManager)
        , m_settingsManager(settings)
    {
    }

    Windows::WindowsProvider& ToolsHolder::getWindowsProvider() {
        return m_windowsProvider;
    }

    Utils::IconManager& ToolsHolder::getIconManager() {
        return m_iconManager;
    }

    rpSettings::SettingManager& ToolsHolder::getSettingManager() {
        return m_settingsManager;
    }

    Glib::RefPtr<Gtk::AccelGroup> ToolsHolder::getAccelGroup() const {
        return m_windowsProvider.getAccelGroup();
    }

    Widgets::Common::UserInterfaceBlocker& ToolsHolder::getUiBlocker() {
        return m_blocker;
    }

}
}
