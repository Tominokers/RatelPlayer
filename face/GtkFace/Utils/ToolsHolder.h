#pragma once

#include "GtkFace/Utils/IconManager.h"
#include "GtkFace/Widgets/Common/UserInterfaceBlocker.h"
#include "GtkFace/Windows/WindowsProvider.h"

namespace rpSettings {
    class SettingManager;
}

namespace GtkFace {

namespace Windows {
    class AppWindow;
}

namespace Utils {

    class ToolsHolder final
    {
    public:
        ToolsHolder(Windows::AppWindow &app,
                    rpSettings::SettingManager &settings);

        Windows::WindowsProvider& getWindowsProvider();

        Utils::IconManager& getIconManager();

        rpSettings::SettingManager& getSettingManager();

        Glib::RefPtr<Gtk::AccelGroup> getAccelGroup() const;

        Widgets::Common::UserInterfaceBlocker& getUiBlocker();

    private:
        Utils::IconManager m_iconManager;
        Windows::WindowsProvider m_windowsProvider;
        rpSettings::SettingManager &m_settingsManager;
        Widgets::Common::UserInterfaceBlocker m_blocker;
    };

}
}
