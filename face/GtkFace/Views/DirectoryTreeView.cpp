#include "DirectoryTreeView.h"

#include "GtkFace/Utils/DragDropTargetBuilder.h"
#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Utils/IconManager.h"
#include "GtkFace/Utils/ToolsHolder.h"
#include "GtkFace/Widgets/Common/UserInterfaceBlocker.h"
#include "GtkFace/Widgets/ObserverPanel/MediaInfoBoard.h"
#include "GtkFace/Widgets/ObserverPanel/ObserversHolder.h"

#include "rpCore/Media/MediaFile.h"
#include "rpCore/Media/MediaProxy.h"
#include "rpFacade/ObserverInterface.h"
#include "rpFacade/ObserverItemInformation.h"
#include "rpFacade/Observers/ObserverMediator.h"
#include "rpObservers/Observer.h"

#include <gtkmm/icontheme.h>

namespace GtkFace {
namespace Views {

    DirectoryTreeView::DirectoryTreeView(rpFacade::ObserverInterface &observerMediator,
                                         Widgets::ObserverPanel::ObserversHolder &holder,
                                         Widgets::Common::UserInterfaceBlocker &blocker)
        : m_model(Models::ObserverTreeModel::create())
        , m_mediator(observerMediator)
        , m_observersHolder(holder)
        , m_blocker(blocker)
    {
        this->setupModel();
        this->setupMenu();
        this->setupColumns();
        this->setupSelection();
    }

    void DirectoryTreeView::addItem(const rpFacade::MediaProxy &item) {
        Gtk::TreeModel::iterator it = m_model->append();
        if (item.get()->getType() == rpCore::Media::MediaFileType::Composable)
        {
            (*it)[m_model->getIcon()] = this->getIcon(IconType::ComposableDocument);
            (*it)[m_model->getName()] = item.get()->getFileName();
        }
        else
        {
            (*it)[m_model->getIcon()] = this->getIcon(IconType::MediaMP3);
            (*it)[m_model->getName()] = item.get()->getArtist() + " | " + item.get()->getSongTitle();
        }
        (*it)[m_model->getDate()] = item.get()->getYear();
    }

    void DirectoryTreeView::addItem(const rpFacade::Observer &item) {
        Gtk::TreeModel::iterator it = m_model->append();
        (*it)[m_model->getIcon()] = this->getIcon(IconType::Observer);
        (*it)[m_model->getName()] = item.getName();
        (*it)[m_model->getDate()] = item.getDate();
    }

    void DirectoryTreeView::clear() {
        this->unset_model();
        m_model->clear();
    }

    void DirectoryTreeView::setupModel() {
        this->set_model(m_model);
    }

    void DirectoryTreeView::onRowActivated(const Gtk::TreeModel::Path& path,
                                           Gtk::TreeViewColumn* column) {
        static_cast<void>(column);

        if (!path.empty())
        {
            auto index = static_cast<rpFacade::ContainerIndex>(path.front());
            m_mediator.executeItem(index);
        }
    }

    void DirectoryTreeView::onCursorChanged() {
        Gtk::TreeModel::Path path;
        Gtk::TreeViewColumn *column = nullptr;
        this->get_cursor(path, column);

        if(!path.empty())
        {
            auto index = static_cast<rpFacade::ContainerIndex>(path.front());
            auto info = m_mediator.requestItemInfo(index);
            if (info.getType() == rpFacade::ItemType::Media)
                m_observersHolder.getMediaInfoBoard().fillInformation(*info.getMediaInfo());
        }
    }

    bool DirectoryTreeView::onButtonRelease(GdkEventButton* event) {
        if (event->button == 3)
            this->popupMenu();

        return false;
    }

    void DirectoryTreeView::on_drag_data_get(const Glib::RefPtr<Gdk::DragContext>& context,
                                             Gtk::SelectionData& selection_data,
                                             guint info,
                                             guint time)
    {
        static_cast<void>(context);
        static_cast<void>(selection_data);
        static_cast<void>(info);
        static_cast<void>(time);

        auto selection = this->get_selection();
        auto selectedRows = selection->get_selected_rows();
        std::vector<rpFacade::ContainerIndex> items;

        for (const auto &path : selectedRows)
            if (!path.empty())
                items.push_back(static_cast<rpFacade::ContainerIndex>(path.front()));

        m_mediator.delayItems(items);
    }

    void DirectoryTreeView::on_drag_end(const Glib::RefPtr<Gdk::DragContext>& context) {
        static_cast<void>(context);
    }

    void DirectoryTreeView::setupMenu() {
        m_contextMenu.connectAddSignal(sigc::mem_fun(*this, &DirectoryTreeView::handleAddMenuItem));
        m_contextMenu.connectNewPanelSignal(sigc::mem_fun(*this, &DirectoryTreeView::handleOpenInNewPanel));

        this->signal_popup_menu().connect(sigc::mem_fun(*this, &DirectoryTreeView::popupMenu));
    }

    void DirectoryTreeView::setupColumns() {
        const auto iconColumn = Utils::localize(rpLocalization::LocalizerKeys::Icon);
        const auto nameColumn = Utils::localize(rpLocalization::LocalizerKeys::FileName);
        const auto dateColumn = Utils::localize(rpLocalization::LocalizerKeys::FileDate);
        append_column(iconColumn, m_model->getIcon());
        append_column(nameColumn, m_model->getName());
        append_column(dateColumn, m_model->getDate());

        for (auto &column : get_columns())
            column->set_resizable(true);
    }

    void DirectoryTreeView::setupSelection() {
        auto selection = this->get_selection();
        selection->set_mode(Gtk::SelectionMode::SELECTION_MULTIPLE);

        this->signal_row_activated().connect(sigc::mem_fun(*this, &DirectoryTreeView::onRowActivated));
        this->signal_cursor_changed().connect(sigc::mem_fun(*this, &DirectoryTreeView::onCursorChanged));
        this->signal_button_release_event().connect(sigc::mem_fun(*this, &DirectoryTreeView::onButtonRelease));

        Utils::DragDropTargetBuilder dndBuilder;
        dndBuilder.requestRegularFile();

        this->drag_source_set(dndBuilder.getTargets(), Gdk::ModifierType::BUTTON1_MASK);
    }

    void DirectoryTreeView::handleAddMenuItem() {
        auto selection = this->get_selection();
        auto selectedRows = selection->get_selected_rows();

        std::vector<rpFacade::ContainerIndex> indexes;
        for (const auto &path : selectedRows)
            if (!path.empty())
            {
                auto index = static_cast<rpFacade::ContainerIndex>(path.front());
                indexes.push_back(index);
            }

        m_mediator.delayItems(indexes);
        m_mediator.processDelayedItems();
    }

    void DirectoryTreeView::handleOpenInNewPanel() {
        auto selection = this->get_selection();
        auto selectedRows = selection->get_selected_rows();
        for (const auto &path : selectedRows)
        {
            if (!path.empty())
            {
                auto index = static_cast<rpFacade::ContainerIndex>(path.front());
                auto info = m_mediator.requestItemInfo(index);
                if (info.getType() == rpFacade::ItemType::Observer)
                {
                    m_observersHolder.addTap(info.getObserverInfo());
                }
                else
                {
                    break; // Observers go first, after them there are nothing interesting.
                }
            }
        }
    }

    bool DirectoryTreeView::popupMenu() {
        auto selection = this->get_selection();
        if (selection->count_selected_rows() > 0)
        {
            this->configureMenu();
            m_contextMenu.popup(0, 0);
            return true;
        }

        return false;
    }

    void DirectoryTreeView::configureMenu() {
        auto selection = this->get_selection();
        const auto selectedRows = selection->get_selected_rows();

        auto index = static_cast<rpFacade::ContainerIndex>(selectedRows.front().front());
        auto info = m_mediator.requestItemInfo(index);
        m_contextMenu.setNewPanelItemVisibility( info.getType() == rpFacade::ItemType::Observer );

        m_contextMenu.setAddItemSensitive(!m_blocker.isOpenedPlaylistBlocked());
    }

    Glib::RefPtr<Gdk::Pixbuf> DirectoryTreeView::getIcon(IconType type) const {
        Glib::RefPtr<Gdk::Pixbuf> icon;
        try
        {
            switch(type)
            {
            case IconType::Observer:
                icon = m_observersHolder.getTools().getIconManager().getFolder();
                break;
            case IconType::MediaMP3:
                icon = m_observersHolder.getTools().getIconManager().getSound();
                break;
            case IconType::ComposableDocument:
                icon = m_observersHolder.getTools().getIconManager().getComposableDocument();
                break;
            case IconType::NoIcon:
                break;
            }
        }
        catch (Gtk::IconThemeError&)
        {
        }

        return icon;
    }

}
}
