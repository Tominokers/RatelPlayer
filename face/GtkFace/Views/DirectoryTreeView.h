#pragma once

#include "GtkFace/Menus/ObserverContextMenu.h"
#include "GtkFace/Models/ObserverTreeModel.h"

#include "rpFacade/FacadeObserverTypes.h"

#include <gtkmm/treeview.h>

namespace rpFacade {
    class ObserverInterface;

namespace Container {
    enum class IconType;
}
}

namespace GtkFace {
namespace Widgets {
    namespace Common {
        class UserInterfaceBlocker;
    }
    namespace ObserverPanel {
        class ObserversHolder;
    }
}

namespace Views {

    class DirectoryTreeView : public Gtk::TreeView
    {
    public:
        DirectoryTreeView(rpFacade::ObserverInterface &observerMediator,
                          Widgets::ObserverPanel::ObserversHolder &holder,
                          Widgets::Common::UserInterfaceBlocker &blocker);

        void addItem(const rpFacade::MediaProxy &item);
        void addItem(const rpFacade::Observer &item);

        void clear();

        void setupModel();

    protected:
        void onRowActivated(const Gtk::TreeModel::Path& path,
                            Gtk::TreeViewColumn* column);

        void onCursorChanged();

        bool onButtonRelease(GdkEventButton* event);

        virtual void on_drag_data_get(const Glib::RefPtr<Gdk::DragContext>& context,
                                      Gtk::SelectionData& selection_data,
                                      guint info,
                                      guint time) override;

        virtual void on_drag_end(const Glib::RefPtr<Gdk::DragContext>& context) override;

    private:
        /**
         * List of possible icons of items
         */
        enum class IconType
        {
            //! Type doesn't have icon.
            NoIcon,
            //! Type of and Observer.
            Observer,
            //! Type of mp3 file.
            MediaMP3,
            //! Type of CUE files,
            ComposableDocument,
        };

        void setupMenu();
        void setupColumns();
        void setupSelection();

        void handleAddMenuItem();
        void handleOpenInNewPanel();

        bool popupMenu();
        void configureMenu();

        Glib::RefPtr<Gdk::Pixbuf> getIcon(IconType type) const;

    private:
        Menus::ObserverContextMenu m_contextMenu;

        Glib::RefPtr<Models::ObserverTreeModel> m_model;

        rpFacade::ObserverInterface &m_mediator;
        Widgets::ObserverPanel::ObserversHolder &m_observersHolder;
        Widgets::Common::UserInterfaceBlocker &m_blocker;
    };

}
}
