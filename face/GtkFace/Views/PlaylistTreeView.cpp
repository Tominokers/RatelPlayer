#include "PlaylistTreeView.h"

#include "GtkFace/Utils/DragDropTargetBuilder.h"
#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Utils/KeyValue.h"

#include "rpFacade/PlayerInterface.h"
#include "rpFacade/PlaylistAssistant.h"

#include <glibmm/markup.h>

#include <cassert>

namespace GtkFace {
namespace Views {

    PlaylistTreeView::PlaylistTreeView(const UpdateNameHandler &updatePlaylistName,
                                       rpFacade::PlayerInterface &playerInstance,
                                       rpFacade::PlaylistIndex playlistIndex)
        : m_model(Models::PlaylistTreeModel::create(updatePlaylistName))
        , m_player(playerInstance)
        , m_assistant(m_player.getPlaylistAssistant())
        , m_index(playlistIndex)
        , m_currentPlayedItem(rpFacade::NoIndex)
    {
        setUpView();
        setUpDnD();
        setUpMenu();
        setUpColumns();
    }

    void PlaylistTreeView::setIndex(rpFacade::PlaylistIndex playlistIndex) {
        m_index = playlistIndex;
    }

    void PlaylistTreeView::highlightItem(rpFacade::TrackIndex itemIndex) {
        m_currentPlayedItem = itemIndex;
        this->queue_draw();
    }

    rpFacade::PlaylistFiller& PlaylistTreeView::getPlaylistFiller() {
        return m_model->getFiller();
    }

    void PlaylistTreeView::unsetModel() {
        this->unset_model();
    }

    void PlaylistTreeView::setModel() {
        this->set_model(m_model);
    }

    void PlaylistTreeView::onRowActivated(const Gtk::TreeModel::Path& path,
                                          Gtk::TreeViewColumn* column) {
        static_cast<void>(column);

        if (!path.empty())
        {
            m_assistant.setUsedPlaylist(m_index);
            m_player.setTrack(static_cast<rpFacade::TrackIndex>(path.front()));
            m_player.play();
        }
    }

    void PlaylistTreeView::on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& context,
                                                 int x,
                                                 int y,
                                                 const Gtk::SelectionData& selection_data,
                                                 guint info,
                                                 guint time) {
        static_cast<void>(info);

        auto listTargets = context->list_targets();
        if (Utils::DragDropTargetBuilder::hasOutsidePlainText(listTargets))
        {
            auto position = this->getPosition(x, y);
            auto data = selection_data.get_data_as_string();

            m_assistant.insertSerializedData(data, position);

            context->drop_finish(true, time);
        }
    }

    bool PlaylistTreeView::on_drag_drop(const Glib::RefPtr<Gdk::DragContext>& context, int x, int y, guint time) {
        static_cast<void>(time);

        auto position = this->getPosition(x, y);
        auto listTargets = context->list_targets();
        if (Utils::DragDropTargetBuilder::hasRegularFileTarget(listTargets))
        {
            m_assistant.insertDelayedMedia(m_index, position);
        }
        else if (Utils::DragDropTargetBuilder::hasPlaylistTrackTarget(listTargets))
        {
            auto indexes = getSelectedIndexes();
            m_assistant.insertByCopying(m_index, m_index, indexes, position);

            indexes = getSelectedIndexes();
            m_assistant.removeMedia(m_index, indexes);
        }
        else if (Utils::DragDropTargetBuilder::hasOutsidePlainText(listTargets))
        {
            // Simple return true. Handling is in on_drag_data_received.
        }
        else
        {
            assert(false && "Handling unknown type of drag\'n\'drop");
        }

        return true;
    }

    bool PlaylistTreeView::on_key_press_event(GdkEventKey *event) {
        if (event->keyval == DELETE_KEY)
            handleRemove();

        return Gtk::TreeView::on_key_press_event(event);

    }

    void PlaylistTreeView::onSortingByTrackAlbum() {
        m_assistant.sortPlaylist(m_index, rpFacade::PlaylistSortType::Album);
    }

    void PlaylistTreeView::onSortingByTrackArtist() {
        m_assistant.sortPlaylist(m_index, rpFacade::PlaylistSortType::Artist);
    }

    void PlaylistTreeView::onSortingByTrackNumber() {
        m_assistant.sortPlaylist(m_index, rpFacade::PlaylistSortType::TrackNumber);
    }

    void PlaylistTreeView::onSortingByTrackName() {
        m_assistant.sortPlaylist(m_index, rpFacade::PlaylistSortType::TrackName);
    }

    void PlaylistTreeView::onSortingByTrackYear() {
        m_assistant.sortPlaylist(m_index, rpFacade::PlaylistSortType::Year);
    }

    void PlaylistTreeView::onSortingByTrackDuration() {
        m_assistant.sortPlaylist(m_index, rpFacade::PlaylistSortType::Duration);
    }

    void PlaylistTreeView::setUpView() {
        set_model(m_model);
        auto selection = get_selection();
        selection->set_mode(Gtk::SelectionMode::SELECTION_MULTIPLE);
        signal_row_activated().connect(sigc::mem_fun(*this, &PlaylistTreeView::onRowActivated));
    }

    void PlaylistTreeView::setUpColumns() {
        const Glib::ustring trackNumberColumn(Utils::localize(rpLocalization::LocalizerKeys::TrackNumber));
        const Glib::ustring songTitleColumn(Utils::localize(rpLocalization::LocalizerKeys::SongTitle));
        const Glib::ustring artistColumn(Utils::localize(rpLocalization::LocalizerKeys::ArtistTitle));
        const Glib::ustring albumColumn(Utils::localize(rpLocalization::LocalizerKeys::AlbumTitle));
        const Glib::ustring yearColumn(Utils::localize(rpLocalization::LocalizerKeys::TrackDate));
        const Glib::ustring durationColumn(Utils::localize(rpLocalization::LocalizerKeys::Duration));

        this->append_column(trackNumberColumn, m_model->getTrackNumber());
        this->append_column(songTitleColumn, m_model->getSongTitle());
        this->append_column(artistColumn, m_model->getArtist());
        this->append_column(albumColumn, m_model->getAlbum());
        this->append_column(yearColumn, m_model->getYear());
        this->append_column(durationColumn, m_model->getTrackDuration());

        this->set_headers_clickable(true);

        setUpColumn(0, &PlaylistTreeView::onSortingByTrackNumber);
        setUpColumn(1, &PlaylistTreeView::onSortingByTrackName);
        setUpColumn(2, &PlaylistTreeView::onSortingByTrackArtist);
        setUpColumn(3, &PlaylistTreeView::onSortingByTrackAlbum);
        setUpColumn(4, &PlaylistTreeView::onSortingByTrackYear);
        setUpColumn(5, &PlaylistTreeView::onSortingByTrackDuration);
    }

    void PlaylistTreeView::setUpColumn(int columnIndex, void (PlaylistTreeView::* sortingMethod)()) {
        if (auto column = this->get_column(columnIndex))
        {
            column->set_resizable(true);
            column->signal_clicked().connect(sigc::mem_fun(*this, sortingMethod));
            if (auto cellRenderer = column->get_first_cell())
                column->set_cell_data_func(*cellRenderer, sigc::mem_fun(*this, &PlaylistTreeView::onCellDataFunc));
        }

    }

    void PlaylistTreeView::setUpDnD() {
        Utils::DragDropTargetBuilder dndBuilder;
        dndBuilder.requestPlaylistTrack();

        drag_source_set(dndBuilder.getTargets(), Gdk::ModifierType::BUTTON1_MASK);

        dndBuilder.requestRegularFile()
                  .requestOutsidePlainText();
        drag_dest_set(dndBuilder.getTargets());
    }

    void PlaylistTreeView::setUpMenu() {
        m_contextMenu.connectRemoveSignal(sigc::mem_fun(*this, &PlaylistTreeView::handleRemove));
        signal_popup_menu().connect(sigc::mem_fun(*this, &PlaylistTreeView::popupMenu));
        signal_button_release_event().connect(sigc::mem_fun(*this, &PlaylistTreeView::onButtonRelease));
    }

    bool PlaylistTreeView::onButtonRelease(GdkEventButton* event) {
        if (event->button == 3)
            this->popupMenu();

        return false;
    }

    void PlaylistTreeView::onCellDataFunc(Gtk::CellRenderer *renderer, const Gtk::TreeModel::iterator &iterator) {
        auto *textRenderer = dynamic_cast<Gtk::CellRendererText*>(renderer);
        if (!textRenderer)
            return;

        const auto path = m_model->get_path(iterator);
        if (static_cast<std::size_t>(path.front()) == m_currentPlayedItem)
        {
            textRenderer->property_markup() = "<b>" + Glib::Markup::escape_text(textRenderer->property_text()) + "</b>";
            textRenderer->property_underline() = Pango::UNDERLINE_DOUBLE;
        }
        else
        {
            textRenderer->property_underline() = Pango::UNDERLINE_NONE;
        }
    }

    void PlaylistTreeView::handleRemove() {
        auto indexes = getSelectedIndexes();

        m_assistant.removeMedia(m_index, indexes);
    }

    bool PlaylistTreeView::popupMenu() {
        auto selection = this->get_selection();
        if (selection->count_selected_rows() > 0)
        {
            m_contextMenu.popup(0, 0);
            return true;
        }

        return false;
    }

    std::size_t PlaylistTreeView::getPosition(int x, int y) const {
        Gtk::TreeModel::Path path;
        auto position = (get_path_at_pos(x, y, path)) ?
                        (static_cast<std::size_t>(path.front() - 1)) :
                        (m_model->getSize());
        return position;
    }

    rpFacade::TrackIndexList PlaylistTreeView::getSelectedIndexes() const {
        auto selection = this->get_selection();
        auto selectedRows = selection->get_selected_rows();

        rpFacade::TrackIndexList indexes;
        for (const auto &path : selectedRows)
            if (!path.empty())
                indexes.push_back(static_cast<rpFacade::TrackIndex>(path.front()));

        return indexes;
    }

}
}
