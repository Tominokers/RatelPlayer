#pragma once

#include "GtkFace/Menus/PlaylistContextMenu.h"
#include "GtkFace/Models/PlaylistTreeModel.h"

#include "rpFacade/FacadeTypes.h"

#include <gtkmm/treeview.h>

namespace rpFacade {
    class PlayerInterface;
    class PlaylistAssistant;
    class PlaylistFiller;
}

namespace GtkFace {
namespace Views {

    class PlaylistTreeView : public Gtk::TreeView
    {
    public:
        PlaylistTreeView(const UpdateNameHandler &updatePlaylistName,
                         rpFacade::PlayerInterface &playerInstance,
                         rpFacade::PlaylistIndex playlistIndex);

        void setIndex(rpFacade::PlaylistIndex playlistIndex);

        void highlightItem(rpFacade::TrackIndex itemIndex);

        rpFacade::PlaylistFiller& getPlaylistFiller();

        void unsetModel();

        void setModel();

    private:
        void setUpView();
        void setUpColumns();
        void setUpColumn(int columnIndex, void (PlaylistTreeView::* sortingMethod)());
        void setUpDnD();
        void setUpMenu();
        void onRowActivated(const Gtk::TreeModel::Path& path,
                            Gtk::TreeViewColumn* column);

        void onSortingByTrackAlbum();
        void onSortingByTrackArtist();
        void onSortingByTrackNumber();
        void onSortingByTrackName();
        void onSortingByTrackYear();
        void onSortingByTrackDuration();

        // --- Gtk::TreeView ---
        virtual void on_drag_data_received (const Glib::RefPtr<Gdk::DragContext>& context,
                                            int x,
                                            int y,
                                            const Gtk::SelectionData& selection_data,
                                            guint info,
                                            guint time) override;
        virtual bool on_drag_drop(const Glib::RefPtr<Gdk::DragContext>& context, int x, int y, guint time) override;

        virtual bool on_key_press_event(GdkEventKey *event) override;

        bool onButtonRelease(GdkEventButton* event);
        void onCellDataFunc(Gtk::CellRenderer *renderer, const Gtk::TreeModel::iterator &iterator);

        void handleRemove();

        bool popupMenu();

        std::size_t getPosition(int x, int y) const;
        rpFacade::TrackIndexList getSelectedIndexes() const;

    private:
        Menus::PlaylistContextMenu m_contextMenu;
        Glib::RefPtr<Models::PlaylistTreeModel> m_model;
        rpFacade::PlayerInterface &m_player;
        rpFacade::PlaylistAssistant &m_assistant;
        rpFacade::PlaylistIndex m_index;
        rpFacade::TrackIndex m_currentPlayedItem;
    };

}
}
