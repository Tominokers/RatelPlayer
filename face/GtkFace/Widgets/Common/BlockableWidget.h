#pragma once

namespace GtkFace {
namespace Widgets {
namespace Common {

    class BlockableWidget
    {
    public:
        virtual ~BlockableWidget() = default;

        virtual void blockCloseOperations() =0;

        virtual void unblockCloseOperations() =0;
    };

}
}
}
