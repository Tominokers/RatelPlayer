#include "ClosableTapLabel.h"

namespace GtkFace {
namespace Widgets {
namespace Common {

    ClosableTapLabel::ClosableTapLabel(const Glib::ustring &name,
                                       std::size_t labelIndex,
                                       Gtk::Orientation labelOrientation)
        : Gtk::Box(labelOrientation)
        , m_labelName(name)
        , m_index(labelIndex)
    {
        m_closeButton.set_image_from_icon_name("window-close");
        m_closeButton.set_relief(Gtk::ReliefStyle::RELIEF_NONE);

        if (labelOrientation == Gtk::Orientation::ORIENTATION_VERTICAL)
        {
            m_labelName.set_angle(90.0);
            this->pack_start(m_closeButton, Gtk::PackOptions::PACK_SHRINK);
            this->pack_start(m_labelName);
        }
        else
        {
            this->pack_start(m_labelName);
            this->pack_start(m_closeButton, Gtk::PackOptions::PACK_SHRINK);
        }

        this->show_all();
    }

    void ClosableTapLabel::setIndex(std::size_t labelIndex) {
        m_index = labelIndex;
    }

    std::size_t ClosableTapLabel::getIndex() const {
        return m_index;
    }

    void ClosableTapLabel::setName(const Glib::ustring &name) {
        m_labelName.set_text(name.substr(0, 20));
    }

    void ClosableTapLabel::connectCloseSignal(const sigc::slot<void> &onClose) {
        m_closeButton.signal_clicked().connect(onClose);
    }

    Gtk::Label& ClosableTapLabel::getLabel()
    {
        return m_labelName;
    }

    const Gtk::Label& ClosableTapLabel::getLabel() const
    {
        return m_labelName;
    }

    Gtk::Button& ClosableTapLabel::getButton()
    {
        return m_closeButton;
    }

    const Gtk::Button& ClosableTapLabel::getButton() const
    {
        return m_closeButton;
    }

}
}
}
