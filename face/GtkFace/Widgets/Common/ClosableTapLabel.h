#pragma once

#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>

namespace GtkFace {
namespace Widgets {
namespace Common {

    class ClosableTapLabel : public Gtk::Box
    {
    public:
        explicit ClosableTapLabel(const Glib::ustring &name,
                                  std::size_t labelIndex,
                                  Gtk::Orientation labelOrientation = Gtk::Orientation::ORIENTATION_HORIZONTAL);

        void setIndex(std::size_t labelIndex);

        std::size_t getIndex() const;

        void setName(const Glib::ustring &name);

        void connectCloseSignal(const sigc::slot<void> &onClose);

    protected:
        Gtk::Label& getLabel();

        const Gtk::Label& getLabel() const;

        Gtk::Button& getButton();

        const Gtk::Button& getButton() const;

    private:
        Gtk::Label m_labelName;

        Gtk::Button m_closeButton;

        std::size_t m_index;
    };

}
}
}
