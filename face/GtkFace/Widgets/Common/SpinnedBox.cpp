#include "SpinnedBox.h"

namespace GtkFace {
namespace Widgets {
namespace Common {

    SpinnedBox::SpinnedBox(Gtk::Orientation boxOrientation)
        : m_box(boxOrientation)
    {
        this->add(m_box, "panelBox");
        this->add(m_spin, "progress");
    }

    Gtk::Box& SpinnedBox::getBox() {
        return m_box;
    }

    void SpinnedBox::startProgressSpinning() {
        m_box.hide();
        m_spin.start();
        m_spin.show();
    }

    void SpinnedBox::stopProgressSpining() {
        m_box.show();
        m_spin.stop();
        m_spin.hide();

        this->takeFocus();
    }

    void SpinnedBox::takeFocus() {
    }

}
}
}
