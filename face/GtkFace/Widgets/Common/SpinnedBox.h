#pragma once

#include <gtkmm/hvbox.h>
#include <gtkmm/spinner.h>
#include <gtkmm/stack.h>

namespace GtkFace {
namespace Widgets {
namespace Common {

    class SpinnedBox : public Gtk::Stack
    {
    public:
        explicit SpinnedBox(Gtk::Orientation boxOrientation = Gtk::Orientation::ORIENTATION_VERTICAL);

        Gtk::Box& getBox();

        void startProgressSpinning();

        void stopProgressSpining();

        virtual void takeFocus();

    private:
        Gtk::Box m_box;
        Gtk::Spinner m_spin;
    };

}
}
}
