#include "TapController.h"

#include "GtkFace/Utils/KeyValue.h"
#include "GtkFace/Utils/HotKeyInformation.h"

namespace GtkFace {
namespace Widgets {
namespace Common {

    TapController::TapController(Gtk::PositionType position,
                                 const Utils::HotKeyInformation &createTapHotKey)
    {
        this->set_tab_pos(position);
        this->set_scrollable(true);
        this->setupCreateTapButton(createTapHotKey);
    }

    void TapController::connectCreateTapSignal(const sigc::slot<void> &onAddTap) {
        m_createTap.signal_clicked().connect(onAddTap);
    }

    void TapController::setupCreateTapButton(const Utils::HotKeyInformation &createTapHotKey) {
        m_createTap.set_image_from_icon_name("list-add");
        m_createTap.add_accelerator("clicked",
                                    createTapHotKey.getAccelGroup(),
                                    createTapHotKey.getKey(),
                                    createTapHotKey.getModifier(),
                                    Gtk::AccelFlags::ACCEL_LOCKED);

        m_createTapWrapper.pack_start(m_createTap, Gtk::PackOptions::PACK_SHRINK);
        this->set_action_widget(&m_createTapWrapper);
        m_createTapWrapper.show_all();
    }

}
}
}
