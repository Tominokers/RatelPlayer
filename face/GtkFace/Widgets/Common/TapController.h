#pragma once

#include "BlockableWidget.h"

#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/notebook.h>
#include <sigc++/functors/slot.h>

namespace GtkFace {

namespace Utils {
    class HotKeyInformation;
}

namespace Widgets {
namespace Common {

    class TapController : public Gtk::Notebook
                        , public BlockableWidget
    {
    public:
        explicit TapController(Gtk::PositionType position,
                               const Utils::HotKeyInformation &createTapHotKey);

        void connectCreateTapSignal(const sigc::slot<void> &onAddTap);

    private:
        void setupCreateTapButton(const Utils::HotKeyInformation &createTapHotKey);

    private:
        Gtk::Box m_createTapWrapper;
        Gtk::Button m_createTap;
    };

}
}
}
