#pragma once

namespace Gtk {
    class Widget;
}

namespace GtkFace {
namespace Widgets {
namespace Common {

    class ClosableTapLabel;

    class TapPage
    {
    public:
        virtual ~TapPage() = default;

        virtual Gtk::Widget& getView() =0;

        virtual ClosableTapLabel& getLabel() =0;
    };

}
}
}
