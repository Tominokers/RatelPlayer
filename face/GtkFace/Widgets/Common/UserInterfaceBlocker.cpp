#include "UserInterfaceBlocker.h"

#include "GtkFace/Widgets/Common/BlockableWidget.h"

#include <algorithm>
#include <cassert>

namespace GtkFace {
namespace Widgets {
namespace Common {

    UserInterfaceBlocker::UserInterfaceBlocker()
        : m_closeCounter(0)
        , m_openedPlaylist(0)
    {
    }

    UserInterfaceBlocker::~UserInterfaceBlocker()
    {
        assert(m_closeCounter == 0);
    }

    void UserInterfaceBlocker::registryClosableWidget(BlockableWidget &controller) {
        m_closeWidgets.push_back(&controller);
    }

    void UserInterfaceBlocker::unregistryClosableWidget(BlockableWidget &controller) {
        auto it = std::find(std::cbegin(m_closeWidgets), std::cend(m_closeWidgets), &controller);
        if (it != std::cend(m_closeWidgets))
            m_closeWidgets.erase(it);
    }

    void UserInterfaceBlocker::blockCloseOperations() {
        if (m_closeCounter == 0)
        {
            for (auto &controller : m_closeWidgets)
                controller->blockCloseOperations();
        }

        ++m_closeCounter;
    }

    void UserInterfaceBlocker::unblockCloseOperations() {
        --m_closeCounter;

        if (m_closeCounter == 0)
        {
            for (auto &controller : m_closeWidgets)
                controller->unblockCloseOperations();
        }
    }

    bool UserInterfaceBlocker::isCloseBlocked() {
        return m_closeCounter != 0;
    }

    void UserInterfaceBlocker::markPlaylistBlocked(rpFacade::PlaylistIndex index) {
        m_blockedPlaylists.push_back(index);
    }

    void UserInterfaceBlocker::unmarkPlaylistBlocked(rpFacade::PlaylistIndex index) {
        m_blockedPlaylists.erase(std::find(std::cbegin(m_blockedPlaylists),
                                           std::cend(m_blockedPlaylists),
                                           index));
    }

    void UserInterfaceBlocker::setOpenedPlaylist(rpFacade::PlaylistIndex index) {
        m_openedPlaylist = index;
    }

    bool UserInterfaceBlocker::isOpenedPlaylistBlocked() {
        return std::find(std::cbegin(m_blockedPlaylists),
                         std::cend(m_blockedPlaylists),
                         m_openedPlaylist) != std::cend(m_blockedPlaylists);
    }

}
}
}
