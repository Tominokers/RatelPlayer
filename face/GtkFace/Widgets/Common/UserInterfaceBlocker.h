#pragma once

#include "rpFacade/FacadeTypes.h"

#include <vector>

namespace GtkFace {
namespace Widgets {
namespace Common {

    class BlockableWidget;


    class UserInterfaceBlocker final
    {
    public:
        UserInterfaceBlocker();

        ~UserInterfaceBlocker();

        UserInterfaceBlocker(const UserInterfaceBlocker&) = delete;
        UserInterfaceBlocker(UserInterfaceBlocker&&) = delete;

        UserInterfaceBlocker& operator=(const UserInterfaceBlocker&) = delete;
        UserInterfaceBlocker& operator=(UserInterfaceBlocker&&) = delete;

        void registryClosableWidget(BlockableWidget &widget);

        void unregistryClosableWidget(BlockableWidget &widget);

        void blockCloseOperations();

        void unblockCloseOperations();

        bool isCloseBlocked();

        void markPlaylistBlocked(rpFacade::PlaylistIndex index);

        void unmarkPlaylistBlocked(rpFacade::PlaylistIndex index);

        void setOpenedPlaylist(rpFacade::PlaylistIndex index);

        bool isOpenedPlaylistBlocked();

    private:
        std::vector<BlockableWidget*> m_closeWidgets;
        std::vector<rpFacade::PlaylistIndex> m_blockedPlaylists;
        std::size_t m_closeCounter;
        rpFacade::PlaylistIndex m_openedPlaylist;
    };

}
}
}
