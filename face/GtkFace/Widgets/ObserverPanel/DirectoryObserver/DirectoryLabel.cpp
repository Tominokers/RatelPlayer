#include "DirectoryLabel.h"

#include "GtkFace/Widgets/ObserverPanel/ObserverCollector.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    DirectoryLabel::DirectoryLabel(ObserverPanel::ObserversHolder &holder, const Glib::ustring &name)
        : ClosableTapLabel(name, holder.getObserverCount(), Gtk::Orientation::ORIENTATION_VERTICAL)
        , m_observersHolder(holder)
    {
        this->connectCloseSignal(sigc::mem_fun(this, &DirectoryLabel::onCloseButton));
    }

    void DirectoryLabel::onCloseButton() {
        m_observersHolder.closeObserverAt(this->getIndex());
    }

}
}
}
}
