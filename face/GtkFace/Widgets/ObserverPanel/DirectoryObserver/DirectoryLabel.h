#pragma once

#include "GtkFace/Widgets/Common/ClosableTapLabel.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
    class ObserversHolder;

namespace DirectoryObserver {

    class DirectoryLabel final : public Common::ClosableTapLabel
    {
    public:
        explicit DirectoryLabel(ObserverPanel::ObserversHolder &holder, const Glib::ustring &name);

    private:
        void onCloseButton();

    private:
        ObserverPanel::ObserversHolder &m_observersHolder;
    };

}
}
}
}
