#include "DirectoryObserverPanel.h"

#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Utils/ToolsHolder.h"
#include "GtkFace/Views/DirectoryTreeView.h"
#include "GtkFace/Widgets/Common/ClosableTapLabel.h"
#include "GtkFace/Widgets/ObserverPanel/MediaObserverPanel.h"

#include "rpFacade/FacadeManager.h"
#include "rpFacade/Observers/ObserverMediator.h"
#include "rpObservers/States/DirectoryState.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    DirectoryObserverPanel::DirectoryObserverPanel(const std::string &path,
                                                   Common::ClosableTapLabel &directoryLabel,
                                                   ObserversHolder &holder,
                                                   Common::UserInterfaceBlocker &blocker)
        : m_toolbar(*this, holder.getTools().getAccelGroup())
        , m_label(directoryLabel)
        , m_observersHolder(holder)
    {
        rpObservers::States::DirectoryState state(path);
        m_mediator = rpFacade::FacadeManager::createObserver(state, this);
        this->setupFileObserver(blocker);

        this->getBox().pack_start(m_toolbar, Gtk::PackOptions::PACK_SHRINK);
        this->getBox().pack_start(*m_fileObserver);

        m_setObserverName.connect(sigc::mem_fun(*this, &DirectoryObserverPanel::setObserverNameOnUI));

        m_mediator->Update();
    }

    DirectoryObserverPanel::~DirectoryObserverPanel() = default;

    void DirectoryObserverPanel::switchToParentPath() {
        m_mediator->switchToParentObserver();
    }

    void DirectoryObserverPanel::switchToPath(const std::string &path) {
        m_mediator->switchToState(rpObservers::States::DirectoryState(path));
    }

    const std::string& DirectoryObserverPanel::getObserverName() const {
        return m_observerName;
    }

    rpFacade::ObserverInterface& DirectoryObserverPanel::getObserver() const noexcept {
        return *m_mediator;
    }

    void DirectoryObserverPanel::observerNameChanged(const std::string &newObserver) noexcept {
        m_fileTree->clear();
        m_observerName = newObserver;
        m_setObserverName();
    }

    void DirectoryObserverPanel::obtainingMediaStarted() noexcept {
        this->startProgressSpinning();
    }

    void DirectoryObserverPanel::obtainingMediaFinished() noexcept {
        m_fileTree->setupModel();
        this->stopProgressSpining();
    }

    void DirectoryObserverPanel::takeFocus() {
        m_fileTree->grab_focus();
    }

    void DirectoryObserverPanel::setObserverNameOnUI() {
        m_toolbar.setObserverName(m_observerName);

        // Gets name only of last directory in path.
        // If it is root folder, charactor / will be used.
        auto pos = m_observerName.rfind('/');
        if (m_observerName.size() > pos+1)
            ++pos;
        m_label.setName(m_observerName.substr(pos));
    }

    void DirectoryObserverPanel::passObtainedMedia(const rpFacade::MediaProxy &proxy) noexcept {
        m_fileTree->addItem(proxy);
    }

    void DirectoryObserverPanel::passObtainedObservers(const rpFacade::Observer &observer) noexcept {
        m_fileTree->addItem(observer);
    }

    void DirectoryObserverPanel::setupFileObserver(Common::UserInterfaceBlocker &blocker) {
        m_fileTree = std::make_unique<Views::DirectoryTreeView>(*m_mediator, m_observersHolder, blocker);

        Glib::RefPtr<Gtk::Adjustment> hadj = m_fileTree->get_hadjustment();
        Glib::RefPtr<Gtk::Adjustment> vadj = m_fileTree->get_vadjustment();

        m_fileObserver = std::make_unique<Gtk::ScrolledWindow>(hadj, vadj);
        m_fileObserver->add(*m_fileTree);
        m_fileObserver->set_size_request(300);
    }

}
}
}
}
