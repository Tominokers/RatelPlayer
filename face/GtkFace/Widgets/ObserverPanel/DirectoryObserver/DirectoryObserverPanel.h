#pragma once

#include "GtkFace/Widgets/Common/SpinnedBox.h"
#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/DirectoryToolbar.h"

#include "rpFacade/ObserverClient.h"

#include <glibmm/dispatcher.h>
#include <gtkmm/scrolledwindow.h>

namespace rpFacade {
    class ObserverInterface;
}

namespace GtkFace {
namespace Views {
    class DirectoryTreeView;
}

namespace Widgets {
namespace Common {
    class ClosableTapLabel;
    class UserInterfaceBlocker;
}

namespace ObserverPanel {

    class MediaInfoBoard;
    class ObserversHolder;

namespace DirectoryObserver {

    class DirectoryObserverPanel final : public Common::SpinnedBox,
                                         public rpFacade::ObserverClient
    {
    public:
        DirectoryObserverPanel(const std::string &path,
                               Common::ClosableTapLabel &directoryLabel,
                               ObserversHolder &holder,
                               Common::UserInterfaceBlocker &blocker);

        virtual ~DirectoryObserverPanel();

        void switchToParentPath();

        void switchToPath(const std::string &path);

        const std::string& getObserverName() const;

        virtual rpFacade::ObserverInterface& getObserver() const noexcept override;

    private:
        // --- rpFacade::ObserverClient ---
        virtual void passObtainedMedia(const rpFacade::MediaProxy &proxy) noexcept override;
        virtual void passObtainedObservers(const rpFacade::Observer &observer) noexcept override;
        virtual void observerNameChanged(const std::string &newObserver) noexcept override;
        virtual void obtainingMediaStarted() noexcept override;
        virtual void obtainingMediaFinished() noexcept override;

        virtual void takeFocus() override;

        void setupFileObserver(Common::UserInterfaceBlocker &blocker);

        void setObserverNameOnUI();

    private:
        DirectoryToolbar m_toolbar;
        std::unique_ptr<Views::DirectoryTreeView> m_fileTree;
        std::unique_ptr<Gtk::ScrolledWindow> m_fileObserver;
        std::unique_ptr<rpFacade::ObserverInterface> m_mediator;

        std::string m_observerName;
        Glib::Dispatcher m_setObserverName;

        Common::ClosableTapLabel &m_label;
        ObserversHolder &m_observersHolder;
    };

}
}
}
}
