#include "DirectoryPage.h"

#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Widgets/Common/UserInterfaceBlocker.h"
#include "GtkFace/Widgets/ObserverPanel/ObserverPageCreator.h"
#include "GtkFace/Widgets/ObserverPanel/ObserversHolder.h"

#include "rpObservers/ObserverInformation.h"
#include "rpObservers/Observers/DirectoryObserver.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    DirectoryPage::DirectoryPage(const rpObservers::ObserverInformation *info,
                                 ObserversHolder &holder,
                                 Widgets::Common::UserInterfaceBlocker &blocker)
        : DirectoryPage(info ? info->getFullName()
                             : rpObservers::Observers::DirectoryObserver::getHomePath(),
                        holder,
                        blocker)
    {
    }

    DirectoryPage::DirectoryPage(const std::string &data,
                                 ObserversHolder &holder,
                                 Widgets::Common::UserInterfaceBlocker &blocker)
        : m_label(holder, Utils::localize(rpLocalization::LocalizerKeys::DefaultDirectoryTap))
        , m_directory(data, m_label, holder, blocker)
        , m_connector(m_directory.getObserver(), holder.getPlayer())
    {
        if (blocker.isCloseBlocked())
            getLabel().set_sensitive(false);
    }

    Gtk::Widget& DirectoryPage::getView() {
        return m_directory;
    }

    Common::ClosableTapLabel& DirectoryPage::getLabel() {
        return m_label;
    }

    void DirectoryPage::switchToParentObserver() {
        m_directory.switchToParentPath();
    }

    std::string DirectoryPage::serialize() const {
        auto identifier = std::to_string(ObserverPageCreator::getDirectoryObserverIdentifier());
        auto separator = ObserverPageCreator::getSerializeSeparator();
        const auto &name = m_directory.getObserverName();

        return identifier + separator + name;
    }

}
}
}
}
