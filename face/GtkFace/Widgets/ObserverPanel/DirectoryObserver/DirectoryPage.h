#pragma once

#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/DirectoryLabel.h"
#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/DirectoryObserverPanel.h"
#include "GtkFace/Widgets/ObserverPanel/ObserverPage.h"

#include "rpFacade/Bridge/BridgeConnector.h"

namespace rpObservers {
    class ObserverInformation;
}

namespace GtkFace {

    namespace Utils {
        class IconManager;
    }

namespace Widgets {

    namespace Common {
        class UserInterfaceBlocker;
    }

namespace ObserverPanel {

    class ObserversHolder;

namespace DirectoryObserver {

    class DirectoryPage : public ObserverPage
    {
    public:
        explicit DirectoryPage(const rpObservers::ObserverInformation *info,
                               ObserversHolder &holder,
                               Widgets::Common::UserInterfaceBlocker &blocker);

        explicit DirectoryPage(const std::string &data,
                               ObserversHolder &holder,
                               Widgets::Common::UserInterfaceBlocker &blocker);

        virtual Gtk::Widget& getView() override;

        virtual Common::ClosableTapLabel& getLabel() override;

        virtual void switchToParentObserver() override;

        virtual std::string serialize() const override;

    private:
        DirectoryLabel m_label;
        DirectoryObserverPanel m_directory;
        rpFacade::Bridge::BridgeConnector m_connector;
    };

}
}
}
}
