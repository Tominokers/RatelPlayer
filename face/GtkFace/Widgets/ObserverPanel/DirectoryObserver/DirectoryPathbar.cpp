#include "DirectoryPathbar.h"

#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/PathButton.h"

#include "rpObservers/Observers/DirectoryObserver.h"

#include <cassert>
#include <sstream>

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    DirectoryPathbar::DirectoryPathbar(DirectoryToolbar &parentToolbar)
        : m_homePath(rpObservers::Observers::DirectoryObserver::getHomePath())
        , m_rootPath("/")
        , m_homeIcon("user-home")
        , m_harddiskIcon("drive-harddisk")
        , m_parent(parentToolbar)
        , m_lastActiveButton(nullptr)
        , m_delim('/')
    {
        m_sliders.getLeft().signal_clicked().connect(sigc::mem_fun(*this, &DirectoryPathbar::onPrevSlider));
        m_sliders.getRight().signal_clicked().connect(sigc::mem_fun(*this, &DirectoryPathbar::onNextSlider));

        this->get_style_context()->add_class("linked");
    }

    DirectoryPathbar::~DirectoryPathbar() = default;

    void DirectoryPathbar::setPath(std::string path) {
        if (m_currentPath.compare(path) == 0)
            return;

        if (m_fullPath.compare(0, path.size(), path) == 0)
            this->reuseShownPath(std::move(path));
        else
            this->formComplitlyNewPath(std::move(path));
    }

    void DirectoryPathbar::formComplitlyNewPath(std::string path) {
        this->clearAllButtons();

        this->addPreviousSlider();

        std::string buttonPath;
        if (path.compare(0, m_homePath.size(), m_homePath) == 0)
        {
            path.erase(0, m_homePath.size() + 1);
            buttonPath = this->addStartIconButton(m_homePath, m_homeIcon);
        }
        else
        {
            buttonPath = m_rootPath;
            this->addStartIconButton(buttonPath, m_harddiskIcon);
        }

        this->splitPathOnButtons(path, buttonPath);
        this->addNextSlider();

        if (!m_buttons.empty())
        {
            if (m_lastActiveButton)
                m_lastActiveButton->setKeepActive(false);

            setLastActiveButton(m_buttons.back().get());
            m_fullPath = m_currentPath;
        }

        this->show_all();
    }

    void DirectoryPathbar::reuseShownPath(std::string path) {
        if (path.compare(0, m_homePath.size(), m_homePath))
        {
            // kill home-icon button, because we are in parent directory.
            this->clearAllButtons();
            this->addPreviousSlider();
            std::string fullPathWithoutRoot = m_fullPath.substr(1);
            std::string buttonPath = m_rootPath;
            this->addStartIconButton(buttonPath, m_harddiskIcon);
            this->splitPathOnButtons(fullPathWithoutRoot, buttonPath);
        }
        else if (path.compare(0, m_homePath.size(), m_homePath) == 0 &&
                 m_buttons.front()->getPath() != m_homePath)
        {
            // restore home-icon button, because we are back in home directory from parent.
            this->clearAllButtons();
            this->addPreviousSlider();
            std::string fullPathWithoutHome = m_fullPath.substr(m_homePath.size() + 1);
            std::string buttonPath = this->addStartIconButton(m_homePath, m_homeIcon);
            this->splitPathOnButtons(fullPathWithoutHome, buttonPath);
        }
        else
        {
            this->addPreviousSlider();
        }

        this->addNextSlider();

        if (m_lastActiveButton)
        {
            m_lastActiveButton->setKeepActive(false);
            m_lastActiveButton->set_active(false);
        }

        for (auto &it : m_buttons)
        {
            if (it->getPath().compare(0, path.size(), path) == 0)
            {
                setLastActiveButton(it.get());
                break;
            }
        }

        this->show_all();
    }

    DirectoryPathbar::Metrics DirectoryPathbar::getBarMetrics() {
        int shownWidth = 5; // some extra space
        std::size_t shownCount = 0;

        this->foreach([&] (const auto &button) {
                      if (button.is_visible())
                          {
                          int dummy = 0;
                          int width = 0;
                          button.get_preferred_width(dummy, width);
                          shownWidth += width;
                          ++shownCount;
                      }
                      });

        if (m_sliders.isShown())
        {
            shownCount -= 2;
            assert(shownCount > 0 && "At least one button should be visible");
        }

        return std::make_tuple(shownWidth, shownCount);
    }

    std::string DirectoryPathbar::addStartIconButton(const std::string &path,
                                                     const std::string &iconName) {
        m_buttons.push_back(std::make_unique<PathButton>(m_parent, path));
        m_buttons.back()->set_image_from_icon_name(iconName);
        this->pack_start(*m_buttons.back());
        return path + m_delim;
    }

    void DirectoryPathbar::addNamedButton(const std::string &name, std::string &path) {
        path += name;
        m_buttons.push_back(std::make_unique<PathButton>(m_parent, name, path));
        auto &widget = *m_buttons.back();
        this->pack_start(widget);
        path += m_delim;
    }

    void DirectoryPathbar::addPreviousSlider() {
        if (!m_sliders.getLeft().get_parent())
            this->pack_start(m_sliders.getLeft());
    }

    void DirectoryPathbar::addNextSlider() {
        if (!m_sliders.getRight().get_parent())
            this->pack_start(m_sliders.getRight());
    }

    void DirectoryPathbar::tryHideButtons(int allocatedWidth, Metrics &metrics) {
        auto &shownWidth = std::get<widthIndex>(metrics);
        auto shownCount = std::get<countIndex>(metrics);

        if (shownCount < 2)
            return;

        if (!m_sliders.isShown())
        {
            m_sliders.show();
            metrics = this->getBarMetrics();
            shownWidth = std::get<widthIndex>(metrics);

            assert (shownCount == std::get<countIndex>(metrics) &&
                    "After showing sliders shown count should be the same as before.");
        }

        auto hideButtonIfVisible = [&shownWidth, &shownCount](auto &button)
        {
            if (button->is_visible())
            {
                shownWidth -=  button->get_width();
                button->set_visible(false);
                --shownCount;
            }
        };

        bool needToRemoveFromEnd = false;
        // Lets try to hide buttons from begining.
        std::size_t i = 0;
        while (shownWidth > allocatedWidth && shownCount > 1)
        {
            assert(i < m_buttons.size());
            auto &button = m_buttons[i];
            if (button.get() == m_lastActiveButton)
            {
                needToRemoveFromEnd = shownWidth > allocatedWidth && shownCount != 1;
                break;
            }

            hideButtonIfVisible(button);
            ++i;
        }

        if (needToRemoveFromEnd)
        {
            // Lets try to hide buttons from end.
            i = m_buttons.size() - 1;
            while (shownWidth > allocatedWidth && shownCount > 1)
            {
                assert(i > 0);
                assert(m_buttons[i].get() != m_lastActiveButton);
                hideButtonIfVisible(m_buttons[i]);
                --i;
            }
        }
    }

    void DirectoryPathbar::tryShowButtons(int allocatedWidth, Metrics &metrics) {
        auto &shownWidth = std::get<widthIndex>(metrics);
        auto &shownCount = std::get<countIndex>(metrics);
        auto buttonWidth = allocatedWidth; // initialize with big barrier value.
        std::size_t buttonIndex = 0;
        int delta = allocatedWidth - shownWidth;

        if (shownCount + 1 == m_buttons.size())
            delta += m_sliders.getWidth();

        buttonIndex = getFirstShownButton();
        if (buttonIndex > 0 &&
            buttonIndex < m_buttons.size())
        {
            --buttonIndex; // show buttons from the start.
        }
        else if (shownCount != m_buttons.size())
        {
            buttonIndex = getLastHiddenButtonIndex(); // show buttons from the end.
            if (buttonIndex >= m_buttons.size())
                return;
        }

        buttonWidth = getButtonWidth(buttonIndex);
        if (delta > buttonWidth)
        {
            assert(!m_buttons[buttonIndex]->get_visible() &&
                   "This button shoundn't be visible, something went wrong!");

            m_buttons[buttonIndex]->set_visible(true);
            ++shownCount;
        }
    }

    std::size_t DirectoryPathbar::getLastHiddenButtonIndex() const {
        assert(!m_buttons.empty());
        auto lastHiddenButton = m_buttons.size() - 1;
        while (lastHiddenButton > 0 &&
               !m_buttons[lastHiddenButton]->is_visible())
            --lastHiddenButton;
        ++lastHiddenButton;

        return lastHiddenButton;
    }

    std::size_t DirectoryPathbar::getFirstShownButton() const {
        assert(!m_buttons.empty());
        std::size_t firstShownButton = 0;
        while (firstShownButton < m_buttons.size() &&
               !m_buttons[firstShownButton]->is_visible())
            ++firstShownButton;

        return firstShownButton;
    }

    int DirectoryPathbar::getButtonWidth(std::size_t index) {
        int dummy = 0;
        auto buttonWidth = 0;

        auto &button = m_buttons[index];
        // Hidden widget returns 0 width, so I show and hide widget.
        button->set_visible(true);
        button->get_preferred_width(dummy, buttonWidth);
        button->set_visible(false);

        return buttonWidth;
    }

    void DirectoryPathbar::onNextSlider() {
        if (m_buttons.size() < 2)
            return;

        auto lastRightHidden = getLastHiddenButtonIndex();
        if (lastRightHidden == m_buttons.size())
            return;

        auto firstLeftShown = getFirstShownButton();

        if (lastRightHidden > 0 &&
            firstLeftShown < m_buttons.size() &&
            !m_buttons[lastRightHidden]->is_visible() &&
            m_buttons[firstLeftShown]->is_visible())
        {
            assert(lastRightHidden != firstLeftShown &&
                   "Hidden index can't be shown index at same time!");

            m_buttons[lastRightHidden]->set_visible(true);
            m_buttons[firstLeftShown]->set_visible(false);
        }
    }

    void DirectoryPathbar::onPrevSlider() {
        if (m_buttons.size() < 2)
            return;

        auto firstLeftHidden = getFirstShownButton();
        if (firstLeftHidden == 0)
            return;
        --firstLeftHidden;

        auto lastRightShown = getLastHiddenButtonIndex() - 1;

        if (firstLeftHidden < m_buttons.size() &&
            lastRightShown > 0 &&
            !m_buttons[firstLeftHidden]->is_visible() &&
            m_buttons[lastRightShown]->is_visible())
        {
            assert(firstLeftHidden != lastRightShown && "Hidden can't be shown at same time!");

            m_buttons[firstLeftHidden]->set_visible(true);
            m_buttons[lastRightShown]->set_visible(false);
        }
    }

    void DirectoryPathbar::splitPathOnButtons(const std::string &path, std::string &buttonPath) {
        std::stringstream ss(path);
        std::string name;
        while (std::getline(ss, name, m_delim))
            if (!name.empty())
                this->addNamedButton(name, buttonPath);
    }

    void DirectoryPathbar::clearAllButtons() {
        m_lastActiveButton = nullptr;
        this->foreach(sigc::mem_fun(*this, &DirectoryPathbar::remove));
        m_buttons.clear();
    }

    void DirectoryPathbar::setLastActiveButton(PathButton *button) {
        m_lastActiveButton = button;
        m_currentPath = m_lastActiveButton->getPath();
        m_lastActiveButton->set_active();
        m_lastActiveButton->setKeepActive(true);
    }

    void DirectoryPathbar::on_size_allocate(Gtk::Allocation& allocation) {
        auto metrics = this->getBarMetrics();
        auto &shownWidth = std::get<widthIndex>(metrics);
        const auto &shownCount = std::get<countIndex>(metrics);
        auto allocatedWidth = allocation.get_width();
        int sumWidth = allocation.get_x();
        int dummy = 0;
        int width = 0;

        if (shownWidth > allocatedWidth)
            tryHideButtons(allocatedWidth, metrics);
        else if (shownWidth < allocatedWidth &&
                 shownCount != m_buttons.size())
            tryShowButtons(allocatedWidth, metrics);

        // ensure that sliders are correctly hidden or shown.
        metrics = this->getBarMetrics();
        if (std::get<countIndex>(metrics) == m_buttons.size())
            m_sliders.hide();
        else
            m_sliders.show();

        this->foreach([sumWidth, dummy, width, allocation]
                      (auto &child) mutable {
                      if (child.is_visible())
                      {
                          child.get_preferred_width(dummy, width);

                          Gtk::Allocation childAllocation(sumWidth, allocation.get_y(), width, allocation.get_height());
                          child.size_allocate(childAllocation);

                          sumWidth += width;
                          static_cast<void>(sumWidth); // makes cppcheck happy.
                      }
                      });

        this->set_allocation(allocation);
    }

}
}
}
}
