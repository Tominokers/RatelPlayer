#pragma once

#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/PathBarSliders.h"

#include <gtkmm/box.h>

#include <memory>
#include <string>
#include <tuple>
#include <vector>

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    class DirectoryToolbar;
    class PathButton;

    class DirectoryPathbar final : public Gtk::Box
    {
        static constexpr std::size_t widthIndex = 0;
        static constexpr std::size_t countIndex = 1;
        using Metrics = std::tuple<int, std::size_t>;

    public:
        explicit DirectoryPathbar(DirectoryToolbar &parentToolbar);

        virtual ~DirectoryPathbar();

        void setPath(std::string path);

    private:
        void formComplitlyNewPath(std::string path);
        void reuseShownPath(std::string path);
        Metrics getBarMetrics();

        std::string addStartIconButton(const std::string &path, const std::string &iconName);
        void addNamedButton(const std::string &name, std::string &path);
        void addPreviousSlider();
        void addNextSlider();

        void onNextSlider();
        void onPrevSlider();

        void tryHideButtons(int allocatedWidth, Metrics &metrics);
        void tryShowButtons(int allocatedWidth, Metrics &metrics);

        std::size_t getLastHiddenButtonIndex() const;
        std::size_t getFirstShownButton() const;
        int getButtonWidth(std::size_t index);

        void splitPathOnButtons(const std::string &path, std::string &buttonPath);
        void clearAllButtons();

        void setLastActiveButton(PathButton *button);

        virtual void on_size_allocate(Gtk::Allocation& allocation) override;

    private:
        std::vector<std::unique_ptr<PathButton>> m_buttons;
        PathBarSliders m_sliders;

        const std::string m_homePath;
        const std::string m_rootPath;
        const std::string m_homeIcon;
        const std::string m_harddiskIcon;
        std::string m_fullPath;
        std::string m_currentPath;

        DirectoryToolbar &m_parent;
        PathButton *m_lastActiveButton;

        const char m_delim;
    };

}
}
}
}
