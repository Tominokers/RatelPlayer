#include "DirectoryToolbar.h"

#include "GtkFace/Utils/KeyValue.h"
#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/DirectoryObserverPanel.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    DirectoryToolbar::DirectoryToolbar(DirectoryObserverPanel &parent,
                                       const Glib::RefPtr<Gtk::AccelGroup> &accelGroup)
        : m_parentPanel(parent)
        , m_pathbar(*this)
    {
        m_up.set_image_from_icon_name("go-up");
        m_up.signal_clicked().connect(sigc::mem_fun(*this, &DirectoryToolbar::onUpClicked));
        m_up.add_accelerator("clicked",
                             accelGroup,
                             ARROW_UP_KEY,
                             ALT_MODIFIER,
                             Gtk::AccelFlags::ACCEL_LOCKED);

        this->pack_start(m_up, Gtk::PackOptions::PACK_SHRINK);
        this->pack_start(m_pathbar, Gtk::PackOptions::PACK_EXPAND_WIDGET);
        this->show_all();
    }

    void DirectoryToolbar::setObserverName(const std::string &name) {
        m_pathbar.setPath(name);
    }

    void DirectoryToolbar::onUpClicked() {
        m_parentPanel.switchToParentPath();
    }

    void DirectoryToolbar::changePath(const std::string &path) {
        m_parentPanel.switchToPath(path);
    }

}
}
}
}
