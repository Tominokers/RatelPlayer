#pragma once

#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/DirectoryPathbar.h"

#include <gtkmm/box.h>
#include <gtkmm/button.h>

#include <string>

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    class DirectoryObserverPanel;

    class DirectoryToolbar final : public Gtk::Box
    {
    public:
        DirectoryToolbar(DirectoryObserverPanel &parent,
                         const Glib::RefPtr<Gtk::AccelGroup> &accelGroup);

        void setObserverName(const std::string &name);

        void changePath(const std::string &path);

    protected:
        void onUpClicked();

    private:
        DirectoryObserverPanel &m_parentPanel;

        Gtk::Button m_up;
        DirectoryPathbar m_pathbar;
    };

}
}
}
}
