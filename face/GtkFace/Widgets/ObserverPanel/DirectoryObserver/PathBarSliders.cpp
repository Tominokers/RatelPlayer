#include "PathBarSliders.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    PathBarSliders::PathBarSliders()
        : m_arrowLeft(Gtk::ARROW_LEFT, Gtk::SHADOW_OUT)
        , m_arrowRight(Gtk::ARROW_RIGHT, Gtk::SHADOW_OUT)
    {
        m_sliders[0].add(m_arrowLeft);
        m_sliders[1].add(m_arrowRight);
    }

    bool PathBarSliders::isShown() {
        return m_sliders[0].get_visible() && m_sliders[1].get_visible();
    }

    int PathBarSliders::getWidth() {
        return m_sliders[0].get_width() + m_sliders[1].get_width();
    }

    Gtk::Button& PathBarSliders::getLeft() {
        return m_sliders[0];
    }

    Gtk::Button& PathBarSliders::getRight() {
        return m_sliders[1];
    }

    void PathBarSliders::hide() {
        m_sliders[0].set_visible(false);
        m_sliders[1].set_visible(false);
    }

    void PathBarSliders::show() {
        m_sliders[0].set_visible(true);
        m_sliders[1].set_visible(true);
    }

}
}
}
}
