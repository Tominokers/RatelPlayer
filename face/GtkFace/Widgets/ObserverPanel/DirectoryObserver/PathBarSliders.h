#pragma once

#include <gtkmm/arrow.h>
#include <gtkmm/button.h>

#include <array>

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    class PathBarSliders final
    {
    public:
        PathBarSliders();

        bool isShown();

        int getWidth();

        Gtk::Button& getLeft();

        Gtk::Button& getRight();

        void hide();

        void show();

    private:
        std::array<Gtk::Button, 2> m_sliders;
        Gtk::Arrow m_arrowLeft;
        Gtk::Arrow m_arrowRight;
    };

}
}
}
}
