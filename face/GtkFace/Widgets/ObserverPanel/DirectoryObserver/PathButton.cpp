#include "PathButton.h"

#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/DirectoryToolbar.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    PathButton::PathButton(DirectoryToolbar &parentToolbar,
                           const std::string &buttonPath)
        : Gtk::ToggleButton()
        , m_path(buttonPath)
        , m_toolbar(parentToolbar)
        , m_isKeptActive(false)
    {
    }

    PathButton::PathButton(DirectoryToolbar &parentToolbar,
                           const std::string &buttonName,
                           const std::string &buttonPath)
        : Gtk::ToggleButton(buttonName)
        , m_path(buttonPath)
        , m_toolbar(parentToolbar)
        , m_isKeptActive(false)
    {
        for (auto *child : get_children())
        {
            if (auto label = dynamic_cast<Gtk::Label*>(child))
            {
                label->set_max_width_chars(25);
                label->set_ellipsize(Pango::EllipsizeMode::ELLIPSIZE_END);
            }
        }
    }

    const std::string& PathButton::getPath() const {
        return m_path;
    }

    void PathButton::setKeepActive(bool keepActive) {
        m_isKeptActive = keepActive;
    }

    void PathButton::on_toggled() {
        if (this->get_active())
            m_toolbar.changePath(this->getPath());
        else if (m_isKeptActive)
            this->set_active();
    }

}
}
}
}
