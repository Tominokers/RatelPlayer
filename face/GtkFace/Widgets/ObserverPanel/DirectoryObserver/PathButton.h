#pragma once

#include <gtkmm/togglebutton.h>

#include <string>

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {
namespace DirectoryObserver {

    class DirectoryToolbar;

    class PathButton final : public Gtk::ToggleButton
    {
    public:
        explicit PathButton(DirectoryToolbar &parentToolbar,
                            const std::string &buttonPath);

        explicit PathButton(DirectoryToolbar &parentToolbar,
                            const std::string &buttonName,
                            const std::string &buttonPath);

        const std::string& getPath() const;

        void setKeepActive(bool keepActive);

    private:
        virtual void on_toggled() override;

    private:
        std::string m_path;
        DirectoryToolbar &m_toolbar;
        bool m_isKeptActive;
    };


}
}
}
}
