#include "MediaInfoBoard.h"

#include "GtkFace/Utils/HelpFunctions.h"

#include "rpCore/Media/MediaInformation.h"

#include <glibmm/markup.h>

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {

    MediaInfoBoard::MediaInfoBoard()
        : m_artistLabel(wrapWithItalic(Utils::localize(rpLocalization::LocalizerKeys::ArtistTitle)))
        , m_albumLabel(wrapWithItalic(Utils::localize(rpLocalization::LocalizerKeys::AlbumTitle)))
        , m_songLabel(wrapWithItalic(Utils::localize(rpLocalization::LocalizerKeys::SongTitle)))
        , m_yearLabel(wrapWithItalic(Utils::localize(rpLocalization::LocalizerKeys::TrackDate)))
        , m_artistTitle(m_artistLabel, Gtk::Align::ALIGN_START)
        , m_albumTitle(m_albumLabel, Gtk::Align::ALIGN_START)
        , m_songTitle(m_songLabel, Gtk::Align::ALIGN_START)
        , m_yearTitle(m_yearLabel, Gtk::Align::ALIGN_START)
    {
        m_artistTitle.set_use_markup();
        m_albumTitle.set_use_markup();
        m_songTitle.set_use_markup();
        m_yearTitle.set_use_markup();
        this->pack_start(m_artistTitle);
        this->pack_start(m_albumTitle);
        this->pack_start(m_songTitle);
        this->pack_start(m_yearTitle);
    }

    Glib::ustring MediaInfoBoard::wrapWithItalic(const Glib::ustring &label) const {
        return Glib::ustring("<i>") + label + Glib::ustring("</i>: ");
    }

    void MediaInfoBoard::fillInformation(const rpCore::Media::MediaInformation &info) {
        m_artistTitle.set_label(m_artistLabel + Glib::Markup::escape_text(info.getArtist()));
        m_albumTitle.set_label(m_albumLabel + Glib::Markup::escape_text(info.getAlbum()));
        m_songTitle.set_label(m_songLabel + Glib::Markup::escape_text(info.getSongTitle()));
        m_yearTitle.set_label(m_yearLabel + info.getYear());
    }

}
}
}
