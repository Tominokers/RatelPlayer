#pragma once

#include <gtkmm/hvbox.h>
#include <gtkmm/label.h>

namespace rpCore {
namespace Media {
    class MediaInformation;
}
}

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {

    class MediaInfoBoard final : public Gtk::VBox
    {
    public:
        MediaInfoBoard();

        void fillInformation(const rpCore::Media::MediaInformation &info);

    private:
        Glib::ustring wrapWithItalic(const Glib::ustring &label) const;

    private:
        const Glib::ustring m_artistLabel;
        const Glib::ustring m_albumLabel;
        const Glib::ustring m_songLabel;
        const Glib::ustring m_yearLabel;

        Gtk::Label m_artistTitle;
        Gtk::Label m_albumTitle;
        Gtk::Label m_songTitle;
        Gtk::Label m_yearTitle;
    };

}
}
}
