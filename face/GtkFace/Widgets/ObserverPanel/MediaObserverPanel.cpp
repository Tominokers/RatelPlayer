#include "MediaObserverPanel.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {

    MediaObserverPanel::MediaObserverPanel(rpFacade::PlayerInterface &player,
                                           Utils::ToolsHolder &tools)
        : Gtk::Box(Gtk::Orientation::ORIENTATION_VERTICAL)
        , m_observers(*this, player, tools)
    {
        this->pack_start(m_observers);
        this->pack_end(m_mediaBoard, Gtk::PackOptions::PACK_SHRINK);

        m_mediaBoard.set_margin_left(10);
        m_mediaBoard.set_margin_right(10);
        m_mediaBoard.set_margin_top(10);
        m_mediaBoard.set_margin_bottom(10);

        m_observers.loadObservers();
    }

    MediaInfoBoard& MediaObserverPanel::getMediaInfoBoard() {
        return m_mediaBoard;
    }

    ObserverCollector& MediaObserverPanel::getCollector() {
        return m_observers;
    }

    void MediaObserverPanel::switchToParentObserver() {
        m_observers.switchCurrentToParentObserver();
    }

}
}
}
