#pragma once

#include "GtkFace/Widgets/ObserverPanel/MediaInfoBoard.h"
#include "GtkFace/Widgets/ObserverPanel/ObserverCollector.h"

#include <gtkmm/box.h>

namespace rpFacade {
    class PlayerInterface;
}

namespace GtkFace {

namespace Utils {
    class ToolsHolder;
}

namespace Widgets {
namespace ObserverPanel {

    class MediaObserverPanel final : public Gtk::Box
    {
    public:
        explicit MediaObserverPanel(rpFacade::PlayerInterface &player,
                                    Utils::ToolsHolder &tools);

        MediaInfoBoard& getMediaInfoBoard();

        ObserverCollector& getCollector();

        void switchToParentObserver();

    private:
        MediaInfoBoard m_mediaBoard;

        ObserverCollector m_observers;
    };

}
}
}
