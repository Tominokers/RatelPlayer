#include "ObserverCollector.h"

#include "GtkFace/Utils/HotKeyInformation.h"
#include "GtkFace/Utils/KeyValue.h"
#include "GtkFace/Utils/ToolsHolder.h"
#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/DirectoryPage.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {

    ObserverCollector::ObserverCollector(MediaObserverPanel& observerPanel,
                                         rpFacade::PlayerInterface &playerInstance,
                                         Utils::ToolsHolder &tools)
        : TapController(Gtk::PositionType::POS_LEFT,
                        Utils::HotKeyInformation(tools.getAccelGroup(), T_SHIFT_KEY, CTRL_MODIFIER | SHIFT_MODIFIER))
        , ObserversHolder(observerPanel, playerInstance, tools)
        , m_blocker(tools.getUiBlocker())
    {
        this->connectCreateTapSignal(sigc::mem_fun(this, &ObserverCollector::addDefaultTap));
        m_blocker.registryClosableWidget(*this);
    }

    ObserverCollector::~ObserverCollector() {
        this->storeObservers(static_cast<std::size_t>(this->get_current_page()));
        m_blocker.unregistryClosableWidget(*this);
    }

    void ObserverCollector::addDefaultTap() {
        addTap(nullptr);
    }

    void ObserverCollector::addTap(const rpObservers::ObserverInformation *info) {
        m_pages.emplace_back(std::make_unique<DirectoryObserver::DirectoryPage>(info, *this, m_blocker));

        this->addTap(*(m_pages.back()));
    }

    void ObserverCollector::addTap(ObserverPage &page) {
        page.getView().show_all();
        page.getLabel().show_all();

        this->append_page(page.getView(), page.getLabel());
        this->set_current_page(static_cast<int>(this->getObserverCount()-1));
    }

    void ObserverCollector::closeObserverAt(std::size_t index) {
        if (this->getObserverCount() < 2)
            return;

        auto advance = static_cast<decltype(m_pages)::difference_type>(index);
        m_pages.erase(std::begin(m_pages) + advance);

        for (; index < m_pages.size(); ++index)
            m_pages[index]->getLabel().setIndex(index);
    }

    void ObserverCollector::switchCurrentToParentObserver() {
        auto index = static_cast<decltype(m_pages)::size_type>(this->get_current_page());
        m_pages[index]->switchToParentObserver();
    }

    void ObserverCollector::setActivePage(std::size_t index) {
        this->set_current_page(static_cast<int>(index));
    }

    void ObserverCollector::blockCloseOperations() {
        for (auto &page : m_pages)
            page->getLabel().set_sensitive(false);
    }

    void ObserverCollector::unblockCloseOperations() {
        for (auto &page : m_pages)
            page->getLabel().set_sensitive(true);
    }

}
}
}
