#pragma once

#include "GtkFace/Widgets/Common/TapController.h"
#include "GtkFace/Widgets/ObserverPanel/ObserversHolder.h"

namespace GtkFace {

    namespace Utils {
        class ToolsHolder;
    }

namespace Widgets {

    namespace Common {
        class UserInterfaceBlocker;
    }

namespace ObserverPanel {

    class ObserverCollector : public Common::TapController
                            , public ObserversHolder
    {
    public:
        explicit ObserverCollector(MediaObserverPanel& observerPanel,
                                   rpFacade::PlayerInterface &playerInstance,
                                   Utils::ToolsHolder &tools);

        virtual ~ObserverCollector();

        void switchCurrentToParentObserver();

    private:
        // --- from ObserversHolder ---
        virtual void addDefaultTap() override;
        virtual void addTap(const rpObservers::ObserverInformation *info) override;
        virtual void addTap(ObserverPage &page) override;
        virtual void closeObserverAt(std::size_t index) override;
        virtual void setActivePage(std::size_t index) override;

        // --- from Common::BlockableWidget ---
        virtual void blockCloseOperations() override;
        virtual void unblockCloseOperations() override;

    private:
        Common::UserInterfaceBlocker &m_blocker;
    };

}
}
}
