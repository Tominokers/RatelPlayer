#pragma once

#include "GtkFace/Widgets/Common/TapPage.h"

#include "rpFacade/Observers/ObserverStorage.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {

    class ObserverPage : public Common::TapPage
                       , public rpFacade::Observers::SerializableObserver
    {
    public:
        virtual ~ObserverPage() = default;

        virtual void switchToParentObserver() =0;
    };

}
}
}
