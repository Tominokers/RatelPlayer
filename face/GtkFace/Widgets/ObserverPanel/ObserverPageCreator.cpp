#include "ObserverPageCreator.h"

#include "GtkFace/Widgets/ObserverPanel/DirectoryObserver/DirectoryPage.h"

#include "rpCore/Tools/ConvertUtilityFunctions.h"

#include <cassert>

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {

    std::unique_ptr<ObserverPage> ObserverPageCreator::create(const std::string &data,
                                                              ObserversHolder &holder,
                                                              Widgets::Common::UserInterfaceBlocker &blocker) {
        auto pos = data.find(getSerializeSeparator());
        assert(pos != std::string::npos);

        auto identifierText = data.substr(0, pos);
        auto observerData = data.substr(pos + 1);

        auto identifier = rpCore::Tools::convertTextToInteger(identifierText);
        switch(identifier)
        {
        case m_directoryObserverIndetifier:
            return std::make_unique<DirectoryObserver::DirectoryPage>(observerData, holder, blocker);
        default:
            assert(false && "Unknown identifier!");
            break;
        }

        return nullptr;
    }

    std::size_t ObserverPageCreator::getDirectoryObserverIdentifier() {
        return m_directoryObserverIndetifier;
    }

    char ObserverPageCreator::getSerializeSeparator() {
        return m_separator;
    }

}
}
}
