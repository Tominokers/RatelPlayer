#pragma once

#include "GtkFace/Widgets/ObserverPanel/ObserverPage.h"

#include <memory>

namespace GtkFace {
namespace Widgets {

    namespace Common {
        class UserInterfaceBlocker;
    }

namespace ObserverPanel {

    class ObserversHolder;

    class ObserverPageCreator final
    {
    public:
        static std::size_t getDirectoryObserverIdentifier();
        static char getSerializeSeparator();

        static std::unique_ptr<ObserverPage> create(const std::string &data,
                                                    ObserversHolder &holder,
                                                    Widgets::Common::UserInterfaceBlocker &blocker);
    private:
        static const std::size_t m_directoryObserverIndetifier = 0;
        static const char m_separator = '|';
    };

}
}
}
