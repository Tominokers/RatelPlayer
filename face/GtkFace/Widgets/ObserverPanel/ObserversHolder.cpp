#include "ObserversHolder.h"

#include "GtkFace/Settings/StorageIds.h"
#include "GtkFace/Utils/ToolsHolder.h"
#include "GtkFace/Widgets/ObserverPanel/MediaObserverPanel.h"
#include "GtkFace/Widgets/ObserverPanel/ObserverPageCreator.h"

#include "rpSettings/SettingManager.h"

namespace GtkFace {
namespace Widgets {
namespace ObserverPanel {

    ObserversHolder::ObserversHolder(MediaObserverPanel& observerPanel,
                                     rpFacade::PlayerInterface &playerInstance,
                                     Utils::ToolsHolder &toolsHolder)
        : m_storage(toolsHolder.getSettingManager().getStorage(Settings::DirectoryObserverPanelId))
        , m_panel(observerPanel)
        , m_player(playerInstance)
        , m_tools(toolsHolder)
    {
    }

    ObserversHolder::~ObserversHolder() = default;

    std::size_t ObserversHolder::getObserverCount() const {
        return m_pages.size();
    }

    MediaInfoBoard& ObserversHolder::getMediaInfoBoard() {
        return m_panel.getMediaInfoBoard();
    }

    rpFacade::PlayerInterface& ObserversHolder::getPlayer() {
        return m_player;
    }

    Utils::ToolsHolder& ObserversHolder::getTools() {
        return m_tools;
    }

    void ObserversHolder::storeObservers(std::size_t lastActiveObserver) {
        std::vector<rpFacade::Observers::SerializableObserver*> observers;
        for (auto &page : m_pages)
            observers.push_back(page.get());

        m_storage.store(observers);
        m_storage.setLastActiveObserver(lastActiveObserver);
    }

    void ObserversHolder::load(const std::string &data) {
        auto page = ObserverPageCreator::create(data, *this, m_tools.getUiBlocker());
        m_pages.push_back(std::move(page));

        this->addTap(*(m_pages.back()));
    }

    void ObserversHolder::loadObservers() {
        m_storage.load(*this);

        if (this->getObserverCount() == 0)
            this->addDefaultTap();
        else
            this->setActivePage(m_storage.getLastActiveObserver());
    }

}
}
}
