#pragma once

#include "rpFacade/Observers/ObserverStorage.h"

#include <memory>
#include <vector>

namespace rpFacade {
    class PlayerInterface;
}

namespace rpObservers {
    class ObserverInformation;
}

namespace GtkFace {
namespace Utils {
    class ToolsHolder;
}

namespace Widgets {
namespace ObserverPanel {

    class MediaInfoBoard;
    class MediaObserverPanel;
    class ObserverPage;

    class ObserversHolder : rpFacade::Observers::ObserverLoader
    {
    public:
        ObserversHolder(MediaObserverPanel& observerPanel,
                        rpFacade::PlayerInterface &playerInstance,
                        Utils::ToolsHolder &toolsHolder);

        ObserversHolder(const ObserversHolder&) = delete;
        ObserversHolder(ObserversHolder&&) = delete;

        ObserversHolder& operator=(const ObserversHolder&) = delete;
        ObserversHolder& operator=(ObserversHolder&&) = delete;

        virtual ~ObserversHolder();

        virtual void addDefaultTap() =0;

        virtual void addTap(const rpObservers::ObserverInformation *info) =0;

        virtual void addTap(ObserverPage &page) =0;

        virtual void closeObserverAt(std::size_t index) =0;

        virtual void setActivePage(std::size_t index) =0;

        std::size_t getObserverCount() const;

        MediaInfoBoard& getMediaInfoBoard();

        rpFacade::PlayerInterface& getPlayer();

        Utils::ToolsHolder& getTools();

        void loadObservers();

    protected:
        void storeObservers(std::size_t lastActiveObserver);

    private:
        // --- rpFacade::Observers::ObserverLoader ---
        virtual void load(const std::string &data) override;

    protected:
        std::vector<std::unique_ptr<ObserverPage>> m_pages;

    private:
        rpFacade::Observers::ObserverStorage m_storage;
        MediaObserverPanel &m_panel;
        rpFacade::PlayerInterface &m_player;
        Utils::ToolsHolder &m_tools;
    };

}
}
}
