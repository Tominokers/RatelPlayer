#include "ControlToolbar.h"

#include "rpFacade/PlayerClient.h"
#include "rpFacade/PlayerInterface.h"

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    ControlToolbar::ControlToolbar(rpFacade::PlayerInterface &playerMediator,
                                   Windows::WindowsProvider &windowsProvider)
        : m_settingsMenu(playerMediator, windowsProvider)
        , m_player(playerMediator)
    {
        m_settings.set_image_from_icon_name("system-run");
        m_settings.set_menu(m_settingsMenu);

        m_timebar.set_show_text();
        this->resetShownProgress();

        m_timeEventCatcher.add(m_timebar);
        m_timeEventCatcher.signal_button_press_event().connect(sigc::mem_fun(*this, &ControlToolbar::onProgressClick));

        m_mainControls.pack_start(m_timeEventCatcher);
        m_mainControls.pack_end(m_settings, Gtk::PackOptions::PACK_SHRINK);

        this->pack_start(m_mainControls);
    }

    void ControlToolbar::setTrackProgress(const rpFacade::TrackProgress &progress) {
        m_timebar.set_fraction(progress.m_position);
        std::string position = progress.m_positionHours + ":" +
                               progress.m_positionMinutes + ":" +
                               progress.m_positionSeconds;

        std::string duration = progress.m_durationHours + ":" +
                               progress.m_durationMinutes + ":" +
                               progress.m_durationSeconds;

        m_timebar.set_text((position + "/" + duration.c_str()));
    }

    void ControlToolbar::handlePlayState(rpFacade::PlayerState state) {
        if (state == rpFacade::PlayerState::Stop)
            resetShownProgress();
    }

    void ControlToolbar::handlePlayerError(const std::string &errorMessage) {
        if (!m_errorNotificator.get_parent())
            this->pack_end(m_errorNotificator, Gtk::PackOptions::PACK_SHRINK);

        m_errorNotificator.showError(errorMessage);
        resetShownProgress();
    }

    void ControlToolbar::resetShownProgress() {
        m_timebar.set_fraction(0.0);
        m_timebar.set_text("-");
    }

    bool ControlToolbar::onProgressClick(GdkEventButton* event) {
        auto location = m_timebar.get_allocation();
        auto x = location.get_x();
        auto width = location.get_width();
        if (width != 0)
            m_player.setPosition((event->x - x) / static_cast<double>(width));

        return false;
    }

}
}
}
