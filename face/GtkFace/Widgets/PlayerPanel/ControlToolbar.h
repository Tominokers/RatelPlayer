#pragma once

#include "GtkFace/Menus/SettingsButtonMenu.h"
#include "GtkFace/Widgets/PlayerPanel/PlayerErrorNotification.h"

#include <gtkmm/eventbox.h>
#include <gtkmm/hvbox.h>
#include <gtkmm/menubutton.h>
#include <gtkmm/progressbar.h>
#include <gtkmm/volumebutton.h>

#include <memory>

namespace rpFacade {
    struct TrackProgress;
    enum class PlayerState;

    class PlayerInterface;
}

namespace GtkFace {
namespace Windows {
    class WindowsProvider;
}

namespace Widgets {
namespace PlayerPanel {

    class ControlToolbar final : public Gtk::VBox
    {
    public:
        ControlToolbar(rpFacade::PlayerInterface &playerMediator,
                       Windows::WindowsProvider &windowsProvider);

        void setTrackProgress(const rpFacade::TrackProgress &progress);

        void handlePlayState(rpFacade::PlayerState state);

        void handlePlayerError(const std::string &errorMessage);

    private:
        void resetShownProgress();

        bool onProgressClick(GdkEventButton* event);

    private:
        Gtk::MenuButton m_settings;
        Gtk::ProgressBar m_timebar;
        Gtk::EventBox m_timeEventCatcher;
        Menus::SettingsButtonMenu m_settingsMenu;
        PlayerErrorNotification m_errorNotificator;
        Gtk::HBox m_mainControls;

        rpFacade::PlayerInterface &m_player;
    };

}
}
}
