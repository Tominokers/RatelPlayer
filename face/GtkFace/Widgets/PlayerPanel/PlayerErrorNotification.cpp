#include "PlayerErrorNotification.h"

#include <gtkmm/cssprovider.h>

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    PlayerErrorNotification::PlayerErrorNotification()
        : Common::ClosableTapLabel("", 0)
    {
        connectCloseSignal(sigc::mem_fun(this, &PlayerErrorNotification::hide));

        auto labelCss = Gtk::CssProvider::create();
        labelCss->load_from_data("label { color: red }");
        this->getLabel().get_style_context()->add_provider(labelCss, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

        auto buttonCss = Gtk::CssProvider::create();
        buttonCss->load_from_data("button { background-color: red }");
        this->getButton().get_style_context()->add_provider(buttonCss, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    void PlayerErrorNotification::showError(const std::string &errorMessage) {
        this->setName(errorMessage);
        this->show();
    }

}
}
}
