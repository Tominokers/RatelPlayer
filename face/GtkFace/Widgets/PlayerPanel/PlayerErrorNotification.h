#pragma once

#include "GtkFace/Widgets/Common/ClosableTapLabel.h"

#include <string>

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    class PlayerErrorNotification final : public Common::ClosableTapLabel
    {
    public:
        PlayerErrorNotification();

        void showError(const std::string &errorMessage);

    };

}
}
}
