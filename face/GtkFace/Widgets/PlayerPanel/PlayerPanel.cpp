#include "PlayerPanel.h"

#include "GtkFace/Settings/SettingKeys.h"
#include "GtkFace/Settings/StorageIds.h"
#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Utils/ToolsHolder.h"
#include "GtkFace/Widgets/PlayerPanel/PlayerTitleBar.h"
#include "GtkFace/Windows/WindowsProvider.h"

#include "rpFacade/FacadeManager.h"
#include "rpFacade/PlayerInterface.h"
#include "rpFacade/PlaylistAssistant.h"
#include "rpFacade/Track.h"
#include "rpSettings/SettingManager.h"
#include "rpSettings/SettingStorage.h"

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    PlayerPanel::PlayerPanel(PlayerTitleBar &playerTitleBar,
                             Utils::ToolsHolder &tools)
        : m_mediator(rpFacade::FacadeManager::createPlayer(tools.getSettingManager().getStorage(Settings::PlaylistStorageId), this))
        , m_playlists(*m_mediator, tools)
        , m_controlBar(*m_mediator, tools.getWindowsProvider())
        , m_titleBar(playerTitleBar)
    {
        this->pack_start(m_controlBar, Gtk::PackOptions::PACK_SHRINK);
        this->pack_start(m_playlists);

        m_titleBar.connectPlay(sigc::mem_fun(m_mediator.get(), &rpFacade::PlayerInterface::play));
        m_titleBar.connectStop(sigc::mem_fun(m_mediator.get(), &rpFacade::PlayerInterface::stop));
        m_titleBar.connectNextTrack(sigc::mem_fun(m_mediator.get(), &rpFacade::PlayerInterface::next));
        m_titleBar.connectPreviousTrack(sigc::mem_fun(m_mediator.get(), &rpFacade::PlayerInterface::previous));

        auto &storage = tools.getSettingManager().getStorage(Settings::PlaylistStorageId);
        storage.store(Settings::PlaylistDirectoryKey, Utils::getAutoPlaylistDirectory());

        m_mediator->getPlaylistAssistant().loadSerializedPlaylists();
        if (m_playlists.getSize() == 0)
        {
            m_playlists.addTap(); // At least one playlist should be in collector
            m_mediator->getPlaylistAssistant().setUsedPlaylist(0);
        }
        else
        {
            auto usedIndex = m_mediator->getPlaylistAssistant().getUsedPlaylistIndex();
            m_playlists.set_current_page(static_cast<int>(usedIndex));
        }
    }

    rpFacade::PlayerInterface& PlayerPanel::getPlayer() noexcept {
        return *m_mediator;
    }

    rpFacade::PlaylistIndex PlayerPanel::requestPlaylistCreation() {
        return m_playlists.addTap();
    }

    void PlayerPanel::requestPlaylistClose(rpFacade::PlaylistIndex index) {
        m_playlists.removeTap(index);
    }

    void PlayerPanel::passChangedPlayState(rpFacade::PlayerState state) noexcept {
        m_titleBar.handlePlayState(state);
        m_controlBar.handlePlayState(state);
    }

    void PlayerPanel::passTrackProgress(const rpFacade::TrackProgress &progress) noexcept {
        m_controlBar.setTrackProgress(progress);
    }

    void PlayerPanel::passCurrentTrack(const rpFacade::Track &track,
                                       rpFacade::PlaylistIndex playlist,
                                       rpFacade::TrackIndex item) noexcept {
        Glib::ustring trackName = "[" + track.getArtist() + "|" + track.getSongTitle() + "]";
        m_titleBar.setTitleSuffix(trackName);

        m_playlists.highlightItem(playlist, item);
    }

    void PlayerPanel::passErrorNotification(const std::string &errorMessage) noexcept {
        m_controlBar.handlePlayerError(errorMessage);
    }

}
}
}
