#pragma once

#include "GtkFace/Widgets/PlayerPanel/ControlToolbar.h"
#include "GtkFace/Widgets/PlayerPanel/PlaylistCollector.h"

#include "rpFacade/PlayerClient.h"

#include <gtkmm/hvbox.h>

#include <memory>

namespace rpFacade {
    class PlayerInterface;
}

namespace rpSettings {
    class SettingManager;
}

namespace GtkFace {
namespace Windows {
    class WindowsProvider;
}

namespace Widgets {
namespace PlayerPanel {

    class ControlToolbar;
    class PlayerTitleBar;
    class PlaylistCollector;

    class PlayerPanel final : public Gtk::VBox,
                              public rpFacade::PlayerClient
    {
    public:
        PlayerPanel(PlayerTitleBar &playerTitleBar,
                    Utils::ToolsHolder &tools);

        virtual rpFacade::PlayerInterface& getPlayer() noexcept override;

    private:
        // --- from rpFacade::PlayerClient ---
        virtual rpFacade::PlaylistIndex requestPlaylistCreation() override;
        virtual void requestPlaylistClose(rpFacade::PlaylistIndex index) override;
        virtual void passChangedPlayState(rpFacade::PlayerState state) noexcept override;
        virtual void passTrackProgress(const rpFacade::TrackProgress &progress) noexcept override;
        virtual void passCurrentTrack(const rpFacade::Track &track,
                                      rpFacade::PlaylistIndex playlist,
                                      rpFacade::TrackIndex item) noexcept override;
        virtual void passErrorNotification(const std::string &errorMessage) noexcept override;

    private:
        std::unique_ptr<rpFacade::PlayerInterface> m_mediator;

        PlaylistCollector m_playlists;
        ControlToolbar m_controlBar;

        PlayerTitleBar &m_titleBar;
    };

}
}
}
