#include "PlayerTitleBar.h"

#include "GtkFace/ProgramInfo.h"

#include "rpFacade/PlayerState.h"

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    PlayerTitleBar::PlayerTitleBar()
        : m_title(GTK_APPLICATION_NAME)
    {
        m_playButton.set_image_from_icon_name("media-playback-start");
        m_stopButton.set_image_from_icon_name("media-playback-stop");
        m_nextButton.set_image_from_icon_name("media-skip-forward");
        m_prevButton.set_image_from_icon_name("media-skip-backward");

        this->pack_end(m_nextButton);
        this->pack_end(m_stopButton);
        this->pack_end(m_playButton);
        this->pack_end(m_prevButton);

        this->set_show_close_button(true);
        this->set_title(m_title);
    }

    void PlayerTitleBar::connectPlay(const sigc::slot<void> &playMethod) {
        m_playButton.signal_clicked().connect(playMethod);
    }

    void PlayerTitleBar::connectStop(const sigc::slot<void> &stopMethod) {
        m_stopButton.signal_clicked().connect(stopMethod);
    }

    void PlayerTitleBar::connectNextTrack(const sigc::slot<void> &nextTrackMethod) {
        m_nextButton.signal_clicked().connect(nextTrackMethod);
    }

    void PlayerTitleBar::connectPreviousTrack(const sigc::slot<void> &previousTrackMethod) {
        m_prevButton.signal_clicked().connect(previousTrackMethod);
    }

    void PlayerTitleBar::handlePlayState(rpFacade::PlayerState state) {
        switch(state)
        {
        case rpFacade::PlayerState::Playing:
            m_playButton.set_image_from_icon_name("media-playback-pause");
            break;
        case rpFacade::PlayerState::Pause:
            m_playButton.set_image_from_icon_name("media-playback-start");
            break;
        case rpFacade::PlayerState::Stop:
            m_playButton.set_image_from_icon_name("media-playback-start");
            break;

        }
    }

    void PlayerTitleBar::setTitleSuffix(const Glib::ustring &suffix) {
        this->set_title(m_title + " | " + suffix );
    }

}
}
}
