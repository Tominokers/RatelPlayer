#pragma once

#include <gtkmm/headerbar.h>
#include <gtkmm/button.h>

namespace rpFacade {
    enum class PlayerState;
}

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    class PlayerTitleBar final : public Gtk::HeaderBar
    {
    public:
        PlayerTitleBar();

        void handlePlayState(rpFacade::PlayerState state);

        void setTitleSuffix(const Glib::ustring &suffix);

        void connectPlay(const sigc::slot<void> &playMethod);

        void connectStop(const sigc::slot<void> &stopMethod);

        void connectNextTrack(const sigc::slot<void> &nextTrackMethod);

        void connectPreviousTrack(const sigc::slot<void> &previousTrackMethod);

    private:
        Gtk::Button m_playButton;

        Gtk::Button m_stopButton;

        Gtk::Button m_nextButton;

        Gtk::Button m_prevButton;

        const Glib::ustring m_title;
    };
}
}
}
