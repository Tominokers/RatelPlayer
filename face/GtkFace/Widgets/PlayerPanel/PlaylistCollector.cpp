#include "PlaylistCollector.h"

#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Utils/HotKeyInformation.h"
#include "GtkFace/Utils/KeyValue.h"
#include "GtkFace/Utils/ToolsHolder.h"

#include "GtkFace/Widgets/PlayerPanel/PlaylistPage.h"

#include "rpFacade/PlayerInterface.h"
#include "rpFacade/PlaylistAssistant.h"

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    PlaylistCollector::PlaylistCollector(rpFacade::PlayerInterface &playerMediator,
                                         Utils::ToolsHolder &tools)
        : TapController(Gtk::PositionType::POS_RIGHT,
                        Utils::HotKeyInformation(tools.getAccelGroup(), T_KEY, CTRL_MODIFIER))
        , m_playlistDefaultName(Utils::localize(rpLocalization::LocalizerKeys::DefaultPlaylistTap))
        , m_player(playerMediator)
        , m_assistant(m_player.getPlaylistAssistant())
        , m_blocker(tools.getUiBlocker())
        , m_currentPlaylist(0)
    {
        this->signal_switch_page().connect(sigc::mem_fun(this, &PlaylistCollector::onSwitchPage));

        auto addMethod = reinterpret_cast<void (PlaylistCollector::*)()>(&PlaylistCollector::addTap);
        this->connectCreateTapSignal(sigc::mem_fun(this, addMethod));

        m_blocker.registryClosableWidget(*this);
    }

    PlaylistCollector::~PlaylistCollector() {
        m_blocker.unregistryClosableWidget(*this);
    }

    rpFacade::PlayerInterface& PlaylistCollector::getPlayer() const {
        return m_player;
    }

    std::size_t PlaylistCollector::getSize() const {
        return m_pages.size();
    }

    rpFacade::PlaylistIndex PlaylistCollector::addTap() {
        auto page = std::make_unique<PlaylistPage>(*this, m_playlistDefaultName, m_pages.size());

        if (m_blocker.isCloseBlocked())
            page->getLabel().set_sensitive(false);

        m_pages.emplace_back(std::move(page));
        auto &lastPage = m_pages.back();
        this->append_page(lastPage->getView(), lastPage->getLabel());
        lastPage->show();

        auto pageIndex = m_assistant.createPlaylist(lastPage->getFiller(), *lastPage);
        this->set_current_page(static_cast<int>(pageIndex));
        m_assistant.setOpenedPlaylist(pageIndex);

        return pageIndex;
    }

    void PlaylistCollector::removeTap(rpFacade::PlaylistIndex index) {
        auto it = m_pages.erase(std::begin(m_pages) + static_cast<decltype(m_pages)::difference_type>(index));
        for (; it != std::end(m_pages); ++it, ++index)
            (*it)->setIndex(index);
    }

    void PlaylistCollector::closePlaylistAt(rpFacade::PlaylistIndex index) {
        m_assistant.closePlaylist(index);
    }

    void PlaylistCollector::highlightItem(rpFacade::PlaylistIndex index, rpFacade::TrackIndex item) {
        if (index != m_currentPlaylist && m_currentPlaylist < m_pages.size())
        {
            m_pages[m_currentPlaylist]->highlightItem(rpFacade::NoIndex);
        }

        m_currentPlaylist = index;
        m_pages[m_currentPlaylist]->highlightItem(item);
    }

    Common::UserInterfaceBlocker& PlaylistCollector::getUiBlocker() {
        return m_blocker;
    }

    void PlaylistCollector::onSwitchPage(Widget* page, guint pageNumber) {
        static_cast<void>(page);
        m_assistant.setOpenedPlaylist(pageNumber);
        m_blocker.setOpenedPlaylist(pageNumber);
    }

    void PlaylistCollector::blockCloseOperations() {
        for (auto &page : m_pages)
            page->getLabel().set_sensitive(false);
    }

    void PlaylistCollector::unblockCloseOperations() {
        for (auto &page : m_pages)
            page->getLabel().set_sensitive(true);
    }

}
}
}
