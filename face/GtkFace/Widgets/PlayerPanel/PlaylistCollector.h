#pragma once

#include "GtkFace/Widgets/Common/TapController.h"

#include "rpFacade/FacadeTypes.h"

namespace rpFacade {
    class PlayerInterface;
    class PlaylistAssistant;
}

#include <memory>
#include <vector>

namespace GtkFace {

    namespace Utils {
        class ToolsHolder;
    }

namespace Widgets {

    namespace Common {
        class UserInterfaceBlocker;
    }

namespace PlayerPanel {

    class PlaylistPage;

    class PlaylistCollector final : public Common::TapController
    {
    public:
        PlaylistCollector(rpFacade::PlayerInterface &playerMediator,
                          Utils::ToolsHolder &tools);

        ~PlaylistCollector();

        rpFacade::PlayerInterface& getPlayer() const;

        std::size_t getSize() const;

        rpFacade::PlaylistIndex addTap();

        void removeTap(rpFacade::PlaylistIndex index);

        void closePlaylistAt(rpFacade::PlaylistIndex index);

        void highlightItem(rpFacade::PlaylistIndex index, rpFacade::TrackIndex item);

        Common::UserInterfaceBlocker& getUiBlocker();

    private:
        void onSwitchPage(Widget* page, guint pageNumber);

        // --- from Common::BlockableWidget ---
        virtual void blockCloseOperations() override;
        virtual void unblockCloseOperations() override;

    private:
        const Glib::ustring m_playlistDefaultName;
        std::vector<std::unique_ptr<PlaylistPage>> m_pages;
        rpFacade::PlayerInterface &m_player;
        rpFacade::PlaylistAssistant &m_assistant;
        Common::UserInterfaceBlocker &m_blocker;
        rpFacade::PlaylistIndex m_currentPlaylist;
    };

}
}
}
