#include "PlaylistLabel.h"

#include "GtkFace/Widgets/PlayerPanel/PlaylistCollector.h"

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    PlaylistLabel::PlaylistLabel(PlaylistCollector &playlistCollector,
                                 const Glib::ustring &name,
                                 rpFacade::PlaylistIndex index)
        : ClosableTapLabel(name, index)
        , m_collector(playlistCollector)
    {
        this->connectCloseSignal(sigc::mem_fun(this, &PlaylistLabel::onCloseButton));
    }

    void PlaylistLabel::onCloseButton() {
        m_collector.closePlaylistAt(this->getIndex());
    }

}
}
}
