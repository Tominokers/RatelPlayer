#pragma once

#include "GtkFace/Widgets/Common/ClosableTapLabel.h"

#include "rpFacade/FacadeTypes.h"

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    class PlaylistCollector;

    class PlaylistLabel final : public Common::ClosableTapLabel
    {
    public:
        PlaylistLabel(PlaylistCollector &playlistCollector,
                      const Glib::ustring &name,
                      rpFacade::PlaylistIndex index);

    private:
        void onCloseButton();

    private:
        PlaylistCollector &m_collector;
    };

}
}
}
