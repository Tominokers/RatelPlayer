#include "PlaylistPage.h"

#include "GtkFace/Widgets/PlayerPanel/PlaylistCollector.h"

#include "GtkFace/Widgets/Common/UserInterfaceBlocker.h"

namespace GtkFace {
namespace Widgets {
namespace PlayerPanel {

    PlaylistPage::PlaylistPage(PlaylistCollector &playlistCollector,
                               const Glib::ustring &name,
                               rpFacade::PlaylistIndex index)
        : m_label(playlistCollector, name, index)
        , m_view([this](const std::string &playlistName)
                 {
                     m_label.setName(playlistName);
                 },
                 playlistCollector.getPlayer(),
                 index)
        , m_scrolledPlaylist(m_view.get_hadjustment(), m_view.get_vadjustment())
        , m_blocker(playlistCollector.getUiBlocker())
    {
        m_scrolledPlaylist.add(m_view);
        m_scrolledPlaylist.set_size_request(300);

        m_spinnedBox.getBox().pack_start(m_scrolledPlaylist);
    }

    Gtk::Widget& PlaylistPage::getView() {
        return m_spinnedBox;
    }

    Common::ClosableTapLabel& PlaylistPage::getLabel() {
        return m_label;
    }

    void PlaylistPage::show() {
        m_spinnedBox.show_all();
        m_label.show();
    }

    void PlaylistPage::setIndex(rpFacade::PlaylistIndex index) {
        m_view.setIndex(index);
        m_label.setIndex(index);
    }

    void PlaylistPage::highlightItem(rpFacade::TrackIndex index) {
        m_view.highlightItem(index);
    }

    rpFacade::PlaylistFiller& PlaylistPage::getFiller() {
        return m_view.getPlaylistFiller();
    }

    void PlaylistPage::handleStartNotification() {
        m_blocker.blockCloseOperations();
        m_blocker.markPlaylistBlocked(m_label.getIndex());

        m_spinnedBox.startProgressSpinning();
        m_view.unsetModel();
    }

    void PlaylistPage::handleEndNotification() {
        m_view.setModel();
        m_spinnedBox.stopProgressSpining();

        m_blocker.unmarkPlaylistBlocked(m_label.getIndex());
        m_blocker.unblockCloseOperations();
    }

}
}
}
