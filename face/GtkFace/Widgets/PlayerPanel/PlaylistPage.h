#pragma once

#include "GtkFace/Views/PlaylistTreeView.h"
#include "GtkFace/Widgets/Common/SpinnedBox.h"
#include "GtkFace/Widgets/Common/TapPage.h"
#include "GtkFace/Widgets/PlayerPanel/PlaylistLabel.h"

#include "rpFacade/Concurrency/ActionListener.h"
#include "rpFacade/FacadeTypes.h"

#include <gtkmm/scrolledwindow.h>

namespace rpFacade {
    class PlaylistFiller;
}

namespace GtkFace {
namespace Widgets {

    namespace Common {
        class UserInterfaceBlocker;
    }

namespace PlayerPanel {

    class PlaylistCollector;

    class PlaylistPage : public Common::TapPage
                       , public rpFacade::Concurrency::ActionListener
    {
    public:
        PlaylistPage(PlaylistCollector &playlistCollector,
                     const Glib::ustring &name,
                     rpFacade::PlaylistIndex index);

        virtual Gtk::Widget& getView() override;

        virtual Common::ClosableTapLabel& getLabel() override;

        void show();

        void setIndex(rpFacade::PlaylistIndex index);

        void highlightItem(rpFacade::TrackIndex index);

        rpFacade::PlaylistFiller& getFiller();

    private:
        // --- from rpFacade::Concurrency::ActionListener ---
        virtual void handleStartNotification() override;
        virtual void handleEndNotification() override;

    private:
        PlaylistLabel m_label;
        Views::PlaylistTreeView m_view;
        Common::SpinnedBox m_spinnedBox;
        Gtk::ScrolledWindow m_scrolledPlaylist;
        Common::UserInterfaceBlocker &m_blocker;
    };

}
}
}
