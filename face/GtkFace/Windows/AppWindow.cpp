#include "AppWindow.h"

#include "GtkFace/Settings/SettingKeys.h"
#include "GtkFace/Settings/StorageIds.h"
#include "GtkFace/Utils/KeyValue.h"

#include "rpFacade/PlayerInterface.h"
#include "rpFacade/PlaylistAssistant.h"
#include "rpSettings/SettingManager.h"
#include "rpSettings/SettingStorage.h"

namespace GtkFace {
namespace Windows {

    AppWindow::AppWindow(Glib::RefPtr<Gtk::Application> &application,
                         rpSettings::SettingManager &settingsManager)
        : m_tools(*this, settingsManager)
        , m_titleBar()
        , m_player(m_titleBar, m_tools)
        , m_observer(m_player.getPlayer(), m_tools)
        , m_keysListener(m_player.getPlayer())
        , m_mainPaned(Gtk::Orientation::ORIENTATION_HORIZONTAL)
        , m_app(application)
        , m_storage(settingsManager.getStorage(Settings::ApplicationWindowStorageId))
    {
        this->set_default_size(800, 600);
        this->set_titlebar(m_titleBar);

        m_mainPaned.pack1(m_observer, true, false);
        m_mainPaned.pack2(m_player, true, false);
        this->add(m_mainPaned);

        m_mainPaned.show_all();
        m_titleBar.show_all();

        m_app->hold();

        this->loadSettings();
    }

    AppWindow::~AppWindow() {
        this->storeSettings();
    }

    void AppWindow::quit() {
        m_app->quit();
    }

    void AppWindow::appendFiles(const std::vector<std::string> &files) {
        m_player.getPlayer().getPlaylistAssistant().appendToOpenedPlaylist(files);
    }

    bool AppWindow::on_delete_event(GdkEventAny *event) {
        m_app->release();
        return Gtk::ApplicationWindow::on_delete_event(event);
    }

    bool AppWindow::on_key_press_event(GdkEventKey *event) {
        if (event->keyval == BACKSPACE_KEY)
            m_observer.switchToParentObserver();

        return Gtk::ApplicationWindow::on_key_press_event(event);
    }

    void AppWindow::loadSettings() {
        auto xCoordinate = m_storage.load(Settings::ApplicationXCoordinate);
        auto yCoordinate = m_storage.load(Settings::ApplicationYCoordinate);
        auto width = m_storage.load(Settings::ApplicationWidth);
        auto height = m_storage.load(Settings::ApplicationHeight);

        if (!xCoordinate.empty() && !yCoordinate.empty())
        {
            auto x = std::atoi(xCoordinate.c_str());
            auto y = std::atoi(yCoordinate.c_str());

            this->move(x, y);
        }

        if (!width.empty() && !height.empty())
        {
            auto w = std::atoi(width.c_str());
            auto h = std::atoi(height.c_str());

            this->resize(w, h);
        }

        auto isMaximized = m_storage.load(Settings::IsApplicationMaximizedKey);
        if (!isMaximized.empty() && std::atoi(isMaximized.c_str()))
            this->maximize();

        auto panePosition = m_storage.load(Settings::ApplicationPanePosition);
        if (!panePosition.empty())
            m_mainPaned.set_position(std::atoi(panePosition.c_str()));
    }

    void AppWindow::storeSettings() {
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        int isMaximized = static_cast<int>(this->is_maximized());

        this->get_window()->get_position(x, y);
        this->get_size(w, h);
        auto panePosition = m_mainPaned.get_position();

        m_storage.store(Settings::ApplicationXCoordinate, std::to_string(x));
        m_storage.store(Settings::ApplicationYCoordinate, std::to_string(y));
        m_storage.store(Settings::ApplicationWidth, std::to_string(w));
        m_storage.store(Settings::ApplicationHeight, std::to_string(h));
        m_storage.store(Settings::IsApplicationMaximizedKey, std::to_string(isMaximized));
        m_storage.store(Settings::ApplicationPanePosition, std::to_string(panePosition));
    }

}
}
