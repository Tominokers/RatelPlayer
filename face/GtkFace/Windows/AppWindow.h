#pragma once

#include "GtkFace/Utils/MediaKeysListener.h"
#include "GtkFace/Utils/ToolsHolder.h"
#include "GtkFace/Widgets/ObserverPanel/MediaObserverPanel.h"
#include "GtkFace/Widgets/PlayerPanel/PlayerPanel.h"
#include "GtkFace/Widgets/PlayerPanel/PlayerTitleBar.h"

#include <gtkmm/application.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/paned.h>

#include <string>
#include <vector>

namespace rpSettings {
    class SettingManager;
    class SettingStorage;
}

namespace GtkFace {
namespace Windows {

    class AppWindow final : public Gtk::ApplicationWindow
    {
    public:
        explicit AppWindow(Glib::RefPtr<Gtk::Application> &application,
                           rpSettings::SettingManager &settingsManager);

        virtual ~AppWindow();

        void appendFiles(const std::vector<std::string> &files);

        void quit();

    protected:
        virtual bool on_delete_event(GdkEventAny *event) override;

        virtual bool on_key_press_event(GdkEventKey *event) override;

    private:
        void loadSettings();
        void storeSettings();

    private:
        Utils::ToolsHolder m_tools;

        Widgets::PlayerPanel::PlayerTitleBar m_titleBar;
        Widgets::PlayerPanel::PlayerPanel m_player;
        Widgets::ObserverPanel::MediaObserverPanel m_observer;
        Utils::MediaKeysListener m_keysListener;

        Gtk::Paned m_mainPaned;

        Glib::RefPtr<Gtk::Application> &m_app;

        rpSettings::SettingStorage &m_storage;
    };

}
}
