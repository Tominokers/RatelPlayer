#include "WindowsProvider.h"

#include "GtkFace/ProgramInfo.h"
#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Utils/IconManager.h"
#include "GtkFace/Windows/AppWindow.h"

namespace GtkFace {
namespace Windows {

    WindowsProvider::WindowsProvider(AppWindow &applicationWindows, Utils::IconManager &iconManager)
        : m_mainWindow(applicationWindows)
    {
        this->setupAboutDialog();
        this->setupIcons(iconManager);
    }

    void WindowsProvider::openAboutDialog() {
        m_aboutDialog.show_all();
    }

    void WindowsProvider::hideAboutDialog() {
        m_aboutDialog.hide();
    }

    void WindowsProvider::openMainWindow() {
        m_mainWindow.show_all();
    }

    void WindowsProvider::hideMainWindow() {
        m_mainWindow.hide();
    }

    bool WindowsProvider::isMainVisible() {
        return m_mainWindow.is_visible();
    }

    void WindowsProvider::quit() {
        m_mainWindow.quit();
    }

    Glib::RefPtr<Gtk::AccelGroup> WindowsProvider::getAccelGroup() const {
        return m_mainWindow.get_accel_group();
    }

    void WindowsProvider::setupAboutDialog() {
        m_aboutDialog.set_program_name(GTK_APPLICATION_NAME);
        m_aboutDialog.set_version(GTK_APPLICATION_VERSION);
        m_aboutDialog.set_website(GTK_APPLICATION_WEBSITE);
        m_aboutDialog.set_website_label(Utils::localize(rpLocalization::LocalizerKeys::RepositoryInfo));
        m_aboutDialog.set_transient_for(m_mainWindow);
        m_aboutDialog.set_authors(GTK_APPLICATION_AUTHORS);
        m_aboutDialog.set_comments(Utils::localize(rpLocalization::LocalizerKeys::ProgramInfo));
        m_aboutDialog.set_modal();
    }

    void WindowsProvider::setupIcons(Utils::IconManager &iconManager) {
        try
        {
            auto logo = iconManager.getApplicationLogo();
            m_aboutDialog.set_logo(logo);

            std::vector<Glib::RefPtr<Gdk::Pixbuf>> icons_set;
            icons_set.push_back(iconManager.getAppIcon16());
            icons_set.push_back(iconManager.getAppIcon22());
            icons_set.push_back(iconManager.getAppIcon24());
            icons_set.push_back(iconManager.getAppIcon32());
            icons_set.push_back(iconManager.getAppIcon48());
            icons_set.push_back(iconManager.getAppIcon64());
            icons_set.push_back(iconManager.getAppIcon128());
            Gtk::Window::set_default_icon_list(icons_set);
        }
        catch(Gdk::PixbufError&)
        {
        }
    }

}
}
