#pragma once

#include <gtkmm/aboutdialog.h>

#include <memory>

namespace GtkFace {
namespace Utils {
    class IconManager;
}

namespace Windows {

    class AppWindow;

    class WindowsProvider
    {
    public:
        explicit WindowsProvider(AppWindow &applicationWindows, Utils::IconManager &iconManager);

        WindowsProvider(const WindowsProvider&) = delete;
        WindowsProvider(WindowsProvider&&) = delete;
        WindowsProvider& operator=(const WindowsProvider&) = delete;
        WindowsProvider& operator=(WindowsProvider&&) = delete;

        void openMainWindow();
        void hideMainWindow();
        bool isMainVisible();

        void quit();

        void openAboutDialog();
        void hideAboutDialog();

        Glib::RefPtr<Gtk::AccelGroup> getAccelGroup() const;

    private:
        void setupAboutDialog();
        void setupIcons(Utils::IconManager &iconManager);

    private:
        Gtk::AboutDialog m_aboutDialog;

        AppWindow &m_mainWindow;
    };
}
}
