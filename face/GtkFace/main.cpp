#include "GtkFace/Settings/GnomeSettingManager.h"
#include "GtkFace/Utils/HelpFunctions.h"
#include "GtkFace/Windows/AppWindow.h"

#include "rpCore/MediaInit.h"
#include "rpLocalization/Localizer.h"
#include "rpObservers/ObserverIniter.h"

#include <exception>

int main(int argc, char **argv)
{
    rpCore::MediaInit initer;
    auto files = initer.init(argc, argv);

    rpObservers::ObserverIniter observersIniter;
    observersIniter.init();

    rpLocalization::Localizer loc(GtkFace::Utils::getDictionaryDirectory());
    GtkFace::Settings::GnomeSettingManager settings;

    int returnValue = 0;

    try
    {
        auto app = Gtk::Application::create(argc, argv, "gtk.interface.ratelplayer");
        GtkFace::Windows::AppWindow window(app, settings);
        window.appendFiles(files);
        returnValue = app->run(window);
    }
    catch(...)
    {
        // Settings should be dumped even exception was thrown and wasn't caught.
        settings.dump();
        std::rethrow_exception(std::current_exception());
    }

    settings.dump();
    return returnValue;
}
