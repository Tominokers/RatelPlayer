#SHELL=/bin/bash

include config/common.mk

ifneq (,$(findstring g++,$(CXX)))
include config/gcc.mk
endif
ifneq (,$(findstring clang,$(CXX)))
include config/clang.mk
endif

include config/core.mk
include config/settings.mk
include config/localization.mk
include config/observer.mk
include config/facade.mk
include config/test.mk
include config/gtkface.mk
include config/cppcheck.mk

-include $(DEPENDICIES)

all: core observer facade settings localization face

run: all
	bash scripts/run-application.sh $(OUTPUT_DIR)

run-app-check-leaks: all
	bash scripts/run-valgrind-application.sh $(OUTPUT_DIR)

run-tests: tests
	bash scripts/run-tests.sh $(OUTPUT_DIR) $(TEST_FILTER)

run-tests-check-leaks: tests
	bash scripts/run-valgrind-tests.sh $(OUTPUT_DIR) $(TEST_FILTER)

run-tests-with-ubsan: clean run-tests

run-tests-with-leaksan: clean run-tests

debug-tests: tests
	bash scripts/run-gdb-tests-session.sh $(OUTPUT_DIR)

debug-app: all
	bash scripts/run-gdb-app-session.sh $(OUTPUT_DIR)

prepare-perf: all tests

perf-pid:
	bash scripts/perf-pid.sh $(PERF_PID)

perf-report:
	bash scripts/perf-report.sh

update-version:
	bash scripts/update-gtk-version.sh $(VERSION)

face: $(PRECOMPILED_HEADER_GTK) $(GTK_EXECUTABLE) local-dictionaries

tests: $(PRECOMPILED_HEADER_TESTS) $(TEST_EXECUTABLE)

core: $(PRECOMPILED_HEADER_CORE) $(CORE_LIB)

observer: $(OBSERVER_LIB)

facade: $(PRECOMPILED_HEADER_FACADE) $(FACADE_LIB)

settings: $(SETTINGS_LIB)

localization: $(LOCALIZATION_LIB)

local-dictionaries: $(GTK_TEST_INSTALLED_DICTIONARIES)

$(GTK_EXECUTABLE): $(SETTINGS_LIB) $(LOCALIZATION_LIB) $(FACADE_LIB) $(GTK_OUTDIRS) $(GTK_OBJS)
	$(CXX) -o $@ $(EXECUTABLE_CFLAGS) $(GTK_OBJS) $(GTK_INCLUDE_LIB_DIRS) $(GTK_INCLUDE_LIBS)
	$(STRIP_TARGET) $@

$(TEST_EXECUTABLE): $(SETTINGS_LIB) $(LOCALIZATION_LIB) $(FACADE_LIB) $(TEST_OUTDIRS) $(TEST_OBJS)
	$(CXX) -o $@ $(EXECUTABLE_CFLAGS) $(TEST_OBJS) $(TEST_GMOCK_LIB) $(TEST_INCLUDE_LIB_DIRS) $(TEST_INCLUDE_LIBS)

$(CORE_LIB): $(CORE_OUTDIRS) $(CORE_OBJS)
	ar rcs $@ $(CORE_OBJS)

$(OBSERVER_LIB): $(CORE_LIB) $(OBSERVER_OUTDIRS) $(OBSERVER_OBJS)
	ar rcs $@ $(OBSERVER_OBJS)

$(FACADE_LIB): $(OBSERVER_LIB) $(FACADE_OUTDIRS) $(FACADE_OBJS)
	ar rcs $@ $(FACADE_OBJS)

$(SETTINGS_LIB): $(SETTINGS_OUTDIRS) $(SETTINGS_OBJS)
	ar rcs $@ $(SETTINGS_OBJS)

$(LOCALIZATION_LIB): $(LOCALIZATION_OUTDIRS) $(LOCALIZATION_OBJS)
	ar rcs $@ $(LOCALIZATION_OBJS)

$(CORE_TARGET_DIR)/%.o: $(CORE_PROJ_DIR)/%.cpp $(CORE_PROJ_DIR)/%.h
	$(CXX) $(CORE_INCLUDE_DIRS) $(FORECED_CORE_HEADER) $(CFLAGS) $< -o $@

$(OBSERVER_TARGET_DIR)/%.o: $(OBSERVER_PROJ_DIR)/%.cpp $(OBSERVER_PROJ_DIR)/%.h
	$(CXX) $(OBSERVER_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(FACADE_TARGET_DIR)/%.o: $(FACADE_PROJ_DIR)/%.cpp $(FACADE_PROJ_DIR)/%.h
	$(CXX) $(FACADE_INCLUDE_DIRS) $(FORECED_FACADE_HEADER) $(CFLAGS) $< -o $@

$(SETTINGS_TARGET_DIR)/%.o: $(SETTINGS_PROJ_DIR)/%.cpp $(SETTINGS_PROJ_DIR)/%.h
	$(CXX) $(SETTINGS_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(LOCALIZATION_TARGET_DIR)/%.o: $(LOCALIZATION_PROJ_DIR)/%.cpp $(LOCALIZATION_PROJ_DIR)/%.h
	$(CXX) $(LOCALIZATION_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(TEST_TARGET_DIR)/%.o: $(TEST_PROJ_DIR)/%.cpp $(TEST_PROJ_DIR)/%.h
	$(CXX) $(TEST_INCLUDE_DIRS) $(FORECED_TESTS_HEADER) $(CFLAGS) $(TESTS_CFLAGS) $< -o $@

$(TEST_TARGET_DIR)/main.o: $(TEST_PROJ_DIR)/main.cpp
	$(CXX) $(TEST_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(GTK_TARGET_DIR)/%.o: $(GTK_PROJ_DIR)/%.cpp $(GTK_PROJ_DIR)/%.h
	$(CXX) $(GTK_INCLUDE_DIRS) $(FORECED_GTK_HEADER) $(CFLAGS) $< -o $@

$(GTK_TARGET_DIR)/main.o: $(GTK_PROJ_DIR)/main.cpp
	$(CXX) $(GTK_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(GTK_TARGET_DIR)/gen/resource.o: $(GTK_GEN_DIR)/resource.c
	$(CC) $(GTK_INCLUDE_DIRS) $(CFLAGS_C) $< -o $@

$(PRECOMPILED_HEADER_CORE): $(CORE_HEADER_TO_PRECOMPILE)
	$(CXX) $(CORE_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(PRECOMPILED_HEADER_FACADE): $(FACADE_HEADER_TO_PRECOMPILE)
	$(CXX) $(FACADE_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(PRECOMPILED_HEADER_TESTS): $(TESTS_HEADER_TO_PRECOMPILE)
	$(CXX) $(TEST_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(PRECOMPILED_HEADER_GTK): $(GTK_HEADER_TO_PRECOMPILE)
	$(CXX) $(GTK_INCLUDE_DIRS) $(CFLAGS) $< -o $@

$(GTK_GEN_DIR)/resource.c: resources/resources.xml $(GTK_RESOURCES)
	glib-compile-resources --sourcedir=resources $< --generate-source --target=$@

$(DIRS_TO_CREATE):
	mkdir -p $@

$(GTK_TEST_INSTALLED_DICTIONARIES_DIR)/%.dic: $(DICTIONARIES_DIR)/%.dic $(GTK_TEST_INSTALLED_DICTIONARIES_DIR)
	cp $< $@

cppcheck:
	@echo
	cppcheck $(CPPCHECK_FLAGS) $(CPPCHECK_INCLUDES) $(CPPCHECK_IGNORE) $(CPPCHECK_SUPPRESS) *

install: all
	install -m 777 -D $(GTK_EXECUTABLE) $(GTK_INSTALLED_EXECUTABLE)
	install -m 644 -D $(GTK_DESKTOP_FILE) $(GTK_INSTALLED_DESKTOP_FILE)
	install -m 644 -D $(ICONS_DIR)/ratel-player-128.png $(GTK_INSTALLED_ICON)
	install -m 644 -D $(DICTIONARIES_RESOURCES) -t $(GTK_INSTALLED_DICTIONARIES)
ifeq ($(DESTDIR),)
	ldconfig
endif

uninstall:
	rm -f $(GTK_INSTALLED_EXECUTABLE)
	rm -f $(GTK_INSTALLED_DESKTOP_FILE)
	rm -f $(GTK_INSTALLED_ICON)
	rm -rf $(GTK_INSTALLATION_DIR)
	ldconfig

rpm:
	bash scripts/build-rpm.sh $(ARCH)

dpkg:
	bash scripts/build-dpkg.sh

doc: core observer facade
	doxygen config/Doxygen.config

clean-core:
	rm -rf $(CORE_TARGET_DIR)

clean-observer:
	rm -rf $(OBSERVER_TARGET_DIR)

clean-facade:
	rm -rf $(FACADE_TARGET_DIR)

clean-settings:
	rm -rf $(SETTINGS_TARGET_DIR)

clean-localization:
	rm -rf $(LOCALIZATION_TARGET_DIR)

clean-tests:
	rm -rf $(TEST_TARGET_DIR)

clean-gtk:
	rm -rf $(GTK_TARGET_DIR)

clean-precompiled:
	rm -rf $(PRECOMPILED_HEADER_CORE) $(PRECOMPILED_HEADER_FACADE) $(PRECOMPILED_HEADER_TESTS) $(PRECOMPILED_HEADER_GTK)

clean:  clean-precompiled clean-core clean-observer clean-facade clean-settings clean-localization clean-tests clean-gtk
