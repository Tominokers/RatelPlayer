#!/bin/bash

dh_make -p ratel-player_0.8 -n -s -e golmagrad@gmail.com -y
rm debian/README debian/README.Debian debian/README.source
cp -f config/debian-control-file debian/control

fakeroot debian/rules clean
debian/rules build CODEGEN=Release

fakeroot debian/rules binary

rm -rf debian
