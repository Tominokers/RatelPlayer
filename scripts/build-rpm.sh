#!/bin/bash

if [ -z $1 ]; then
    ARCH="x64"
else
    ARCH=$1
fi

if [ "$ARCH" == "x32" ]; then
    TARGET_ARCH="i686"
fi
if [ "$ARCH" == "x64" ]; then
   TARGET_ARCH="x86_64"
fi

RPMSOURCESDIR=~/rpmbuild/SOURCES
mkdir -p $RPMSOURCESDIR

git archive --prefix=RatelPlayer-0.8/ -o $RPMSOURCESDIR/RatelPlayer-0.8.tar.gz HEAD
rpmbuild --target $TARGET_ARCH -ba config/RatelPlayer_$ARCH.spec
