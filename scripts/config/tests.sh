#!/bin/bash

if [ -z $2 ]; then
    TEST_FILTER="*"
else
    TEST_FILTER=$2
fi

export G_SLICE=always-malloc
export G_DEBUG=gc-friendly
