#!/bin/bash

wget http://cgit.freedesktop.org/gstreamer/common/plain/gst.supp
wget http://cgit.freedesktop.org/gstreamer/gst-plugins-base/plain/tests/check/gst-plugins-base.supp
wget http://cgit.freedesktop.org/gstreamer/gst-plugins-good/plain/tests/check/gst-plugins-good.supp
wget http://cgit.freedesktop.org/gstreamer/gst-plugins-bad/plain/tests/check/gst-plugins-bad.supp
wget http://cgit.freedesktop.org/gstreamer/gst-plugins-ugly/plain/tests/check/gst-plugins-ugly.supp

mv gst.supp ../.gst.supp
mv gst-plugins-base.supp ../.gst-plugins-base.supp
mv gst-plugins-good.supp ../.gst-plugins-good.supp
mv gst-plugins-bad.supp ../.gst-plugins-bad.supp
mv gst-plugins-ugly.supp ../.gst-plugins-ugly.supp
