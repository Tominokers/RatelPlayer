#!/bin/bash

if [ -z $1 ]; then
   echo "use PERF_PID=<id> to specify process to analyze"
   exit 1
else
    PERF_PID=$1
fi

perf record --call-graph=dwarf --freq=10000 -p $PERF_PID
