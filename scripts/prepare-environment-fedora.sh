#!/bin/bash

sudo yum install -y gcc-c++
sudo yum install -y clang
sudo yum install -y gdb
sudo yum install -y compiler-rt
sudo yum install -y valgrind
sudo yum install -y rpm-build

sudo yum install -y gtkmm30-devel.x86_64
sudo yum install -y gstreamer1-devel.x86_64
sudo yum install -y gstreamer1-plugins-base-devel.x86_64
sudo yum install -y glib2-devel.x86_64

sudo yum install -y glibc-devel.i686
sudo yum install -y glibmm24-devel.i686
sudo yum install -y libsigc++20-devel.i686
sudo yum install -y freetype-devel.i686
sudo yum install -y gtkmm30-devel.i686
sudo yum install -y gstreamer1-devel.i686
sudo yum install -y gstreamer1-plugins-base-devel.i686
sudo yum install -y glib2-devel.i686
sudo yum install -y atkmm-devel.i686
sudo yum install -y pangomm-devel.i686
sudo yum install -y pango-devel.i686
sudo yum install -y cairo-devel.i686
sudo yum install -y cairomm-devel.i686
sudo yum install -y atk-devel.i686
sudo yum install -y gtk3-devel.i686
sudo yum install -y gdk-pixbuf2-devel.i686
sudo yum install -y pango-devel.i686
sudo yum install -y cairo-gobject-devel.i686

sudo yum install -y gtkmm30-doc
sudo yum install -y gstreamer1-devel-docs
sudo yum install -y glibmm24-doc

sudo yum install -y devhelp
sudo yum install -y emacs
sudo yum install -y emacs-auto-complete
sudo yum install -y doxygen
sudo yum install -y cppcheck

./prepare-environment-copy-tools-files.sh
./grab-submodules.sh
