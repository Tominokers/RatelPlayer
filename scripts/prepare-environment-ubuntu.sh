#!/bin/bash

sudo apt-get install gcc
sudo apt-get install clang
sudo apt-get install gdb
sudo apt-get install valgrind
sudo apt-get install libgtkmm-3.0-dev
sudo apt-get install libgtkmm-3.0-doc
sudo apt-get install libgstreamer1.0-dev
sudo apt-get install libgstreamer-plugins-base1.0-dev
sudo apt-get install libglibmm-2.4-dev
sudo apt-get install libglibmm-2.4-doc
sudo apt-get install libglib2.0-dev
sudo apt-get install devhelp
sudo apt-get install emacs
sudo apt-get install doxygen
sudo apt-get install cppcheck
sudo apt-get install dh-make

./prepare-environment-copy-tools-files.sh
./grab-submodules.sh
