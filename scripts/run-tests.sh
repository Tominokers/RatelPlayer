#!/bin/bash

source `pwd`/scripts/config/output_dir.sh
source `pwd`/scripts/config/tests.sh

cd tests

./$OUTPUT_DIR/RatelPlayerTests --gtest_filter=$TEST_FILTER

RETVAL=$?

cd ../

exit $RETVAL
