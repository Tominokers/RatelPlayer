#!/bin/bash

source `pwd`/scripts/config/output_dir.sh

cd face/GtkFace

valgrind --leak-check=full --show-reachable=yes --error-limit=no --suppressions=./../../.valgrind.supp ./$OUTPUT_DIR/RatelPlayer

cd ../../
