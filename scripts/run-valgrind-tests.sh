#!/bin/bash

source `pwd`/scripts/config/output_dir.sh
source `pwd`/scripts/config/tests.sh

cd tests

valgrind --error-exitcode=1 --leak-check=full --show-reachable=yes --error-limit=no --suppressions=./../.valgrind.supp --suppressions=./../.gst.supp --suppressions=./../.gst-plugins-base.supp --suppressions=./../.gst-plugins-good.supp --suppressions=./../.gst-plugins-bad.supp --suppressions=./../.gst-plugins-ugly.supp ./$OUTPUT_DIR/RatelPlayerTests --gtest_filter=$TEST_FILTER

RETVAL=$?

cd ../

exit $RETVAL
