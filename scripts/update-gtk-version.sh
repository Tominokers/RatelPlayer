#!/bin/bash

if [ -z $1 ]; then
    echo Ups, value of new version is not provided.
    exit 1
fi

NEW_VERSION=$1
CURRENT_VERSION=$(<config/version)

echo Changing version from $CURRENT_VERSION to $NEW_VERSION

sed -i "s/Version: $CURRENT_VERSION/Version: $NEW_VERSION/g" config/RatelPlayer_x32.spec
sed -i "s/Source: RatelPlayer-$CURRENT_VERSION.tar.gz/Source: RatelPlayer-$NEW_VERSION.tar.gz/g" config/RatelPlayer_x32.spec
sed -i "s/Version: $CURRENT_VERSION/Version: $NEW_VERSION/g" config/RatelPlayer_x64.spec
sed -i "s/Source: RatelPlayer-$CURRENT_VERSION.tar.gz/Source: RatelPlayer-$NEW_VERSION.tar.gz/g" config/RatelPlayer_x64.spec
sed -i "s/RatelPlayer-$CURRENT_VERSION/RatelPlayer-$NEW_VERSION/g" scripts/build-rpm.sh
sed -i "s/ratel-player_$CURRENT_VERSION/ratel-player_$NEW_VERSION/g" scripts/build-dpkg.sh
sed -i "s/GTK_APPLICATION_VERSION     \"$CURRENT_VERSION\"/GTK_APPLICATION_VERSION     \"$NEW_VERSION\"/g" face/GtkFace/ProgramInfo.h
sed -i "s/Version=$CURRENT_VERSION/Version=$NEW_VERSION/g" face/GtkFace/RatelPlayer.desktop
sed -i "s/player at version $CURRENT_VERSION/player at version $NEW_VERSION/g" README.md

#put new value of current version
sed -i "s/$CURRENT_VERSION/$NEW_VERSION/g" config/version

echo Version is successfully updated.
echo Use \"git diff\" to review changes or \"git commit -a\" to commit everything.
echo NOTE: Please do not forget to make new screenshots for new release!
