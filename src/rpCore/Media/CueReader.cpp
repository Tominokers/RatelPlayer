#include "CueReader.h"

#include "rpCore/Tools/Gst/GstCommonFunctions.h"
#include "rpCore/Tools/Gst/GstDateTimeUnref.h"

#include <array>
#include <cassert>
#include <chrono>
#include <cstring>
#include <fstream>

#include <glibmm.h>

namespace {
    const char tagRemark[]    = "REM";
    const char tagFile[]      = "FILE";
    const char tagIndex[]     = "INDEX";
    const char tagPerformer[] = "PERFORMER";
    const char tagTitle[]     = "TITLE";
    const char tagTrack[]     = "TRACK";

    const char remarkDate[]   = "DATE";

    const char indexStart[]   = "01";
    const char indexPreGap[]  = "00";

    constexpr auto tagRemarkSize    = (sizeof tagRemark) - 1u;
    constexpr auto tagFileSize      = (sizeof tagFile) - 1u;
    constexpr auto tagIndexSize     = (sizeof tagIndex) - 1u;
    constexpr auto tagPerformerSize = (sizeof tagPerformer) - 1u;
    constexpr auto tagTitleSize     = (sizeof tagTitle) - 1u;
    constexpr auto tagTrackSize     = (sizeof tagTrack) - 1u;

    constexpr auto remarkDateSize   = (sizeof remarkDate) - 1u;
    constexpr auto indexStartSize   = (sizeof indexStart) - 1u;
    constexpr auto indexPreGapSize  = (sizeof indexPreGap) - 1u;
}

namespace rpCore {
namespace Media {

    struct CommonData
    {
        std::string date;
        std::string performer;
        std::string album;
        std::string file;
    };

    CueReader::CueReader(const std::string &cueFileName)
        : m_cueFile(cueFileName)
    {
    }

    const std::string& CueReader::getReferencedFile() const {
        return m_referencedFile;
    }

    bool CueReader::read(CueTrackEntries &entries) {
        std::fstream fs(m_cueFile, std::ios::in);
        if (!fs.is_open())
            return false;

        bool isTrack = false;
        CommonData data;
        std::array<char, 512> buffer;
        while(fs.good())
        {
            fs.getline(buffer.data(), buffer.size());
            auto *line = g_strstrip(buffer.data());

            if (std::strncmp(line, tagRemark, tagRemarkSize) == 0)
            {
                handleRemark(line, data);
            }
            else if (std::strncmp(line, tagFile, tagFileSize) == 0)
            {
                if (!handleFile(line, data))
                    return false;
            }
            else if (std::strncmp(line, tagIndex, tagIndexSize) == 0)
            {
                handleTrackIndex(line, entries);
            }
            else if (std::strncmp(line, tagPerformer, tagPerformerSize) == 0)
            {
                if (!isTrack)
                    handleKeyValue(line, tagPerformer, tagPerformerSize, data.performer);
                else
                    handleTrackArtist(line, entries);
            }
            else if (std::strncmp(line, tagTitle, tagTitleSize) == 0)
            {
                if (!isTrack)
                    handleKeyValue(line, tagTitle, tagTitleSize, data.album);
                else
                    handleTrackTitle(line, entries);
            }
            else if (std::strncmp(line, tagTrack, tagTrackSize) == 0)
            {
                isTrack = true;
                handleTrack(line, data, entries);
            }
        }

        handleEndOfLastTrack(data, entries);
        handleMissedEndOfTracks(entries);
        return true;
    }

    void CueReader::handleKeyValue(char *line,
                                   const char* key,
                                   std::size_t keySize,
                                   std::string& value) const {
        assert(std::strncmp(line, key, keySize) == 0);

        auto *cueValue = &line[keySize];
        value = g_strchug(cueValue);

        if (!value.empty() && value.front() == '\"')
            value.erase(std::begin(value));

        if (!value.empty() && value.back() == '\"')
            value.pop_back();
    }

    void CueReader::handleRemark(char *line, CommonData &data) const {
        assert(std::strncmp(line, tagRemark, tagRemarkSize) == 0);

        auto *remark = &line[tagRemarkSize];
        auto *chompedRemark = g_strchug(remark);

        if (std::strncmp(chompedRemark, remarkDate, remarkDateSize) == 0)
        {
            auto *dateValue = &chompedRemark[remarkDateSize];
            data.date = g_strchug(dateValue);
        }
    }

    bool CueReader::handleFile(char *line, CommonData &data) {
        auto &file = data.file;
        handleKeyValue(line, tagFile, tagFileSize, file);

        assert(!file.empty() && file.front() != '\"');

        auto end = file.find('\"');
        assert(end != decltype(data.file)::npos);

        // File type is ignored.
        file.erase(end);

        const auto pathSeparator = '/';
        auto index = m_cueFile.rfind(pathSeparator);
        file = m_cueFile.substr(0, index) + pathSeparator + file;

        std::fstream stream(file, std::ios_base::in);
        if (!stream.is_open())
            return false;

        try
        {
            file = Glib::filename_to_uri(file);
        }
        catch (Glib::ConvertError&)
        {
            return false;
        }

        m_referencedFile = file;
        return true;
    }

    void CueReader::handleTrack(char *line, const CommonData &data, CueTrackEntries &entries) const {
        std::string trackNumber;
        handleKeyValue(line, tagTrack, tagTrackSize, trackNumber);

        // Ignore AUDIO part.
        auto end = trackNumber.find(' ');
        trackNumber.erase(end);

        // Remove leading zeroes.
        end = trackNumber.find_first_not_of('0');
        trackNumber.erase(0, end);

        entries.emplace_back();
        auto &entry = entries.back();
        entry.setTrackNumber(trackNumber);
        entry.setArtist(data.performer);
        entry.setAlbum(data.album);
        entry.setYear(data.date);
    }

    void CueReader::handleTrackTitle(char *line, CueTrackEntries &entries) const {
        std::string songTitle;
        handleKeyValue(line, tagTitle, tagTitleSize, songTitle);

        assert(!entries.empty());
        entries.back().setSongTitle(songTitle);
    }

    void CueReader::handleTrackIndex(char *line, CueTrackEntries &entries) const {
        std::string indexEntry;
        handleKeyValue(line, tagIndex, tagIndexSize, indexEntry);

        // indexEntry looks like "XX MM:SS:FF".
        // Extract MM:SS part of index, FF is ignored.
        auto timeText = indexEntry.substr(3, 5);
        auto minutes = std::stoul(timeText.substr(0,2));
        auto seconds = std::stoul(timeText.substr(3,2));

        std::chrono::nanoseconds timeStamp = std::chrono::minutes(minutes) + std::chrono::seconds(seconds);

        assert(!entries.empty());
        if (indexEntry.compare(0, indexPreGapSize, indexPreGap) == 0)
        {
            if (entries.size() > 1)
            {
                auto entry = std::prev(std::end(entries), 2);
                entry->setEnd(timeStamp);
                entry->setTrackDuration(timeStamp - entry->getStart());
            }
        }
        else if (indexEntry.compare(0, indexStartSize, indexStart) == 0)
        {
            entries.back().setStart(timeStamp);
        }
    }

    void CueReader::handleTrackArtist(char *line, CueTrackEntries &entries) const {
        std::string artistTitle;
        handleKeyValue(line, tagPerformer, tagPerformerSize, artistTitle);

        if (!artistTitle.empty())
        {
            assert(!entries.empty());
            entries.back().setArtist(artistTitle);
        }
    }

    void CueReader::handleEndOfLastTrack(const CommonData &data, CueTrackEntries &entries) const {
        if (entries.empty())
            return;

        auto &entry = entries.back();
        auto discovererInstances = Tools::Gst::createDiscoverer(data.file);
        if (   !std::get<Tools::GstDiscovererPtr>(discovererInstances)
            || !std::get<Tools::GstDiscovererInfoPtr>(discovererInstances))
        {
            entry.setEnd(entry.getStart());
            entry.setTrackDuration(std::chrono::nanoseconds(0));
            return;
        }

        auto *info = std::get<Tools::GstDiscovererInfoPtr>(discovererInstances).get();
        GstClockTime duration = gst_discoverer_info_get_duration(info);
        auto timeStamp = std::chrono::nanoseconds(duration);
        entry.setEnd(timeStamp);
        entry.setTrackDuration(timeStamp - entry.getStart());
    }

    void CueReader::handleMissedEndOfTracks(CueTrackEntries &entries) const {
        if (entries.size() < 2)
            return;

        // Because of the fact that pregap is not always specified, some tracks can miss their end offsets.
        // They are calculated by looking to start offest of the next track.
        auto end = std::prev(std::end(entries)); // Skip last entry, since it had to be handled in CueReader::handleEndOfLastTrack.
        for (auto it = std::begin(entries); it != end; ++it)
        {
            if (it->getEnd().count() == 0)
            {
                auto nextIt = std::next(it);
                it->setEnd(nextIt->getStart());
                it->setTrackDuration(it->getEnd() - it->getStart());
            }
        }
    }

}
}
