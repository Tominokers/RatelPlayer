#pragma once

#include "rpCore/Media/MediaInformation.h"

namespace rpCore {
namespace Media {

    struct CommonData;

    /**
     * Parser for CUE files.
     */
    class CueReader final
    {
    public:
        /// Colletion of media informations from CUE file.
        using CueTrackEntries = std::vector<MediaInformation>;

        /**
         * Create instance which will parse specified CUE file.
         * @param cueFileName the path to CUE file.
         * @remark cueFileName shouldn't be URI, it should be regular path on file system.
         */
        explicit CueReader(const std::string &cueFileName);

        /**
         * Gets URI to file to which CUE file references.
         * Can return a value after success call to read method.
         * @return URI to media file or empty string in case of failuries.
         */
        const std::string& getReferencedFile() const;

        /**
         * Parses CUE file and fills collection of media informations.
         * @param entries the collection of media informations which will be filled.
         * @return true in case of success, otherwise false.
         */
        bool read(CueTrackEntries &entries);

    private:
        void handleKeyValue(char *line,
                            const char* key,
                            std::size_t keySize,
                            std::string& value) const;

        void handleRemark(char *line, CommonData &data) const;
        bool handleFile(char *line, CommonData &data);
        void handleTrack(char *line, const CommonData &data, CueTrackEntries &entries) const;
        void handleTrackTitle(char *line, CueTrackEntries &entries) const;
        void handleTrackIndex(char *line, CueTrackEntries &entries) const;
        void handleTrackArtist(char *line, CueTrackEntries &entries) const;
        void handleEndOfLastTrack(const CommonData &data, CueTrackEntries &entries) const;
        void handleMissedEndOfTracks(CueTrackEntries &entries) const;

    private:
        std::string m_cueFile;
        std::string m_referencedFile;
    };

}
}
