#include "ApeFile.h"

namespace rpCore {
namespace Media {

    ApeFile::ApeFile(const std::string &fileUri)
        : MediaFile(fileUri)
        , m_fileType(MediaFileType::Regular)
    {
    }

    ApeFile::ApeFile(const std::string &fileUri, MediaInformation info)
        : MediaFile(fileUri, info)
        , m_fileType(MediaFileType::InventedByComposable)
    {
    }

    MediaFileType ApeFile::getType() const {
        return m_fileType;
    }

}
}
