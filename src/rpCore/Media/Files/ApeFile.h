#pragma once

#include "rpCore/Media/MediaFile.h"

namespace rpCore {
namespace Media {

    /**
     * Entity of ape media file.
     */
    class ApeFile final : public MediaFile
    {
    public:
        /**
         * Constructs empty ape file.
         */
        ApeFile() = default;

        /// Copy constructor is forbidden.
        ApeFile(const ApeFile&) = delete;

        /// Move semantic is not allowed.
        ApeFile(ApeFile&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        ApeFile& operator=(const ApeFile&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        ApeFile& operator=(ApeFile&&) = delete;

        /**
         * Constructs ape file by given uri.
         * @param fileUri uri to media file.
         */
        explicit ApeFile(const std::string &fileUri);

        /**
         * Constructs ape file with custom media information.
         * @param fileUri uri to media file.
         * @param info the custom media information.
         */
        explicit ApeFile(const std::string &fileUri, MediaInformation info);

        /**
         * Destructor of ape file.
         */
        virtual ~ApeFile() = default;

    private:
        virtual MediaFileType getType() const override;

    private:
        MediaFileType m_fileType;
    };

}
}
