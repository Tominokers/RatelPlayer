#include "rpCore/Media/Files/CueFile.h"

namespace rpCore {
namespace Media {

    CueFile::CueFile(const std::string &fileUri)
        : MediaFile(fileUri, MediaInformation())
    {
    }

    MediaFileType CueFile::getType() const {
        return MediaFileType::Composable;
    }

}
}
