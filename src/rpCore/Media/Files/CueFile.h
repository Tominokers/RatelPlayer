#pragma once

#include "rpCore/Media/MediaFile.h"

namespace rpCore {
namespace Media {

    /**
     * Entity of CUE file which has table of content for other media file.
     * During constuction it is not verified whether CUE is valid.
     * You need to pass URI of file to CueParser in order to get table of content.
     */
    class CueFile final : public MediaFile
    {
    public:
        /**
         * Constructs cue file by given uri.
         * @param fileUri uri to media file.
         */
        explicit CueFile(const std::string &fileUri);

        /// Copy constructor is forbidden.
        CueFile(const CueFile&) = delete;

        /// Move semantic is not allowed.
        CueFile(CueFile&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        CueFile& operator=(const CueFile&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        CueFile& operator=(CueFile&&) = delete;

        /// Destructor of cue file.
        virtual ~CueFile() = default;

    private:
        virtual MediaFileType getType() const override;

    };

}
}
