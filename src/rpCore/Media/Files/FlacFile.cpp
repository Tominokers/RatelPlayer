#include "FlacFile.h"

namespace rpCore {
namespace Media {

    FlacFile::FlacFile(const std::string &fileUri)
        : MediaFile(fileUri)
        , m_fileType(MediaFileType::Regular)
    {
    }

    FlacFile::FlacFile(const std::string &fileUri, MediaInformation info)
        : MediaFile(fileUri, std::move(info))
        , m_fileType(MediaFileType::InventedByComposable)
    {
    }

    MediaFileType FlacFile::getType() const {
        return m_fileType;
    }

}
}
