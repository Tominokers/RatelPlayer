#pragma once

#include "rpCore/Media/MediaFile.h"

namespace rpCore {
namespace Media {

    /**
     * Entity of flac media file.
     */
    class FlacFile final : public MediaFile
    {
    public:
        /**
         * Constructs empty flac file.
         */
        FlacFile() = default;

        /// Copy constructor is forbidden.
        FlacFile(const FlacFile&) = delete;

        /// Move semantic is not allowed.
        FlacFile(FlacFile&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        FlacFile& operator=(const FlacFile&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        FlacFile& operator=(FlacFile&&) = delete;

        /**
         * Constructs flac file by given uri.
         * @param fileUri uri to media file.
         */
        explicit FlacFile(const std::string &fileUri);

        /**
         * Constructs flac file with custom media information.
         * @param fileUri uri to media file.
         * @param info the custom media information.
         */
        explicit FlacFile(const std::string &fileUri, MediaInformation info);

        /**
         * Destructor of flac file.
         */
        virtual ~FlacFile() = default;

    private:
        virtual MediaFileType getType() const override;

    private:
        MediaFileType m_fileType;
    };

}
}
