#include "M4aFile.h"

namespace rpCore {
namespace Media {

    M4aFile::M4aFile() = default;

    M4aFile::M4aFile(const std::string &fileUri)
        : MediaFile(fileUri)
    {
    }

    M4aFile::~M4aFile() = default;

    MediaFileType M4aFile::getType() const {
        return MediaFileType::Regular;
    }

}
}
