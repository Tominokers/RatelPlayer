#pragma once

#include "rpCore/Media/MediaFile.h"

namespace rpCore {
namespace Media {

    /**
     * Entity of m4a media file.
     */
    class M4aFile final : public MediaFile
    {
    public:
        /**
         * Constructs empty m4a file.
         */
        M4aFile();

        /// Copy constructor is forbidden.
        M4aFile(const M4aFile&) = delete;

        /// Move semantic is not allowed.
        M4aFile(M4aFile&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        M4aFile& operator=(const M4aFile&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        M4aFile& operator=(M4aFile&&) = delete;

        /**
         * Constructs m4a file by given uri.
         * @param fileUri uri to media file.
         */
        explicit M4aFile(const std::string &fileUri);

        /**
         * Destructor of m4a file.
         */
        virtual ~M4aFile();

    private:
        virtual MediaFileType getType() const override;

    };

}
}
