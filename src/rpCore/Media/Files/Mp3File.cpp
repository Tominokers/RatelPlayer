#include "Mp3File.h"

namespace rpCore {
namespace Media {

    Mp3File::Mp3File(const std::string &fileUri)
        : MediaFile(fileUri)
    {
    }

    MediaFileType Mp3File::getType() const {
        return MediaFileType::Regular;
    }

}
}
