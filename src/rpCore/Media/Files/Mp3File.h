#pragma once

#include "rpCore/Media/MediaFile.h"

namespace rpCore {
namespace Media {

    /**
     * Entity of mp3 media file.
     */
    class Mp3File final : public MediaFile
    {
    public:
        /**
         * Constructs empty mp3 file.
         */
        Mp3File() = default;

        /// Copy constructor is forbidden.
        Mp3File(const Mp3File&) = delete;

        /// Move semantic is not allowed.
        Mp3File(Mp3File&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        Mp3File& operator=(const Mp3File&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        Mp3File& operator=(Mp3File&&) = delete;

        /**
         * Constructs mp3 file by given uri.
         * @param fileUri uri to media file.
         */
        explicit Mp3File(const std::string &fileUri);

        /**
         * Destructor of mp3 file.
         */
        virtual ~Mp3File() = default;

    private:
        virtual MediaFileType getType() const override;

    };

}
}
