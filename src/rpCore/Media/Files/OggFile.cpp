#include "OggFile.h"

namespace rpCore {
namespace Media {

    OggFile::OggFile() = default;

    OggFile::OggFile(const std::string &fileUri)
        : MediaFile(fileUri)
    {
    }

    OggFile::~OggFile() = default;

    MediaFileType OggFile::getType() const {
        return MediaFileType::Regular;
    }

}
}
