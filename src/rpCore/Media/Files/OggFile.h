#pragma once

#include "rpCore/Media/MediaFile.h"

namespace rpCore {
namespace Media {

    /**
     * Entity of ogg media file.
     */
    class OggFile final : public MediaFile
    {
    public:
        /**
         * Constructs empty ogg file.
         */
        OggFile();

        /// Copy constructor is forbidden.
        OggFile(const OggFile&) = delete;

        /// Move semantic is not allowed.
        OggFile(OggFile&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        OggFile& operator=(const OggFile&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        OggFile& operator=(OggFile&&) = delete;

        /**
         * Constructs ogg file by given uri.
         * @param fileUri uri to media file.
         */
        explicit OggFile(const std::string &fileUri);

        /**
         * Destructor of ogg file.
         */
        virtual ~OggFile();

    private:
        virtual MediaFileType getType() const override;

    };

}
}
