#include "WavFile.h"

namespace rpCore {
namespace Media {

    WavFile::WavFile(const std::string &fileUri)
        : MediaFile(fileUri)
    {
    }

    MediaFileType WavFile::getType() const {
        return MediaFileType::Regular;
    }

}
}
