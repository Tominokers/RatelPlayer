#pragma once

#include "rpCore/Media/MediaFile.h"

namespace rpCore {
namespace Media {

    /**
     * Entity of wav media file.
     */
    class WavFile final : public MediaFile
    {
    public:
        /**
         * Constructs empty wav file.
         */
        WavFile() = default;

        /// Copy constructor is forbidden.
        WavFile(const WavFile&) = delete;

        /// Move semantic is not allowed.
        WavFile(WavFile&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        WavFile& operator=(const WavFile&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        WavFile& operator=(WavFile&&) = delete;

        /**
         * Constructs wav file by given uri.
         * @param fileUri uri to media file.
         */
        explicit WavFile(const std::string &fileUri);

        /**
         * Destructor of wav file.
         */
        virtual ~WavFile() = default;

    private:
        virtual MediaFileType getType() const override;
    };

}
}
