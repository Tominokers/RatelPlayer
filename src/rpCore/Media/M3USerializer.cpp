#include "M3USerializer.h"

#include "rpCore/Media/MediaFactory.h"
#include "rpCore/Media/MediaFile.h"

#include <cassert>

namespace rpCore {
namespace Media {

    std::ostream& M3USerializer::serialize(const MediaList &list, std::ostream &stream) {
        for (auto &item : list)
        {
            if (item.get()->getType() == MediaFileType::InventedByComposable)
                item.get()->getMediaInfo().serialize(stream);

            assert(item.getUri() != nullptr);
            stream << *item.getUri() << '\n';
        }

        return stream;
    }

    MediaList M3USerializer::deserialize(std::istream &stream) {
        MediaList list;
        bool useCustomMediaInfo = false;
        std::array<char, 512> buffer;
        MediaInformation mediaInfo;

        while(stream.good())
        {
            stream.getline(buffer.data(), buffer.size());

            if (buffer.front() == '\0')
                continue;

            std::string line = buffer.data();
            if (buffer.front() == '#')
            {
                if (line == MediaInformation::getSerializationTag())
                    if (mediaInfo.deserialize(stream))
                        useCustomMediaInfo = true;

                continue;
            }

            MediaProxy proxy;
            if (useCustomMediaInfo)
            {
                proxy = MediaFactory::createCustomMedia(line, mediaInfo);
                useCustomMediaInfo = false;
            }
            else
            {
                proxy = MediaFactory::createMedia(line);
            }

            if (proxy)
                list.emplace_back(std::move(proxy));
        }

        return list;
    }

}
}
