#pragma once

#include "rpCore/Media/MediaProxy.h"

#include <iostream>

namespace rpCore {
namespace Media {

    /**
     * Entity which serialize playilst to M3U format
     * or deserialize playlist from stream with M3U file content.
     */
    class M3USerializer
    {
    public:
        /**
         * Serializes list of media items into stream.
         * Stream will contain media in format of M3U playlists.
         * @param list the container of media items to serialize.
         * @param stream the stream into which serialization will be done.
         * @return stream with serialized content.
         */
        static std::ostream& serialize(const MediaList &list, std::ostream &stream);

        /**
         * Deserializes list of media items out of stream.
         * Stream should containt media in format of M3U playlist format.
         * @param stream the stream with M3U content.
         * @return container with deserialized media items.
         */
        static MediaList deserialize(std::istream &stream);

    };

}
}
