#include "MediaFactory.h"

#include "rpCore/Media/CueReader.h"
#include "rpCore/Media/Files/ApeFile.h"
#include "rpCore/Media/Files/CueFile.h"
#include "rpCore/Media/Files/FlacFile.h"
#include "rpCore/Media/Files/M4aFile.h"
#include "rpCore/Media/Files/Mp3File.h"
#include "rpCore/Media/Files/OggFile.h"
#include "rpCore/Media/Files/WavFile.h"
#include "rpCore/Tools/Media/SupportedMedia.h"

#include <glibmm.h>

#include <cassert>

namespace rpCore {
namespace Media {

    MediaProxy MediaFactory::createMedia(const std::string &uri) {
        switch (rpCore::Tools::Media::getMediaType(uri))
        {
        case Tools::Media::MediaType::MP3:
            return MediaProxy(std::make_shared<Mp3File>(uri));
        case Tools::Media::MediaType::FLAC:
            return MediaProxy(std::make_shared<FlacFile>(uri));
        case Tools::Media::MediaType::WAV:
            return MediaProxy(std::make_shared<WavFile>(uri));
        case Tools::Media::MediaType::APE:
            return MediaProxy(std::make_shared<ApeFile>(uri));
        case Tools::Media::MediaType::M4A:
            return MediaProxy(std::make_shared<M4aFile>(uri));
        case Tools::Media::MediaType::OGG:
            return MediaProxy(std::make_shared<OggFile>(uri));
        case Tools::Media::MediaType::CUE:
            return MediaProxy(std::make_shared<CueFile>(uri));
        case Tools::Media::MediaType::NOT_SUPPORTED:
            return MediaProxy();
        }

        return MediaProxy();
    }

    MediaProxy MediaFactory::createCustomMedia(const std::string &uri,
                                               const MediaInformation &customInfo) {
        switch(Tools::Media::getMediaType(uri))
        {
        case Tools::Media::MediaType::FLAC:
            return MediaProxy(std::make_shared<FlacFile>(uri, customInfo));
        case Tools::Media::MediaType::APE:
            return MediaProxy(std::make_shared<ApeFile>(uri, customInfo));
        case Tools::Media::MediaType::MP3:
        case Tools::Media::MediaType::WAV:
        case Tools::Media::MediaType::M4A:
        case Tools::Media::MediaType::OGG:
        case Tools::Media::MediaType::CUE:
            return MediaFactory::createMedia(uri);
        case Tools::Media::MediaType::NOT_SUPPORTED:
            return MediaProxy();
        }

        return MediaProxy();
    }

    MediaList MediaFactory::createMediasFromComposable(const MediaProxy &composableMediaItem) {
        assert(composableMediaItem.get()->getType() == Media::MediaFileType::Composable);
        assert(composableMediaItem.getUri());
        // At the moment only CUE files are supported as composable items.
        assert(Tools::Media::getMediaType(*composableMediaItem.getUri()) == Tools::Media::MediaType::CUE);

        MediaList medias;
        std::string filePath;
        try
        {
            filePath = Glib::filename_from_uri(*composableMediaItem.getUri());
        }
        catch(Glib::ConvertError&)
        {
            return medias;
        }

        CueReader::CueTrackEntries entries;
        CueReader cueReader(filePath);
        if (!cueReader.read(entries))
            return medias;

        const auto &file = cueReader.getReferencedFile();
        auto fileType = rpCore::Tools::Media::getMediaType(file);
        if (fileType == Tools::Media::MediaType::FLAC)
        {
            for (auto &trackEntry : entries)
                medias.emplace_back(std::make_shared<FlacFile>(file, std::move(trackEntry)));
        }
        else if (fileType == Tools::Media::MediaType::APE)
        {
            for (auto &trackEntry : entries)
                medias.emplace_back(std::make_shared<ApeFile>(file, std::move(trackEntry)));
        }

        return medias;

    }

    bool MediaFactory::isSupported(const std::string &uri) {
        return Tools::Media::isSupportedMedia(uri);
    }

}
}
