#pragma once

#include "rpCore/Media/MediaProxy.h"

#include <string>

namespace rpCore {
namespace Media {

    class MediaInformation;

    /**
     * Entity which creates media items.
     */
    class MediaFactory
    {
    public:
        /**
         * Creates media by uri.
         * @param uri the URI is address to media.
         * @return proxy instance to media item.
         * If media doesn't exist by uri or is not valid, proxy will be empty.
         */
        static MediaProxy createMedia(const std::string &uri);

        /**
         * Creates media item with custom media information.
         * Only limited set of items support such creation.
         * Other will be redirected to trivial createMedia method.
         * @param uri the URI is address to media.
         * @param customInfo the custom media information.
         * @return proxy instance to media item.
         */
        static MediaProxy createCustomMedia(const std::string &uri,
                                            const MediaInformation &customInfo);

        /**
         * Creates collection of media items out of composable media document.
         * @param composableMediaItem the composable media document out of which generate media items.
         * @return the media collection from content of composable media item.
         */
        static MediaList createMediasFromComposable(const MediaProxy &composableMediaItem);

        /**
         * Checks whether media by uri is supported.
         * @param uri is address to media.
         * @return true if media is supported, otherwise false.
         */
        static bool isSupported(const std::string &uri);

    };

}
}
