#include "MediaFile.h"

#include "rpCore/Tools/Gst/GObjectFree.h"
#include "rpCore/Tools/Gst/GstCommonFunctions.h"
#include "rpCore/Tools/Gst/GstDateTimeUnref.h"
#include "rpCore/Tools/ConvertUtilityFunctions.h"

#include <gst/gst.h>

namespace rpCore {
namespace Media {

    MediaFile::MediaFile(const std::string& uri)
        : m_fileUri(uri)
        , m_fileName(Tools::convertUriToNameOfFile(uri))
    {
        this->initiateMediaInfo();
    }

    MediaFile::MediaFile(const std::string& uri, MediaInformation mediaInfo)
        : m_fileUri(uri)
        , m_fileName(Tools::convertUriToNameOfFile(uri))
        , m_information(std::move(mediaInfo))
    {
    }

    const std::string& MediaFile::getFileName() const noexcept {
        return m_fileName;
    }

    const std::string& MediaFile::getFileUri() const noexcept {
        return m_fileUri;
    }

    const std::string& MediaFile::getArtist() const noexcept {
        return m_information.getArtist();
    }

    const std::string& MediaFile::getAlbum() const noexcept {
        return m_information.getAlbum();
    }

    const std::string& MediaFile::getSongTitle() const noexcept {
        return m_information.getSongTitle();
    }

    const std::string& MediaFile::getYear() const noexcept {
        return m_information.getYear();
    }

    const std::string& MediaFile::getTrackNumber() const noexcept {
        return m_information.getTrackNumber();
    }

    const std::string& MediaFile::getTrackDuration() const noexcept {
        return m_information.getTrackDuration();
    }

    const std::chrono::nanoseconds& MediaFile::getDuration() const noexcept {
        return m_information.getDuration();
    }

    const std::chrono::nanoseconds MediaFile::getStart() const noexcept {
        return m_information.getStart();
    }

    const std::chrono::nanoseconds MediaFile::getEnd() const noexcept {
        return m_information.getEnd();
    }

    const MediaInformation& MediaFile::getMediaInfo() const noexcept {
        return m_information;
    }

    void MediaFile::initiateMediaInfo() noexcept {
        auto discovererInstances = Tools::Gst::createDiscoverer(m_fileUri);
        if (   !std::get<Tools::GstDiscovererPtr>(discovererInstances)
            || !std::get<Tools::GstDiscovererInfoPtr>(discovererInstances))
        {
            return;
        }

        auto *info = std::get<Tools::GstDiscovererInfoPtr>(discovererInstances).get();
        const GstTagList *tags = gst_discoverer_info_get_tags (info);
        if (!tags)
            return;

        Tools::GCharPtr str;
        if (gst_tag_list_get_string(tags, GST_TAG_ARTIST, &str.getRef()))
            m_information.setArtist(std::string(str.get()));

        str.reset(nullptr);
        if (gst_tag_list_get_string(tags, GST_TAG_ALBUM, &str.getRef()))
            m_information.setAlbum(std::string(str.get()));

        str.reset(nullptr);
        if (gst_tag_list_get_string(tags, GST_TAG_TITLE, &str.getRef()))
            m_information.setSongTitle(std::string(str.get()));

        Tools::GstDateTimePtr date;
        if (gst_tag_list_get_date_time(tags, GST_TAG_DATE_TIME, &date.getRef()))
        {
            auto year = gst_date_time_get_year(date.get());
            m_information.setYear(std::to_string(year));
        }

        guint number;
        if (gst_tag_list_get_uint(tags, GST_TAG_TRACK_NUMBER, &number))
            m_information.setTrackNumber(std::to_string(number));

        GstClockTime duration = gst_discoverer_info_get_duration(info);
        auto nanoseconds = std::chrono::nanoseconds(duration);
        m_information.setTrackDuration(nanoseconds);
    }

}
}
