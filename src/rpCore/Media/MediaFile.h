#pragma once

#include "rpCore/Media/MediaInformation.h"

namespace rpCore {
namespace Media {

    /**
     * Enumerates possible types of media files
     */
    enum class MediaFileType : unsigned char
    {
        /**
         * Regular media file, e.g.: mp3, wav and etc.
         */
        Regular,
        /**
         * File which contains information about other media files.
         * E.g.: CUE.
         */
        Composable,
        /**
         * Media file which has been created by infomration from composable file.
         * E.g.: CUE which is referenced to flac file,
         * can be used to create pack of flac media files with offsets.
         */
        InventedByComposable,
    };

    /**
     * Basic entity which represents media file on disk.
     */
    class MediaFile
    {
    public:
        /**
         * Descructs instance of MediaFile class.
         */
        virtual ~MediaFile() = default;

        /// Copy constructor is forbidden.
        MediaFile(const MediaFile&) = delete;

        /// Move semantic is not allowed.
        MediaFile(MediaFile&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        MediaFile& operator=(const MediaFile&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        MediaFile& operator=(MediaFile&&) = delete;

        /**
         * Gets type of media file.
         * @return specific entry from MediaFileType enumeration.
         */
        virtual MediaFileType getType() const = 0;

        /**
         * Gets file name of media file.
         * @return string of file name;
         */
        const std::string& getFileName() const noexcept;

        /**
         * Gets path to media file disk as uri.
         * @return uri as string.
         */
        const std::string& getFileUri() const noexcept;

        /**
         * Gets artist of media file from metainforamtion.
         * @return name of artist as string.
         */
        const std::string& getArtist() const noexcept;

        /**
         * Gets album from which is media file.
         * It is taken from metainformation of file.
         * @return title of album as string.
         */
        const std::string& getAlbum() const noexcept;

        /**
         * Gets title of song from metainformation of media file.
         * @return title of song as string.
         */
        const std::string& getSongTitle() const noexcept;

        /**
         * Gets year when media file was released.
         * @return year of release as string.
         */
        const std::string& getYear() const noexcept;

        /**
         * Gets number of track in album.
         * @return number of track as string.
         */
        const std::string& getTrackNumber() const noexcept;

        /**
         * Gets duration of track.
         * @return track duration as string.
         */
        const std::string& getTrackDuration() const noexcept;

        /**
         * Gets duration of track.
         * @return track duration in nanoseconds.
         */
        const std::chrono::nanoseconds& getDuration() const noexcept;

        /**
         * Gets start offset of media file.
         * @return 0 if there is no offset, otherwise some value in stream.
         */
        const std::chrono::nanoseconds getStart() const noexcept;

        /**
         * Gets end offset of media file.
         * @return 0 if there is no offset, otherwise some value in stream.
         */
        const std::chrono::nanoseconds getEnd() const noexcept;

        /**
         * Gets information about media file.
         * @return information about media file.
         */
        const MediaInformation& getMediaInfo() const noexcept;

    protected:
        /**
         * Constructs empty instance of MediaFile.
         */
        MediaFile() = default;

        /**
         * Constructs instance of MediaFile from passed URI.
         * @param uri the URI path to media file.
         */
        explicit MediaFile(const std::string &uri);

        /**
         * Constructs instance of MediaFile with custom media information.
         * @param uri the URI path to media file.
         * @param mediaInfo the custom media information for file.
         */
        explicit MediaFile(const std::string& uri, MediaInformation mediaInfo);

    protected:
        /// URI path to media file.
        std::string m_fileUri;

        /// Name of the media file.
        std::string m_fileName;

        /// Media information about media file.
        MediaInformation m_information;

    private:
        void initiateMediaInfo() noexcept;
    };

}
}
