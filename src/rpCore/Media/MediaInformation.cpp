#include "MediaInformation.h"

#include "rpCore/Tools/Media/TimeFunctions.h"

#include <sstream>

namespace rpCore {
namespace Media {

    MediaInformation::MediaInformation()
        : m_artist("-")
        , m_album("-")
        , m_songTitle("-")
        , m_year("-")
        , m_trackNumber("-")
        , m_trackDuration("-:-")
        , m_duration(0)
        , m_start(0)
        , m_end(0)
    {
    }

    MediaInformation::MediaInformation(const MediaInformation&) = default;

    MediaInformation::MediaInformation(MediaInformation&&) = default;

    MediaInformation::~MediaInformation() = default;

    const std::string& MediaInformation::getArtist() const noexcept {
        return m_artist;
    }

    const std::string& MediaInformation::getAlbum() const noexcept {
        return m_album;
    }

    const std::string& MediaInformation::getSongTitle() const noexcept {
        return m_songTitle;
    }

    const std::string& MediaInformation::getYear() const noexcept {
        return m_year;
    }

    const std::string& MediaInformation::getTrackNumber() const noexcept {
        return m_trackNumber;
    }

    const std::string& MediaInformation::getTrackDuration() const noexcept {
        return m_trackDuration;
    }

    const std::chrono::nanoseconds& MediaInformation::getDuration() const noexcept {
        return m_duration;
    }

    std::chrono::nanoseconds MediaInformation::getStart() const {
        return m_start;
    }

    std::chrono::nanoseconds MediaInformation::getEnd() const {
        return m_end;
    }

    void MediaInformation::setArtist(const std::string& artistName) noexcept {
        m_artist = artistName;
    }

    void MediaInformation::setAlbum(const std::string& albumTitle) noexcept {
        m_album = albumTitle;
    }

    void MediaInformation::setSongTitle(const std::string& song) noexcept {
        m_songTitle = song;
    }

    void MediaInformation::setYear(const std::string& yearDate) noexcept {
        m_year = yearDate;
    }

    void MediaInformation::setTrackNumber(const std::string& number) noexcept {
        m_trackNumber = number;
    }

    void MediaInformation::setTrackDuration(const std::chrono::nanoseconds &nanoseconds) noexcept {
        auto timeDuration = Tools::Media::parseTimeToTuple(nanoseconds);

        std::string timeString;
        if (std::get<0>(timeDuration) != "0")
            timeString = std::get<0>(timeDuration) + ":";
        timeString += std::get<1>(timeDuration) + ":";

        if (std::get<2>(timeDuration).size() < 2)
            timeString += "0";
        timeString += std::get<2>(timeDuration);

        m_trackDuration = timeString;
        m_duration = nanoseconds;
    }

    void MediaInformation::setStart(std::chrono::nanoseconds startMark) {
        m_start = startMark;
    }

    void MediaInformation::setEnd(std::chrono::nanoseconds endMark) {
        m_end = endMark;
    }

    void MediaInformation::serialize(std::ostream &stream) const {
        stream << MediaInformation::getSerializationTag() << '\n'
               << '#' << m_artist << '\n'
               << '#' << m_album << '\n'
               << '#' << m_songTitle << '\n'
               << '#' << m_year << ' '
                      << m_trackNumber << ' '
                      << m_trackDuration << ' '
                      << m_duration.count() << ' '
                      << m_start.count() << ' '
                      << m_end.count() << '\n';
    }

    bool MediaInformation::deserialize(std::istream &stream) {
        std::array<char, 512> buffer;

        auto readLineWithoutComment = [&stream, &buffer](std::string &text) -> bool
            {
                stream.getline(buffer.data(), buffer.size());
                if (!stream.good())
                    return false;

                text = buffer.data();
                if (text.empty() || text.front() != '#')
                    return false;

                text.erase(std::begin(text));
                return true;
            };

        std::string artistText;
        if (!readLineWithoutComment(artistText))
            return false;

        std::string albumText;
        if (!readLineWithoutComment(albumText))
            return false;

        std::string songText;
        if (!readLineWithoutComment(songText))
            return false;

        std::string text;
        if (!readLineWithoutComment(text))
            return false;

        auto readValueTillSeparator = [&text](std::string &value) -> bool
            {
                auto position = text.find(' ');
                if (position == std::string::npos)
                    return false;

                value = text.substr(0, position);
                text = text.substr(position + 1);
                return true;
            };

        std::string yearText;
        if (!readValueTillSeparator(yearText))
            return false;

        std::string trackNumberText;
        if (!readValueTillSeparator(trackNumberText))
            return false;

        std::string durationText;
        if (!readValueTillSeparator(durationText))
            return false;

        std::stringstream valueStream(text);
        decltype(m_duration)::rep durationValue;
        decltype(m_start)::rep startValue;
        decltype(m_end)::rep endValue;

        valueStream >> durationValue;
        if (!valueStream.good())
            return false;

        valueStream >> startValue;
        if (!valueStream.good())
            return false;

        valueStream >> endValue;
        if (valueStream.fail() || valueStream.bad())
            return false;

        m_artist = std::move(artistText);
        m_album = std::move(albumText);
        m_songTitle = std::move(songText);
        m_year = std::move(yearText);
        m_trackNumber = std::move(trackNumberText);
        m_trackDuration = std::move(durationText);
        m_duration = std::chrono::nanoseconds(durationValue);
        m_start = std::chrono::nanoseconds(startValue);
        m_end = std::chrono::nanoseconds(endValue);

        return true;
    }

    const char* MediaInformation::getSerializationTag() {
        return "#SerializedMediaInformation";
    }

}
}
