#pragma once

#include <chrono>
#include <string>
#include <vector>

namespace rpCore {
namespace Media {

    /**
     * Entity which consists meta information of media entity.
     */
    class MediaInformation final
    {
    public:
        /// Default constructor which leaves everything empty.
        MediaInformation();

        /// Copy semantic
        MediaInformation(const MediaInformation&);

        /// Move semantic
        MediaInformation(MediaInformation&&);

        ~MediaInformation();

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        MediaInformation& operator=(const MediaInformation&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        MediaInformation& operator=(MediaInformation&&) = delete;

        /**
         * Gets name of artist.
         * @return name of artist as string.
         */
        const std::string& getArtist() const noexcept;

        /**
         * Gets title of an album.
         * @return title of album as string.
         */
        const std::string& getAlbum() const noexcept;

        /**
         * Gets title of song.
         * @return title of song as string.
         */
        const std::string& getSongTitle() const noexcept;

        /**
         * Gets year when media was released.
         * @return year of release as string.
         */
        const std::string& getYear() const noexcept;

        /**
         * Gets number of track in album.
         * @return number of track as string.
         */
        const std::string& getTrackNumber() const noexcept;

        /**
         * Gets duration of track.
         * @return track duration as string.
         */
        const std::string& getTrackDuration() const noexcept;

        /**
         * Gets duration of track in nanoseconds.
         * @return track duration as nanoseconds.
         */
        const std::chrono::nanoseconds& getDuration() const noexcept;

        /**
         * Gets start offset of track in whole stream record.
         * @return time in seconds.
         */
        std::chrono::nanoseconds getStart() const;

        /**
         * Gets end offset of track in whole stream record.
         * @return time in seconds.
         */
        std::chrono::nanoseconds getEnd() const;

        /**
         * Sets name of artist.
         * @param artistName name of artist as string.
         */
        void setArtist(const std::string& artistName) noexcept;

        /**
         * Sets title of an album.
         * @param albumTitle title of album as string.
         */
        void setAlbum(const std::string& albumTitle) noexcept;

        /**
         * Sets title of song.
         * @param song title of song as string.
         */
        void setSongTitle(const std::string& song) noexcept;

        /**
         * Sets year when media was released.
         * @param yearDate year of release as string.
         */
        void setYear(const std::string& yearDate) noexcept;

        /**
         * Sets number of track in album.
         * @param number number of track as string.
         */
        void setTrackNumber(const std::string& number) noexcept;

        /**
         * Sets duration of track.
         * @param nanoseconds track length in nanoseconds.
         */
        void setTrackDuration(const std::chrono::nanoseconds &nanoseconds) noexcept;

        /**
         * Sets start offset of track in whole stream record.
         * @param startMark start time offset in seconds.
         */
        void setStart(std::chrono::nanoseconds startMark);

        /**
         * Sets end offset of track in whole stream record.
         * @param endMark end time offset in seconds.
         */
        void setEnd(std::chrono::nanoseconds endMark);

        /**
         * Serializes media information into stream.
         * @param stream the stream into which serialization will be performed.
         */
        void serialize(std::ostream &stream) const;

        /**
         * Deserializes media information from stream.
         * In case of successful deserialization
         * media information will be overwritten with new values,
         * otherwise media information stays the same.
         * @param stream the stream from which deserialization will be performed.
         * @return true in case of success, otherwise false.
         */
        bool deserialize(std::istream &stream);

        /**
         * Gets serialization tag which can be put at the begining of stream
         * in which serialization can be performed.
         * @return text representation of serialization tag.
         */
        static const char* getSerializationTag();

    private:
        std::string m_artist;

        std::string m_album;

        std::string m_songTitle;

        std::string m_year;

        std::string m_trackNumber;

        std::string m_trackDuration;

        std::chrono::nanoseconds m_duration;

        std::chrono::nanoseconds m_start;

        std::chrono::nanoseconds m_end;
    };

}
}
