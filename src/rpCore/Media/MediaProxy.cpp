#include "MediaProxy.h"

#include "rpCore/Media/MediaFile.h"

namespace rpCore {
namespace Media {

    MediaProxy::MediaProxy() = default;

    MediaProxy::MediaProxy(std::shared_ptr<MediaFile> &&mediaFile)
        : m_file(std::move(mediaFile))
    {
    }

    const MediaFile* MediaProxy::get() const noexcept {
        return m_file.get();
    }

    const std::string* MediaProxy::getUri() const noexcept {
        if (m_file)
            return &(m_file->getFileUri());

        return nullptr;
    }

    MediaProxy::operator bool() const {
        return this->get() != nullptr;
    }

}
}
