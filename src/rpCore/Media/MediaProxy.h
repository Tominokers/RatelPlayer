#pragma once

#include <memory>
#include <vector>

namespace rpCore {
namespace Media {

    class MediaFile;

    /**
     * Proxy of media item which is held in smart pointer.
     * Basically this is a wraper over std::shared_ptr<MediaFile>.
     */
    class MediaProxy final
    {
    public:
        /**
         * Create instance of proxy which return nullptr by getters.
         */
        MediaProxy();

        /**
         * Create isntance of proxy with passed std::shared_ptr.
         * Ownership of smart pointer is taken.
         * @param mediaFile the instance of media file to store.
         */
        explicit MediaProxy(std::shared_ptr<MediaFile> &&mediaFile);

        /**
         * Get pointer to held MediaFile.
         * @return pointer to MediaFile or nullptr.
         */
        const MediaFile* get() const noexcept;

        /**
         * Get URI of held MediaFile.
         * @return URI as pointer to string.
         * If MediaFile is not initialized then returned value is nullptr.
         */
        const std::string* getUri() const noexcept;

        /**
         * Checks whether MediaProxy is initialized with media item.
         * @return true if initialized, otherwise false.
         */
        explicit operator bool() const;

    private:
        std::shared_ptr<MediaFile> m_file;
    };

    using MediaList =  std::vector<MediaProxy>;

}
}
