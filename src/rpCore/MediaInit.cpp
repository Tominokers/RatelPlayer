#include "MediaInit.h"

#include "rpCore/Tools/Media/SupportedMedia.h"

#include <glibmm.h>
#include <gst/gst.h>
#include <gst/pbutils/pbutils.h>

#include <cassert>

namespace rpCore {

    MediaInit::MediaInit() noexcept
        : m_isInitialized(false)
    {
    }

    MediaInit::~MediaInit() noexcept {
        this->deinit();
    }

    void MediaInit::init() noexcept {
        if (!m_isInitialized)
        {
            gst_init(0,NULL);
            gst_pb_utils_init();
            Tools::Media::initSupportedMedia();
            m_isInitialized = true;
        }
    }

    std::vector<std::string> MediaInit::init(int& argc, char**& argv) noexcept {
        if (!m_isInitialized)
        {
            Tools::Media::initSupportedMedia();

            auto files = getFilesFromCommandArguments(argc, argv);
            argc -= files.size();
            assert(argc > 0);

            gst_init(&argc, &argv);
            gst_pb_utils_init();
            m_isInitialized = true;

            return files;
        }

        return {};
    }

    void MediaInit::deinit() noexcept {
        if (m_isInitialized)
        {
            gst_deinit();
            Tools::Media::deinitSupportedMedia();
            m_isInitialized = false;
        }
    }

    std::vector<std::string> MediaInit::getFilesFromCommandArguments(int argc, char** argv) const
    {
        std::vector<std::string> files;
        // It is assumed that files are in the end of argument list.
        // If we meet something what is not supported, then parsing is stopped.
        for (int i = argc-1; i > 0; --i)
        {
            auto possibleFile = std::string(Glib::filename_to_uri(argv[i]));
            if (Tools::Media::isSupportedMedia(possibleFile))
                files.emplace_back(std::move(possibleFile));
            else
                break;
        }

        return files;
    }

}
