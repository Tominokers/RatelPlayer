#pragma once

#include <string>
#include <vector>

namespace rpCore {

    /**
     * Entity which initialize and deinitialize
     * resources of media core library.
     */
    class MediaInit final
    {
    public:
        /**
         * Constructs instance of MediaInit class.
         */
        MediaInit() noexcept;

        /// Copy constructor is forbidden.
        MediaInit(const MediaInit&) = delete;

        /// Move semantic is not allowed.
        MediaInit(MediaInit&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        MediaInit& operator=(const MediaInit&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        MediaInit& operator=(MediaInit&&) = delete;

        /**
         * Destructs instance of MediaInit class.
         * If it is uninitialized, than it will be performed.
         */
        ~MediaInit() noexcept;

        /**
         * Initialize core with no arguments.
         */
        void init() noexcept;

        /**
         * Initialize core with passed arguments from command line.
         * Command line arguments in the end can contain list of files to open in player.
         * They will be parsed and returned as list of uri strings.
         * @param argc number of arguments.
         * @param argv arguments from command line.
         * @return container of strings which holds possible uri to files to open.
         */
        std::vector<std::string> init(int& argc, char**& argv) noexcept;

        /**
         * Deinitialize core.
         * Must be performed after finishing work with library.
         */
        void deinit() noexcept;

    private:
        std::vector<std::string> getFilesFromCommandArguments(int argc, char** argv) const;

    private:
        bool m_isInitialized;
    };
}
