#include "CommonPlayer.h"

#include "rpCore/Player/PlayerClient.h"
#include "rpCore/Player/PlayerFlowManager.h"

#include "rpCore/Media/MediaFile.h"
#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Tools/Gst/GErrorClear.h"

#include <glibmm/main.h>

#include <cassert>

namespace rpCore {
namespace Player {

    constexpr GstClockTime changeStateTimeOut = GST_CLOCK_TIME_NONE;
    constexpr GstFormat format = GST_FORMAT_TIME;
    constexpr GstSeekFlags seekFlag = GST_SEEK_FLAG_FLUSH;
    constexpr unsigned int timeoutInterval = 1000u;

    gboolean handleBusMessage(GstBus *bus, GstMessage *message, gpointer data) {
        static_cast<void>(bus);
        auto player = static_cast<CommonPlayer*>(data);

        if (message && player)
        {
            return player->handleGstMessage(*message);
        }

        return TRUE;
    }

    CommonPlayer::CommonPlayer() noexcept
        : m_playback("playbin", "playbin")
        , m_flowManager(std::make_unique<PlayerFlowManager>())
        , m_client(nullptr)
    {
        if (m_playback)
        {
            m_bus.reset(gst_element_get_bus(m_playback.get()));
            m_busHandlerId = gst_bus_add_watch(m_bus.get(), &handleBusMessage, this);
        }
        m_progressTimeout = Glib::signal_timeout().connect(sigc::mem_fun(*this,&CommonPlayer::requestPosition),
                                                           timeoutInterval);
    }

    CommonPlayer::~CommonPlayer() noexcept {
       m_progressTimeout.disconnect();
       if (m_bus)
       {
           g_source_remove(m_busHandlerId);
       }

       gst_element_set_state(m_playback.get(), GST_STATE_NULL);
    }

    void CommonPlayer::setPlaylist(const Playlist *playlist) noexcept {
        m_flowManager->setPlaylist(playlist);
    }

    void CommonPlayer::setClient(PlayerClient *playerClient) noexcept {
        if (m_client)
        {
            m_client->setListening(false);
        }

        m_client = playerClient;
        m_flowManager->setClient(m_client);
    }

    bool CommonPlayer::play(bool toggle) noexcept {
        if (m_flowManager->getCurrentIndex() == NoIndex)
            return !this->stop();

        const Media::MediaFile *fileToPlay = nullptr;
        bool isSuccess = false;

        GstState playState;
        gst_element_get_state(m_playback.get(), &playState, NULL, changeStateTimeOut);
        if (playState == GST_STATE_PLAYING)
        {
            isSuccess = !this->pause();
        }
        else if (playState == GST_STATE_PAUSED)
        {
            toggle = true;
        }
        else
        {
            const auto &track = m_flowManager->getCurrentTrack();
            if (track)
            {
                toggle = true;
                fileToPlay = track.get();
                assert(fileToPlay);

                g_object_set(m_playback.getObject(), "uri", fileToPlay->getFileUri().c_str(), NULL);
            }
            else
                toggle = false;
        }

        if (toggle)
        {
            gst_element_set_state(m_playback.get(), GST_STATE_PLAYING);
            GstStateChangeReturn state = gst_element_get_state(m_playback.get(), NULL, NULL, changeStateTimeOut);
            isSuccess = state == GstStateChangeReturn::GST_STATE_CHANGE_SUCCESS;
            if (isSuccess)
            {
                if (m_client)
                    m_client->setListening(true);

                if (fileToPlay && fileToPlay->getType() == Media::MediaFileType::InventedByComposable)
                {
                    gint64 seekPos = fileToPlay->getStart().count();
                    if (gst_element_seek_simple(m_playback.get(), format, seekFlag, seekPos))
                    {
                        notifyClientAboutNewPosition(seekPos);
                    }
                }

                if (m_client)
                    m_client->setTrackIndex(m_flowManager->getPlaylist(),
                                            m_flowManager->getCurrentIndex());
            }
            else if (m_client)
            {
                std::string message = "Couldn't play file ";
                message += fileToPlay->getFileUri();
                m_client->handleErrorNotification(message);
            }

        }

        return isSuccess;
    }

    bool CommonPlayer::pause() noexcept {
        if (m_client)
        {
            m_client->setListening(false);
        }

        gst_element_set_state(m_playback.get(), GST_STATE_PAUSED);
        GstStateChangeReturn state = gst_element_get_state(m_playback.get(), NULL, NULL, changeStateTimeOut);
        return state == GstStateChangeReturn::GST_STATE_CHANGE_SUCCESS ||
               state == GstStateChangeReturn::GST_STATE_CHANGE_NO_PREROLL;
    }

    bool CommonPlayer::stop() noexcept {
        if (m_client)
        {
            m_client->setListening(false);
        }

        gst_element_set_state(m_playback.get(), GST_STATE_READY);
        GstStateChangeReturn state = gst_element_get_state(m_playback.get(), NULL, NULL, changeStateTimeOut);
        return state == GstStateChangeReturn::GST_STATE_CHANGE_SUCCESS ||
               state == GstStateChangeReturn::GST_STATE_CHANGE_NO_PREROLL;
    }

    bool CommonPlayer::nextTrack() {
        this->stop();
        if (m_flowManager->next())
        {
            return this->play(true);
        }
        return false;
    }

    bool CommonPlayer::previousTrack() {
        this->stop();
        if (m_flowManager->previous())
        {
            return this->play(true);
        }
        return false;
    }

    void CommonPlayer::setPosition(double position) noexcept {
        gint64 duration;
        if (gst_element_query_duration(m_playback.get(), format, &duration))
        {
            if (position < 0)
                position = 0;
            if (position > 1)
                position = 1;
            auto seekPos = static_cast<gint64>(position * duration);

            const auto &track = m_flowManager->getCurrentTrack();
            if (track && track.get()->getType() == Media::MediaFileType::InventedByComposable)
            {
                auto nanosecondsPosition = std::chrono::nanoseconds(seekPos);
                if (track.get()->getEnd() <= nanosecondsPosition)
                {
                    this->nextTrack();
                    return;
                }
                else if (track.get()->getStart() > nanosecondsPosition)
                {
                    this->previousTrack();
                    return;
                }
            }

            if (gst_element_seek_simple(m_playback.get(), format, seekFlag, seekPos))
                this->notifyClientAboutNewPosition(seekPos);
        }
    }

    void CommonPlayer::setCurrentTrackIndex(TrackIndex track) {
        this->stop();
        m_flowManager->setCurrentIndex(track);
    }

    void CommonPlayer::setRepeatMode(RepeatMode mode) noexcept {
        m_flowManager->setRepeatMode(mode);
    }

    RepeatMode CommonPlayer::getRepeatMode() const noexcept {
        return m_flowManager->getRepeatMode();
    }

    void CommonPlayer::setShuffleMode(ShuffleMode mode) noexcept {
        m_flowManager->setShuffleMode(mode);
    }

    ShuffleMode CommonPlayer::getShuffleMode() const noexcept {
        return m_flowManager->getShuffleMode();
    }

    bool CommonPlayer::requestPosition() {
        gint64 position;
        if (!gst_element_query_position(m_playback.get(), format, &position))
            return true;

        const auto &track = m_flowManager->getCurrentTrack();
        if(track &&
           track.get()->getType() == Media::MediaFileType::InventedByComposable &&
           track.get()->getEnd() <= std::chrono::nanoseconds(position))
        {
            // Send message that track is finised.
            auto *eos = gst_message_new_eos(GST_OBJECT_CAST(m_playback.get()));
            gst_bus_post(m_bus.get(), eos);
        }

        if (m_client && m_client->isListening())
            this->notifyClientAboutNewPosition(position);

        return true;
    }

    gboolean CommonPlayer::handleGstMessage(GstMessage &message) {
        if (message.type == GST_MESSAGE_EOS)
            this->nextTrack();
        if (message.type == GST_MESSAGE_ERROR && m_client)
        {
            gst_element_set_state(m_playback.get(), GST_STATE_NULL);

            Tools::GErrorPtr error;
            gst_message_parse_error(&message, &error.getRef(), nullptr);

            std::string errorText = error.get()->message;
            m_client->handleErrorNotification(errorText);
        }

        return TRUE;
    }

    void CommonPlayer::notifyClientAboutNewPosition(gint64 position) {
        if (!m_client)
            return;

        gint64 duration;
        if (gst_element_query_duration(m_playback.get(), format, &duration))
        {
            PlayingProgress progress;
            progress.m_position = std::chrono::nanoseconds(position);
            progress.m_duration = std::chrono::nanoseconds(duration);
            m_client->setProgress(progress);
        }
    }

    Tools::Gst::GstMessagePtr CommonPlayer::waitForBusMessage(GstClockTime timeout,
                                                              GstMessageType types) noexcept {
        return Tools::Gst::GstMessagePtr(gst_bus_timed_pop_filtered(m_bus.get(), timeout, types));
    }

}
}
