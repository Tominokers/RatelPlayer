#pragma once

#include "rpCore/Player/PlayerTypes.h"
#include "rpCore/Tools/Gst/GstMessagePtr.h"
#include "rpCore/Tools/Gst/GstObjectUnref.h"

#include <sigc++/connection.h>

#include <memory>

namespace PlayerTests {
    class MediaPlayerTest;
}

namespace rpCore {
namespace Player {

    class PlayerFlowManager;
    class Playlist;
    class PlayerClient;

    enum class RepeatMode;
    enum class ShuffleMode;

    gboolean handleBusMessage(GstBus*, GstMessage*, gpointer);

    /**
     * Entity which provides playing media content from playlist.
     */
    class CommonPlayer final
    {
        /// Callback function which handles bus messages.
        friend gboolean handleBusMessage(GstBus*, GstMessage*, gpointer);
        /// Entity which tests this player.
        friend class PlayerTests::MediaPlayerTest;

    public:
        /**
         * Constructs instance of CommonPlayer class
         * and sets up it by default settings.
         */
        CommonPlayer() noexcept;

        /**
         * Destroys instance of CommonPlayer class
         * and stops playing of current media.
         */
        ~CommonPlayer() noexcept;

        /// Copy constructor is forbidden.
        CommonPlayer(const CommonPlayer&) = delete;

        /// Move semantic is not allowed.
        CommonPlayer(CommonPlayer&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        CommonPlayer& operator=(const CommonPlayer&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        CommonPlayer& operator=(CommonPlayer&&) = delete;

        /**
         * Sets playlist for playing its media items.
         * @param playlist pointer to playlist for playing.
         */
        void setPlaylist(const Playlist *playlist) noexcept;

        /**
         * Sets client of player for getting different callbacks.
         * @param playerClient the client to set.
         */
        void setClient(PlayerClient *playerClient) noexcept;

        /**
         * Plays current playlist from current track.
         * @param toggle if parameter is true,
         * it will force playing without possible pausing instead.
         * @return result of the state change:
         * true if state is changed, false otherwise.
         */
        bool play(bool toggle = false) noexcept;

        /**
         * Pauses playing of current playlist.
         * @return result of the state change:
         * true if state is changed, false otherwise.
         */
        bool pause() noexcept;

        /**
         * Stops playing of current playlist.
         * @return result of the state change:
         * true if state is changed, false otherwise.
         */
        bool stop() noexcept;

        /**
         * Switchs playing to next track of playlist.
         * @return result of the state change:
         * true if state is changed, false otherwise.
         */
        bool nextTrack();

        /**
         * Switchs playing to previous track of playlist.
         * @return result of the state change:
         * true if state is changed, false otherwise.
         */
        bool previousTrack();

        /**
         * Requests getting current position of current playing track.
         * Position will be returned via PlayerClient.
         * @return true if position is received, false otherwise.
         */
        bool requestPosition();

        /**
         * Sets current position of current playing track.
         * @param position the new position in stream from 0 to 1.
         * @remarks PlayerClient will recieve new position even if
         * isListening is set to false.
         */
        void setPosition(double position) noexcept;

        /**
         * Sets current track by its index in current played playlist.
         * @param track the index of track.
         */
        void setCurrentTrackIndex(TrackIndex track);

        /**
         * Sets type of repeating mode.
         * @param mode the mode of repeating to set.
         */
        void setRepeatMode(RepeatMode mode) noexcept;

        /**
         * Gets repeating mode of player.
         * @return the current repeat mode.
         */
        RepeatMode getRepeatMode() const noexcept;

        /**
         * Sets type of shuffle mode.
         * @param mode the mode of shuffling to set.
         */
        void setShuffleMode(ShuffleMode mode) noexcept;

        /**
         * Gets shuffle mode of player.
         * @return the current shuffle mode.
         */
        ShuffleMode getShuffleMode() const noexcept;

    private:
        gboolean handleGstMessage(GstMessage &message);

        void notifyClientAboutNewPosition(gint64 position);

        /**
         * Method for tests which waits for specific message from bus.
         * @param timeout timeout to wait on message.
         * @param types the type of message to wait.
         * @return Wrapper with requested message.
         */
        Tools::Gst::GstMessagePtr waitForBusMessage(GstClockTime timeout,
                                                    GstMessageType types) noexcept;

    private:
        Tools::GstPlaybackPtr m_playback;

        Tools::GstBusPtr m_bus;

        guint m_busHandlerId;

        std::unique_ptr<PlayerFlowManager> m_flowManager;

        PlayerClient *m_client;

        sigc::connection m_progressTimeout;
    };

}
}
