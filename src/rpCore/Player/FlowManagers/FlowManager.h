#pragma once

#include "rpCore/Player/PlayerTypes.h"

namespace rpCore {
namespace Player {

    class Playlist;

namespace FlowManagers {

    /**
     * Entity which decides which index from playlist
     * should be taken to play next or previous after current.
     */
    class FlowManager
    {
    public:
        virtual ~FlowManager() = default;

        /**
         * Decides which index from playlist should be taken next.
         * @param[in,out] index the current index as input value,
         * the next item which should be played as output.
         * @param playlist the playlist which is played.
         * @return true if it was decided which index will be next, false otherwise.
         */
        virtual bool next(TrackIndex &index, const Playlist *playlist) =0;

        /**
         * Decides which index from playlist should be taken as previous.
         * @param[in,out] index the current index as input value,
         * the previous item which should be played as output.
         * @param playlist the playlist which is played.
         * @return true if it was decided which index will be previous, false otherwise.
         */
        virtual bool previous(TrackIndex &index, const Playlist *playlist) =0;
    };

}
}
}
