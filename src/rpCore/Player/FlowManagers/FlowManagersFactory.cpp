#include "FlowManagersFactory.h"

#include "rpCore/Player/FlowManagers/NoRepeatNoShuffleFlowManager.h"
#include "rpCore/Player/FlowManagers/NoRepeatShuffleByTrackFlowManager.h"
#include "rpCore/Player/FlowManagers/PlaylistRepeatNoShuffleFlowManager.h"
#include "rpCore/Player/FlowManagers/PlaylistRepeatShuffleByTrack.h"
#include "rpCore/Player/FlowManagers/RepeatTrack.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    std::unique_ptr<FlowManager> FlowManagersFactory::create(RepeatMode repeatMode, ShuffleMode shuffleMode) {
        if (repeatMode == RepeatMode::None && shuffleMode == ShuffleMode::None)
        {
            return std::make_unique<NoRepeatNoShuffleFlowManager>();
        }
        else if (repeatMode == RepeatMode::WholePlaylist && shuffleMode == ShuffleMode::None)
        {
            return std::make_unique<PlaylistRepeatNoShuffleFlowManager>();
        }
        else if (repeatMode == RepeatMode::None && shuffleMode == ShuffleMode::ByTrack)
        {
            return std::make_unique<NoRepeatShuffleByTrackFlowManager>();
        }
        else if (repeatMode == RepeatMode::WholePlaylist && shuffleMode == ShuffleMode::ByTrack)
        {
            return std::make_unique<PlaylistRepeatShuffleByTrack>();
        }
        else if (repeatMode == RepeatMode::Track)
        {
            return std::make_unique<RepeatTrack>();
        }

        throw std::logic_error("Combination of modes is not supported.");
    }

}
}
}
