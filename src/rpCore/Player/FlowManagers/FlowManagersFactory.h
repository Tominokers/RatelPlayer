#pragma once

#include "rpCore/Player/PlayerModes.h"

#include <memory>

namespace rpCore {
namespace Player {
namespace FlowManagers {

    class FlowManager;

    /**
     * Entity which creates instance of FlowManager according to mode values.
     */
    class FlowManagersFactory
    {
    public:
        /**
         * Creates FlowManager instance.
         * @param repeatMode the repeat mode of requested FlowManager.
         * @param shuffleMode the shuffle mode of requested FlowManager.
         * @exception std::logical_error in case combination of mode values is not supported.
         * @return created FlowManager instance.
         */
        static std::unique_ptr<FlowManager> create(RepeatMode repeatMode, ShuffleMode shuffleMode);
    };

}
}
}
