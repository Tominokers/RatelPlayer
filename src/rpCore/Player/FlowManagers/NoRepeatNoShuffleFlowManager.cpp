#include "NoRepeatNoShuffleFlowManager.h"

#include "rpCore/Player/Playlist.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    bool NoRepeatNoShuffleFlowManager::next(TrackIndex &index, const Playlist *playlist) {
        bool result = true;

        ++index;

        if (index >= playlist->size())
        {
            index = NoIndex;
            result = false;
        }

        return result;
    }

    bool NoRepeatNoShuffleFlowManager::previous(TrackIndex &index, const Playlist *playlist) {
        static_cast<void>(playlist);

        bool result = true;

        if (index == FirstIndex)
        {
            index = NoIndex;
            result = false;
        }
        else
        {
            --index;
        }

        return result;
    }

}
}
}
