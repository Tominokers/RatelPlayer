#pragma once

#include "rpCore/Player/FlowManagers/FlowManager.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    /**
     * Entity which provides trivial flow of playing playlist.
     * It will be played once from the begining to the end.
     */
    class NoRepeatNoShuffleFlowManager : public FlowManager
    {
    private:
        virtual bool next(TrackIndex &index, const Playlist *playlist) override;
        virtual bool previous(TrackIndex &index, const Playlist *playlist) override;
    };

}
}
}
