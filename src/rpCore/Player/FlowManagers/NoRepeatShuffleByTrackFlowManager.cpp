#include "NoRepeatShuffleByTrackFlowManager.h"

#include "rpCore/Player/Playlist.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    bool NoRepeatShuffleByTrackFlowManager::next(TrackIndex &index, const Playlist *playlist) {
        bool result = true;

        if (shuffle(playlist))
            getCurrentTrack() = NoIndex;

        ++getCurrentTrack();
        if (getCurrentTrack() >= playlist->size())
        {
            index = NoIndex;
            getCurrentTrack() = NoIndex;
            result = false;
        }
        else
        {
            index = getTrack();
        }

        return result;
    }

    bool NoRepeatShuffleByTrackFlowManager::previous(TrackIndex &index, const Playlist *playlist) {
        bool result = true;

        if (shuffle(playlist))
            getCurrentTrack() = 1;

        if (getCurrentTrack() == FirstIndex)
        {
            index = NoIndex;
            getCurrentTrack() = NoIndex;
            result = false;
        }
        else
        {
            --getCurrentTrack();
            index = getTrack();
        }

        return result;
    }

}
}
}
