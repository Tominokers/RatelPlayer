#pragma once

#include "rpCore/Player/FlowManagers/ShuffleByTrackFlowManager.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    /**
     * Entity which provides random playing of playlist by tracks once.
     */
    class NoRepeatShuffleByTrackFlowManager  : public ShuffleByTrackFlowManager
    {
    private:
        virtual bool next(TrackIndex &index, const Playlist *playlist) override;
        virtual bool previous(TrackIndex &index, const Playlist *playlist) override;
    };

}
}
}
