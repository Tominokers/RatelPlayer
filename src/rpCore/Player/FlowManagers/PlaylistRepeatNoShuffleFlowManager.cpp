#include "PlaylistRepeatNoShuffleFlowManager.h"

#include "rpCore/Player/Playlist.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    bool PlaylistRepeatNoShuffleFlowManager::next(TrackIndex &index, const Playlist *playlist) {
        ++index;
        if (index >= playlist->size())
            index = FirstIndex;

        return true;
    }

    bool PlaylistRepeatNoShuffleFlowManager::previous(TrackIndex &index, const Playlist *playlist) {
        if (index == FirstIndex || index == NoIndex)
            index = playlist->size() - 1;
        else
            --index;

        return true;

    }

}
}
}
