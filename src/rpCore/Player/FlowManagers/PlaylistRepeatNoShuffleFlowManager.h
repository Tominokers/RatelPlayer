#pragma once

#include "rpCore/Player/FlowManagers/FlowManager.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    /**
     * Entity which repeats playlist from beginning to the end.
     */
    class PlaylistRepeatNoShuffleFlowManager : public FlowManager
    {
    public:
        virtual bool next(TrackIndex &index, const Playlist *playlist) override;
        virtual bool previous(TrackIndex &index, const Playlist *playlist) override;
    };

}
}
}
