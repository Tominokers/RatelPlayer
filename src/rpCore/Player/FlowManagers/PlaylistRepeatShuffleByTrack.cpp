#include "PlaylistRepeatShuffleByTrack.h"

#include "rpCore/Player/Playlist.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    bool PlaylistRepeatShuffleByTrack::next(TrackIndex &index, const Playlist *playlist) {
        if (shuffle(playlist))
            getCurrentTrack() = NoIndex;

        ++getCurrentTrack();
        if (getCurrentTrack() >= playlist->size())
        {
            getCurrentTrack() = NoIndex;
            shuffle(playlist);
            getCurrentTrack() = FirstIndex;
        }

        index = getTrack();

        return true;
    }

    bool PlaylistRepeatShuffleByTrack::previous(TrackIndex &index, const Playlist *playlist) {
        if (shuffle(playlist))
            getCurrentTrack() = FirstIndex + 1;

        if (getCurrentTrack() == FirstIndex)
        {
            getCurrentTrack() = NoIndex;
            shuffle(playlist);
            getCurrentTrack() = FirstIndex + 1;
        }
        else
        {
            --getCurrentTrack();
        }

        index = getTrack();

        return true;
    }

}
}
}
