#pragma once

#include "rpCore/Player/FlowManagers/ShuffleByTrackFlowManager.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    /**
     * Entity which repeats playlist with randomized order of tracks.
     */
    class PlaylistRepeatShuffleByTrack : public ShuffleByTrackFlowManager
    {
    private:
        virtual bool next(TrackIndex &index, const Playlist *playlist) override;
        virtual bool previous(TrackIndex &index, const Playlist *playlist) override;
    };

}
}
}
