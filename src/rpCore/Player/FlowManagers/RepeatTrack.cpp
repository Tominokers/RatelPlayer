#include "RepeatTrack.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    bool RepeatTrack::next(TrackIndex &index, const Playlist *playlist) {
        static_cast<void>(index); // Keep index the same.
        static_cast<void>(playlist);
        return true;
    }

    bool RepeatTrack::previous(TrackIndex &index, const Playlist *playlist) {
        static_cast<void>(index); // Keep index the same.
        static_cast<void>(playlist);
        return true;
    }

}
}
}
