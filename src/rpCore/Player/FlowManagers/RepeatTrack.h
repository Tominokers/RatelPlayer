#pragma once

#include "rpCore/Player/FlowManagers/FlowManager.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    /**
     * Entity which provides flow of playing playlist
     * by repeating one chosen track each time it ends.
     */
    class RepeatTrack : public FlowManager
    {
    private:
        virtual bool next(TrackIndex &index, const Playlist *playlist) override;
        virtual bool previous(TrackIndex &index, const Playlist *playlist) override;
    };

}
}
}
