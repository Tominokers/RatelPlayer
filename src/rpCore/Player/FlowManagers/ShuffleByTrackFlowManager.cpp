#include "ShuffleByTrackFlowManager.h"

#include "rpCore/Player/Playlist.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <random>

namespace rpCore {
namespace Player {
namespace FlowManagers {

    ShuffleByTrackFlowManager::ShuffleByTrackFlowManager()
        : m_currentTrack(NoIndex)
        , m_lastShuffledPlaylist(nullptr)
    {
    }

    ShuffleByTrackFlowManager::~ShuffleByTrackFlowManager() {
        if (m_lastShuffledPlaylist)
            m_lastShuffledPlaylist->delayUnregistration(this);
    }

    TrackIndex& ShuffleByTrackFlowManager::getCurrentTrack() {
        return m_currentTrack;
    }

    TrackIndex ShuffleByTrackFlowManager::getTrack() {
        assert(m_currentTrack != NoIndex);
        return m_trackOrder[m_currentTrack];
    }

    bool ShuffleByTrackFlowManager::shuffle(const Playlist *playlist) {
        bool result = false;

        if ((m_currentTrack == NoIndex) || (m_lastShuffledPlaylist != playlist))
        {
            this->reshuffle(playlist);

            if (m_lastShuffledPlaylist != playlist)
            {
                if (m_lastShuffledPlaylist)
                    m_lastShuffledPlaylist->delayUnregistration(this);

                m_lastShuffledPlaylist = playlist;
                m_lastShuffledPlaylist->registrateListener(this);
            }

            result = true;
        }

        return result;
    }

    void ShuffleByTrackFlowManager::reportRemovedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept {
        this->reshuffle(playlist);

        if (m_currentTrack == NoIndex)
            return;

        if ((m_currentTrack >= begin) && (m_currentTrack < end))
            m_currentTrack -= m_currentTrack - begin;
        else if(m_currentTrack > playlist->size())
            m_currentTrack -= end - begin;
    }

    void ShuffleByTrackFlowManager::reportAddedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept {
        static_cast<void>(begin);
        static_cast<void>(end);

        this->reshuffle(playlist);
    }

    void ShuffleByTrackFlowManager::reportPlaylistSorting(const Playlist *playlist) noexcept {
        static_cast<void>(playlist);
        // This case is not important.
    }

    void ShuffleByTrackFlowManager::reportPlaylistDestroying(Playlist *playlist) noexcept {
        assert(m_lastShuffledPlaylist != nullptr);
        assert(playlist == m_lastShuffledPlaylist);

        m_lastShuffledPlaylist = nullptr;
        playlist->unregistrateListener(this);
    }

    void ShuffleByTrackFlowManager::reshuffle(const Playlist *playlist) noexcept {
        m_trackOrder.resize(playlist->size());
        std::iota(std::begin(m_trackOrder), std::end(m_trackOrder), FirstIndex);

        std::random_device device;
        std::mt19937 g(device());
        std::shuffle(std::begin(m_trackOrder),
                     std::end(m_trackOrder),
                     g);
    }

}
}
}
