#pragma once

#include "rpCore/Player/FlowManagers/FlowManager.h"
#include "rpCore/Player/PlaylistChangesListener.h"

namespace rpCore {
namespace Player {
namespace FlowManagers {

    /**
     * Entity which provides shuffling of playlist by tracks.
     */
    class ShuffleByTrackFlowManager : public FlowManager
                                    , private PlaylistChangesListener
    {
    public:
        ///! Default constructor.
        ShuffleByTrackFlowManager();

        ~ShuffleByTrackFlowManager();

    protected:
        /**
         * Gets current internal track number.
         * @return current track number.
         */
        TrackIndex& getCurrentTrack();

        /**
         * Gets track index which is selected by random.
         * @return the track which should be used.
         */
        TrackIndex getTrack();

        /**
         * Shuffles input playlist by tracks.
         * @param playlist the playlist to shuffle.
         * @return true of playlist was shuffled, false otherwise.
         */
        bool shuffle(const Playlist *playlist);

    private:
        virtual void reportRemovedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept override;
        virtual void reportAddedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept override;
        virtual void reportPlaylistSorting(const Playlist *playlist) noexcept override;
        virtual void reportPlaylistDestroying(Playlist *playlist) noexcept override;

        void reshuffle(const Playlist *playlist) noexcept;

    private:
        TrackIndexList m_trackOrder;
        TrackIndex m_currentTrack;
        const Playlist *m_lastShuffledPlaylist;

    };

}
}
}
