#include "PlayerClient.h"

namespace rpCore {
namespace Player {

    std::ostream& operator<<(std::ostream& os, const PlayingProgress& progress) {
        return os << "[[position: " << progress.m_position.count() << " ns] "
                  << "[duration: " << progress.m_duration.count() << " ns]]";
    }

    PlayerClient::PlayerClient(bool isListening) noexcept
        : m_listening(isListening)
    {
    }

    bool PlayerClient::isListening() const noexcept {
        return m_listening;
    }

    void PlayerClient::setListening(bool isListening) noexcept {
        m_listening = isListening;
    }

}
}
