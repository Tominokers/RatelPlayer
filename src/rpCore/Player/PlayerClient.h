#pragma once

#include "rpCore/Player/PlayerTypes.h"

#include <chrono>
#include <ostream>

namespace rpCore {
namespace Player {

    class Playlist;

    /**
     * Simple structure which holds information
     * about position and duration of current track.
     */
    struct PlayingProgress
    {
        /**
         * Position in current track.
         */
        std::chrono::nanoseconds m_position;

        /**
         * Duration of current track.
         */
        std::chrono::nanoseconds m_duration;
    };

    std::ostream& operator<<(std::ostream& os, const PlayingProgress& progress);

    /**
     * Entity which is able to receive inforamtion
     * about current played track in the player.
     */
    class PlayerClient
    {
    public:
        /**
         * Constructs base of PlayerClient.
         * @param isListening flag which indicates that
         * client will receive information about progress.
         */
        explicit PlayerClient(bool isListening) noexcept;

        /**
         * Descructs instance of PlayerClient.
         */
        virtual ~PlayerClient() = default;

        /**
         * Sets current progress of played track.
         * @param progress the struct which holds information progress.
         */
        virtual void setProgress(const PlayingProgress &progress) =0;

        /**
         * Notifies client about current track index in playlist which is played.
         * @param playlist current played playlist.
         * @param index the index of current played track.
         */
        virtual void setTrackIndex(const Playlist *playlist, TrackIndex index) =0;

        /**
         * Notifies client about error in player.
         * @param errorMessage the text of error message.
         */
        virtual void handleErrorNotification(const std::string &errorMessage) =0;

        /**
         * Checks that player client can receive information about progress.
         * @return true if client is linstening to player, flase othewise.
         */
        bool isListening() const noexcept;

        /**
         * Sets state of listening to information about progress.
         * @param isListening flag which sets state of listening.
         */
        void setListening(bool isListening) noexcept;

    private:
        bool m_listening;
    };

}
}
