#include "PlayerFlowManager.h"

#include "rpCore/Player/FlowManagers/FlowManager.h"
#include "rpCore/Player/FlowManagers/FlowManagersFactory.h"
#include "rpCore/Player/PlayerClient.h"
#include "rpCore/Player/Playlist.h"

#include <cassert>
#include <stdexcept>

namespace rpCore {
namespace Player {

    using namespace FlowManagers;

    PlayerFlowManager::PlayerFlowManager(const Playlist *playlist,
                                         PlayerClient *client) noexcept
        : m_currentPlaylist(playlist)
        , m_playerClient(client)
        , m_currentMediaFile(nullptr)
        , m_currentIndex(this->getInitialIndex(playlist))
        , m_repeatMode(RepeatMode::None)
        , m_shuffleMode(ShuffleMode::None)
        , m_flowManager(FlowManagers::FlowManagersFactory::create(this->getRepeatMode(),
                                                                  this->getShuffleMode()))
    {
        if (m_currentPlaylist)
            m_currentPlaylist->registrateListener(this);
    }

    PlayerFlowManager::~PlayerFlowManager() {
        if (m_currentPlaylist)
            m_currentPlaylist->delayUnregistration(this);
    }

    void PlayerFlowManager::setClient(PlayerClient *client) {
        m_playerClient = client;
    }

    RepeatMode PlayerFlowManager::getRepeatMode() const noexcept {
        return m_repeatMode;
    }

    void PlayerFlowManager::setRepeatMode(RepeatMode mode) noexcept {
        m_repeatMode = mode;

        m_flowManager = FlowManagers::FlowManagersFactory::create(this->getRepeatMode(),
                                                                  this->getShuffleMode());
    }

    ShuffleMode PlayerFlowManager::getShuffleMode() const noexcept {
        return m_shuffleMode;
    }

    void PlayerFlowManager::setShuffleMode(ShuffleMode mode) noexcept {
        m_shuffleMode = mode;

        m_flowManager = FlowManagers::FlowManagersFactory::create(this->getRepeatMode(),
                                                                  this->getShuffleMode());
    }

    const Playlist* PlayerFlowManager::getPlaylist() const noexcept {
        return m_currentPlaylist;
    }

    void PlayerFlowManager::setPlaylist(const Playlist *playlist) noexcept {
        if (playlist == m_currentPlaylist)
            return;

        if (m_currentPlaylist)
            m_currentPlaylist->delayUnregistration(this);

        m_currentPlaylist = playlist;
        m_currentIndex = this->getInitialIndex(m_currentPlaylist);

        if (m_currentPlaylist)
            m_currentPlaylist->registrateListener(this);
    }

    bool PlayerFlowManager::next() {
        if (!this->isPlaylistValid())
            return false;

        return m_flowManager->next(m_currentIndex, m_currentPlaylist);
    }

    bool PlayerFlowManager::previous() {
        if (!this->isPlaylistValid())
            return false;

        return m_flowManager->previous(m_currentIndex, m_currentPlaylist);
    }

    const Media::MediaProxy& PlayerFlowManager::getCurrentTrack() const {
        try
        {
            return m_currentPlaylist->at(this->getCurrentIndex());
        }
        catch(const std::out_of_range&)
        {
        }

        return m_emptyMedia;
    }

    TrackIndex PlayerFlowManager::getCurrentIndex() const noexcept {
        return m_currentIndex;
    }

    void PlayerFlowManager::setCurrentIndex(TrackIndex index) noexcept {
        if (index >= m_currentPlaylist->size())
        {
            m_currentIndex = NoIndex;
            m_currentMediaFile = nullptr;
        }
        else
        {
            m_currentIndex = index;
            m_currentMediaFile = getCurrentTrack().get();
        }

        if (m_playerClient)
            m_playerClient->setTrackIndex(this->getPlaylist(), this->getCurrentIndex());
    }

    bool PlayerFlowManager::isPlaylistValid() noexcept {
        bool isValid = m_currentPlaylist != nullptr && m_currentPlaylist->size() != 0;

        if (!isValid)
            m_currentIndex = NoIndex;

        return isValid;
    }

    void PlayerFlowManager::reportRemovedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept {
        assert(playlist == m_currentPlaylist);

        if (m_currentIndex < begin || m_currentIndex == NoIndex)
            return; // Some tracks were removed after us, we don't care about them.

        if (begin <= m_currentIndex && m_currentIndex < end)
        {
            // Current track was removed.
            this->setCurrentIndex(NoIndex);
        }
        else if (end < m_currentIndex)
        {
            // Some tracks were removed before us, current index should be moved backward.
            auto index = m_currentIndex - (end - begin);
            this->setCurrentIndex(index);
        }
    }

    void PlayerFlowManager::reportAddedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept {
        assert(playlist == m_currentPlaylist);

        if (m_currentIndex < begin)
            return; // Some tracks were added after us, we don't care about them.

        if (m_currentIndex == NoIndex && begin == FirstIndex && playlist->size() == end)
        {
            // Special case: playlist was empty, but now first tracks were added.
            // Put current index to first tracks of playlist.
            this->setCurrentIndex(begin);
        }
        else if (m_currentIndex != NoIndex)
        {
            // Some tracks were added before us,  current index should be moved forward.
            auto index = m_currentIndex + (end - begin);
            this->setCurrentIndex(index);
        }
    }

    void PlayerFlowManager::reportPlaylistSorting(const Playlist *playlist) noexcept {
        assert(playlist == m_currentPlaylist);

        if (m_currentMediaFile == nullptr)
            return;

        for(TrackIndex i = 0; i < playlist->size(); ++i)
            if (playlist->at(i).get() == m_currentMediaFile)
            {
                this->setCurrentIndex(i);
                return;
            }

        assert(false && "MediaFile was disappeared from playlist? Not possible!");
    }

    void PlayerFlowManager::reportPlaylistDestroying(Playlist *playlist) noexcept {
        m_currentPlaylist = nullptr;
        playlist->unregistrateListener(this);
    }

    TrackIndex PlayerFlowManager::getInitialIndex(const Playlist *playlist) const noexcept {
        return playlist && playlist->size() > 0 ? FirstIndex : NoIndex;
    }

}
}
