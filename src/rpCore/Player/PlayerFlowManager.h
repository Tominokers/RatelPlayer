#pragma once

#include "rpCore/Player/PlayerModes.h"
#include "rpCore/Player/PlaylistChangesListener.h"

#include "rpCore/Media/MediaProxy.h"

#include <memory>

namespace rpCore {
namespace Media {
    class MediaFile;
}
}

namespace rpCore {
namespace Player {

namespace FlowManagers {
    class FlowManager;
}

    class Playlist;
    class PlayerClient;

    /**
     * Enitity which manages modes of player, how it will take tracks for playing.
     * In general it provides logic of repeating (track, album, playlist and etc),
     * and shuffling tracks order in playlist by some filter or rule.
     */
    class PlayerFlowManager final : public PlaylistChangesListener
    {
    public:
        /**
         * Constructs player flow manager with given playlist.
         * @param playlist the playlist to manage. Can be nullptr.
         * @param client the player client which should be notified about flow events.
         */
        explicit PlayerFlowManager(const Playlist *playlist = nullptr,
                                   PlayerClient *client = nullptr) noexcept;

        ~PlayerFlowManager();

        /// Copy constructor is forbidden.
        PlayerFlowManager(const PlayerFlowManager&) = delete;

        /// Move semantic is not allowed.
        PlayerFlowManager(PlayerFlowManager&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        PlayerFlowManager& operator=(const PlayerFlowManager&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        PlayerFlowManager& operator=(PlayerFlowManager&&) = delete;

        /**
         * Sets player client.
         * @param client the client instance to interact with.
         */
        void setClient(PlayerClient *client);

        /**
         * Gets repeating mode of manager.
         * @return the current repeat mode.
         */
        RepeatMode getRepeatMode() const noexcept;

        /**
         * Sets type of repeating mode.
         * @param mode the mode of repeating to set.
         */
        void setRepeatMode(RepeatMode mode) noexcept;

        /**
         * Gets shuffle mode of manager.
         * @return the current shuffle mode.
         */
        ShuffleMode getShuffleMode() const noexcept;

        /**
         * Sets type of shuffle mode.
         * @param mode the mode of shuffling to set.
         */
        void setShuffleMode(ShuffleMode mode) noexcept;

        /**
         * Gets playlist which is managed.
         * @return the current playlist.
         */
        const Playlist* getPlaylist() const noexcept;

        /**
         * Sets playlist to manage.
         * @param playlist the playlist to set.
         */
        void setPlaylist(const Playlist *playlist) noexcept;

        /**
         * Chooses next track from playlist.
         * @return true if track is chosen, false otherwise.
         */
        bool next();

        /**
         * Chooses previous track from playlist.
         * @return true if track is chosen, false otherwise.
         */
        bool previous();

        /**
         * Gets representation of current chosen track from playlist.
         * @return pointer to current chosen track;
         */
        const Media::MediaProxy& getCurrentTrack() const;

        /**
         * Gets index of current chosen track in playlist.
         * @return the index of playlist.
         */
        TrackIndex getCurrentIndex() const noexcept;

        /**
         * Sets current track in current playlist by index.
         * @param index the new index of played track in playlist.
         * @remark It doesn't check bounding of index from 0 to size of playlist.
         * Argument index can be rpCore::Player::NoIndex. It means that no track is played at the moment.
         */
        void setCurrentIndex(TrackIndex index) noexcept;

    private:
        // --- PlaylistChangesListener ---
        virtual void reportRemovedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept override;
        virtual void reportAddedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept override;
        virtual void reportPlaylistSorting(const Playlist *playlist) noexcept override;
        virtual void reportPlaylistDestroying(Playlist *playlist) noexcept override;

    private:
        bool isPlaylistValid() noexcept;

        bool performNoneNoneModeNext() noexcept;
        bool performNoneNoneModePrevious() noexcept;

        bool performWholePlaylistNoneModeNext() noexcept;
        bool performWholePlaylistNoneModePrevious() noexcept;

        TrackIndex getInitialIndex(const Playlist *playlist) const noexcept;

    private:
        const Playlist *m_currentPlaylist;

        PlayerClient *m_playerClient;

        const Media::MediaFile *m_currentMediaFile;

        TrackIndex m_currentIndex;

        RepeatMode m_repeatMode;

        ShuffleMode m_shuffleMode;

        std::unique_ptr<FlowManagers::FlowManager> m_flowManager;

        Media::MediaProxy m_emptyMedia;
    };

}
}
