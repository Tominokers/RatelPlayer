#pragma once

namespace rpCore {
namespace Player {

    /**
     * Enumeration which contains repeat modes for player.
     */
    enum class RepeatMode
    {
        /**
         * Playing will be stopped after last track from playlist.
         */
        None,
        /**
         * Playlist will be cycled.
         * The last track goes to the first track on call to next method.
         * The first track goes to the last track on call to previous method.
         */
        WholePlaylist,
        /**
         * One chosen track in playlist will be cycled.
         * It will ignore shuffle mode and next/previous method will play same track.
         */
        Track
    };

    /**
     * Enumeration which contains shuffle modes for player.
     */
    enum class ShuffleMode
    {
        /**
         * Tracks will be taken in such order as is in playlist, one by one.
         */
        None,
        /**
         * Shuffle playlist by track.
         * Random track will be taken one by one.
         */
        ByTrack,
    };

}
}
