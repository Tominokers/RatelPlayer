#pragma once

#include <cstddef>
#include <limits>
#include <vector>

namespace rpCore {
namespace Player {

    /**
     * Entity which represents type for using it during indexing in playlist.
     */
    using TrackIndex = std::size_t;

    /**
     * Enitity which represents type for using it as collection of playlist indexes.
     */
    using TrackIndexList = std::vector<TrackIndex>;

    /**
     * Constant which should be used to indicate that no index is selected in playlist.
     */
    constexpr TrackIndex NoIndex = std::numeric_limits<TrackIndex>::max();

    /**
     * Constant which should be used to indicate first index in playlist.
     */
    constexpr TrackIndex FirstIndex = 0;

}
}
