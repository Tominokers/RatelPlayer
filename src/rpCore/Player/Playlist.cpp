#include "Playlist.h"

#include "rpCore/Media/M3USerializer.h"
#include "rpCore/Media/MediaFactory.h"
#include "rpCore/Media/MediaFile.h"
#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Player/PlaylistChangesListener.h"
#include "rpCore/Player/PlaylistSortType.h"
#include "rpCore/Tools/ConvertUtilityFunctions.h"

#include <algorithm>
#include <cassert>

namespace rpCore {
namespace Player {

    Playlist::Playlist()
        : PlaylistListenerRegistrator(this)
    {
    }

    Playlist::~Playlist() = default;

    void Playlist::addTrack(const Media::MediaProxy &track) {
        auto size = m_tracks.size();

        if (!track || track.get()->getType() != Media::MediaFileType::Composable)
        {
            m_tracks.push_back(track);
        }
        else
        {
            auto list = Media::MediaFactory::createMediasFromComposable(track);
            m_tracks.insert(std::end(m_tracks),
                            std::make_move_iterator(std::begin(list)),
                            std::make_move_iterator(std::end(list)));
        }

        if (size != m_tracks.size())
            reportAdding(size, m_tracks.size());
    }

    void Playlist::insertTracks(TrackIndex index, const Media::MediaList &list) {
        decltype(m_tracks)::iterator pos;
        if (index < this->size())
        {
            pos = std::begin(m_tracks);
            std::advance(pos, index);
        }
        else
        {
            pos = std::end(m_tracks);
            index = this->size();
        }

        auto lastComposableIt = std::find_if(std::cbegin(list), std::cend(list),
                                             [](const auto &mediaProxy)
                                             {
                                                 return mediaProxy &&
                                                     mediaProxy.get()->getType() == Media::MediaFileType::Composable;
                                             });

        if (lastComposableIt == std::cend(list))
        {
            m_tracks.insert(pos, std::cbegin(list), std::cend(list));
            this->reportAdding(index, index + list.size());
        }
        else
        {
            insertComposableTracks(pos, index, list, lastComposableIt);
        }
    }

    void Playlist::removeTracks(const TrackIndexList &indexes) {
        if (indexes.empty())
            return;

        auto ranges = Tools::convertIndexesToRanges(indexes);
        for (auto it = std::crbegin(ranges); it != std::crend(ranges); ++it)
        {
            reportRemoving(it->first, it->second);

            using difference_type = decltype(m_tracks)::difference_type;

            assert(it->first < this->size());
            auto begin = std::next(std::cbegin(m_tracks),
                                   static_cast<difference_type>(it->first));
            auto end = (it->second < this->size()) ?
                std::next(std::cbegin(m_tracks), static_cast<difference_type>(it->second)) :
                std::cend(m_tracks);
            m_tracks.erase(begin, end);
        }
    }

    void Playlist::clearTracks() {
        reportRemoving(FirstIndex, m_tracks.size());
        m_tracks.clear();
    }

    const Media::MediaProxy& Playlist::at(TrackIndex index) const noexcept(false) {
        return (*this)[index];
    }

    size_t Playlist::size() const noexcept {
        return m_tracks.size();
    }

    void Playlist::sort(PlaylistSortType type) {
        switch(type)
        {
        case PlaylistSortType::Album:
            m_sorter.sortByAlbum(m_tracks);
            break;
        case PlaylistSortType::Artist:
            m_sorter.sortByArtist(m_tracks);
            break;
        case PlaylistSortType::TrackName:
            m_sorter.sortByTrackName(m_tracks);
            break;
        case PlaylistSortType::TrackNumber:
            m_sorter.sortByTrackNumber(m_tracks);
            break;
        case PlaylistSortType::Year:
            m_sorter.sortByYear(m_tracks);
            break;
        case PlaylistSortType::Duration:
            m_sorter.sortByDuration(m_tracks);
            break;
        case PlaylistSortType::NoSorting:
            break;
        }

        this->reportSorting();
    }

    void Playlist::serialize(SerializeType type, std::ostream &stream) const {
        switch(type)
        {
        case SerializeType::M3U:
            Media::M3USerializer::serialize(m_tracks, stream);
            break;
        }
    }

    void Playlist::deserialize(SerializeType type, std::istream &stream) {
        assert(m_tracks.empty());

        switch(type)
        {
        case SerializeType::M3U:
            this->insertTracks(FirstIndex, Media::M3USerializer::deserialize(stream));
            break;
        }
    }

    const Media::MediaProxy& Playlist::operator [](TrackIndex index) const noexcept(false) {
        if (index >= m_tracks.size())
        {
            std::string text = "Index ";
            text += std::to_string(index);
            text += " is out of playlist.";
            throw std::out_of_range(text);
        }

        return m_tracks[index];
    }

    void Playlist::insertComposableTracks(Media::MediaList::iterator pos,
                                          TrackIndex index,
                                          const Media::MediaList &list,
                                          Media::MediaList::const_iterator lastComposableIt) {
        Media::MediaList extendedList(std::cbegin(list), lastComposableIt);

        auto insertComposable = [&extendedList](const auto &mediaProxy)
            {
                auto composableTracks = Media::MediaFactory::createMediasFromComposable(mediaProxy);
                extendedList.insert(std::end(extendedList),
                                    std::make_move_iterator(std::begin(composableTracks)),
                                    std::make_move_iterator(std::end(composableTracks)));
            };

        auto isComposable = [](const auto &mediaProxy)
            {
                return mediaProxy &&
                    mediaProxy.get()->getType() == Media::MediaFileType::Composable;
            };

        insertComposable(*lastComposableIt);

        auto nextComposableIt = std::find_if(std::next(lastComposableIt), std::cend(list), isComposable);
        while (nextComposableIt != std::cend(list))
        {
            if (std::distance(lastComposableIt, nextComposableIt) > 1)
            {
                extendedList.insert(std::cend(extendedList),
                                    std::next(lastComposableIt),
                                    nextComposableIt);
            }
            insertComposable(*nextComposableIt);

            lastComposableIt = nextComposableIt;
            nextComposableIt = std::find_if(std::next(lastComposableIt), std::cend(list), isComposable);
        }

        if (std::distance(lastComposableIt, nextComposableIt) > 1)
            extendedList.insert(std::cend(extendedList),
                                std::next(lastComposableIt),
                                nextComposableIt);

        m_tracks.insert(pos, std::cbegin(extendedList), std::cend(extendedList));
        this->reportAdding(index, index + extendedList.size());
    }

}
}
