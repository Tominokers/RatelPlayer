#pragma once

#include "rpCore/Player/PlayerTypes.h"
#include "rpCore/Player/PlaylistListenerRegistrator.h"
#include "rpCore/Player/PlaylistSorter.h"

#include <memory>
#include <stdexcept>

namespace rpCore {
namespace Media {
    class MediaProxy;
    class M3USerializer;
}
}

namespace rpCore {
namespace Player {
    enum class PlaylistSortType;

    /**
     * Formats of possible serialization of playlist
     */
    enum class SerializeType
    {
        /// Use M3U format.
        M3U,
    };

    /**
     * Entity provides collecting media files to one container as pointers,
     * but it doesn't remove them from memory during destroying.
     * Playlist uses for managment of media files.
     */
    class Playlist final : public PlaylistListenerRegistrator
    {
    public:
        /// Default constructor.
        Playlist();

        /// Default descructor.
        ~Playlist();

        /**
         * Adds track to playlist collection.
         * @param track represents media file.
         */
        void addTrack(const Media::MediaProxy &track);

        /**
         * Inserts media items to playlist collection at specific position.
         * @param index the place to insert media items.
         * @param list the list of media items to insert.
         */
        void insertTracks(TrackIndex index, const Media::MediaList &list);

        /**
         * Removes tracks from playlist.
         * @param indexes the container of track indexes to remove.
         */
        void removeTracks(const TrackIndexList &indexes);

        /**
         * Clears all tracks from playlist.
         */
        void clearTracks();

        /**
         * Gets track at index position.
         * @param index represents index in playlist collection.
         * @return the media file.
         * @throws std::out_of_range
         */
        const Media::MediaProxy& at(TrackIndex index) const noexcept(false);

        /**
         * Gets size of track collection in playlist.
         * @returns size of playlist.
         */
        size_t size() const noexcept;

        /**
         * Sorts playlist by specific criteria.
         * @param type criteria of sorting.
         */
        void sort(PlaylistSortType type);

        /**
         * Serializes content of playlist to requested playlist type into stream.
         * @param type the format of serialization.
         * @param stream the stream into which serialization should be performed.
         */
        void serialize(SerializeType type, std::ostream &stream) const;

        /**
         * Deserializes playlist from stream.
         * Content of playlist will be overwritten
         * @param type the format of serialization in stream.
         * @param stream the stream on which deserialization should be performed.
         */
        void deserialize(SerializeType type, std::istream &stream);

    private:
        /**
         * Gets media file by index from playlist.
         * @param index the index of item in playlist.
         * @return the media file.
         * @throws std::out_of_range If index is out of playlist size.
         */
        const Media::MediaProxy& operator [](TrackIndex index) const noexcept(false);

        void insertComposableTracks(Media::MediaList::iterator pos,
                                    TrackIndex index,
                                    const Media::MediaList &list,
                                    Media::MediaList::const_iterator lastComposableIt);

    private:
        Media::MediaList m_tracks;
        PlaylistSorter m_sorter;
    };

}
}
