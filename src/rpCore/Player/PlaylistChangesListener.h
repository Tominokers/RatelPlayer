#pragma once

#include "rpCore/Player/PlayerTypes.h"

namespace rpCore {
namespace Player {

    class Playlist;

    /**
     * Entity which listens to changes which is performed in playlist.
     * It reports about removing, adding, sorting in playlist.
     * The adding and removing notify about range [begin, end) of indexes which were changed.
     * Also it provides callback when playlist is going to be destroyed.
     */
    class PlaylistChangesListener
    {
    public:
        PlaylistChangesListener() = default;

        /// Copy constructor.
        PlaylistChangesListener(const PlaylistChangesListener&) = default;
        /// Move constructor.
        PlaylistChangesListener(PlaylistChangesListener&&) = default;

        /**
         * Copy assign operator.
         * @return
         */
        PlaylistChangesListener& operator=(const PlaylistChangesListener&) = default;
        /**
         * Move assign operator.
         * @return
         */
        PlaylistChangesListener& operator=(PlaylistChangesListener&&) = default;

        /// Default destructor.
        virtual ~PlaylistChangesListener() =default;

        /**
         * Reports about removed media indexes in playlist.
         * @param playlist the playlist which was modified.
         * @param begin the first index in range of removed indexes [begin, end).
         * @param end the last index in range of removed indexes [begin, end).
         */
        virtual void reportRemovedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept =0;

        /**
         * Reports about added media indexes in playlist.
         * @param playlist the playlist which was modified.
         * @param begin the first index in range of added indexes [begin, end).
         * @param end the last index in range of added indexes [begin, end).
         */
        virtual void reportAddedMedia(const Playlist *playlist, TrackIndex begin, TrackIndex end) noexcept =0;

        /**
         * Reports about perforing sort operation on playlist.
         * @param playlist the playlist which was sorted.
         */
        virtual void reportPlaylistSorting(const Playlist *playlist) noexcept =0;

        /**
         * Reports about destoying of playlist.
         * Normally you want to remove your listener on that event.
         * Otherwise it will cause UB.
         * @param playlist the playlist which is going to destoyed.
         */
        virtual void reportPlaylistDestroying(Playlist *playlist) noexcept =0;

    };

}
}
