#include "PlaylistListenerRegistrator.h"

#include "rpCore/Player/PlaylistChangesListener.h"

#include <algorithm>
#include <cassert>

namespace rpCore {
namespace Player {

    PlaylistListenerRegistrator::PlaylistListenerRegistrator(Playlist *playlistInstance)
        : m_playlist(playlistInstance)
    {
    }

    PlaylistListenerRegistrator::~PlaylistListenerRegistrator() {
        this->reportDestroying();
    }

    void PlaylistListenerRegistrator::registrateListener(PlaylistChangesListener *listener) const {
        this->handleDelayedUnregistration();

        assert(listener != nullptr);
        assert(std::find(std::begin(m_listeners),
                         std::end(m_listeners),
                         listener)
               == std::end(m_listeners));

        m_listeners.push_back(listener);
    }

    void PlaylistListenerRegistrator::unregistrateListener(PlaylistChangesListener *listener) {
        assert(listener != nullptr);
        assert(!m_listeners.empty());

        auto it = std::find(std::cbegin(m_listeners),
                            std::cend(m_listeners),
                            listener);

        assert(it != std::cend(m_listeners));
        if (*it == m_listeners.back())
            m_listeners.pop_back();
        else
            m_listeners.erase(it);
    }

    void PlaylistListenerRegistrator::delayUnregistration(PlaylistChangesListener *listener) const {
        m_listenersToRemove.push_back(listener);
    }

    void PlaylistListenerRegistrator::reportAdding(TrackIndex begin, TrackIndex end) {
        this->handleDelayedUnregistration();

        for (auto &listener : m_listeners)
            listener->reportAddedMedia(m_playlist, begin, end);
    }

    void PlaylistListenerRegistrator::reportRemoving(TrackIndex begin, TrackIndex end) {
        this->handleDelayedUnregistration();

        for (auto &listener : m_listeners)
            listener->reportRemovedMedia(m_playlist, begin, end);
    }

    void PlaylistListenerRegistrator::reportSorting() {
        this->handleDelayedUnregistration();

        for (auto &listener : m_listeners)
            listener->reportPlaylistSorting(m_playlist);
    }

    void PlaylistListenerRegistrator::reportDestroying() {
        this->handleDelayedUnregistration();

        for (auto it = std::crbegin(m_listeners); it != std::crend(m_listeners); ++it)
            (*it)->reportPlaylistDestroying(m_playlist);

        assert(m_listeners.empty());
    }

    void PlaylistListenerRegistrator::handleDelayedUnregistration() const {
        for (auto &listener : m_listenersToRemove)
            m_listeners.erase(std::find(std::begin(m_listeners),
                                        std::end(m_listeners),
                                        listener));

        m_listenersToRemove.clear();
    }

}
}
