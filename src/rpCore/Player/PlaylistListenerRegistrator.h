#pragma once

#include "rpCore/Player/PlayerTypes.h"

namespace rpCore {
namespace Player {

    class Playlist;
    class PlaylistChangesListener;

    /**
     * Entity which provides registration and notifying logic of playlist changes listeners.
     * It should be inherited from playlist and playlist should use methods in protected section.
     */
    class PlaylistListenerRegistrator
    {
    public:
        /**
         * Registrates changes listener of playlist.
         * Method can be called on constant playlist.
         * @param listener the listener to registrate.
         */
        void registrateListener(PlaylistChangesListener *listener) const;

        /**
         * Unregistrates changes listener of playlist.
         * Method can't be called on constant playlist.
         * Use PlaylistListenerRegistrator::delayUnregistration for this purpose.
         * @param listener the listener to unregistrate.
         */
        void unregistrateListener(PlaylistChangesListener *listener);

        /**
         * Unregistrates chagnes listener of playlist with delay.
         * Safe operation to perform on constant playlist.
         * Listener will be unregistered before occurring of next reporting.
         * @param listener the listener to unregistrate with delay.
         */
        void delayUnregistration(PlaylistChangesListener *listener) const;

    protected:
        /**
         * Constructor with playlist on which registration will be performed.
         * @param playlistInstance the playlist for registration and reporting logic.
         */
        explicit PlaylistListenerRegistrator(Playlist *playlistInstance);

        /**
         * Default destructor which reports about destroying of playlist.
         */
        virtual ~PlaylistListenerRegistrator();

        /**
         * Reports about adding tracks to playlist.
         * @param begin the first track index in range [begin, end).
         * @param end the last track index in range [begin, end).
         */
        void reportAdding(TrackIndex begin, TrackIndex end);

        /**
         * Reports about removing tracks from playlist.
         * @param begin the frist track in range [begin, end).
         * @param end the last track in range [begin, end).
         */
        void reportRemoving(TrackIndex begin, TrackIndex end);

        /**
         * Reports about performing sorting in playlist.
         */
        void reportSorting();

    private:
        void reportDestroying();
        void handleDelayedUnregistration() const;

    private:
        mutable std::vector<PlaylistChangesListener*> m_listeners;
        mutable std::vector<PlaylistChangesListener*> m_listenersToRemove;
        Playlist *m_playlist;
    };

}
}
