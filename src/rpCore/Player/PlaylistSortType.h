#pragma once

namespace rpCore {
namespace Player {

    /**
     * Enum which describes which type of sorting
     * can be applied to playlist.
     */
    enum class PlaylistSortType
    {
        /**
         * Sorting will not be performed.
         */
        NoSorting,
        /**
         * Sorting of playlist by album title.
         * Track number will be also sorted as secondary key.
         */
        Album,
        /**
         * Sorting of playlist by artist name.
         * Album titles and their track numbers
         * will be also sorted as secondary keys.
         */
        Artist,
        /**
         * Sorting of playlist by name of tracks.
         */
        TrackName,
        /**
         * Sorting of playlist by track number in album.
         */
        TrackNumber,
        /**
         * Sorting of playlist by year of release.
         * Artist name, album titles and track numbers
         * will be also sorted as secondary kes.
         */
        Year,
        /**
         * Sorting of playlist by track duration.
         */
        Duration,
    };

}
}
