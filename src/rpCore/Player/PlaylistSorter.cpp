#include "PlaylistSorter.h"

#include "rpCore/Media/MediaFile.h"

#include <algorithm>
#include <functional>

namespace rpCore {
namespace Player {

    template <typename T>
    inline bool compare(const T &lhs, const T &rhs, bool isAscending) {
        return isAscending ? lhs < rhs : lhs > rhs;
    }

    template <typename T>
    inline bool lexicographical_compare(const T &lhs, const T &rhs, bool isAscending) {
        return std::lexicographical_compare(std::begin(lhs), std::end(lhs),
                                            std::begin(rhs), std::end(rhs),
                                            [&isAscending](const auto &l, const auto &r) -> bool
                                            {
                                                return compare(std::toupper(l),
                                                               std::toupper(r),
                                                               isAscending);
                                            });
    }

    inline void sort(const std::function<bool(const Media::MediaProxy&, const Media::MediaProxy&)> &comparator,
                     Media::MediaList &tracks,
                     bool &makeAscendingOrder) {
        auto comparingLambda = [&comparator](const auto &lhs, const auto &rhs)
                               { return comparator(lhs, rhs); };
        makeAscendingOrder = !std::is_sorted(std::begin(tracks),
                                             std::end(tracks),
                                             comparingLambda);

        std::sort(std::begin(tracks),
                  std::end(tracks),
                  comparingLambda);
    }

    inline bool compareTrackNumbers(const Media::MediaFile &lhs,
                                    const Media::MediaFile &rhs,
                                    bool isAscending) {
        const auto ltn = lhs.getTrackNumber();
        const auto rtn = rhs.getTrackNumber();

        if (ltn.size() == rtn.size())
            return compare(ltn, rtn, isAscending);

        return compare(ltn.size(), rtn.size(), isAscending);
    }

    inline bool compareAlbums(const Media::MediaFile &lhs,
                              const Media::MediaFile &rhs,
                              bool isAscending) {
        const auto &la = lhs.getAlbum();
        const auto &ra = rhs.getAlbum();

        if (la == ra)
            return compareTrackNumbers(lhs, rhs, true);

        return compare(la, ra, isAscending);
    }

    void PlaylistSorter::sortByAlbum(Media::MediaList &tracks) {
        bool makeAscendingOrder = true;
        auto comparator = [&makeAscendingOrder](const Media::MediaProxy &lhs,
                                                const Media::MediaProxy &rhs) -> bool
                          {
                              const auto &lfile = *lhs.get();
                              const auto &rfile = *rhs.get();

                              return compareAlbums(lfile,
                                                   rfile,
                                                   makeAscendingOrder);
                          };

        sort(comparator, tracks, makeAscendingOrder);
    }

    void PlaylistSorter::sortByTrackNumber(Media::MediaList &tracks) {
        bool makeAscendingOrder = true;
        auto comparator = [&makeAscendingOrder](const Media::MediaProxy &lhs,
                                                const Media::MediaProxy &rhs) -> bool
                          {
                              const auto &lfile = *lhs.get();
                              const auto &rfile = *rhs.get();

                              return compareTrackNumbers(lfile,
                                                         rfile,
                                                         makeAscendingOrder);
                          };

        sort(comparator, tracks, makeAscendingOrder);
    }

    void PlaylistSorter::sortByTrackName(Media::MediaList &tracks) {
        bool makeAscendingOrder = true;
        auto comparator = [&makeAscendingOrder](const Media::MediaProxy &lhs,
                                                const Media::MediaProxy &rhs) -> bool
                          {
                              const auto &lst = lhs.get()->getSongTitle();
                              const auto &rst = rhs.get()->getSongTitle();

                              return lexicographical_compare(lst, rst, makeAscendingOrder);
                          };

        sort(comparator, tracks, makeAscendingOrder);
    }

    void PlaylistSorter::sortByYear(Media::MediaList &tracks) {
        bool makeAscendingOrder = true;
        auto comparator = [&makeAscendingOrder](const Media::MediaProxy &lhs,
                                                const Media::MediaProxy &rhs) -> bool
                          {
                              const auto &lfile = *lhs.get();
                              const auto &rfile = *rhs.get();

                              const auto &ly = lfile.getYear();
                              const auto &ry = rfile.getYear();

                              if (ly == ry)
                                  return compareAlbums(lfile, rfile, true);

                              return compare(ly, ry, makeAscendingOrder);
                          };

        sort(comparator, tracks, makeAscendingOrder);
    }

    void PlaylistSorter::sortByArtist(Media::MediaList &tracks) {
        bool makeAscendingOrder = true;
        auto comparator = [&makeAscendingOrder] (const Media::MediaProxy &lhs,
                                                 const Media::MediaProxy &rhs) -> bool
                          {
                              const auto &lfile = *lhs.get();
                              const auto &rfile = *rhs.get();

                              const auto &lar = lfile.getArtist();
                              const auto &rar = rfile.getArtist();

                              if (lar == rar)
                                  return compareAlbums(lfile, rfile, true);

                              return compare(lar, rar, makeAscendingOrder);
                          };

        sort(comparator, tracks, makeAscendingOrder);
    }

    void PlaylistSorter::sortByDuration(Media::MediaList &tracks) {
        bool makeAscendingOrder = true;
        auto comparator = [&makeAscendingOrder] (const Media::MediaProxy &lhs,
                                                 const Media::MediaProxy &rhs) -> bool
                          {
                              const auto &lfile = *lhs.get();
                              const auto &rfile = *rhs.get();

                              const auto &ldr = lfile.getDuration();
                              const auto &rdr = rfile.getDuration();

                              return compare(ldr, rdr, makeAscendingOrder);
                          };

        sort(comparator, tracks, makeAscendingOrder);
    }

}
}
