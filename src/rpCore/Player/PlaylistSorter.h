#pragma once

#include "rpCore/Media/MediaProxy.h"

namespace rpCore {
namespace Player {

    /**
     * Entity which sorts tracks by different conditions
     * in ascending order if tracks are not sorted.
     * If tracks is already sorted in ascending,
     * descending sorting will be performed.
     */
    class PlaylistSorter
    {
    public:
        /**
         * Sorts tracks by album title.
         * Track number will be also sorted as secondary key.
         * @param tracks the collection of tracks to sort.
         */
        void sortByAlbum(Media::MediaList &tracks);

        /**
         * Sorts playlist by artist name.
         * Album titles and their track numbers
         * will be also sorted as secondary keys.
         * @param tracks the collection of tracks to sort.
         */
        void sortByArtist(Media::MediaList &tracks);

        /**
         * Sorts playlist by name of tracks.
         * @param tracks the collection of tracks to sort.
         */
        void sortByTrackName(Media::MediaList &tracks);

        /**
         * Sorts playlist by track number in album.
         * @param tracks the collection of tracks to sort.
         */
        void sortByTrackNumber(Media::MediaList &tracks);

        /**
         * Sorts playlist by year of release.
         * Artist name, album titles and track numbers
         * will be also sorted as secondary kes.
         * @param tracks the collection of tracks to sort.
         */
        void sortByYear(Media::MediaList &tracks);

        /**
         * Sorts playlist by track duration.
         * @param tracks the collection of tracks to sort.
         */
        void sortByDuration(Media::MediaList &tracks);

    };

}
}
