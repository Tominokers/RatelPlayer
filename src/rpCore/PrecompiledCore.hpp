#include "rpCore/Media/MediaFile.h"
#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Player/Playlist.h"

#include <algorithm>
#include <cassert>
#include <memory>
#include <unordered_map>
#include <vector>

#include <glibmm.h>
#include <gst/gst.h>
