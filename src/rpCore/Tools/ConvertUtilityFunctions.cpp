#include "ConvertUtilityFunctions.h"

#include <algorithm>
#include <cassert>
#include <sstream>

namespace {
    const static auto pathSeparator = '/';
}

namespace rpCore {
namespace Tools {

    VectorOfRanges convertIndexesToRanges(const Player::TrackIndexList &indexes) {
        if (indexes.empty())
            return {};

        if (indexes.size() == 1)
            return { std::make_pair(indexes.front(), indexes.front() + 1) };

        assert(std::is_sorted(std::cbegin(indexes), std::cend(indexes)));

        VectorOfRanges ranges;
        const auto barrier = Player::NoIndex;
        Range range = std::make_pair(indexes.front(), barrier);

        for (auto it = std::next(std::cbegin(indexes)); it != std::cend(indexes); ++it)
        {
            auto index = *it;

            bool isEndSet = (range.second != barrier);
            if ((!isEndSet && (index - range.first)  == 1) ||
                ( isEndSet && (index - range.second) == 1))
            {
                range.second = index;
            }
            else
            {
                range.second = (range.second == barrier) ? (range.first + 1) : (range.second + 1);
                ranges.push_back(range);
                range = std::make_pair(index, barrier);
            }
        }

        if (ranges.empty() || ranges.back() != range)
        {
            range.second = (range.second == barrier) ? (range.first + 1) : (range.second + 1);
            ranges.push_back(range);
        }

        return ranges;

    }

    std::size_t convertTextToInteger(const std::string &text) {
        std::size_t number = 0;
        std::stringstream(text) >> number;

        return number;
    }

    std::string convertUriToNameOfFile(const std::string &uri) {
        auto index = uri.rfind(pathSeparator);
        if (index == std::string::npos)
            return uri;

        return uri.substr(index + 1);
    }

}
}
