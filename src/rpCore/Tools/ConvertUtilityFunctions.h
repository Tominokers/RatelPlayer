#pragma once

#include "rpCore/Player/PlayerTypes.h"

#include <string>

namespace rpCore {
namespace Tools {

    using Range = std::pair<Player::TrackIndex, Player::TrackIndex>;
    using VectorOfRanges = std::vector<Range>;

    /**
     * Converts track index list to contain ranges of indexes.
     * It means that it will replace indexes which go one after another
     * with difference one with pair of first and last index.
     * Standalone indexes will be replaced with pair of itself and next index.
     * Last index in range is excluded.
     * E.g. input is { 0, 1, 2, 3, 6, 8, 9, 10, 11, 12, 13, 14, 15 }.
     * The output will be {{0, 4}, {6, 7}, {8, 16}}.
     * @param indexes the collection of indexes to compress.
     * @return compressed collection of ranges.
     */
    VectorOfRanges convertIndexesToRanges(const Player::TrackIndexList &indexes);

    /**
     * Converts text to unsigned integer.
     * @param text the text to convert.
     * @return converted number or 0 as default value in case of failures.
     */
    std::size_t convertTextToInteger(const std::string &text);

    /**
     * Converts URI string to name of file.
     * @param uri the string which represents URI.
     * @return file name of referenced URI.
     */
    std::string convertUriToNameOfFile(const std::string &uri);

}
}
