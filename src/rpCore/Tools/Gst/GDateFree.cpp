#include "GDateFree.h"

namespace rpCore {
namespace Tools {
namespace Gst {

    void GDateFree::unref(ObjectType &object) noexcept {
        g_date_free(object);
    }

}
}
}
