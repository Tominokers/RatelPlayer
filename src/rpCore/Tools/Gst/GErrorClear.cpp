#include "GErrorClear.h"

namespace rpCore {
namespace Tools {
namespace Gst {

    void GErrorClear::unref(ObjectType &object) noexcept {
        if (object)
        {
            g_print("%s\n",GST_STR_NULL(object->message));
        }

        g_clear_error(&object);
        object = nullptr;
    }

}
}
}
