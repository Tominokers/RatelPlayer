#pragma once

#include "rpCore/Tools/Gst/GstElementPtr.h"

#include <glib.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    /**
     * Entity which clears correctly passed error.
     */
    class GErrorClear
    {
    public:
        /**
         * The type of object which is handled.
         */
        typedef GError* ObjectType;

        /**
         * Perform actions on passed objects
         * what can cause its descruction.
         * @param object the instance to unref.
         */
        static void unref(ObjectType &object) noexcept;
    };
}

    using GErrorPtr = Gst::GstElementPtr<GError, Gst::GErrorClear>;

}
}
