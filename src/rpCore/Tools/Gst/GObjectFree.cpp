#include "GObjectFree.h"

#include <glib-object.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    void GObjectFree::unref(ObjectType &object) noexcept {
        g_free(object);
        object = nullptr;
    }

}
}
}
