#pragma once

#include "rpCore/Tools/Gst/GstElementPtr.h"

#include <glib.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    /**
     * Entity which clears correctly gpointer.
     */
    class GObjectFree
    {
    public:
        /**
         * The type of object which is handled.
         */
        typedef gpointer ObjectType;

        /**
         * Perform actions on passed objects
         * what can cause its descruction.
         * @param object the instance to unref.
         */
        static void unref(ObjectType &object) noexcept;
    };

}

    using GObjectPtr = Gst::GstElementPtr<gpointer, Gst::GObjectFree>;
    using GCharPtr = Gst::GstElementPtr<gchar, Gst::GObjectFree>;

}
}
