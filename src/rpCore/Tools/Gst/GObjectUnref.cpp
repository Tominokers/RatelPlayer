#include "GObjectUnref.h"

#include <glib-object.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    void GObjectUnref::unref(ObjectType &object) noexcept {
        g_object_unref(object);
    }

}
}
}
