#pragma once

#include "rpCore/Tools/Gst/GstElementPtr.h"

#include <gst/pbutils/pbutils.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    /**
     * Entity which unreferences correctly gpointer.
     */
    class GObjectUnref
    {
    public:
        /**
         * The type of object which is handled.
         */
        typedef gpointer ObjectType;

        /**
         * Perform actions on passed objects
         * what can cause its descruction.
         * @param object the instance to unref.
         */
        static void unref(ObjectType &object) noexcept;
    };

}

    using GstDiscovererPtr = Tools::Gst::GstElementPtr<GstDiscoverer, Tools::Gst::GObjectUnref>;

}
}
