#include "GstCommonFunctions.h"

namespace rpCore {
namespace Tools {
namespace Gst {

    GstDiscovererInstances createDiscoverer(const std::string &mediaFile) {
        GErrorPtr err;

        GstDiscovererPtr discoverer(gst_discoverer_new(GST_SECOND, &err.getRef()));
        if (!discoverer)
            return std::make_tuple(GstDiscovererPtr(), GstDiscovererInfoPtr(), std::move(err));

        err.reset(nullptr);
        GstDiscovererInfoPtr info(gst_discoverer_discover_uri(discoverer.get(),
                                                                     mediaFile.c_str(),
                                                                     &err.getRef()));
        if (!info)
            return std::make_tuple(GstDiscovererPtr(), GstDiscovererInfoPtr(), std::move(err));

        return std::make_tuple(std::move(discoverer), std::move(info), std::move(err));
    }

}
}
}
