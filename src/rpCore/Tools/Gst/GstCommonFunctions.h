#pragma once

#include "rpCore/Tools/Gst/GObjectUnref.h"
#include "rpCore/Tools/Gst/GErrorClear.h"
#include "rpCore/Tools/Gst/GstDiscovererInfoUnref.h"

#include <tuple>

namespace rpCore {
namespace Tools {
namespace Gst {

    using GstDiscovererInstances = std::tuple<GstDiscovererPtr, GstDiscovererInfoPtr, GErrorPtr>;

    GstDiscovererInstances createDiscoverer(const std::string &mediaFile);

}
}
}
