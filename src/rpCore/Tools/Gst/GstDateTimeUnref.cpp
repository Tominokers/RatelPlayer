#include "GstDateTimeUnref.h"

namespace rpCore {
namespace Tools {
namespace Gst {

    void GstDateTimeUnref::unref(ObjectType &object) noexcept {
        gst_date_time_unref(object);
    }

}
}
}
