#pragma once

#include "rpCore/Tools/Gst/GstElementPtr.h"

#include <gst/gst.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    /**
     * Entity which unrefs correctly passed date_time.
     */
    class GstDateTimeUnref
    {
    public:
        /**
         * The type of object which is handled.
         */
        typedef GstDateTime* ObjectType;

        /**
         * Perform actions on passed objects
         * what can cause its descruction.
         * @param object the instance to unref.
         */
        static void unref(ObjectType &object) noexcept;
    };

}

    using GstDateTimePtr = Gst::GstElementPtr<GstDateTime, Gst::GstDateTimeUnref>;

}
}
