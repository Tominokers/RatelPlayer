#include "GstDiscovererInfoUnref.h"

#include <gst/gst.h>

namespace rpCore {
namespace Tools {
namespace Gst {

        void GstDiscovererInfoUnref::unref(ObjectType &object) noexcept {
            gst_discoverer_info_unref(object);
        }

}
}
}
