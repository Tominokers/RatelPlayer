#pragma once

#include <glib-object.h>
#include <gst/gst.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    /**
     * Entity which represent unique container for GstElement,
     * implement correct freeing of element and useful access interface to it.
     */
    template<typename TElement, typename Deleter>
    class GstElementPtr
    {
    public:
        /**
         * Default ctor which sets container to nullptr.
         */
        GstElementPtr() noexcept
            : m_element(nullptr)
        {
        }

        /**
         * ctor which sets container to passed element.
         * @param gstElement Some gstreamer element.
         */
        explicit GstElementPtr(TElement* gstElement) noexcept
            : m_element(gstElement)
        {
        }

        /**
         * ctor which sets container to element created via factory.
         * @param factoryname Name of factory.
         * @param name Name of new element.
         */
        GstElementPtr(const gchar *factoryname, const gchar *name) noexcept {
            m_element = gst_element_factory_make(factoryname, name);
        }

        /**
         * dtor which makes correct cleaning of all resources.
         */
        ~GstElementPtr() noexcept {
            this->cleanUp();
        }

        /// Copy constructor is forbidden.
        GstElementPtr(const GstElementPtr<TElement, Deleter>&) = delete;

        /**
         * Move contstructor for GstElements is allowed.
         * @param instance GstElementPtr instance to move.
         */
        GstElementPtr(GstElementPtr<TElement, Deleter>&& instance) noexcept
            : m_element(instance.m_element)
        {
            instance.m_element = nullptr;
        }

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        GstElementPtr<TElement, Deleter>& operator =(const GstElementPtr<TElement, Deleter>&) = delete;

        /**
         * Move assign operator for GstElement is allowed.
         * @param instance
         * @return
         */
        GstElementPtr<TElement, Deleter>& operator =(GstElementPtr<TElement, Deleter>&& instance) noexcept {
            m_element = instance.m_element;
            instance.m_element = nullptr;
            return *this;
        }

        /**
         * Resets container to new gstreamer element.
         * @param gstElement new gstreamer element.
         */
        void reset(TElement* gstElement) noexcept {
            this->cleanUp();
            m_element = gstElement;
        }

        /**
         * Gets value of container.
         * @return contained gstreamer element.
         */
        TElement* get() const noexcept {
            return m_element;
        }

        /**
         * Gets reference to value of container.
         * @return reference to contained gstream element.
         */
        TElement*& getRef() noexcept {
            return m_element;
        }

        /**
         * Returns cast of contained GstElement to GObject.
         * @return contained GstElement which cast to GObject.
         */
        GObject* getObject() const noexcept {
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wold-style-cast"
#endif
            return G_OBJECT(m_element);
#ifdef __clang__
#pragma clang diagnostic pop
#endif
        }

        /**
         * Checks that container has value.
         */
        operator bool() const noexcept {
            return m_element != nullptr;
        }

    private:
        /**
         * Cleans internal resources.
         */
        void cleanUp() noexcept {
            if (m_element)
            {
                typedef typename Deleter::ObjectType CastType;
                CastType object = static_cast<CastType>(m_element);
                Deleter::unref(object);
            }
        }

    private:
        TElement* m_element;
    };

}
}
}
