#include "GstMessagePtr.h"

namespace rpCore {
namespace Tools {
namespace Gst {

    GstMessagePtr::GstMessagePtr(GstMessage* gstMessage) noexcept
        : m_message(nullptr)
        , m_counter(nullptr)
    {
        this->init(gstMessage);
    }

    GstMessagePtr::GstMessagePtr(const GstMessagePtr& gstMessage) noexcept
        : m_message(gstMessage.m_message)
        , m_counter(gstMessage.m_counter)
    {
        ++(*m_counter);
    }

    GstMessagePtr::~GstMessagePtr() noexcept {
        this->release();
    }

    void GstMessagePtr::reset(GstMessage* gstMessage) noexcept {
        this->release();
        this->init(gstMessage);
    }

    GstMessage* GstMessagePtr::get() const noexcept {
        return m_message;
    }

    inline void GstMessagePtr::init(GstMessage* gstMessage) noexcept {
        m_message = gstMessage;
        m_counter = new int(1);
    }

    inline void GstMessagePtr::release() noexcept {
        --(*m_counter);

        if ( *m_counter == 0 )
        {
            if ( m_message )
            {
                gst_message_unref(m_message);
            }

            delete m_counter;
        }
    }

}
}
}
