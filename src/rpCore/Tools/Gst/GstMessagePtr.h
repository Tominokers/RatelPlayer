#pragma once

#include <gst/gst.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    /**
     * Smart pointer for correct managing GstMessage objects.
     */
    class GstMessagePtr
    {
    public:
        /**
         * ctor which initiates container with passed gstream message.
         * @param gstMessage gstream message for initialing.
         */
        explicit GstMessagePtr(GstMessage* gstMessage) noexcept;

        /**
         * Copy ctor.
         * @param gstMessage object for copying.
         */
        GstMessagePtr(const GstMessagePtr &gstMessage) noexcept;

        /**
         * dtor. Decrements counter and releases resource if counter is zero.
         */
        ~GstMessagePtr() noexcept;

        /**
         * Resets container to new gstreamer element.
         * @param gstMessage element new gstreamer element.
         */
        void reset(GstMessage* gstMessage) noexcept;

        /**
         * Returns value of container.
         * @return contained gstreamer element.
         */
        GstMessage* get() const noexcept;

    private:
        /**
         * Operator = is forbidden.
         */
        void operator =(const GstMessagePtr&);

        /**
         * Initiates resource with message and create counter.
         * @param gstMessage object to be set in container.
         */
        inline void init(GstMessage* gstMessage) noexcept;

        /**
         * Decrements counter and releases resource if counter is zero.
         */
        inline void release() noexcept;

    private:
        GstMessage *m_message;

        int *m_counter;
    };

}
}
}
