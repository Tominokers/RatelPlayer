#include "GstObjectUnref.h"

#include <gst/gst.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    void GstObjectUnref::unref(ObjectType &object) noexcept {
        gst_object_unref(object);
    }

}
}
}
