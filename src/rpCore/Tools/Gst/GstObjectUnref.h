#pragma once

#include "rpCore/Tools/Gst/GstElementPtr.h"

#include <glib.h>

namespace rpCore {
namespace Tools {
namespace Gst {

    /**
     * Entity which unreferences correctly passed gpointer.
     */
    class GstObjectUnref
    {
    public:
        /**
         * The type of object which is handled.
         */
        typedef gpointer ObjectType;

        /**
         * Perform actions on passed objects
         * what can cause its descruction.
         * @param object the instance to unref.
         */
        static void unref(ObjectType &object) noexcept;
    };

}

    using GstPlaybackPtr = Tools::Gst::GstElementPtr<GstElement, Tools::Gst::GstObjectUnref>;
    using GstBusPtr = Tools::Gst::GstElementPtr<GstBus, Tools::Gst::GstObjectUnref>;

}
}
