#include "SupportedMedia.h"

#include <algorithm>
#include <unordered_map>

namespace rpCore {
namespace Tools {
namespace Media {

    /**
     * List of supported media extensions.
     */
    static std::unordered_map<std::string, MediaType> *MediaExtension = nullptr;

    std::string toLowerCase(const std::string &text) {
        auto loweredText = text;
        std::transform(std::begin(text),
                       std::end(text),
                       std::begin(loweredText),
                       [](std::string::const_reference c) -> std::string::value_type
                       { return static_cast<std::string::value_type>(std::tolower(c)); });

        return loweredText;
    }

    void initSupportedMedia() {
#define X(a, b) std::make_pair(b, MediaType::a),
        MediaExtension = new std::unordered_map<std::string, MediaType>({ MEDIA_TABLE });
#undef X
    }

    void deinitSupportedMedia() {
        delete MediaExtension;
    }

    bool isSupportedMedia(const std::string &fileName) noexcept {
        std::size_t pos = fileName.rfind(".");

        if (pos == std::string::npos)
            return false;

        std::string extension = toLowerCase(fileName.substr(pos));

        return MediaExtension->find(extension) != std::end(*MediaExtension);
    }

    MediaType getMediaType(const std::string &fileName) noexcept {
        std::size_t pos = fileName.rfind(".");

        if (pos == std::string::npos)
            return MediaType::NOT_SUPPORTED;

        std::string extension = toLowerCase(fileName.substr(pos));
        auto result = MediaExtension->find(extension);
        if (result != std::end(*MediaExtension))
            return result->second;

        return MediaType::NOT_SUPPORTED;
    }

}
}
}
