#pragma once

#include <string>

namespace rpCore {
namespace Tools {
namespace Media {

#define MEDIA_TABLE \
    X(MP3, ".mp3") \
    X(FLAC, ".flac") \
    X(WAV, ".wav") \
    X(APE, ".ape") \
    X(M4A, ".m4a") \
    X(OGG, ".ogg") \
    X(CUE, ".cue") \

#define X(a, b) a,
    /**
     * Enum of supported media files.
     */
    enum class MediaType {
        MEDIA_TABLE
        NOT_SUPPORTED
    };

#undef X

    void initSupportedMedia();

    void deinitSupportedMedia();

    /**
     * Checks whether media file is supported or not by its name.
     * @param fileName name of media file on file system.
     * @return true if file is supported otherwise false.
     */
    bool isSupportedMedia(const std::string &fileName) noexcept;

    /**
     * Gets media type of file by its name.
     * @param fileName name of media file on file system.
     * @return type of media file.
     */
    MediaType getMediaType(const std::string &fileName) noexcept;

}
}
}
