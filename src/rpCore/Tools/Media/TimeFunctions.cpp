#include "TimeFunctions.h"

namespace rpCore {
namespace Tools {
namespace Media {

    timeTuple parseTimeToTuple(const std::chrono::nanoseconds &time) noexcept {
        std::chrono::hours hours = std::chrono::duration_cast<std::chrono::hours>(time);
        std::chrono::minutes minutes = std::chrono::duration_cast<std::chrono::minutes>(time) - hours;
        std::chrono::seconds seconds = std::chrono::duration_cast<std::chrono::seconds>(time) - (hours + minutes);

        std::string hourString = std::to_string(hours.count());
        std::string minuteString = std::to_string(minutes.count());
        std::string secondString = std::to_string(seconds.count());

        return std::make_tuple(hourString, minuteString, secondString);
    }

}
}
}
