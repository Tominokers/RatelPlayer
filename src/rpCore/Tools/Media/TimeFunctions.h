#pragma once

#include <chrono>
#include <tuple>

namespace rpCore {
namespace Tools {
namespace Media {

    using timeTuple = std::tuple<std::string, std::string, std::string>;

    timeTuple parseTimeToTuple(const std::chrono::nanoseconds &time) noexcept;

}
}
}
