#include "BridgeAble.h"

#include <algorithm>

namespace rpFacade {
namespace Bridge {

    void BridgeAble::addBridge(BridgeConnector *bridgeConnector) noexcept {
        auto it = std::find(std::begin(m_bridges), std::end(m_bridges), bridgeConnector);
        if (it == std::end(m_bridges))
            m_bridges.push_back(bridgeConnector);
    }

    std::vector<BridgeConnector*>& BridgeAble::getBridges() noexcept {
        return m_bridges;
    }

    void BridgeAble::removeBridge(BridgeConnector *bridgeConnector) noexcept {
        m_bridges.erase(std::remove(std::begin(m_bridges),
                                    std::end(m_bridges),
                                    bridgeConnector),
                        std::end(m_bridges));
    }

}
}
