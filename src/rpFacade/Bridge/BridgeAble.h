#pragma once

#include <vector>

namespace rpFacade {
namespace Bridge {

    class BridgeConnector;

    /**
     * Enitity which is used to connect instance to BridgeConnector.
     * Should be inherited for such object.
     */
    class BridgeAble
    {
    public:
        BridgeAble() = default;

        virtual ~BridgeAble() = default;

        /**
         * Adds BridgeConnector instance to a BridgeAble instance.
         * @param bridgeConnector the instance of bridge between two BridgeAble instances.
         */
        void addBridge(BridgeConnector *bridgeConnector) noexcept;

        /**
         * Removes BridgeConnector instance from a BridgeAble instance.
         * @param bridgeConnector the instance of bridge between two BridgeAble instances.
         */
        void removeBridge(BridgeConnector *bridgeConnector) noexcept;

        /**
         * Gets BridgeConnector instances of a BridgeAble instance.
         * @return the instance of bridge between two BridgeAble instances.
         */
        std::vector<BridgeConnector*>& getBridges() noexcept;

    private:
        std::vector<BridgeConnector*> m_bridges;

    };

}
}
