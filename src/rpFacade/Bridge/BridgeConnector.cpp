#include "BridgeConnector.h"

#include "rpFacade/ObserverInterface.h"
#include "rpFacade/Observers/Visitors/RecursiveMediaGetter.h"
#include "rpFacade/PlayerInterface.h"
#include "rpFacade/PlaylistAssistant.h"

#include "rpCore/Media/MediaProxy.h"

namespace rpFacade {
namespace Bridge {

    BridgeConnector::BridgeConnector(ObserverInterface &observerInstance,
                                     PlayerInterface &playerInstance) noexcept
        : m_observer(observerInstance)
        , m_player(playerInstance)
    {
        m_observer.addBridge(this);
        m_player.addBridge(this);
    }

    BridgeConnector::~BridgeConnector() {
        m_observer.removeBridge(this);
        m_player.removeBridge(this);
    }

    void BridgeConnector::appendToCurrentPlaylist(const MediaProxy &file) noexcept {
        m_player.getPlaylistAssistant().appendToOpenedPlaylist(file);
    }

    void BridgeConnector::appendToCurrentPlaylist(const MediaList &list) noexcept {
        for (const auto &item : list)
            m_player.getPlaylistAssistant().appendToOpenedPlaylist(item);
    }

    void BridgeConnector::appendDelayedMedia(const MediaProxy &file) noexcept {
        m_delayedMedia.push_back(file);
    }

    void BridgeConnector::appendDelayedMedia(MediaList files) noexcept {
        std::move(std::begin(files), std::end(files), std::back_inserter(m_delayedMedia));
    }

    void BridgeConnector::appendDelayedObservers(ObserverList observers) noexcept {
        std::move(std::begin(observers), std::end(observers), std::back_inserter(m_delayedObservers));
    }

    void BridgeConnector::requestOpenedPlaylistUpdate() noexcept {
        auto &assistant = m_player.getPlaylistAssistant();
        assistant.insertDelayedMedia(assistant.getOpenedPlaylistIndex(), NoIndex);
    }

    const MediaList& BridgeConnector::getDelayedMedia() const noexcept  {
        for (const auto &item : m_delayedObservers)
        {
            Observers::Visitors::RecursiveMediaGetter getter(*item);
            const auto &list = getter.get();

            m_delayedMedia.insert(std::end(m_delayedMedia),
                                  std::cbegin(list),
                                  std::cend(list));
        }

        m_delayedObservers.clear();

        return m_delayedMedia;
    }

    void BridgeConnector::clearDelayedMedia() noexcept {
        m_delayedMedia.clear();
    }

}
}
