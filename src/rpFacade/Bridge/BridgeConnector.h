#pragma once

#include "rpFacade/FacadeObserverTypes.h"

#include <memory>

namespace rpFacade {

    class ObserverInterface;
    class PlayerInterface;

namespace Bridge {

    /**
     * Enitity which is a channel between Observer and Player.
     * This channel provides neccesary communication between connected entities.
     * @remark Observer and Player should be BridgeAble.
     */
    class BridgeConnector final
    {
    public:
        /**
         * Constructs BridgeConnector instance between passed observer and player.
         * @param observerInstance the observer to connect.
         * @param playerInstance the player to connect.
         */
        explicit BridgeConnector(ObserverInterface &observerInstance,
                                 PlayerInterface &playerInstance) noexcept;

        ~BridgeConnector();

        /**
         * Adds passed media entity to current playlist of player.
         * @param file the media file to add.
         */
        void appendToCurrentPlaylist(const MediaProxy &file) noexcept;

        /**
         * Adds all media entities from list to current playlist of player.
         * @param list the media entities to add into playlist.
         */
        void appendToCurrentPlaylist(const MediaList &list) noexcept;

        /**
         * Appends passed media entity to internal container which can be used
         * for delayed exchange of media entities between observer and player.
         * @param file the media entity to append.
         */
        void appendDelayedMedia(const MediaProxy &file) noexcept;

        /**
         * Appends media entities into internal container which is used
         * for delayed exchange of media entities between observer and player.
         * @param files the container with media entities to append.
         * @remark passed container should be r-value,
         * because values of passed container will be moved,
         * most probably container will not be valid after this operation.
         */
        void appendDelayedMedia(MediaList files) noexcept;

        /**
         * Appends observers into internal container which is used
         * for delayed exchange of media entities from observers into player.
         * Call to getDelayedMedia will extract media entities from observers.
         * @param observers list of observers to append.
         */
        void appendDelayedObservers(ObserverList observers) noexcept;

        /**
         * Requests appending of delayed medias into currently opened playlist.
         * Operation will be done asynchronously and items will be added in the end of playlist.
         */
        void requestOpenedPlaylistUpdate() noexcept;

        /**
         * Gets media entities which were delayed for usage.
         * @return container with delayed media entities.
         */
        const MediaList& getDelayedMedia() const noexcept;

        /**
         * Clears all held delayed media entities.
         */
        void clearDelayedMedia() noexcept;

    private:
        ObserverInterface &m_observer;

        PlayerInterface &m_player;

        mutable MediaList m_delayedMedia;
        mutable ObserverList m_delayedObservers;
    };

}
}
