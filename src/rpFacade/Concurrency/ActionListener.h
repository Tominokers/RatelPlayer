#pragma once

namespace rpFacade {
namespace Concurrency {

    /**
     * Entity which provides hooks
     * for listening start and end of ConcurrentAction.
     * Normally hooks will be called on UI thread.
     */
    class ActionListener
    {
    public:
        virtual ~ActionListener() = default;

        /**
         * Hook which is called before performaing ConcurrentAction.
         */
        virtual void handleStartNotification() =0;

        /**
         * Hook which is called after ConcurrentAction is finished.
         */
        virtual void handleEndNotification() =0;

    };

}
}
