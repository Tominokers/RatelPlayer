#include "ConcurrentAction.h"

#include "rpFacade/Concurrency/ActionListener.h"

namespace rpFacade {
namespace Concurrency {

    ConcurrentAction::ConcurrentAction(ActionListener &listener)
        : m_actionListener(listener)
    {
        m_mediaFinished.connect(sigc::mem_fun(listener, &ActionListener::handleEndNotification));
    }

    ConcurrentAction::~ConcurrentAction() {
        if (m_updateToken.valid())
            m_updateToken.get();
    }

    SyncToken ConcurrentAction::execute(const std::function<void()> &action) {
        if (m_updateToken.valid())
            m_updateToken.get();

        m_actionListener.handleStartNotification();

        m_updateToken = std::async(std::launch::async,
                                   [this, action]()
                                   {
                                       action();
                                       m_mediaFinished.emit();
                                   });

        return m_updateToken;
    }

}
}
