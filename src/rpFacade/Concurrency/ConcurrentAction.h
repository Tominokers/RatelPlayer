#pragma once

#include "rpFacade/Concurrency/SyncToken.h"
#include "rpFacade/Concurrency/UiDispatcher.h"

#include <functional>

namespace rpFacade {
namespace Concurrency {
    class ActionListener;

    /**
     * Entity which executes some action on another thread.
     */
    class ConcurrentAction final
    {
    public:
        /**
         * Constructs concurrent action with specific listener.
         * Listeners will notify about start and end of action on UI thread.
         * @param listener the listener of concurrent action.
         */
        explicit ConcurrentAction(ActionListener &listener);

        /// Copy constructor is forbidden.
        ConcurrentAction(const ConcurrentAction&) = delete;

        /// Move constructor is forbidden.
        ConcurrentAction(ConcurrentAction&&) = delete;

        /**
         * Destruction will be blocked until concurrent action is finished.
         */
        ~ConcurrentAction();

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        ConcurrentAction& operator=(const ConcurrentAction&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        ConcurrentAction& operator=(ConcurrentAction&&) = delete;

        /**
         * Executes passed action on another thread.
         * @param action the action to perform asynchronously.
         * @return the synchronization token.
         * @remark new action won't be started until previous operation is finished,
         * in such case call will block execution.
         */
        SyncToken execute(const std::function<void()> &action);

    private:
        ActionListener &m_actionListener;
        UiDispatcher m_mediaFinished;
        SyncToken m_updateToken;
    };

}
}
