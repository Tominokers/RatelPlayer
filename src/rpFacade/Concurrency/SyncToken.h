#pragma once

#include <future>

namespace rpFacade {
namespace Concurrency {

    using SyncToken = std::shared_future<void>;

}
}
