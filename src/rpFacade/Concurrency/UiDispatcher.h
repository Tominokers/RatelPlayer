#pragma once

#include <glibmm/dispatcher.h>

namespace rpFacade {
namespace Concurrency {

    using UiDispatcher = Glib::Dispatcher;

}
}
