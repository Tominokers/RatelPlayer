#include "FacadeManager.h"

#include "rpFacade/Observers/ObserverMediator.h"
#include "rpFacade/Player/PlayerMediator.h"

namespace rpFacade {

    std::unique_ptr<PlayerInterface> FacadeManager::createPlayer(rpSettings::SettingStorage &settingStorage,
                                                                 rpFacade::PlayerClient *client) {
        return std::unique_ptr<PlayerInterface>(new Player::PlayerMediator(settingStorage, client));
    }

    std::unique_ptr<ObserverInterface> FacadeManager::createObserver(const rpObservers::ObserverState &state,
                                                                     ObserverClient *client) {
        return std::unique_ptr<ObserverInterface>(new Observers::ObserverMediator(state, client));
    }

}
