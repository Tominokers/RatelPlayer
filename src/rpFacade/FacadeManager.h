#pragma once

#include <memory>

namespace rpObservers {
    class ObserverState;
}

namespace rpSettings {
    class SettingStorage;
}

namespace rpFacade {

    class ObserverClient;
    class ObserverInterface;
    class PlayerClient;
    class PlayerInterface;

    /**
     * Entity which creates instances of facade interfaces.
     */
    class FacadeManager final
    {
    public:
        /**
         * Creates instance of player.
         * @param settingStorage the storage in which player will save its settings.
         * @param client the client of player with which player will interact with.
         * @return new instance of player interface.
         */
        static std::unique_ptr<PlayerInterface> createPlayer(rpSettings::SettingStorage &settingStorage,
                                                             rpFacade::PlayerClient *client);

        /**
         * Create instance of observer.
         * @param state the state of observer whith which it should be initialized.
         * @param client the client of observer with which observer will interact with.
         * @return new instance of observer interface.
         */
        static std::unique_ptr<ObserverInterface> createObserver(const rpObservers::ObserverState &state,
                                                                 ObserverClient *client);

    };

}
