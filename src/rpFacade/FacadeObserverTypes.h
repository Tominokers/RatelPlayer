#pragma once

#include <memory>
#include <vector>

namespace rpCore {
namespace Media {
    class MediaProxy;
}
}

namespace rpObservers {
    class Observer;
}

namespace rpFacade {

    using MediaProxy = rpCore::Media::MediaProxy;
    using MediaList = std::vector<MediaProxy>;

    using Observer = rpObservers::Observer;
    using ObserverList = std::vector<std::shared_ptr<Observer>>;

    using ContainerIndex = std::size_t;
    using ContainerCount = std::size_t;
}
