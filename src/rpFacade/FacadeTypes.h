#pragma once

#include "rpCore/Player/PlayerModes.h"
#include "rpCore/Player/PlayerTypes.h"
#include "rpCore/Player/PlaylistSortType.h"

namespace rpFacade {

    using PlaylistIndex = std::size_t;

    using RepeatMode = rpCore::Player::RepeatMode;
    using ShuffleMode = rpCore::Player::ShuffleMode;
    using TrackIndex = rpCore::Player::TrackIndex;
    using TrackIndexList = rpCore::Player::TrackIndexList;
    using PlaylistSortType = rpCore::Player::PlaylistSortType;

    constexpr TrackIndex NoIndex = rpCore::Player::NoIndex;
    constexpr PlaylistIndex FirstPlaylist = 0;

}
