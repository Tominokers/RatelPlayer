#pragma once

#include "rpFacade/FacadeObserverTypes.h"

namespace rpFacade {

    class ObserverInterface;

    /**
     * Interface which provides obtaining data
     * from ObserverMediator by help of callbacks.
     */
    class ObserverClient
    {
    public:
        virtual ~ObserverClient() = default;

        /**
         * Gets observer to which client is connected.
         * @return reference to interface of Observer instance.
         */
        virtual ObserverInterface& getObserver() const noexcept =0;

        /**
         * Callback which notifies about start of passing media content.
         * @remark operation will be called on UI thread.
         */
        virtual void obtainingMediaStarted() noexcept =0;

        /**
         * Callback which notifies about finish of passing media content.
         * @remark operation will be called on UI thread.
         */
        virtual void obtainingMediaFinished() noexcept =0;

        /**
         * Callback which obtains media items one by one.
         * @param proxy the media item.
         * @remark operation will be called on none-UI thread.
         */
        virtual void passObtainedMedia(const MediaProxy &proxy) noexcept =0;

        /**
         * Callback which obtains observers one by one.
         * @param observer the observer item.
         * @remark operation will be called on none-UI thread.
         */
        virtual void passObtainedObservers(const Observer &observer) noexcept =0;

        /**
         * Callback which notifies about changing of observer name.
         * E.g. it can happen when user changes directory in file manager.
         * @param newObserver name representing of new observer.
         * @remark operation will be called on none-UI thread.
         */
        virtual void observerNameChanged(const std::string &newObserver) noexcept =0;
    };

}
