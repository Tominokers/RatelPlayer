#pragma once

#include "rpFacade/Bridge/BridgeAble.h"
#include "rpFacade/Concurrency/SyncToken.h"
#include "rpFacade/FacadeObserverTypes.h"

namespace rpObservers {
    class ObserverState;
}

namespace rpFacade {

    class ObserverClient;
    class ObserverItemInformation;

    /**
     * Entity which provides common interface for observers.
     * It takes care about updating of observer,
     * requesting of subobservers and media files and etc.
     */
    class ObserverInterface : public Bridge::BridgeAble
    {
    public:
        /// Default constructor.
        ObserverInterface() = default;

        /// Copy constructor is forbidden.
        ObserverInterface(const ObserverInterface&) = delete;

        /// Move semantic is not allowed.
        ObserverInterface(ObserverInterface&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        ObserverInterface& operator=(const ObserverInterface&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        ObserverInterface& operator=(ObserverInterface&&) = delete;

        /// Default destructor.
        virtual ~ObserverInterface() = default;

        /**
         * Replaces current client with passed as argument.
         * @param client new client to set in ObserverInterface.
         */
        virtual void setClient(ObserverClient *client = nullptr) noexcept =0;

        /**
         * Updates information about held observer.
         * @remark Operation is asynchronous.
         * @return synchronization token.
         */
        virtual Concurrency::SyncToken Update() =0;

        /**
         * Switchs observer to parrent state if it can.
         * If switching is successfully performed, callbacks will be fired.
         * @remark Operation is asynchronous.
         * @return synchronization token.
         */
        virtual Concurrency::SyncToken switchToParentObserver() =0;

        /**
         * Switchs observer to new state.
         * @remark Most probably internal observer will be recreated.
         * In case observer is already in new state, nothing will happen.
         * @param state the new state of observer.
         * @return synchronization token.
         */
        virtual Concurrency::SyncToken switchToState(const rpObservers::ObserverState &state) =0;

        /**
         * Requests information about media item by its index.
         * @param item index of item in ObserverMediator.
         * @return information about media file.
         */
        virtual ObserverItemInformation requestItemInfo(ContainerIndex item) const =0;

        /**
         * Executes action which is related to item by index.
         * @param item index of item in ObserverMediator.
         * @remark ObserverMediator supposses that first items are
         * observers after which media files are. So it will look
         * in such order and make appropriate action.
         * If element isn't found, there won't be action and errors.
         */
        virtual void executeItem(ContainerIndex item) =0;

        /**
         * Adds item in observer by given item to current opened playlist.
         * @param item index of item in ObserverMediator.
         */
        virtual void addItem(ContainerIndex item) =0;

        /**
         * Delays passed items from observer for usage.
         * @param items list of items to delay.
         */
        virtual void delayItems(const std::vector<ContainerIndex> &items) noexcept =0;

        /**
         * Causes processing of delayed items by connected to observer player.
         */
        virtual void processDelayedItems() noexcept =0;

    };

}
