#include "ObserverItemInformation.h"

namespace rpFacade {

    ObserverItemInformation::ObserverItemInformation()
        : m_type(ItemType::Invalid)
        , m_mediaInfo(nullptr)
        , m_observerInfo(nullptr)
    {
    }

    ObserverItemInformation::ObserverItemInformation(ItemType itemType, const void *info)
        : m_type(itemType)
        , m_mediaInfo(nullptr)
        , m_observerInfo(nullptr)
    {
        switch(m_type)
        {
        case ItemType::Media:
            m_mediaInfo = static_cast<const MediaInformation*>(info);
            break;
        case ItemType::Observer:
            m_observerInfo = static_cast<const ObserverInformation*>(info);
            break;
        case ItemType::Invalid:
            break;
        }
    }

    ItemType ObserverItemInformation::getType() const {
        return m_type;
    }

    const MediaInformation* ObserverItemInformation::getMediaInfo() const {
        return m_mediaInfo;
    }

    const ObserverInformation* ObserverItemInformation::getObserverInfo() const {
        return m_observerInfo;
    }

}
