#pragma once

namespace rpCore {
namespace Media {
    class MediaInformation;
}
}

namespace rpObservers {
    class ObserverInformation;
}

namespace rpFacade {

    using MediaInformation = rpCore::Media::MediaInformation;
    using ObserverInformation = rpObservers::ObserverInformation;

    /**
     * Enumeration of item types in observer.
     */
    enum class ItemType
    {
        /// Media item which can be played.
        Media,
        /// Child observer of current observer.
        Observer,
        /// Invalid item.
        Invalid,
    };

    /**
     * Entity which has information about observer item.
     * It can be information about subobserver or media item.
     * If user requested info at incorrect position
     * getType will return Invalid enum item
     * and other getters return nulllptr.
     */
    class ObserverItemInformation final
    {
    public:
        /// Constructs invalid item infomration.
        ObserverItemInformation();

        /**
         * Contructs item information with specific type and information.
         * @param itemType the type of item in observer.
         * @param info the information about specific type.
         */
        ObserverItemInformation(ItemType itemType, const void *info);

        /**
         * Gets type of held item.
         * @return item from type enumeration.
         */
        ItemType getType() const;

        /**
         * Gets information about media item.
         * @return information about media item or nullptr.
         */
        const MediaInformation* getMediaInfo() const;

        /**
         * Gets information about subobserver item.
         * @return information about subobserver item or nullptr.
         */
        const ObserverInformation* getObserverInfo() const;

    private:
        ItemType m_type;

        const MediaInformation *m_mediaInfo;
        const ObserverInformation *m_observerInfo;
    };

}
