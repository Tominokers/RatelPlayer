#include "MediaContainer.h"

#include "rpFacade/ObserverItemInformation.h"

#include "rpCore/Media/MediaFile.h"

namespace rpFacade {
namespace Observers {

    MediaContainer::MediaContainer(Observer &observer)
    {
        for (ContainerIndex i = 0; i < observer.getSubobserversCount(); ++i)
            m_subobservers.emplace_back(observer.extractObserverAt(i));

        for (ContainerIndex i = 0; i < observer.getMediaCount(); ++i)
            m_medias.emplace_back(observer.extractMediaAt(i));
    }

    MediaContainer::~MediaContainer() =default;

    const ObserverList& MediaContainer::getSubobservers() const {
        return m_subobservers;
    }

    const MediaList& MediaContainer::getMedias() const {
        return m_medias;
    }

    const std::shared_ptr<Observer>& MediaContainer::getSubobserverAt(TrackIndex i) const {
        return m_subobservers[i];
    }

    const MediaProxy& MediaContainer::getMediaFileAt(TrackIndex i) const {
        return m_medias[i];
    }

    ContainerCount MediaContainer::getSubobserversCount() const noexcept {
        return m_subobservers.size();
    }

    ContainerCount MediaContainer::getMediaFilesCount() const noexcept {
        return m_medias.size();
    }

    void MediaContainer::forItem(ContainerIndex index,
                                 const ObserverHandler &handleObserver,
                                 const MediaHandler &handleMediaItem) {
        if (index < this->getSubobserversCount())
            handleObserver(getSubobserverAt(index));

        index = index - this->getSubobserversCount();
        if (index < this->getMediaFilesCount())
            handleMediaItem(this->getMediaFileAt(index));
    }

    ObserverItemInformation MediaContainer::getItemInfo(ContainerIndex item) const noexcept {
        if (item < this->getSubobserversCount())
        {
            const auto &subobserver = this->getSubobserverAt(item);
            return ObserverItemInformation(ItemType::Observer, &subobserver->getInfo());
        }

        ContainerIndex index = item - this->getSubobserversCount();
        if (index < this->getMediaFilesCount())
        {
            const auto &file = this->getMediaFileAt(index).get();
            return ObserverItemInformation(ItemType::Media, &file->getMediaInfo());
        }

        return ObserverItemInformation();
    }

}
}
