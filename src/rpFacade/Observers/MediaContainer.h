#pragma once

#include "rpFacade/FacadeObserverTypes.h"
#include "rpFacade/FacadeTypes.h"

#include "rpCore/Media/MediaProxy.h"
#include "rpObservers/Observer.h"

namespace rpFacade {

    class ObserverItemInformation;

namespace Observers {

    using ObserverHandler = std::function<void(const std::shared_ptr<Observer>&)>;
    using MediaHandler = std::function<void(const MediaProxy&)>;

    /**
     * Entity which holds extracted observers and media files
     * from given observer and provides access to its content.
     */
    class MediaContainer
    {
    public:
        /**
         * Constructs instance of MediaContainer from given observer.
         * @param observer is observer from which will be taken items.
         */
        explicit MediaContainer(Observer &observer);

        /**
         * Destroys instance of MediaContainer.
         */
        ~MediaContainer();

        /**
         * Gets list of subobservers.
         * @return collection of subobservers.
         */
        const ObserverList& getSubobservers() const;

        /**
         * Gets list of media items.
         * @return collection of media items.
         */
        const MediaList& getMedias() const;

        /**
         * Gets subobserver from held observer by index.
         * @param i index of subobserver to get.
         * @return shared_ptr of subobserver.
         */
        const std::shared_ptr<Observer>& getSubobserverAt(ContainerIndex i) const;

        /**
         * Gets media file from held observer by index.
         * @param i index of media file to get.
         * @return proxy to media file.
         */
        const MediaProxy& getMediaFileAt(ContainerIndex i) const;

        /**
         * Gets count of held subobservers by container.
         * @return count of subobservers.
         */
        ContainerCount getSubobserversCount() const noexcept;

        /**
         * Gets count of held media files by container.
         * @return count of media files.
         */
        ContainerCount getMediaFilesCount() const noexcept;

        /**
         * Provides correct sequencial access to items of container with executing handler on it.
         * handleObserver will be called for indexes [0, getSubobserversCount).
         * handleMediaItem will be called for indexes [getSubobserversCount, getSubobserversCount + getMediaFilesCount).
         * Outside these ranges nothing will be called.
         * @param index the container index to handle.
         * @param handleObserver the handler of observer type items.
         * @param handleMediaItem the handler of media type items.
         */
        void forItem(ContainerIndex index,
                     const ObserverHandler &handleObserver,
                     const MediaHandler &handleMediaItem);

        /**
         * Gets information instance for specific container index.
         * @param item the containder index for which information is get.
         * @return information about observer time,
         * it is empty if item index is outside of range.
         */
        ObserverItemInformation getItemInfo(ContainerIndex item) const noexcept;

    private:
        ObserverList m_subobservers;
        rpCore::Media::MediaList m_medias;
    };

}
}
