#include "ObserverInspector.h"

#include "rpFacade/Observers/ObserverVisitor.h"

namespace rpFacade {
namespace Observers {

    ObserverInspector::ObserverInspector(ObserverVisitor &visitor)
        : m_visitor(visitor)
    {
    }

    std::unique_ptr<MediaContainer> ObserverInspector::inspect(rpObservers::Observer &observer,
                                                               TypeOfVisiting visitType) {
        observer.Update();
        auto container = std::make_unique<MediaContainer>(observer);

        switch(visitType)
        {
        case TypeOfVisiting::Flat:
            this->doFlatInspection(*container);
            break;
        case TypeOfVisiting::RecursiveMedia:
            this->doRecursiveInspection(*container);
            break;
        }

        return container;
    }

    void ObserverInspector::doFlatInspection(const MediaContainer &container) {
        for (auto &observer : container.getSubobservers())
            m_visitor.visitObserver(*observer);

        for (auto &media : container.getMedias())
            m_visitor.visitMedia(media);
    }

    void ObserverInspector::doRecursiveInspection(const MediaContainer &container) {
        for (auto &observer : container.getSubobservers())
            inspect(*observer, TypeOfVisiting::RecursiveMedia);

        for (auto &media : container.getMedias())
            m_visitor.visitMedia(media);
    }

}
}
