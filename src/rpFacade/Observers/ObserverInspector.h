#pragma once

#include "rpFacade/Observers/MediaContainer.h"

#include <memory>

namespace rpObservers {
    class Observer;
}

namespace rpFacade {
namespace Observers {

    class ObserverVisitor;

    /**
     * Entity which inspects observer in flat or recursive way
     * and calls appropriate visitor methods to handle different items.
     */
    class ObserverInspector final
    {
    public:
        /**
         * Enumeration of possible visiting types to inspect observer.
         */
        enum class TypeOfVisiting
        {
            /**
             * Flat visiting doesn't go deeply into observer,
             * it inspects only subobservers and sub media items.
             */
            Flat,
            /**
             * Recursive visiting goes into all subobservers
             * and calls methods of visitor to handle them all.
             */
            RecursiveMedia,
        };

    public:
        /**
         * Constructs ObserverInspector with specified visitor.
         * @param visitor the instance of visitor.
         */
        explicit ObserverInspector(ObserverVisitor &visitor);

        /**
         * Inspects observer with specified type of visiting.
         * @param observer the observer to inspect.
         * @param visitType the type of inspecting observer.
         * @return media container with items of inspected observer.
         */
        std::unique_ptr<MediaContainer> inspect(rpObservers::Observer &observer, TypeOfVisiting visitType);

    private:
        void doFlatInspection(const MediaContainer &container);
        void doRecursiveInspection(const MediaContainer &container);

    private:
        ObserverVisitor &m_visitor;
    };

}
}
