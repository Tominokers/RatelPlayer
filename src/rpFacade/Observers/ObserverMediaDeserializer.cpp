#include "ObserverMediaDeserializer.h"

#include "rpFacade/Observers/Visitors/RecursiveMediaGetter.h"

#include "rpCore/Media/MediaFactory.h"
#include "rpObservers/States/DirectoryState.h"

#include <cassert>
#include <sstream>

namespace rpFacade {
namespace Observers {

    rpCore::Media::MediaList ObserverMediaDeserializer::deserializeObserverData(const std::string &data) {
        rpCore::Media::MediaList mediaList;
        std::string entry;

        std::stringstream stream(data);
        while(std::getline(stream, entry))
        {
            assert(!entry.empty() && entry.back() == '\r');
            entry.pop_back(); // Throw away \r character.
            auto media = rpCore::Media::MediaFactory::createMedia(entry);
            if (media.get())
            {
                mediaList.push_back(std::move(media));
            }
            else if (rpObservers::States::DirectoryState::isDirectoryUri(entry))
            {
                rpObservers::States::DirectoryState state(entry);
                Visitors::RecursiveMediaGetter mediaGetter(state);
                const auto &list = mediaGetter.get();
                mediaList.insert(std::end(mediaList), std::begin(list), std::end(list));
            }
        }

        return mediaList;
    }

}
}
