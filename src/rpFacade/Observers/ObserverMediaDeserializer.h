#pragma once

#include "rpCore/Media/MediaProxy.h"

namespace rpFacade {
namespace Observers {

    /**
     * Entity which deserializes text to list of media items.
     */
    class ObserverMediaDeserializer final
    {
    public:
        /**
         * Deserializes string text into list of media items.
         * @param data the string text in which each media item is separated with \\n\\r.
         * @return deserialized list of media items.
         */
        static rpCore::Media::MediaList deserializeObserverData(const std::string &data);

    };

}
}
