#include "ObserverMediator.h"

#include "rpFacade/Bridge/BridgeConnector.h"
#include "rpFacade/ObserverItemInformation.h"
#include "rpFacade/Observers/MediaContainer.h"
#include "rpFacade/Observers/Visitors/RecursiveMediaGetter.h"

#include "rpObservers/Observer.h"
#include "rpObservers/ObserverCreator.h"
#include "rpObservers/ObserverState.h"

namespace rpFacade {
namespace Observers {

    ObserverMediator::ObserverMediator(const rpObservers::ObserverState &state,
                                       ObserverClient *observerClient)
        : m_observer(rpObservers::ObserverCreator::create(state))
        , m_updater(observerClient, m_observer.get(), m_container)
    {
    }

    ObserverMediator::~ObserverMediator() = default;

    void ObserverMediator::setClient(ObserverClient *observerClient) noexcept {
        m_updater.setClient(observerClient);
    }

    Concurrency::SyncToken ObserverMediator::Update() {
        m_updater.setObserver(m_observer.get());
        return m_updater.run();
    }

    ObserverItemInformation ObserverMediator::requestItemInfo(ContainerIndex item) const noexcept {
        return m_container->getItemInfo(item);
    }

    void ObserverMediator::executeItem(ContainerIndex index) {
        m_container->forItem(index,
                             [this](const auto &item) { this->executeObserver(item); },
                             [this](const auto &item) { this->executeMediaItem(item); });
    }

    void ObserverMediator::addItem(ContainerIndex index) {
        m_container->forItem(index,
                             [this](const auto &item) {
                                 Visitors::RecursiveMediaGetter getter(*item);
                                 auto medias = getter.get();

                                 for (auto bridge : this->getBridges())
                                     bridge->appendToCurrentPlaylist(medias);
                             },
                             [this](const auto &item) {
                                 this->executeMediaItem(item);
                             });
    }

    void ObserverMediator::delayItems(const std::vector<ContainerIndex> &indexes) noexcept {
        MediaList delayedItems;
        ObserverList delayedObservers;
        for (auto &index : indexes)
            m_container->forItem(index,
                                 [this, &delayedObservers](const auto &item) {
                                     delayedObservers.emplace_back(item);
                                 },
                                 [this, &delayedItems](const auto &item) {
                                     delayedItems.emplace_back(item);
                                 });

        for (auto bridge : this->getBridges())
        {
            bridge->appendDelayedObservers(delayedObservers);
            bridge->appendDelayedMedia(delayedItems);
        }
    }

    void ObserverMediator::processDelayedItems() noexcept {
        for (auto bridge : this->getBridges())
            bridge->requestOpenedPlaylistUpdate();
    }

    Concurrency::SyncToken ObserverMediator::switchToParentObserver() {
        if (m_observer->makeParentState())
            return this->Update();

        return Concurrency::SyncToken();
    }

    Concurrency::SyncToken ObserverMediator::switchToState(const rpObservers::ObserverState &state) {
        auto switchObserver = rpObservers::ObserverCreator::create(state);
        if (!switchObserver || m_observer->getFullName() == switchObserver->getFullName())
            return Concurrency::SyncToken();

        m_observer = std::move(switchObserver);
        return this->Update();
    }


    void ObserverMediator::executeObserver(const std::shared_ptr<Observer> &item) {
        m_observer = item;
        this->Update();
    }

    void ObserverMediator::executeMediaItem(const MediaProxy& item) {
        for (auto bridge : this->getBridges())
            bridge->appendToCurrentPlaylist(item);
    }

}
}
