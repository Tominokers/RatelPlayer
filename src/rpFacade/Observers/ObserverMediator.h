#pragma once

#include "rpFacade/ObserverInterface.h"
#include "rpFacade/Observers/ObserverUpdateRunner.h"

#include <functional>
#include <memory>

namespace rpCore {
namespace Media {
    class MediaProxy;
}
}

namespace rpObservers {
    class Observer;
    class ObserverState;
}

namespace rpFacade {
    class FacadeManager;
    class ObserverClient;

namespace Observers {
    class MediaContainer;

    /**
     * Entity which holds observer and provides
     * methods for work with it through client.
     */
    class ObserverMediator final : public ObserverInterface
    {
        friend rpFacade::FacadeManager;

        /**
         * Constructs instance of ObserverMediator which
         * holds created observer by given ObserverState
         * and uses spicified client for returning data.
         * @param state state of observer for creation.
         * @param observerClient instance of client which will obtain data.
         */
        ObserverMediator(const rpObservers::ObserverState &state,
                         ObserverClient *observerClient = nullptr);

    public:
        virtual ~ObserverMediator();

        ObserverMediator(const ObserverMediator&) = delete;
        ObserverMediator(ObserverMediator&&) = delete;
        ObserverMediator& operator=(const ObserverMediator&) = delete;
        ObserverMediator& operator=(ObserverMediator&&) = delete;

        virtual void setClient(ObserverClient *observerClient = nullptr) noexcept override;

        virtual Concurrency::SyncToken Update() override;

        virtual Concurrency::SyncToken switchToState(const rpObservers::ObserverState &state) override;

        virtual Concurrency::SyncToken switchToParentObserver() override;

        virtual ObserverItemInformation requestItemInfo(ContainerIndex index) const noexcept override;

        virtual void executeItem(ContainerIndex index) override;

        virtual void addItem(ContainerIndex index) override;

        virtual void delayItems(const std::vector<ContainerIndex> &indexes) noexcept override;

        virtual void processDelayedItems() noexcept override;

    private:
        void executeObserver(const std::shared_ptr<Observer> &index);
        void executeMediaItem(const MediaProxy& item);

    private:
        std::shared_ptr<rpObservers::Observer> m_observer;
        std::unique_ptr<MediaContainer> m_container;

        ObserverUpdateRunner m_updater;
    };

}
}
