#include "ObserverStorage.h"

#include "rpCore/Tools/ConvertUtilityFunctions.h"
#include "rpSettings/SettingStorage.h"

namespace rpFacade {
namespace Observers {

    SerializableObserver::~SerializableObserver() = default;

    ObserverLoader::~ObserverLoader() = default;

    ObserverStorage::ObserverStorage(rpSettings::SettingStorage &settingStorage)
        : m_storage(settingStorage)
        , m_observersCountKey("ObserversCount")
        , m_observerPrefixKey("SerializedObserver_")
        , m_observerLastActiveKey("LastActiveObserver")
    {
    }

    void ObserverStorage::store(const std::vector<SerializableObserver*> &observers)
    {
        m_storage.store(m_observersCountKey, std::to_string(observers.size()));

        for (ObserverStorageIndex i = 0; i < observers.size(); ++i)
        {
            auto key = m_observerPrefixKey + std::to_string(i);
            auto data = observers[i]->serialize();

            m_storage.store(key, data);
        }
    }

    void ObserverStorage::load(ObserverLoader &loader)
    {
        auto countText = m_storage.load(m_observersCountKey);
        if (countText.empty())
            return;

        auto count = rpCore::Tools::convertTextToInteger(countText);
        for (ObserverStorageIndex i = 0; i < count; ++i)
        {
            auto key = m_observerPrefixKey + std::to_string(i);

            auto data = m_storage.load(key);
            loader.load(data);
        }
    }

    void ObserverStorage::setLastActiveObserver(ObserverStorageIndex index) {
        auto text = std::to_string(index);
        m_storage.store(m_observerLastActiveKey, text);
    }

    ObserverStorageIndex ObserverStorage::getLastActiveObserver() const {
        auto text = m_storage.load(m_observerLastActiveKey);
        if (text.empty())
            return 0;

        auto index = rpCore::Tools::convertTextToInteger(text);
        return index;
    }

}
}
