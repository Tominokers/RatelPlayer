#pragma once

#include <string>
#include <vector>

namespace rpSettings {
    class SettingStorage;
}

namespace rpFacade {
namespace Observers {

    /// Represents index of observer in ObserverStorage.
    using ObserverStorageIndex = std::size_t;

    /**
     * Entity which provides method of observer serialization.
     */
    class SerializableObserver
    {
    public:
        /// Default destructor.
        virtual ~SerializableObserver();

        /**
         * Serializes observer to text information.
         * @return serialized text data of observer.
         */
        virtual std::string serialize() const =0;
    };

    /**
     * Entity which provides method
     * of loading observer from serialized data.
     */
    class ObserverLoader
    {
    public:
        /// Default destructor.
        virtual ~ObserverLoader();

        /**
         * Loads observer from serialized text data.
         * @param data the serialized text data.
         */
        virtual void load(const std::string &data) =0;
    };

    /**
     * Entity which stores information about observers in setting storage.
     * It makes possible to restore observers from storage later.
     */
    class ObserverStorage final
    {
    public:
        /**
         * Constructs observer storage in specified setting storage.
         * @param settingStorage the storage of settings for observers.
         */
        explicit ObserverStorage(rpSettings::SettingStorage &settingStorage);

        /// Copy constructor is forbidden.
        ObserverStorage(const ObserverStorage&) = delete;
        /// Move constructor is forbidden.
        ObserverStorage(ObserverStorage&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        ObserverStorage& operator=(const ObserverStorage&) = delete;
        /**
         * Move semantic is not allowed.
         * @return
         */
        ObserverStorage& operator=(ObserverStorage&&) = delete;

        /**
         * Stores information about observers in storage.
         * @param observers the observers to serialize.
         */
        void store(const std::vector<SerializableObserver*> &observers);

        /**
         * Loads observers from store into ObserverLoader instance.
         * @param loader the instance of ObserverLoader
         * into which observers will be loaded.
         */
        void load(ObserverLoader &loader);

        /**
         * Stores index of last active observer.
         * @param index the index of active observer to store.
         */
        void setLastActiveObserver(ObserverStorageIndex index);

        /**
         * Gets index of last stored active observer.
         * @return the index of observer.
         */
        ObserverStorageIndex getLastActiveObserver() const;

    private:
        rpSettings::SettingStorage &m_storage;

        const std::string m_observersCountKey;
        const std::string m_observerPrefixKey;
        const std::string m_observerLastActiveKey;
    };

}
}
