#include "ObserverUpdateRunner.h"

#include "rpFacade/ObserverClient.h"
#include "rpFacade/Observers/MediaContainer.h"
#include "rpFacade/Observers/ObserverInspector.h"

#include "rpObservers/Observer.h"

namespace rpFacade {
namespace Observers {

    ObserverUpdateRunner::ObserverUpdateRunner(ObserverClient *observerClient,
                                               rpObservers::Observer *observerInstance,
                                               std::unique_ptr<MediaContainer> &containerReference)
        : m_updateAction(*this)
        , m_client(observerClient)
        , m_observer(observerInstance)
        , m_container(containerReference)
    {
    }

    ObserverUpdateRunner::~ObserverUpdateRunner() = default;

    void ObserverUpdateRunner::setClient(ObserverClient *observerClient) {
        m_client = observerClient;
    }

    void ObserverUpdateRunner::setObserver(rpObservers::Observer *observerInstance) {
        m_observer = observerInstance;
    }

    Concurrency::SyncToken ObserverUpdateRunner::run() {
        auto action = [this]()
            {
                if (m_observer)
                {
                    ObserverInspector inspector(*this);
                    m_container = inspector.inspect(*m_observer, ObserverInspector::TypeOfVisiting::Flat);
                }
                else
                {
                    m_container.reset();
                }
            };

        return m_updateAction.execute(action);
    }

    void ObserverUpdateRunner::handleStartNotification() {
        if (m_client)
            m_client->obtainingMediaStarted();

        this->notifyObserverChanged();
    }

    void ObserverUpdateRunner::handleEndNotification() {
        if (m_client)
            m_client->obtainingMediaFinished();
    }

    void ObserverUpdateRunner::notifyObserverChanged() const noexcept {
        if (!m_client)
            return;

        m_client->observerNameChanged(m_observer ? m_observer->getFullName() : std::string());
    }

    void ObserverUpdateRunner::visitMedia(const rpCore::Media::MediaProxy &proxyItem) {
        if (!m_client)
            return;

        m_client->passObtainedMedia(proxyItem);
    }

    void ObserverUpdateRunner::visitObserver(const rpObservers::Observer &observerItem) {
        if (!m_client)
            return;

        m_client->passObtainedObservers(observerItem);
    }

}
}
