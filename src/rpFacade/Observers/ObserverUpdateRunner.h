#pragma once

#include "rpFacade/Concurrency/ActionListener.h"
#include "rpFacade/Concurrency/ConcurrentAction.h"
#include "rpFacade/Observers/ObserverVisitor.h"

#include <memory>

namespace rpObservers {
    class Observer;
}

namespace rpFacade {
    class ObserverClient;

namespace Observers {
    class MediaContainer;

    /**
     * Entity which performs update procedure of passed observer on another thread,
     * fills passed container with observer content and call necessary callbacks on observer client.
     * The order of callbacks should be the following:
     * - obtainingMediaStarted on UI thread.
     * - observerNameChanged on UI thread.
     * - passObtainedObservers on procedure thread.
     * - passObtainedMedia on procedure thread.
     * - obtainingMediaFinished on UI thread.
     *
     * run method is async operation which returns SyncToken.
     * run will be always started on new thread.
     */
    class ObserverUpdateRunner final : public ObserverVisitor
                                     , public Concurrency::ActionListener
    {
    public:
        explicit ObserverUpdateRunner(ObserverClient *observerClient,
                                      rpObservers::Observer *observerInstance,
                                      std::unique_ptr<MediaContainer> &containerReference);

        virtual ~ObserverUpdateRunner();

        void setClient(ObserverClient *observerClient);

        void setObserver(rpObservers::Observer *observerInstance);

        Concurrency::SyncToken run();

    private:
        // --- ActionListener ---
        virtual void handleStartNotification() override;
        virtual void handleEndNotification() override;

        void notifyObserverChanged() const noexcept;

        // --- ObserverVisitor ---
        virtual void visitMedia(const rpCore::Media::MediaProxy &proxyItem) override;
        virtual void visitObserver(const rpObservers::Observer &observerItem) override;

    private:
        Concurrency::ConcurrentAction m_updateAction;

        ObserverClient *m_client;
        rpObservers::Observer *m_observer;
        std::unique_ptr<MediaContainer> &m_container;
    };

}
}
