#pragma once

namespace rpCore {
namespace Media {
    class MediaProxy;
}
}

namespace rpObservers {
    class Observer;
}

namespace rpFacade {
namespace Observers {

    /**
     * Entity which visits items of observer.
     */
    class ObserverVisitor
    {
    public:
        //! Default destructor.
        virtual ~ObserverVisitor() =default;

        /**
         * Visits media item.
         * @param proxy the media item.
         */
        virtual void visitMedia(const rpCore::Media::MediaProxy &proxy) =0;

        /**
         * Visits observer item.
         * @param observer the subobserver item.
         */
        virtual void visitObserver(const rpObservers::Observer &observer) =0;

    };

}
}
