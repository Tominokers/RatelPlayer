#include "RecursiveMediaGetter.h"

#include "rpFacade/Observers/ObserverInspector.h"

#include "rpObservers/ObserverCreator.h"
#include "rpObservers/ObserverState.h"

namespace rpFacade {
namespace Observers {
namespace Visitors {

    RecursiveMediaGetter::RecursiveMediaGetter(const rpObservers::ObserverState &state)
    {
        ObserverInspector inspector(*this);

        if (auto observer = rpObservers::ObserverCreator::create(state))
            inspector.inspect(*observer, ObserverInspector::TypeOfVisiting::RecursiveMedia);
    }

    RecursiveMediaGetter::RecursiveMediaGetter(rpObservers::Observer &observer) {
        ObserverInspector inspector(*this);
        inspector.inspect(observer, ObserverInspector::TypeOfVisiting::RecursiveMedia);
    }

    const rpCore::Media::MediaList& RecursiveMediaGetter::get() const {
        return m_medias;
    }

    void RecursiveMediaGetter::visitMedia(const rpCore::Media::MediaProxy &proxy) {
        m_medias.emplace_back(proxy);
    }

    void RecursiveMediaGetter::visitObserver(const rpObservers::Observer &observer) {
        static_cast<void>(observer);
    }

}
}
}
