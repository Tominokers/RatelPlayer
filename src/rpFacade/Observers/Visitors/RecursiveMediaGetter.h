#pragma once

#include "rpFacade/Observers/ObserverVisitor.h"

#include "rpCore/Media/MediaProxy.h"

namespace rpObservers {
    class ObserverState;
}

namespace rpFacade {
namespace Observers {
namespace Visitors {

    /**
     * Visitor which recursively traverses observer
     * and obtains all media item from it.
     */
    class RecursiveMediaGetter final : public ObserverVisitor
    {
    public:
        /**
         * Constructs RecursiveMediaGetter instance via observer state.
         * @param state the state of observer which should be inspected.
         */
        explicit RecursiveMediaGetter(const rpObservers::ObserverState &state);

        /**
         * Constructs RecursiveMediaGetter instance via observer.
         * @param observer the observer which should be inspected.
         */
        explicit RecursiveMediaGetter(rpObservers::Observer &observer);

        /**
         * Gets all media items of observer.
         * @return list of media item.
         */
        const rpCore::Media::MediaList& get() const;

    private:
        virtual void visitMedia(const rpCore::Media::MediaProxy &proxy) override;
        virtual void visitObserver(const rpObservers::Observer &observer) override;

    private:
        rpCore::Media::MediaList m_medias;
    };

}
}
}
