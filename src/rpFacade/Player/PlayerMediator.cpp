#include "PlayerMediator.h"

#include "rpFacade/PlayerClient.h"

#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Tools/ConvertUtilityFunctions.h"
#include "rpCore/Tools/Media/TimeFunctions.h"
#include "rpSettings/SettingStorage.h"

namespace {

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif

    static const std::string playerRepeatModeKey = "RepeatMode";
    static const std::string playerShuffleModeKey = "ShuffleMode";

#ifdef __clang__
#pragma clang diagnostic pop
#pragma clang diagnostic pop
#endif

}

namespace rpFacade {
namespace Player {

    PlayerMediator::PlayerMediator(rpSettings::SettingStorage &settingStorage,
                                   rpFacade::PlayerClient *playerClient)
        : rpCore::Player::PlayerClient(true)
        , m_player()
        , m_playlistManager(*this)
        , m_client(playerClient)
        , m_settings(settingStorage)
    {
        m_player.setClient(this);

        this->loadSettings();
    }

    PlayerMediator::~PlayerMediator() = default;

    PlaylistAssistant& PlayerMediator::getPlaylistAssistant() noexcept {
        return m_playlistManager;
    }

    void PlayerMediator::setClient(rpFacade::PlayerClient *playerClient) {
        m_client = playerClient;
    }

    void PlayerMediator::play() noexcept {
        bool state = m_player.play();
        rpFacade::PlayerState clientState = state ?
            rpFacade::PlayerState::Playing :
            rpFacade::PlayerState::Pause;

        if (m_client)
            m_client->passChangedPlayState(clientState);
    }

    void PlayerMediator::stop() noexcept {
        m_player.stop();

        if (m_client)
            m_client->passChangedPlayState(rpFacade::PlayerState::Stop);
    }

    void PlayerMediator::next() {
        if (m_player.nextTrack() && m_client)
        {
            m_client->passChangedPlayState(rpFacade::PlayerState::Playing);
        }
        else if (m_client)
        {
            m_client->passChangedPlayState(rpFacade::PlayerState::Stop);
        }
    }

    void PlayerMediator::previous() {
        if (m_player.previousTrack() && m_client)
        {
            m_client->passChangedPlayState(rpFacade::PlayerState::Playing);
        }
        else if (m_client)
        {
            m_client->passChangedPlayState(rpFacade::PlayerState::Stop);
        }
    }

    void PlayerMediator::setPosition(double progress) noexcept {
        m_player.setPosition(progress);
    }

    void PlayerMediator::setTrack(TrackIndex index) {
        m_player.setCurrentTrackIndex(index);
    }

    void PlayerMediator::setRepeatMode(RepeatMode mode) noexcept {
        m_settings.store(playerRepeatModeKey, std::to_string(static_cast<int>(mode)));
        m_player.setRepeatMode(mode);
    }

    RepeatMode PlayerMediator::getRepeatMode() const noexcept {
        return m_player.getRepeatMode();
    }

    void PlayerMediator::setShuffleMode(ShuffleMode mode) noexcept {
        m_settings.store(playerShuffleModeKey, std::to_string(static_cast<int>(mode)));
        m_player.setShuffleMode(mode);
    }

    ShuffleMode PlayerMediator::getShuffleMode() const noexcept {
        return m_player.getShuffleMode();
    }

    void PlayerMediator::setProgress(const rpCore::Player::PlayingProgress &progress) noexcept {
        TrackProgress trackProgress;
        if (progress.m_duration.count())
        {
            auto progressSeconds = std::chrono::duration_cast<std::chrono::seconds>(progress.m_position);
            auto durationSeconds = std::chrono::duration_cast<std::chrono::seconds>(progress.m_duration);
            trackProgress.m_position = static_cast<double>(progressSeconds.count()) / durationSeconds.count();
        }
        else
            trackProgress.m_position = 0.0;

        auto position = rpCore::Tools::Media::parseTimeToTuple(progress.m_position);
        trackProgress.m_positionHours = std::get<0>(position);
        trackProgress.m_positionMinutes = std::get<1>(position);
        trackProgress.m_positionSeconds = std::get<2>(position);

        auto duration = rpCore::Tools::Media::parseTimeToTuple(progress.m_duration);
        trackProgress.m_durationHours = std::get<0>(duration);
        trackProgress.m_durationMinutes = std::get<1>(duration);
        trackProgress.m_durationSeconds = std::get<2>(duration);

        m_client->passTrackProgress(trackProgress);
    }

    void PlayerMediator::setTrackIndex(const rpCore::Player::Playlist *playlist, TrackIndex index) {
        if (index == rpCore::Player::NoIndex)
            return;

        m_playlistManager.setTrackIndex(playlist, index);
    }

    void PlayerMediator::handleErrorNotification(const std::string &errorMessage) {
        if (m_client)
            m_client->passErrorNotification(errorMessage);
    }

    void PlayerMediator::loadSettings() {
        auto repeatModeValue = m_settings.load(playerRepeatModeKey);
        if (!repeatModeValue.empty())
        {
            auto mode = static_cast<RepeatMode>(rpCore::Tools::convertTextToInteger(repeatModeValue));
            m_player.setRepeatMode(mode);
        }

        auto shuffleModeValue = m_settings.load(playerShuffleModeKey);
        if (!shuffleModeValue.empty())
        {
            auto mode = static_cast<ShuffleMode>(rpCore::Tools::convertTextToInteger(shuffleModeValue));
            m_player.setShuffleMode(mode);
        }
    }

}
}
