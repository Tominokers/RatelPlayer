#pragma once

#include "rpFacade/PlayerInterface.h"
#include "rpFacade/Player/PlaylistManager.h"

#include "rpCore/Player/CommonPlayer.h"
#include "rpCore/Player/PlayerClient.h"

namespace rpSettings {
    class SettingStorage;
}

namespace rpFacade {
    class FacadeManager;
    class PlayerClient;

namespace Player {
    class PlaylistDecorator;

    /**
     * Entity which implements interacting with player from MediaCore.
     */
    class PlayerMediator final : public PlayerInterface
                               , public rpCore::Player::PlayerClient
    {
        friend rpFacade::FacadeManager;

        explicit PlayerMediator(rpSettings::SettingStorage &settingStorage,
                                rpFacade::PlayerClient *playerClient = nullptr);

    public:
        PlayerMediator(const PlayerMediator&) = delete;
        PlayerMediator(PlayerMediator&&) = delete;
        PlayerMediator& operator=(const PlayerMediator&) = delete;
        PlayerMediator& operator=(PlayerMediator&&) = delete;

        virtual ~PlayerMediator();

        virtual PlaylistAssistant& getPlaylistAssistant() noexcept override;

        /**
         * Sets client for the music player.
         * @param playerClient the player client which obtains changes from player.
         */
        void setClient(rpFacade::PlayerClient *playerClient = nullptr);

        inline rpFacade::PlayerClient* getClient() {
            return m_client;
        }

        inline rpCore::Player::CommonPlayer& getPlayer() {
            return m_player;
        }

        inline rpSettings::SettingStorage& getSettingStorage() {
            return m_settings;
        }

        // --- PlayerInterface ---
        virtual void play() noexcept override;
        virtual void stop() noexcept override;
        virtual void next() override;
        virtual void previous() override;
        virtual void setPosition(double progress) noexcept override;
        virtual void setTrack(TrackIndex index) override;
        virtual void setRepeatMode(RepeatMode mode) noexcept override;
        virtual RepeatMode getRepeatMode() const noexcept override;
        virtual void setShuffleMode(ShuffleMode mode) noexcept override;
        virtual ShuffleMode getShuffleMode() const noexcept override;

    private:
        // --- PlayerClient ---
        virtual void setProgress(const rpCore::Player::PlayingProgress &progress) noexcept override;
        virtual void setTrackIndex(const rpCore::Player::Playlist *playlist, TrackIndex index) override;
        virtual void handleErrorNotification(const std::string &errorMessage) override;

        void loadSettings();

    private:
        rpCore::Player::CommonPlayer m_player;
        PlaylistManager m_playlistManager;

        rpFacade::PlayerClient *m_client;
        rpSettings::SettingStorage &m_settings;
    };

}
}
