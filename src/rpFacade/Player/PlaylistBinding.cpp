#include "PlaylistBinding.h"

#include "rpFacade/PlaylistFiller.h"
#include "rpFacade/Track.h"

#include "rpCore/Media/MediaFile.h"
#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Player/Playlist.h"

#include <numeric>

namespace rpFacade {
namespace Player {

    PlaylistBinding::PlaylistBinding(PlaylistFiller &fillerInstance)
        : m_playlist(std::make_unique<rpCore::Player::Playlist>())
        , m_filler(fillerInstance)
    {
        this->getFiller().clearItems();
        m_playlist->registrateListener(this);
        m_playlist->registrateListener(&m_suggester);
    }

    PlaylistBinding::PlaylistBinding(PlaylistBinding &&binding)
        : m_playlist(std::move(binding.m_playlist))
        , m_filler(binding.m_filler)
    {
        m_playlist->unregistrateListener(&binding);
        m_playlist->unregistrateListener(&binding.m_suggester);

        m_suggester = std::move(binding.m_suggester);

        m_playlist->registrateListener(this);
        m_playlist->registrateListener(&m_suggester);
    }

    PlaylistBinding& PlaylistBinding::operator=(PlaylistBinding &&binding) {
        m_playlist = std::move(binding.m_playlist);
        m_filler = std::move(binding.m_filler);

        m_playlist->unregistrateListener(&binding);
        m_playlist->unregistrateListener(&binding.m_suggester);

        m_suggester = std::move(binding.m_suggester);

        m_playlist->registrateListener(this);
        m_playlist->registrateListener(&m_suggester);

        return *this;
    }

    PlaylistBinding::~PlaylistBinding() =default;

    rpCore::Player::Playlist* PlaylistBinding::get() const {
        return m_playlist.get();
    }

    Track PlaylistBinding::at(TrackIndex index) {
        return Track(m_playlist->at(index).get()->getMediaInfo());
    }

    void PlaylistBinding::addTrack(const rpCore::Media::MediaProxy &file) {
        m_playlist->addTrack(file);
    }

    void PlaylistBinding::insertTracks(TrackIndex index, const rpCore::Media::MediaList &tracks) {
        m_playlist->insertTracks(index, tracks);
    }

    void PlaylistBinding::removeMedia(const TrackIndexList &mediaIndexes) {
        m_playlist->removeTracks(mediaIndexes);
    }

    void PlaylistBinding::clearMedia() {
        m_playlist->clearTracks();
    }

    void PlaylistBinding::sort(rpCore::Player::PlaylistSortType type) {
        m_playlist->sort(type);
    }

    void PlaylistBinding::serialize(std::ostream &stream) const {
        m_playlist->serialize(rpCore::Player::SerializeType::M3U, stream);
    }

    void PlaylistBinding::deserialize(std::istream &stream) {
        this->clearMedia();
        m_playlist->deserialize(rpCore::Player::SerializeType::M3U, stream);
    }

    PlaylistFiller& PlaylistBinding::getFiller() {
        return m_filler.get();
    }

    void PlaylistBinding::reportRemovedMedia(const rpCore::Player::Playlist *changedPlaylist,
                                             rpCore::Player::TrackIndex begin,
                                             rpCore::Player::TrackIndex end) noexcept {
        auto countToRemove = end - begin;
        if (changedPlaylist->size() == countToRemove)
        {
            getFiller().clearItems();
        }
        else
        {
            TrackIndexList items(countToRemove);
            std::iota(std::begin(items), std::end(items), begin);

            getFiller().removeItems(items);
        }
    }

    void PlaylistBinding::reportAddedMedia(const rpCore::Player::Playlist *changedPlaylist,
                                           rpCore::Player::TrackIndex begin,
                                           rpCore::Player::TrackIndex end) noexcept {
        bool isAppending = changedPlaylist->size() == end;

        if (isAppending)
            for (TrackIndex i = begin; i < end; ++i)
                getFiller().appendNewItem(this->at(i));
        else
            for (TrackIndex i = begin; i < end; ++i)
                getFiller().insertNewItem(this->at(i), i);

    }

    void PlaylistBinding::reportPlaylistSorting(const rpCore::Player::Playlist *changedPlaylist) noexcept {
        this->getFiller().clearItems();
        for (TrackIndex i = 0; i < changedPlaylist->size(); ++i)
            getFiller().appendNewItem(this->at(i));
    }

    void PlaylistBinding::reportPlaylistDestroying(rpCore::Player::Playlist *changedPlaylist) noexcept {
        changedPlaylist->unregistrateListener(this);
    }

    void PlaylistBinding::suggestPlaylistName() {
        getFiller().passSuggestedName(m_suggester.getSuggestedName());
    }

}
}
