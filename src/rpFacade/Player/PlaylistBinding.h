#pragma once

#include "rpFacade/FacadeTypes.h"
#include "rpFacade/Player/PlaylistNameSuggester.h"

#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Player/PlaylistChangesListener.h"

#include <functional>
#include <memory>

namespace rpCore {
namespace Media {
    class MediaProxy;
}

namespace Player {
    class Playlist;
}
}

namespace rpFacade {
    class PlaylistFiller;
    class Track;

namespace Player {

    /**
     * Entity which incapsulates filling logic for core playlist and its view.
     * Adding to view is done by interface PlaylistFiller.
     */
    class PlaylistBinding final : public rpCore::Player::PlaylistChangesListener
    {
    public:
        /**
         * Creates PlaylistBinding with empty playlist.
         * @param fillerInstance the filler of view representation.
         */
        explicit PlaylistBinding(PlaylistFiller &fillerInstance);

        /// Move sematic is allowed.
        PlaylistBinding(PlaylistBinding &&binding);

        /// Copy constructor is forbidden.
        PlaylistBinding(const PlaylistBinding&) = delete;

        /// Default destructor.
        virtual ~PlaylistBinding();

        /// Move sematic is allowed.
        PlaylistBinding& operator=(PlaylistBinding &&binding);

        /// Copy assign operator is forbidden.
        PlaylistBinding& operator=(const PlaylistBinding&) =delete;

        /**
         * Gets core playlist.
         * @return pointer to playlist.
         */
        rpCore::Player::Playlist* get() const;

        /**
         * Gets track from playlist at specific index.
         * @param index the index of track in playlist.
         * @return track wrapper.
         */
        Track at(TrackIndex index);

        /**
         * Adds track into end of playlist.
         * @param file the media file to add.
         */
        void addTrack(const rpCore::Media::MediaProxy &file);

        /**
         * Inserts tracks into playlist at specific index.
         * @param index the position to insert.
         * @param tracks the tracks to insert into playlist.
         */
        void insertTracks(TrackIndex index, const rpCore::Media::MediaList &tracks);

        /**
         * Removes specified tracks from playlist.
         * @param mediaIndexes the container of track indexes to remove.
         */
        void removeMedia(const TrackIndexList &mediaIndexes);

        /**
         * Removes all tracks from playlist.
         */
        void clearMedia();

        /**
         * Sorts playlist.
         * @param type the type of sorting.
         */
        void sort(rpCore::Player::PlaylistSortType type);

        /**
         * Serializes content of playlist into stream.
         * @param stream the output stream for serialization.
         */
        void serialize(std::ostream &stream) const;

        /**
         * Deserializs playlist from stream.
         * @param stream the input stream for deserialization.
         */
        void deserialize(std::istream &stream);

        void suggestPlaylistName();

    private:
        inline PlaylistFiller& getFiller();

        // --- PlaylistChangesListener ---
        virtual void reportRemovedMedia(const rpCore::Player::Playlist *changedPlaylist,
                                        TrackIndex begin,
                                        TrackIndex end) noexcept override;
        virtual void reportAddedMedia(const rpCore::Player::Playlist *changedPlaylist,
                                      TrackIndex begin,
                                      TrackIndex end) noexcept override;
        virtual void reportPlaylistSorting(const rpCore::Player::Playlist *changedPlaylist) noexcept override;
        virtual void reportPlaylistDestroying(rpCore::Player::Playlist *changedPlaylist) noexcept override;

    private:
        PlaylistNameSuggester m_suggester;
        std::unique_ptr<rpCore::Player::Playlist> m_playlist;
        std::reference_wrapper<PlaylistFiller> m_filler;
    };

}
}
