#include "PlaylistManager.h"

#include "rpFacade/Bridge/BridgeConnector.h"
#include "rpFacade/Observers/ObserverMediaDeserializer.h"
#include "rpFacade/Player/PlayerMediator.h"
#include "rpFacade/PlayerClient.h"
#include "rpFacade/Track.h"

#include "rpCore/Media/MediaFactory.h"
#include "rpCore/Player/CommonPlayer.h"
#include "rpCore/Player/Playlist.h"
#include "rpCore/Tools/ConvertUtilityFunctions.h"
#include "rpSettings/SettingStorage.h"

#include <algorithm>
#include <cassert>
#include <fstream>

namespace {

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif

    const std::string playlistDirectoryKey = "PlaylistDirectory";
    const std::string playlistCountKey = "PlaylistCount";
    const std::string playlistUsedIndexKey = "PlaylistUsedIndex";
    const std::string playlistPrefix = "Playlist";

#ifdef __clang__
#pragma clang diagnostic pop
#pragma clang diagnostic pop
#endif

}

namespace rpFacade {
namespace Player {

    PlaylistManager::PlaylistManager(PlayerMediator &mediator)
        : m_usedPlaylist(FirstPlaylist)
        , m_openedPlaylist(FirstPlaylist)
        , m_playerMediator(mediator)
    {
    }

    PlaylistManager::~PlaylistManager() {
        this->serializePlaylists();

        m_playerMediator.getSettingStorage().store(playlistUsedIndexKey, std::to_string(m_usedPlaylist));
    }

    void PlaylistManager::loadSerializedPlaylists() {
        if (!m_playerMediator.getClient())
            return;

        auto &storage = m_playerMediator.getSettingStorage();

        auto playlistDirectory = storage.load(playlistDirectoryKey);
        if (playlistDirectory.empty())
            return;

        auto countText = storage.load(playlistCountKey);
        if (countText.empty())
            return;

        auto count = rpCore::Tools::convertTextToInteger(countText);
        for (auto i = FirstPlaylist; i < count; ++i)
        {
            auto playlistFile = this->getPlaylistFileName(playlistDirectory, i);

            std::fstream file(playlistFile, std::ios_base::in);
            if (file.is_open())
            {
                auto playlistIndex = m_playerMediator.getClient()->requestPlaylistCreation();
                m_playlists[playlistIndex].deserialize(file);
                m_playlists[playlistIndex].suggestPlaylistName();
            }
        }

        auto usedPlaylistValue = storage.load(playlistUsedIndexKey);
        if (!usedPlaylistValue.empty())
        {
            auto playlistIndex = rpCore::Tools::convertTextToInteger(usedPlaylistValue);
            assert(playlistIndex < m_playlists.size());
            this->setUsedPlaylist(playlistIndex);
        }
    }

    void PlaylistManager::appendToOpenedPlaylist(const rpCore::Media::MediaProxy &file) {
        if (m_playlists.empty())
            return;

        getOpenedPlaylist().addTrack(file);
        getOpenedPlaylist().suggestPlaylistName();
    }

    void PlaylistManager::appendToOpenedPlaylist(const std::vector<std::string> &files) {
        for (const auto &file : files)
        {
            if (auto media = rpCore::Media::MediaFactory::createMedia(file))
                this->appendToOpenedPlaylist(media);
        }
    }

    void PlaylistManager::insertDelayedMedia(PlaylistIndex playlistIndex, TrackIndex index) {
        auto &bridges = m_playerMediator.getBridges();
        if (bridges.empty())
            return;

        auto action = [this, playlistIndex, index, bridges]()
            {
                for (auto bridge = std::crbegin(bridges);
                     bridge != std::crend(bridges);
                     ++bridge)
                {
                    const auto &delayedMedia = (*bridge)->getDelayedMedia();
                    {
                        std::lock_guard<std::mutex> guard(m_playlistLock);
                        m_playlists[playlistIndex].insertTracks(index, delayedMedia);
                    }
                    m_playlists[playlistIndex].suggestPlaylistName();
                    (*bridge)->clearDelayedMedia();
                }
            };

        m_playlistActions[playlistIndex]->execute(action);
    }

    void PlaylistManager::insertSerializedData(const std::string &data, TrackIndex position) {
        auto mediaList = Observers::ObserverMediaDeserializer::deserializeObserverData(data);
        getOpenedPlaylist().insertTracks(position, mediaList);
        getOpenedPlaylist().suggestPlaylistName();
    }

    void PlaylistManager::insertByCopying(PlaylistIndex source,
                                          PlaylistIndex destination,
                                          const TrackIndexList &mediaIndexes,
                                          TrackIndex position) {
        auto *sourcePlaylist = m_playlists[source].get();
        rpCore::Media::MediaList itemsToAdd;

        std::for_each(std::cbegin(mediaIndexes), std::cend(mediaIndexes),
                      [sourcePlaylist, &itemsToAdd](TrackIndex index)
                      {
                          itemsToAdd.push_back(sourcePlaylist->at(index));
                      });

        m_playlists[destination].insertTracks(position, itemsToAdd);
        m_playlists[destination].suggestPlaylistName();
    }

    void PlaylistManager::removeMedia(PlaylistIndex playlistIndex,
                                      const TrackIndexList &mediaIndexes) {
        m_playlists[playlistIndex].removeMedia(mediaIndexes);
        m_playlists[playlistIndex].suggestPlaylistName();
    }

    PlaylistIndex PlaylistManager::createPlaylist(PlaylistFiller &filler,
                                                  Concurrency::ActionListener &listener) {
        std::lock_guard<std::mutex> guard(m_playlistLock);

        m_playlists.emplace_back(filler);
        m_playlistActions.emplace_back(std::make_unique<Concurrency::ConcurrentAction>(listener));
        return m_playlists.size() - 1;
    }

    void PlaylistManager::closePlaylist(PlaylistIndex index) {
        if (index >= m_playlists.size())
            return;

        if (m_playlists.size() == 1)
        {
            assert(index == FirstPlaylist);
            m_playlists.front().clearMedia();
            return;
        }

        if (auto client = m_playerMediator.getClient())
            client->requestPlaylistClose(index);

        using PlaylistDifferenceType = decltype(m_playlists)::difference_type;
        auto playlistsIt = std::begin(m_playlists) + static_cast<PlaylistDifferenceType>(index);
        m_playlists.erase(playlistsIt);

        using PlaylistActionDifferenceType = decltype(m_playlistActions)::difference_type;
        auto playlistActionIt = std::begin(m_playlistActions)
            + static_cast<PlaylistActionDifferenceType>(index);
        m_playlistActions.erase(playlistActionIt);

        PlaylistIndex newOpened = index > FirstPlaylist ? index - 1 : index;
        this->setOpenedPlaylist(newOpened);

        if (index == m_usedPlaylist)
            this->setUsedPlaylist(newOpened);
    }

    void PlaylistManager::setUsedPlaylist(PlaylistIndex index) noexcept {
        m_usedPlaylist = index;
        m_playerMediator.getPlayer().setPlaylist(m_playlists[index].get());
    }

    PlaylistIndex PlaylistManager::getUsedPlaylistIndex() const noexcept {
        return m_usedPlaylist;
    }

    void PlaylistManager::setOpenedPlaylist(PlaylistIndex index) noexcept {
        m_openedPlaylist = index;
    }

    PlaylistIndex PlaylistManager::getOpenedPlaylistIndex() const noexcept {
        return m_openedPlaylist;
    }

    void PlaylistManager::sortPlaylist(PlaylistIndex index, PlaylistSortType type) noexcept {
        m_playlists[index].sort(type);
    }

    PlaylistBinding& PlaylistManager::getUsedPlaylist() noexcept {
        assert(m_usedPlaylist < m_playlists.size());
        return m_playlists[m_usedPlaylist];
    }

    PlaylistBinding& PlaylistManager::getOpenedPlaylist() noexcept {
        assert(m_openedPlaylist < m_playlists.size());
        return m_playlists[m_openedPlaylist];
    }

    void PlaylistManager::setTrackIndex(const rpCore::Player::Playlist *playlist, TrackIndex index) {
        if (!m_playerMediator.getClient())
            return;

        if (this->getUsedPlaylist().get() != playlist)
        {
            for (auto playlistIndex = FirstPlaylist; playlistIndex < m_playlists.size(); ++playlistIndex)
            {
                if (m_playlists[playlistIndex].get() == playlist)
                {
                    this->setUsedPlaylist(playlistIndex);
                    break;
                }
            }
        }

        assert(m_usedPlaylist < m_playlists.size());
        assert(m_playlists[m_usedPlaylist].get() == playlist);
        auto track = this->getUsedPlaylist().at(index);
        m_playerMediator.getClient()->passCurrentTrack(track, m_usedPlaylist, index);
    }

    void PlaylistManager::serializePlaylists() {
        auto &storage = m_playerMediator.getSettingStorage();
        auto playlistDirectory = storage.load(playlistDirectoryKey);
        if (playlistDirectory.empty())
            return;

        storage.store(playlistCountKey, std::to_string(m_playlists.size()));

        for (auto i = FirstPlaylist; i < m_playlists.size(); ++i)
        {
            auto playlistFile = this->getPlaylistFileName(playlistDirectory, i);
            std::fstream file(playlistFile, std::ios_base::out|std::ios_base::trunc);
            if (file.is_open())
                m_playlists[i].serialize(file);
        }
    }

    std::string PlaylistManager::getPlaylistFileName(const std::string &directory,
                                                     PlaylistIndex index) {
        const std::string playlistM3U(".m3u");
        return directory + playlistPrefix + std::to_string(index) + playlistM3U;
    }

}
}
