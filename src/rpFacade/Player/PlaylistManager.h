#pragma once

#include "rpFacade/Concurrency/ConcurrentAction.h"
#include "rpFacade/Player/PlaylistBinding.h"
#include "rpFacade/PlaylistAssistant.h"

#include <memory>
#include <mutex>
#include <vector>

namespace rpFacade {
namespace Player {

    class PlayerMediator;

    class PlaylistManager final : public PlaylistAssistant
    {
    public:
        explicit PlaylistManager(PlayerMediator &mediator);

        virtual ~PlaylistManager();

        void setTrackIndex(const rpCore::Player::Playlist *playlist, TrackIndex index);

    private:
        // --- from PlaylistAssistant ---
        virtual void loadSerializedPlaylists() override;

        virtual void appendToOpenedPlaylist(const rpCore::Media::MediaProxy &file) override;
        virtual void appendToOpenedPlaylist(const std::vector<std::string> &files) override;

        virtual void insertDelayedMedia(PlaylistIndex playlistIndex, TrackIndex index) override;
        virtual void insertSerializedData(const std::string &data, TrackIndex position) override;
        virtual void insertByCopying(PlaylistIndex source,
                                     PlaylistIndex destination,
                                     const TrackIndexList &mediaIndexes,
                                     TrackIndex position) override;

        virtual void removeMedia(PlaylistIndex playlistIndex,
                                 const TrackIndexList &mediaIndexes) override;

        virtual PlaylistIndex createPlaylist(PlaylistFiller &filler,
                                             Concurrency::ActionListener &listener) override;
        virtual void closePlaylist(PlaylistIndex index) override;

        virtual void setUsedPlaylist(PlaylistIndex index) noexcept override;
        virtual PlaylistIndex getUsedPlaylistIndex() const noexcept override;
        virtual void setOpenedPlaylist(PlaylistIndex index) noexcept override;
        virtual PlaylistIndex getOpenedPlaylistIndex() const noexcept override;

        virtual void sortPlaylist(PlaylistIndex index, PlaylistSortType type) noexcept override;

    private:
        PlaylistBinding& getUsedPlaylist() noexcept;
        PlaylistBinding& getOpenedPlaylist() noexcept;
        void serializePlaylists();
        std::string getPlaylistFileName(const std::string &directory,PlaylistIndex index);

    private:
        std::vector<PlaylistBinding> m_playlists;
        std::vector<std::unique_ptr<Concurrency::ConcurrentAction>> m_playlistActions;
        std::mutex m_playlistLock;
        PlaylistIndex m_usedPlaylist;
        PlaylistIndex m_openedPlaylist;

        PlayerMediator &m_playerMediator;
    };

}
}
