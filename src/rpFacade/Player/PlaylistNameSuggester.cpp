#include "rpFacade/Player/PlaylistNameSuggester.h"

#include "rpCore/Media/MediaFile.h"
#include "rpCore/Player/Playlist.h"

#include <algorithm>

namespace rpFacade {
namespace Player {

    PlaylistNameSuggester::PlaylistNameSuggester() = default;

    PlaylistNameSuggester::PlaylistNameSuggester(PlaylistNameSuggester&&) = default;

    PlaylistNameSuggester& PlaylistNameSuggester::operator=(PlaylistNameSuggester&&) = default;

    PlaylistNameSuggester::~PlaylistNameSuggester() = default;

    std::string PlaylistNameSuggester::getSuggestedName() const {
        if (m_occurrenceCounters.empty())
            return std::string("Empty playlist");

        auto mostMentioned = std::max_element(std::begin(m_occurrenceCounters),
                                              std::end(m_occurrenceCounters),
                                              [](const auto &lhs, const auto &rhs)
                                              {
                                                  return lhs.second < rhs.second;
                                              });
        return mostMentioned->first;
    }

    void PlaylistNameSuggester::reportRemovedMedia(const rpCore::Player::Playlist *changedPlaylist,
                                                   TrackIndex begin,
                                                   TrackIndex end) noexcept {
        for (auto index = begin; index < end; ++index)
        {
            const auto media = changedPlaylist->at(index).get();

            auto &counter = m_occurrenceCounters[media->getArtist()];
            --counter;

            if (counter == 0)
                m_occurrenceCounters.erase(media->getArtist());
        }
    }

    void PlaylistNameSuggester::reportAddedMedia(const rpCore::Player::Playlist *changedPlaylist,
                                                 TrackIndex begin,
                                                 TrackIndex end) noexcept {
        for (auto index = begin; index < end; ++index)
        {
            const auto media = changedPlaylist->at(index).get();
            ++(m_occurrenceCounters[media->getArtist()]);
        }
    }

    void PlaylistNameSuggester::reportPlaylistSorting(const rpCore::Player::Playlist *changedPlaylist) noexcept {
        static_cast<void>(changedPlaylist);
    }

    void PlaylistNameSuggester::reportPlaylistDestroying(rpCore::Player::Playlist *changedPlaylist) noexcept {
        changedPlaylist->unregistrateListener(this);
    }

}
}
