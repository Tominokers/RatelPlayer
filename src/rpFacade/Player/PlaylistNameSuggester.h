#pragma once

#include "rpCore/Player/PlaylistChangesListener.h"
#include "rpFacade/FacadeTypes.h"

#include <string>
#include <unordered_map>

namespace rpFacade {
namespace Player {

    /**
     * Entity which analyses tracks in playlist
     * and suggests name for playlist basing on tracks.
     */
    class PlaylistNameSuggester final : public rpCore::Player::PlaylistChangesListener
    {
    public:
        /// Default constructor.
        PlaylistNameSuggester();

        /// Copy constructor is forbidden.
        PlaylistNameSuggester(const PlaylistNameSuggester&) = delete;

        /// Move constructor
        PlaylistNameSuggester(PlaylistNameSuggester&&);

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        PlaylistNameSuggester& operator=(const PlaylistNameSuggester&) = delete;

        /**
         * Move assign operator.
         * @return
         */
        PlaylistNameSuggester& operator=(PlaylistNameSuggester&&);

        virtual ~PlaylistNameSuggester();

        /**
         * Gets suggested name for playlist.
         * @return suggested name for playlist.
         */
        std::string getSuggestedName() const;

    private:
        // --- PlaylistChangesListener ---
        virtual void reportRemovedMedia(const rpCore::Player::Playlist *changedPlaylist,
                                        TrackIndex begin,
                                        TrackIndex end) noexcept override;
        virtual void reportAddedMedia(const rpCore::Player::Playlist *changedPlaylist,
                                      TrackIndex begin,
                                      TrackIndex end) noexcept override;
        virtual void reportPlaylistSorting(const rpCore::Player::Playlist *changedPlaylist) noexcept override;
        virtual void reportPlaylistDestroying(rpCore::Player::Playlist *changedPlaylist) noexcept override;

    private:
        std::unordered_map<std::string, std::size_t> m_occurrenceCounters;
    };

}
}
