#pragma once

#include "rpFacade/PlayerState.h"
#include "rpFacade/TrackProgress.h"
#include "rpFacade/FacadeTypes.h"

namespace rpFacade {
namespace Player {
    class PlaylistDecorator;
}

    class PlayerInterface;
    class Track;

    /**
     * Interface of client which provides callbacks
     * for handling changes in media player.
     */
    class PlayerClient
    {
    public:
        virtual ~PlayerClient() = default;

        /**
         * Gets player interface which client is referenced to.
         * @return interface of music player.
         */
        virtual PlayerInterface& getPlayer() noexcept =0;

        /**
         * Callback which causes creation of playlist.
         * @return index of new playlist.
         */
        virtual PlaylistIndex requestPlaylistCreation() =0;

        /**
         * Callback which obtains index of playlist which should be closed.
         * @param index the index of playlist to close.
         */
        virtual void requestPlaylistClose(PlaylistIndex index) =0;

        /**
         * Callback which obtains new state of music player.
         * @param state the current state of music player.
         */
        virtual void passChangedPlayState(PlayerState state) noexcept =0;

        /**
         * Callback which provide state of playing current track.
         * @param progress the structure which holds information about track progress.
         */
        virtual void passTrackProgress(const TrackProgress &progress) noexcept =0;

        /**
         * Callback which notifies about current played track in player.
         * @param track instance which holds information about track.
         * @param playlist the number of playlist in its collector.
         * @param item the number of track in playlist.
         */
        virtual void passCurrentTrack(const Track &track, PlaylistIndex playlist, TrackIndex item) noexcept =0;

        /**
         * Callback which notifies about errors during playing.
         * @param errorMessage the text of error message.
         */
        virtual void passErrorNotification(const std::string &errorMessage) noexcept =0;
    };

}
