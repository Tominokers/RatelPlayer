#pragma once

#include "rpFacade/Bridge/BridgeAble.h"
#include "rpFacade/FacadeTypes.h"

#include <memory>

namespace rpCore {
namespace Media {
    class MediaProxy;
}
}

namespace rpFacade {

    class PlaylistAssistant;

    /**
     * Entity which provides common interface for music player.
     * It takes care about state of playing music.
     */
    class PlayerInterface : public Bridge::BridgeAble
    {
    public:
        /// Default constructor.
        PlayerInterface() = default;

        /// Copy constructor is forbidden.
        PlayerInterface(const PlayerInterface&) = delete;

        /// Move semantic is not allowed.
        PlayerInterface(PlayerInterface&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        PlayerInterface& operator=(const PlayerInterface&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        PlayerInterface& operator=(PlayerInterface&&) = delete;

        /// Default descructor.
        virtual ~PlayerInterface() = default;

        /**
         * Get instance of PlaylistAssistant.
         * @return reference to PlaylistAssistant.
         */
        virtual PlaylistAssistant& getPlaylistAssistant() noexcept =0;

        /**
         * Requests playing state of current track in current playlist.
         * @remark Call of this method in PlayerState::Playing state pauses current track.
         * Next call resumes playing from paused possition.
         */
        virtual void play() noexcept =0;

        /**
         * Requests full stop of playing current track.
         */
        virtual void stop() noexcept =0;

        /**
         * Switch playing from current track to some next track in current playlist.
         * @remark which next track is defined by settings in player.
         */
        virtual void next() =0;

        /**
         * Switch playing from current track to some previous track in current playlist.
         * @remark which previous track is defined by settings in player.
         */
        virtual void previous() =0;

        /**
         * Sets current progress of played track.
         * @param progress the new progress of played track between 0 and 1.
         */
        virtual void setPosition(double progress) noexcept =0;

        /**
         * Sets current track in current playlist to play.
         * @param index the new index of played track.
         */
        virtual void setTrack(TrackIndex index) =0;

        /**
         * Sets type of repeating mode.
         * @param mode the mode of repeating to set.
         */
        virtual void setRepeatMode(RepeatMode mode) noexcept =0;

        /**
         * Gets repeating mode of player.
         * @return the current repeat mode.
         */
        virtual RepeatMode getRepeatMode() const noexcept =0;

        /**
         * Sets type of shuffle mode.
         * @param mode the mode of shuffling to set.
         */
        virtual void setShuffleMode(ShuffleMode mode) noexcept =0;

        /**
         * Gets shuffle mode of player.
         * @return the current shuffle mode.
         */
        virtual ShuffleMode getShuffleMode() const noexcept =0;

    };

}
