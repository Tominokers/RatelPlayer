#pragma once

namespace rpFacade {

    /**
     * Enum of possible player states.
     */
    enum class PlayerState
    {
        /**
         * Player is playing music.
         */
        Playing,
        /**
         * Player is paused on current track.
         */
        Pause,
        /**
         * Player is stopped playing current track.
         */
        Stop
     };

}
