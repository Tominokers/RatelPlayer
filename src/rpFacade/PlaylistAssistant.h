#pragma once

#include "rpFacade/FacadeTypes.h"

#include <string>

namespace rpCore {
namespace Media {
    class MediaProxy;
}
}

namespace rpFacade {
    namespace Concurrency {
        class ActionListener;
    }

    class PlaylistFiller;

    /**
     * Entity which manages control of playler's playlists.
     * It allow's to create, remove and change their content.
     * Playlist enity is not provided for client code.
     * Instead of it client code should operate with their indexes.
     */
    class PlaylistAssistant
    {
    public:
        /// Default ctor.
        PlaylistAssistant() = default;

        /// Copy constructor is forbidden.
        PlaylistAssistant(const PlaylistAssistant&) = delete;

        /// Move semantic is not allowed.
        PlaylistAssistant(PlaylistAssistant&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        PlaylistAssistant& operator=(const PlaylistAssistant&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        PlaylistAssistant& operator=(PlaylistAssistant&&) = delete;

        /// Default destructor.
        virtual ~PlaylistAssistant() = default;

        /**
         * Loads playlists from serialized data.
         */
        virtual void loadSerializedPlaylists() =0;

        /**
         * Adds passed track into playlist which is current opened.
         * @param file the media file which should be added in playlist.
         */
        virtual void appendToOpenedPlaylist(const rpCore::Media::MediaProxy &file) =0;

        /**
         * Adds passed tracks into playlist which is current opened.
         * @param files uri strings of the media files which should be added in playlist.
         */
        virtual void appendToOpenedPlaylist(const std::vector<std::string> &files) =0;

        /**
         * Inserts delayed tracks into playlist after index position.
         * @param playlistIndex the index of playlist to insert in it.
         * @param index the position to insert after in playlist.
         */
        virtual void insertDelayedMedia(PlaylistIndex playlistIndex, TrackIndex index) =0;

        /**
         * Inserts media items which will be deserialized from text data into specific position.
         * @param data the text data with serialized media items.
         * @param position the position to insert into.
         */
        virtual void insertSerializedData(const std::string &data, TrackIndex position) =0;

        /**
         * Inserts tracks by copying them from another playlist by indexes.
         * @param source the source playlist from which tracks will be copied.
         * @param destination the destination playlist to which tracks will be copied.
         * @param mediaIndexes the indexes of tracks in source playlist.
         * @param position the position in destination playlist into which tracks should be copied.
         */
        virtual void insertByCopying(PlaylistIndex source,
                                     PlaylistIndex destination,
                                     const TrackIndexList &mediaIndexes,
                                     TrackIndex position) =0;


        /**
         * Removes tracks from specified playlist.
         * @param playlistIndex the index of playlist.
         * @param mediaIndexes the container of track indexes to remove.
         */
        virtual void removeMedia(PlaylistIndex playlistIndex,
                                 const TrackIndexList &mediaIndexes) =0;

        /**
         * Creates new playlist.
         * @param filler the instance of entity which can fill playlist view representation
         * @param listener the instance of listener for concurrent actions on playlist.
         * @return index of new playlist.
         */
        virtual PlaylistIndex createPlaylist(PlaylistFiller &filler,
                                             Concurrency::ActionListener &listener) =0;

        /**
         * Closes playlist by index.
         * @param index index of plalist to close.
         * @remark If index is out of range, it does nothing.
         * Also player always has at least one playlist,
         * beacause of this closing clears last playlist instead its removing.
         */
        virtual void closePlaylist(PlaylistIndex index) =0;

        /**
         * Sets playlist by index to play it by media player.
         * @param index the index of playlist to play.
         * @remark can be different from playlist to add new media items.
         */
        virtual void setUsedPlaylist(PlaylistIndex index) noexcept =0;

        /**
         * Gets index of playlist which is currently played.
         * @return index of currently played playlist.
         */
        virtual PlaylistIndex getUsedPlaylistIndex() const noexcept =0;

        /**
         * Sets playlist by index to which new media items will be added.
         * @param index the index of playlist to add new media files.
         * @remark can be different from used playlist to play.
         */
        virtual void setOpenedPlaylist(PlaylistIndex index) noexcept =0;

        /**
         * Gets index of playlist which is currently opened.
         * @return index of currently opened playlist.
         */
        virtual PlaylistIndex getOpenedPlaylistIndex() const noexcept =0;

        /**
         * Sorts playlist by required condition.
         * @param index the index of playlist to sort.
         * @param type the type of sorting.
         */
        virtual void sortPlaylist(PlaylistIndex index, PlaylistSortType type) noexcept =0;

    };

}
