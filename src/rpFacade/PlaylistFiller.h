#pragma once

#include "rpFacade/FacadeTypes.h"

#include <string>

namespace rpFacade {

    class Track;

    /**
     * Entity which provides interface to fill playlist on some view.
     */
    class PlaylistFiller
    {
    public:
        /// Default destructor.
        virtual ~PlaylistFiller() = default;

        /**
         * Clears content of playlist view.
         */
        virtual void clearItems() =0;

        /**
         * Appends new item to the end of playlist view.
         * @param track the information about track to append.
         */
        virtual void appendNewItem(const Track &track) =0;

        /**
         * Inserts new items to specific position of playlist view.
         * @param track the information about track to insert.
         * @param position the position of inserting.
         */
        virtual void insertNewItem(const Track &track, TrackIndex position) =0;

        /**
         * Removes items from playlist.
         * @param items the container of item indexes to remove.
         */
        virtual void removeItems(const TrackIndexList &items) =0;

        /**
         * Passes suggested name of playlist
         * after add/remov actions were performed on it.
         * @param name the suggested name for playlist.
         */
        virtual void passSuggestedName(const std::string &name) =0;

    };

}
