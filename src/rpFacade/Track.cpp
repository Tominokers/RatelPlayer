#include "Track.h"

#include "rpCore/Media/MediaFile.h"

namespace rpFacade {

    Track::Track(const rpCore::Media::MediaInformation &mediaInfo) noexcept
        : m_info(mediaInfo)
    {
    }

    Track::Track(Track &&) =default;

    const std::string& Track::getArtist() const noexcept {
        return m_info.getArtist();
    }

    const std::string& Track::getAlbum() const noexcept {
        return m_info.getAlbum();
    }

    const std::string& Track::getSongTitle() const noexcept {
        return m_info.getSongTitle();
    }

    const std::string& Track::getYear() const noexcept {
        return m_info.getYear();
    }

    const std::string& Track::getTrackNumber() const noexcept {
        return m_info.getTrackNumber();
    }

    const std::string& Track::getTrackDuration() const noexcept {
        return m_info.getTrackDuration();
    }

    std::ostream& operator<<(std::ostream& os, const Track& item) {
        return os << "[[" << item.getTrackNumber() << "] ["
                  << item.getSongTitle() << "] ["
                  << item.getAlbum() << "] ["
                  << item.getArtist() << "] ["
                  << item.getYear() << "]]";
    }

}
