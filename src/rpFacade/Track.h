#pragma once

#include <ostream>
#include <string>

namespace rpCore {
namespace Media {
    class MediaInformation;
}
}

namespace rpFacade {
namespace Player {
    class PlaylistBinding;
}

    /**
     * Entity which holds information about media item.
     */
    class Track final
    {
        /// Only PlaylistBinding can create Track instances.
        friend class Player::PlaylistBinding;

    public:
        /// Copy constructor is forbidden.
        Track(const Track&) =delete;

        /**
         * Move sematic is allowed.
         * @param instance the instance to move
         */
        Track(Track &&instance);

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        Track& operator=(const Track&) =delete;

        /**
         * Move sematic is allowed.
         * @param instance the instance to move.
         * @return
         */
        Track& operator=(Track &&instance) =delete;

        /**
         * Gets name of artist.
         * @return artist name as a string.
         */
        const std::string& getArtist() const noexcept;

        /**
         * Gets album title of track.
         * @return the album title as a string.
         */
        const std::string& getAlbum() const noexcept;

        /**
         * Gets title of song.
         * @return the title of song as a string.
         */
        const std::string& getSongTitle() const noexcept;

        /**
         * Gets year of track.
         * @return the year of track.
         */
        const std::string& getYear() const noexcept;

        /**
         * Gets position in album track list.
         * @return number position as a string.
         */
        const std::string& getTrackNumber() const noexcept;

        /**
         * Gets duration of track.
         * @return track duration as string.
         */
        const std::string& getTrackDuration() const noexcept;

    private:
        explicit Track(const rpCore::Media::MediaInformation &mediaInfo) noexcept;

    private:
        const rpCore::Media::MediaInformation &m_info;

    };

    std::ostream& operator<<(std::ostream& os, const Track& item);

}
