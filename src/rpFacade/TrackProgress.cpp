#include "TrackProgress.h"

namespace rpFacade {

    std::ostream& operator<<(std::ostream& os, const TrackProgress& progress) {
        return os << "[ " << progress.m_positionHours << ":"
                  << progress.m_positionMinutes << ":"
                  << progress.m_positionSeconds << " / "
                  << progress.m_durationHours << ":"
                  << progress.m_durationMinutes << ":"
                  << progress.m_durationSeconds << " ]";
    }

}
