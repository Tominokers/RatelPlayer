#pragma once

#include <ostream>
#include <string>

namespace rpFacade {

    /**
     * The scruct which holds necessary information
     * about progress of playing current track.
     * Most of information is represented as string.
     * Combination positionHours + positionMinutes + positionSeconds
     * shows current position in track as a string.
     * Combination durationHours + durationMinutes + durationSeconds
     * shows duration of track as a string.
     */
    struct TrackProgress
    {
        /// Position of playing which is represented as number between 0 and 1.
        double m_position;
        /// Seconds of current position.
        std::string m_positionSeconds;
        /// Minutes of current position.
        std::string m_positionMinutes;
        /// Hours of current position.
        std::string m_positionHours;
        /// Seconds of track duration.
        std::string m_durationSeconds;
        /// Minutes of track duration.
        std::string m_durationMinutes;
        /// Hours of track duration.
        std::string m_durationHours;
    };

    std::ostream& operator<<(std::ostream& os, const TrackProgress& progress);

}
