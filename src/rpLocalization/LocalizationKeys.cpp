#include "LocalizationKeys.h"

#include <unordered_map>

namespace rpLocalization {

    static std::unordered_map<std::wstring, LocalizerKeys> *localizationKeys = nullptr;

    void initKeys() {
#define TOWIDE(A) L##A
#define X(a) std::make_pair(TOWIDE(#a), LocalizerKeys::a),
        localizationKeys = new std::unordered_map<std::wstring, LocalizerKeys>({ LOC_KEYS_TABLE });
#undef X
#undef TOWIDE
    }

    void deinitKeys() {
        delete localizationKeys;
    }

    std::pair<LocalizerKeys, std::wstring> parseLine(const std::wstring &line) {
        static wchar_t separator = '=';
        auto key = LocalizerKeys::NO_VALUE;
        auto value = std::wstring();

        auto pos = line.find(separator);
        auto stringKey = pos != std::wstring::npos ? line.substr(0, pos) : std::wstring();
        if (localizationKeys->find(stringKey) != std::end(*localizationKeys))
        {
            key = localizationKeys->operator[](stringKey);
            value = line.substr(pos+1);
        }

        return std::make_pair(key, value);
    }

}
