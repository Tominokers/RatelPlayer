#pragma once

#include <string>
#include <utility>

namespace rpLocalization {

#define LOC_KEYS_TABLE \
    X(AddMedia) \
    X(OpenInNewPanel) \
    X(Quit) \
    X(NoRepeatMode) \
    X(WholePlaylistRepeat) \
    X(TurnOffShuffling) \
    X(ShufflingByTrack) \
    X(OpenSettings) \
    X(OpenAbout) \
    X(ProgramInfo) \
    X(Icon) \
    X(FileName) \
    X(FileDate) \
    X(TrackNumber) \
    X(SongTitle) \
    X(ArtistTitle) \
    X(AlbumTitle) \
    X(TrackDate) \
    X(Duration) \
    X(DefaultDirectoryTap) \
    X(DefaultPlaylistTap) \
    X(RepositoryInfo) \
    X(RemoveMedia) \
    X(RepeatTrack) \

#define X(a) a,
    enum class LocalizerKeys
    {
        NO_VALUE = 0,
        LOC_KEYS_TABLE
        SIZE
    };
#undef X

    static constexpr auto LocalizerKeysCount = static_cast<std::size_t>(LocalizerKeys::SIZE);

    void initKeys();

    void deinitKeys();

    std::pair<LocalizerKeys, std::wstring> parseLine(const std::wstring &line);

}
