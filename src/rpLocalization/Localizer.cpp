#include "Localizer.h"

#include <array>
#include <cassert>
#include <clocale>
#include <codecvt>
#include <fstream>
#include <stdexcept>

namespace rpLocalization {

    LocalizerImpl* Localizer::m_impl = nullptr;

    class LocalizerImpl
    {
    public:
        LocalizerImpl(const std::string &dicPath, Language lang)
            : m_path(dicPath)
            , m_defaultLang(lang)
            , m_currentLang(m_defaultLang)
        {
            m_aliases[0] = std::make_pair(std::string("en_"), Language::EN);
            m_aliases[1] = std::make_pair(std::string("English"), Language::EN);
            m_aliases[2] = std::make_pair(std::string("uk_UA"), Language::UA);
            m_aliases[3] = std::make_pair(std::string("Ukrainian"), Language::UA);
            m_aliases[4] = std::make_pair(std::string("ru_"), Language::RU);
            m_aliases[5] = std::make_pair(std::string("Russian"), Language::RU);

            this->update();
        }

        void setDefaultLanguage(Language lang) {
            if (lang != m_defaultLang)
            {
                m_defaultLang = lang;
                this->update();
            }
        }

        Language getLanguage() const {
            return m_currentLang;
        }

        void setDictionariesLocation(const std::string &dicPath) {
            if (m_path != dicPath)
            {
                m_path = dicPath;
                this->update();
            }
        }

        const std::wstring& get(LocalizerKeys key) const {
            return m_dictionary[static_cast<std::size_t>(key)];
        }


        void update() {
            m_currentLang = m_defaultLang;
            if (m_currentLang == Language::AUTO)
            {
                std::setlocale(LC_ALL, "");
                std::string currentLocale = std::setlocale(LC_ALL, NULL);
                m_currentLang = determineLanguage(currentLocale);
            }

            if (m_path.empty())
                return;

            switch (m_currentLang)
            {
            case Language::EN:
                this->loadDictionary("en.dic");
                break;
            case Language::UA:
                this->loadDictionary("ua.dic");
                break;
            case Language::RU:
                this->loadDictionary("ru.dic");
                break;
            case Language::AUTO:
                this->loadDictionary("en.dic");
            }
        }

    private:
        Language determineLanguage(const std::string &languageRepresentation) {
            for (auto &it : m_aliases)
            {
                auto alias = it.first;
                if (languageRepresentation.compare(0, alias.size(), alias) == 0)
                    return it.second;
            }

            return Language::EN;
        }

        void loadDictionary(const std::string &file) {
            std::fill(std::begin(m_dictionary), std::end(m_dictionary), std::wstring());

            auto fullFilePath = m_path + file;
            std::wfstream fs(fullFilePath, std::ios::in);
            if (!fs.is_open())
                return;

            fs.imbue(std::locale(fs.getloc(), new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>()));

            std::array<wchar_t, 256> buffer;
            while(fs.good())
            {
                fs.getline(buffer.data(), buffer.size());
                std::wstring line(buffer.data());
                auto pair = parseLine(line);
                if (pair.first != LocalizerKeys::NO_VALUE)
                    m_dictionary[static_cast<std::size_t>(pair.first)] = pair.second;
            }

            fs.close();
        }

    private:
        std::string m_path;
        Language m_defaultLang;
        Language m_currentLang;
        std::array<std::wstring, LocalizerKeysCount> m_dictionary;
        std::array<std::pair<std::string, Language>, 6> m_aliases;
    };

    Localizer::Localizer(const std::string &dicPath, Language lang)
    {
        if (!m_impl)
        {
            rpLocalization::initKeys();
            m_impl = new LocalizerImpl(dicPath, lang);
        }
        else
        {
            throw std::domain_error("The second instance of Localizer is not allowed if the first one is not destroyed.");
        }
    }

    Localizer::~Localizer() {
        deinitKeys();
        delete m_impl;
        m_impl = nullptr;
    }

    void Localizer::setDefaultLanguage(Language lang) {
        assert(m_impl != nullptr);
        m_impl->setDefaultLanguage(lang);
    }

    Language Localizer::getLanguage() {
        assert(m_impl != nullptr);
        return m_impl->getLanguage();
    }

    void Localizer::setDictionariesLocation(const std::string &dicPath) {
        assert(m_impl != nullptr);
        m_impl->setDictionariesLocation(dicPath);
    }

    const std::wstring& Localizer::get(LocalizerKeys key) {
        assert(m_impl != nullptr);
        return m_impl->get(key);
    }

}
