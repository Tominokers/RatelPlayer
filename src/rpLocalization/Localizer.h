#pragma once

#include "rpLocalization/LocalizationKeys.h"

#include <string>

namespace rpLocalization {

    enum class Language
    {
        EN,
        UA,
        RU,
        AUTO
    };

    class LocalizerImpl;

    /**
     * Singleton with manual control of life-time which provides localization functional.
     * Before calling to static methods you should create instance of Localizer
     * and keep it alive till you work with static methods.
     * Creation of the second instance of Localizer is allowed only if the first one is destoyed.
     * Otherwise constructor of Localizer will throw std::domain_error.
     * To work properly with localization client code should specify language to use
     * and where it can find dictionaries of localized string.
     * If user specifies Language::AUTO, Localizer will try to determine language of system.
     * If such operation fails, it will use english dictionary.
     * Dictionaries have name by mask: [Language item value in lower case].dic
     * Keys for localized strings are defined as macro LOC_KEYS_TABLE in LocalizationKeys header.
     */
    class Localizer final
    {
    public:
        /**
         * Constructs instance of Localizer.
         * @param dicPath the path to directory with dictionaries of localized keys.
         * @param lang the language of dictionary to use.
         * @throw std::domain_error if constructor is called before previous instnace is not destroyed.
         */
        Localizer(const std::string &dicPath, Language lang = Language::AUTO);

        ~Localizer();

        /// Copy constructor is forbidden.
        Localizer(const Localizer&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        Localizer& operator=(const Localizer&) = delete;

        /**
         * Sets globaly new default language.
         * @param lang the language of dictionaly to use.
         */
        static void setDefaultLanguage(Language lang);

        /**
         * Gets current used language.
         * @return language value which is not Language::AUTO,
         * because in such case it should be determined automatically.
         */
        static Language getLanguage();

        /**
         * Sets new path to dictionaries.
         * @param dicPath the new path to dictionaries.
         */
        static void setDictionariesLocation(const std::string &dicPath);

        /**
         * Gets localized string value by key.
         * @param key the key of localized string.
         * @return localized string if key is valid, otherwise empty string.
         */
        static const std::wstring& get(LocalizerKeys key);

    private:
        static LocalizerImpl *m_impl;
    };

}
