#include "Observer.h"

#include "rpCore/Media/MediaFactory.h"

namespace rpObservers {

    Observer::Observer() = default;

    bool Observer::isSupported(const std::string &uri) const {
        return rpCore::Media::MediaFactory::isSupported(uri);
    }

    rpCore::Media::MediaProxy Observer::extractMedia(const std::string &uri) const {
        return rpCore::Media::MediaFactory::createMedia(uri);
    }

    const ObserverInformation& Observer::getInfo() const noexcept {
        return m_observerInfo;
    }

}
