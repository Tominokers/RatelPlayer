#pragma once

#include "rpObservers/ObserverInformation.h"

#include <memory>

namespace rpCore {
namespace Media {
    class MediaProxy;
}
}

namespace rpObservers {

    /**
     * Entity which decribes common interface of media observer.
     * Media observer provides methods to get MediaFiles and subobservers.
     */
    class Observer
    {
    public:
        /// Default constructor.
        Observer();

        /// Copy constructor is forbidden.
        Observer(const Observer&) = delete;

        /// Move semantic is not allowed.
        Observer(Observer&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        Observer& operator=(const Observer&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        Observer& operator=(Observer&&) = delete;

        /// Default destructor of observer.
        virtual ~Observer() = default;

        /**
         * Gets short name of an observer.
         * @return name of an observer.
         */
        virtual std::string getName() const noexcept =0;

        /**
         * Gets full name of observer.
         * @return full name of an observer.
         */
        virtual std::string getFullName() const noexcept =0;

        /**
         * Gets date of last modification of observer.
         * @return date of last modification.
         */
        virtual std::string getDate() const noexcept =0;

        /**
         * Gets general information about container.
         * @return reference to observer information.
         */
        const ObserverInformation& getInfo() const noexcept;

        /**
         * Updates content of Observer.
         * @remark In future must be changed on some async algorithm.
         */
        virtual void Update() =0;

        /**
         * Switchs internal state of observer to parrent observer.
         * @return true if state is changed, false otherwise.
         */
        virtual bool makeParentState() =0;

        /**
         * Gets count of MediaFiles in Observer.
         * @return count of MediaFiles.
         */
        virtual std::size_t getMediaCount() const noexcept =0;

        /**
         * Extracts MediaFile from Observer.
         * @param i index of MediaFile in Observer.
         * @return pointer on new MediaFile.
         * @remark Observer mustn't hold MediaFile instance.
         * Usually it returns new instance of MediaFile every time.
         * So client code must manage lifetime of this instance.
         */
        virtual rpCore::Media::MediaProxy extractMediaAt(std::size_t i) const =0;

        /**
         * Gets count of subobservers in Observer.
         * @return count of subobservers.
         */
        virtual std::size_t getSubobserversCount() const noexcept =0;

        /**
         * Extracts subobserver from Observer.
         * @param i index of subobserver.
         * @return pointer on new Observer.
         * @remark Observer mustn't hold new Observer instance.
         * Usually it returns new instance of Observer every time.
         * So client code must manage lifetime of this instance.
         */
        virtual std::unique_ptr<Observer> extractObserverAt(size_t i) const =0;

    protected:
        /**
         * Checks whether media item by URI is supported.
         * @param uri the location of media item.
         * @return true if media item is supported, false otherwise.
         */
        bool isSupported(const std::string &uri) const;

        /**
         * Creates MediaProxy instance of media item by specific URI.
         * @param uri the location of media item.
         * @return instance of MediaProxy which holds media item which located by URI.
         */
        rpCore::Media::MediaProxy extractMedia(const std::string &uri) const;

    protected:
        /// Default observer information.
        ObserverInformation m_observerInfo;
    };

}
