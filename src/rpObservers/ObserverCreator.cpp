#include "ObserverCreator.h"

#include "rpObservers/ObserverTypes.h"
#include "rpObservers/Observers/DirectoryObserver.h"
#include "rpObservers/States/DirectoryState.h"

namespace rpObservers {

    std::unique_ptr<Observer> ObserverCreator::create(const ObserverState &state) {
        std::unique_ptr<Observer> observer;
        switch(state.getType())
        {
        case ObserverType::Directory:
            const auto& dirState = static_cast<const States::DirectoryState&>(state);

            const auto &directory = dirState.getDirectory();
            if (States::DirectoryState::isDirectoryUri(directory))
                observer = Observers::DirectoryObserver::createFromUri(directory);
            else
                observer = Observers::DirectoryObserver::createFromPath(directory);
        }

        return observer;
    }
}
