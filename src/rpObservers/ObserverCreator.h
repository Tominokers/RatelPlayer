#pragma once

#include "rpObservers/Observer.h"
#include "rpObservers/ObserverState.h"

#include <memory>

namespace rpObservers {

    /**
     * Entity which gets observers from its states.
     */
    class ObserverCreator
    {
    public:
        /**
         * Creates observer instance from its state.
         * @param state is a information about observer to create.
         * @return new observer instance if creation is successfully processed, otherwise nullptr.
         * @remark client side code must manage lifetime of returned instance.
         */
        static std::unique_ptr<Observer> create(const ObserverState &state);

    };

}
