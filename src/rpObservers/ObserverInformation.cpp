#include "ObserverInformation.h"

namespace rpObservers {

    ObserverInformation::ObserverInformation() = default;

    const std::string& ObserverInformation::getName() const noexcept {
        return m_observerName;
    }

    const std::string& ObserverInformation::getFullName() const noexcept {
        return m_observerFullName;
    }

    const std::string& ObserverInformation::getDate() const noexcept {
        return m_observerDate;
    }

    void ObserverInformation::setName(const std::string& name) noexcept {
        m_observerName = name;
    }

    void ObserverInformation::setFullName(const std::string& fullName) noexcept {
        m_observerFullName = fullName;
    }

    void ObserverInformation::setDate(const std::string& date) noexcept {
        m_observerDate = date;
    }

}
