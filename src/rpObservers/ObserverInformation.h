#pragma once

#include <string>

namespace rpObservers {

    /**
     * Entity which holds information about observer.
     */
    class ObserverInformation final
    {
    public:
        /**
         * Constructs default information about observer.
         */
        ObserverInformation();

        /// Copy operation is forbidden.
        ObserverInformation(const ObserverInformation&) = delete;
        /**
         * Assign operator is forbidden.
         * @return
         */
        ObserverInformation& operator=(const ObserverInformation&) = delete;

        /**
         * Gets short name of an observer.
         * @return name of an observer.
         */
        const std::string& getName() const noexcept;

        /**
         * Gets full name of observer.
         * @return full name of an observer.
         */
        const std::string& getFullName() const noexcept;

        /**
         * Gets date of last modification of observer.
         * @return date of last modification.
         */
        const std::string& getDate() const noexcept;

        /**
         * Sets short name of an observer.
         * @param name the name of an observer.
         */
        void setName(const std::string& name) noexcept;

        /**
         * Sets full name of observer.
         * @param fullName the full name of an observer.
         */
        void setFullName(const std::string& fullName) noexcept;

        /**
         * Sets date of last modification of observer.
         * @param date the date of last modification.
         */
        void setDate(const std::string& date) noexcept;

    private:
        std::string m_observerName;
        std::string m_observerFullName;
        std::string m_observerDate;
    };

}
