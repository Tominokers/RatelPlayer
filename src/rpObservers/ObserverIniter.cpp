#include "ObserverIniter.h"

#include <giomm/init.h>

namespace rpObservers {

    ObserverIniter::ObserverIniter() noexcept
        : m_isInitialized(false)
    {
    }

    ObserverIniter::~ObserverIniter() noexcept {
        deinit();
    }

    void ObserverIniter::init() noexcept {
        if (!m_isInitialized)
        {
            Gio::init();
            m_isInitialized = true;
        }
    }

    void ObserverIniter::deinit() noexcept {
        m_isInitialized = false;
    }

}
