#pragma once

namespace rpObservers {

    /**
     * Entity which initialize and deinitialize
     * resources of observer library.
     */
    class ObserverIniter final
    {
    public:
        /**
         * Constructs instance of ObserverIniter class.
         */
        ObserverIniter() noexcept;

        /// Copy constructor is forbidden.
        ObserverIniter(const ObserverIniter&) = delete;

        /// Move semantic is not allowed.
        ObserverIniter(ObserverIniter&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        ObserverIniter& operator=(const ObserverIniter&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        ObserverIniter& operator=(ObserverIniter&&) = delete;

        /**
         * Destructs instance of ObserverIniter class.
         * If it is uninitialized, than it will be performed.
         */
        ~ObserverIniter() noexcept;

        /**
         * Intialize observer library.
         */
        void init() noexcept;

        /**
         * Deinitialize observer library.
         */
        void deinit() noexcept;

    private:
        bool m_isInitialized;
    };

}
