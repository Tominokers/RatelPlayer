#include "ObserverState.h"

namespace rpObservers {

    ObserverState::ObserverState(ObserverType observerType)
        : m_type(observerType)
    {
    }

    ObserverType ObserverState::getType() const noexcept {
        return m_type;
    }
}
