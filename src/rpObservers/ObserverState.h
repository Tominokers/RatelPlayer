#pragma once

#include "rpObservers/ObserverTypes.h"

namespace rpObservers {

    /**
     * Basic entity which holds type of observer for which state stands for.
     */
    class ObserverState
    {
    public:
        /// Copy constructor is forbidden.
        ObserverState(const ObserverState&) = delete;

        /// Move semantic is not allowed.
        ObserverState(ObserverState&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        ObserverState& operator=(const ObserverState&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        ObserverState& operator=(ObserverState&&) = delete;

        /**
         * Destructor of ObserverState.
         */
        virtual ~ObserverState() = default;

        /**
         * Gets type of observer.
         * @return type of observer.
         */
        ObserverType getType() const noexcept;

    protected:
        /**
         * Constructs state of observer.
         * @param observerType the type of observer.
         */
        explicit ObserverState(ObserverType observerType);

    private:
        ObserverType m_type;
    };

}
