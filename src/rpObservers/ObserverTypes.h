#pragma once

namespace rpObservers {

    /**
     * Entity which enumerate supported observers.
     */
    enum class ObserverType
    {
        /**
         * Type of observer which can handle local file system.
         */
        Directory
    };

}
