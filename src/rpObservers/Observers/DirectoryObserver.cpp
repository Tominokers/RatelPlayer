#include "DirectoryObserver.h"

#include "rpCore/Media/MediaProxy.h"

#include <glibmm/miscutils.h>

#include <algorithm>
#include <ctime>

namespace rpObservers {
namespace Observers {

    const static char pathSeparator = '/';

    DirectoryObserver::DirectoryObserver(const Glib::RefPtr<Gio::File> &directory)
        : m_file(directory)
    {
        m_observerInfo.setName(m_file->get_basename());
        m_observerInfo.setFullName(m_file->get_path());
        m_observerInfo.setDate(getDate());
    }

    std::string DirectoryObserver::getName() const noexcept {
        return m_file->get_basename();
    }

    std::string DirectoryObserver::getFullName() const noexcept {
        return m_file->get_path();
    }

    std::string DirectoryObserver::getDate() const noexcept {
        Glib::RefPtr<Gio::FileInfo> info = m_file->query_info(G_FILE_ATTRIBUTE_TIME_CHANGED);

        auto date = static_cast<std::time_t>(info->get_attribute_uint64(G_FILE_ATTRIBUTE_TIME_CHANGED));

        char raw[50];
        std::strftime(raw, sizeof(raw), "%d-%m-%Y", std::localtime(&date));

        return std::string(raw);
    }

    std::unique_ptr<DirectoryObserver> DirectoryObserver::createFromPath(const std::string &path) {
        Glib::RefPtr<Gio::File> file = Gio::File::create_for_path(path);
        return createHelper(file);
    }

    std::unique_ptr<DirectoryObserver> DirectoryObserver::createFromUri(const std::string &uri) {
        Glib::RefPtr<Gio::File> file = Gio::File::create_for_uri(uri);
        return createHelper(file);
    }

    std::string DirectoryObserver::getHomePath() noexcept {
        return Glib::get_home_dir();
    }

    void DirectoryObserver::Update() {
        auto enumerator = m_file->enumerate_children();
        m_directoryContent.clear();
        m_subdirectories.clear();

        const auto uriPath = m_file->get_uri();
        while(auto info = enumerator->next_file())
        {
            if (info->is_hidden())
                continue;

            auto fileName = info->get_name();

            switch (info->get_file_type())
            {
            case Gio::FileType::FILE_TYPE_DIRECTORY:
                m_subdirectories.push_back(fileName);
                break;
            case Gio::FileType::FILE_TYPE_REGULAR:
                if (this->isSupported(fileName))
                    m_directoryContent.push_back(uriPath + pathSeparator + fileName);
                break;
            case Gio::FileType::FILE_TYPE_NOT_KNOWN:
            case Gio::FileType::FILE_TYPE_SYMBOLIC_LINK:
            case Gio::FileType::FILE_TYPE_SPECIAL:
            case Gio::FileType::FILE_TYPE_SHORTCUT:
            case Gio::FileType::FILE_TYPE_MOUNTABLE:
                break;
            }
        }

        enumerator->close();
        std::sort(std::begin(m_subdirectories), std::end(m_subdirectories));
        std::sort(std::begin(m_directoryContent), std::end(m_directoryContent));
    }

    bool DirectoryObserver::makeParentState() {
        bool hasParent = m_file->has_parent();
        if (hasParent)
        {
            m_file = m_file->get_parent();
            m_directoryContent.clear();
            m_subdirectories.clear();
        }

        return hasParent;
    }

    bool DirectoryObserver::empty() const noexcept {
        return m_directoryContent.empty() && m_subdirectories.empty();
    }

    std::size_t DirectoryObserver::getMediaCount() const noexcept {
        return m_directoryContent.size();
    }

    rpCore::Media::MediaProxy DirectoryObserver::extractMediaAt(std::size_t i) const {
        std::string fileName = m_directoryContent.at(i);
        return this->extractMedia(fileName);
    }

    std::size_t DirectoryObserver::getSubobserversCount() const noexcept {
        return m_subdirectories.size();
    }

    std::unique_ptr<Observer> DirectoryObserver::extractObserverAt(std::size_t i) const {
        Glib::RefPtr<Gio::File> subdir = m_file->get_child(m_subdirectories.at(i));
        return createHelper(subdir);
    }

    std::unique_ptr<DirectoryObserver> DirectoryObserver::createHelper(const Glib::RefPtr<Gio::File> &file) {
        if (file->query_exists())
        {
            return std::unique_ptr<DirectoryObserver>(new DirectoryObserver(file));
        }
        return std::unique_ptr<DirectoryObserver>();
    }

}
}
