#pragma once

#include "rpObservers/Observer.h"

#include <giomm/file.h>

#include <memory>
#include <vector>

namespace rpObservers {
namespace Observers {

    /**
     * Entity which can read files from directory on file system.
     * DirectoryObserver provides methods to get subdirectories and
     * media files from directory.
     */
    class DirectoryObserver final : public Observer
    {
    private:
        /**
         * Constructs new instance of DirectoryObserver from Gio::File.
         * @param directory is abstraction of object on file system.
         */
        explicit DirectoryObserver(const Glib::RefPtr<Gio::File> &directory);

    public:
        /// Copy constructor is forbidden.
        DirectoryObserver(const DirectoryObserver&) = delete;

        /// Move semantic is not allowed.
        DirectoryObserver(DirectoryObserver&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        DirectoryObserver& operator=(const DirectoryObserver&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        DirectoryObserver& operator=(DirectoryObserver&&) = delete;

        /// Default destructor.
        virtual ~DirectoryObserver() = default;

        /**
         * Gets name of directory.
         * @return name of directory.
         */
        virtual std::string getName() const noexcept override;

        /**
         * Gets full path to directory.
         * @return path to directory.
         */
        virtual std::string getFullName() const noexcept override;

        /**
         * Gets date of last modification of directory.
         * @return date of last modification.
         */
        virtual std::string getDate() const noexcept override;

        /**
         * Creates new instance of DirectoryObserver by path on file system.
         * @param path is address of directory on file system.
         * @return new instance of DirectoryObserver.
         */
        static std::unique_ptr<DirectoryObserver> createFromPath(const std::string &path);

        /**
         * Creates new instance of DirectoryObserver by uri on file system.
         * @param uri is specific address of directory on file system.
         * @return new instance of DirectoryObserver.
         */
        static std::unique_ptr<DirectoryObserver> createFromUri(const std::string &uri);

        /**
         * Gets path of home directory on local machine.
         * @return path to home directory on file system.
         */
        static std::string getHomePath() noexcept;

        /**
         * Reads content of directory.
         */
        virtual void Update() override;

        /**
         * Switchs to parent directory if it can.
         * @return true if state is changed, false otherwise.
         */
        virtual bool makeParentState() override;

        /**
         * Checks directory is empty or not.
         * @return true if directory doesn't have files
         * or subdirectories, otherwise false.
         */
        bool empty() const noexcept;

        /**
         * Gets count of MediaFiles in directory.
         * @return count of MediaFiles.
         */
        virtual std::size_t getMediaCount() const noexcept override;

        /**
         * Extracts MediaFile from directory.
         * @param i index of MediaFile in directory.
         * @return pointer on new MediaFile
         * or nullptr if type if MediaFile isn't supported.
         * @exception std::out_of_range if index is invalid.
         * @remark Observer doesn't hold MediaFile instance.
         * It returns new instance of MediaFile every time.
         * So client code must manage lifetime of this instance.
         */
        virtual rpCore::Media::MediaProxy extractMediaAt(std::size_t i) const override;

        /**
         * Gets count of subdirectories in DirectoryObserver.
         * @return count of subdirectories.
         */
        virtual std::size_t getSubobserversCount() const noexcept override;

        /**
         * Extracts subdirectory from DirectoryObserver.
         * @param i index of subdirectory.
         * @return pointer on new DirectoryObserver
         * or nullptr if directory doesn't exist
         * @exception std::out_of_range if index is invalid.
         * @remark Observer doesn't hold new DirectoryObserver instance.
         * It returns new instance of DirectoryObserver every time.
         * So client code must manage lifetime of this instance.
         */
        virtual std::unique_ptr<Observer> extractObserverAt(std::size_t i) const override;

    private:
        /**
         * Helper for creating new instance of DirectoryObserver.
         * It checks directory on existing.
         * @param file is abstraction of object on file system.
         * @return new DirectoryObserver or nullptr if directory doesn't exist.
         */
        static std::unique_ptr<DirectoryObserver> createHelper(const Glib::RefPtr<Gio::File> &file);

    private:
        /**
         * File which represents directory on file system.
         */
        Glib::RefPtr<Gio::File> m_file;

        /**
         * List of media files in directory.
         * It holds only supported media files.
         */
        std::vector<std::string> m_directoryContent;

        /**
         * List of subdirectories in directory.
         */
        std::vector<std::string> m_subdirectories;
    };

}
}
