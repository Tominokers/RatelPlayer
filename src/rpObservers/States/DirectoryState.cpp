#include "DirectoryState.h"

#include "rpObservers/ObserverTypes.h"

namespace rpObservers {
namespace States {

    DirectoryState::DirectoryState(const std::string &directoryPath)
        : ObserverState(ObserverType::Directory)
        , m_directory(directoryPath)
    {
    }

    const std::string& DirectoryState::getDirectory() const noexcept {
        return m_directory;
    }

    bool DirectoryState::isDirectoryUri(const std::string &uri) {
        constexpr auto count = 8u;
        static char fileProtocol[count] = "file://";
        return uri.compare(0, count-1, fileProtocol) == 0;
    }

}
}
