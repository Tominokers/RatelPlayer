#pragma once

#include "rpObservers/ObserverState.h"

#include <string>

namespace rpObservers {
namespace States {

    /**
     * Entity which holds necessary information for
     * creation or recovery of DirectoryObserver instance.
     */
    class DirectoryState final : public ObserverState
    {
    public:
        /**
         * Constructs new instance of DirectoryState.
         * @param directoryPath path to directory which must be observed.
         */
        explicit DirectoryState(const std::string &directoryPath);

        /// Copy constructor is forbidden.
        DirectoryState(const DirectoryState&) = delete;

        /// Move semantic is not allowed.
        DirectoryState(DirectoryState&&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        DirectoryState& operator=(const DirectoryState&) = delete;

        /**
         * Move semantic is not allowed.
         * @return
         */
        DirectoryState& operator=(DirectoryState&&) = delete;

        /**
         * Gets path to observed directory.
         * @return path to directory.
         */
        const std::string& getDirectory() const noexcept;

        /**
         * Checks whether uri is local path on file system.
         * @param uri the uri to some directory.
         * @return true if uri is directory, otherwise false.
         */
        static bool isDirectoryUri(const std::string &uri);

    private:
        std::string m_directory;
    };

}
}
