#include "SettingManager.h"

#include "rpSettings/SettingStorage.h"

#include <array>

namespace rpSettings {

    SettingManager::SettingManager(std::unique_ptr<SettingStorage> storage)
        : m_prototype(std::move(storage))
        , m_storageTag("[StorageIds]")
        , m_settingsTag("[Settings]")
    {
    }

    SettingManager::~SettingManager() = default;

    SettingStorage& SettingManager::getStorage(const std::string &storageId) {
        auto result = m_storages.find(storageId);

        if (result == std::end(m_storages))
        {
            auto newStorage = m_prototype->clone();
            newStorage->setId(storageId);
            m_storages[storageId] = std::move(newStorage);
        }

        return *(m_storages[storageId]);
    }

    void SettingManager::load() {
        auto &stream = this->getLoadStream();

        auto mode = LoadMode::None;
        std::array<char, 512> buffer;
        std::string id;
        std::string key;
        std::string value;

        while(stream.good())
        {
            stream.getline(buffer.data(), buffer.size());

            if (buffer.front() == '\0')
                continue;

            if (m_storageTag.compare(buffer.data()) == 0)
            {
                mode = LoadMode::Storages;
            }
            else if (m_settingsTag.compare(buffer.data()) == 0)
            {
                mode = LoadMode::Settings;
            }
            else if (mode == LoadMode::Storages)
            {
                getStorage(buffer.data());
            }
            else if (mode == LoadMode::Settings)
            {
                std::tie(id, key, value) = m_prototype->extractOneFlush(buffer.data());
                m_storages[id]->store(key, value);
            }
        }
    }

    void SettingManager::dump() {
        auto &stream = this->getDumpStream();

        stream << m_storageTag << "\n";
        for (auto &it: m_storages)
        {
            stream << it.first << '\n';
        }

        stream << m_settingsTag << "\n";
        for (auto &it : m_storages)
        {
            it.second->flush(stream);
        }
    }
}
