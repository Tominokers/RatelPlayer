#pragma once

#include <istream>
#include <memory>
#include <ostream>
#include <unordered_map>

namespace rpSettings {

    class SettingStorage;

    /**
     * Entity which manages saving, loading and tracking setting storages.
     * It is able to create or get storages for keeping settings in it.
     * Client code should be inherited from this class and provides streams
     * which will be used for loading and dumping settings.
     * Also client code should pass prototype of storage to constructor.
     * @remark Class uses NVI idiom.
     */
    class SettingManager
    {
    public:
        /**
         * Destroys settings manager instance.
         */
        virtual ~SettingManager();

        /**
         * Gets settings storage by its id.
         * If manager doesn't have storage with given id,
         * new empty storage will be created.
         * @param storageId the id of the necessary storage.
         * @return reference to specific storage with settings.
         */
        SettingStorage& getStorage(const std::string &storageId);

        /**
         * Loads all storages and their settings from stream
         * which will be taken from call of getLoadStream() method.
         */
        void load();

        /**
         * Dumps (saves) all settings from storages to the stream
         * which will be taken from call of getDumpStream() method.
         */
        void dump();

    protected:
        /**
         * Creates instance of SettingManager with provided type of storage.
         * @param storage instance-prototype which will be used for cloning.
         */
        explicit SettingManager(std::unique_ptr<SettingStorage> storage);

    private:
        /**
         * Gets stream in which will be written
         * all storages and their settings as text information.
         * @return stream with write access.
         */
        virtual std::ostream& getDumpStream() noexcept =0;

        /**
         * Gets stream from wich will be read
         * all storages and their settings as text information.
         * @return stream with read access.
         */
        virtual std::istream& getLoadStream() noexcept =0;

    private:
        enum class LoadMode
        {
            None,
            Storages,
            Settings
        };

    private:
        std::unique_ptr<SettingStorage> m_prototype;

        std::unordered_map<std::string, std::unique_ptr<SettingStorage>> m_storages;

        const std::string m_storageTag;
        const std::string m_settingsTag;
    };

}
