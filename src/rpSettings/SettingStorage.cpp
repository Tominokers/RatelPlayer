#include "SettingStorage.h"

namespace rpSettings {

    SettingStorage::SettingStorage(const std::string &storeId)
        : m_id(storeId)
    {
    }

    const std::string& SettingStorage::getId() const noexcept {
        return m_id;
    }

    void SettingStorage::setId(const std::string &storageId) noexcept {
        m_id = storageId;
    }

    void SettingStorage::store(const std::string &key, const std::string &value) noexcept {
        this->storeImpl(key, value);
    }

    std::string SettingStorage::load(const std::string &key) noexcept {
        return this->loadImpl(key);
    }

}
