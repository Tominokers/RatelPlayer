#pragma once

#include <memory>
#include <ostream>
#include <string>
#include <tuple>

namespace rpSettings {

    using ExtractedDump = std::tuple<std::string, std::string, std::string>;

    /**
     * Entity which collects specific settings.
     * Client code shoud be inherited from this class and provide implementation for
     * loading, saving, flushing, extracting settings.
     * @remark in general implementation settings are held in memory,
     * but (e.g.) they can be written to file system as well.
     * But memory is strong advice because SettingManager should take care about it.
     * @remark Class uses NVI and virtual ctor idioms.
     */
    class SettingStorage
    {
        /// Only SettingManager can create SettingStorage instances.
        friend class SettingManager;

    public:
        /// Copy constructor is forbidden.
        SettingStorage(const SettingStorage&) = delete;

        /**
         * Copy assign operator is forbidden.
         * @return
         */
        SettingStorage& operator=(const SettingStorage&) = delete;

        /**
         * Destroys instance of SettingStorage.
         */
        virtual ~SettingStorage() = default;

        /**
         * Gets id of the storage.
         * @return string as id.
         */
        const std::string& getId() const noexcept;

        /**
         * Stores value of setting by specific key.
         * @param key the key of the setting.
         * @param value the value of the setting.
         */
        void store(const std::string &key, const std::string &value) noexcept;

        /**
         * Gets value of the setting by specific key.
         * @param key the key of the setting.
         * @return value of the setting.
         */
        std::string load(const std::string &key) noexcept;

    protected:
        /**
         * Creates instance of SettingStorage with provided storage id.
         * @param storageId the id of the storage.
         */
        explicit SettingStorage(const std::string &storageId);

    private:
        /**
         * Create clone of the instance.
         * @return new clonned instance.
         * @remark it is used by SettingManager to create new storage.
         */
        virtual std::unique_ptr<SettingStorage> clone() const =0;

        /**
         * Exports all settings in specific format into the stream.
         * @param stream the stream to save settings.
         */
        virtual void flush(std::ostream &stream) =0;

        /**
         * Extracts one exported setting from the string.
         * @param data the information to parse.
         * @return tuple instance which contains:
         * id of storage, setting key and setting value.
         */
        virtual ExtractedDump extractOneFlush(const std::string &data) const noexcept =0;

        virtual void storeImpl(const std::string &key, const std::string &value) noexcept =0;

        virtual std::string loadImpl(const std::string &key) noexcept =0;

    private:
        void setId(const std::string &storageId) noexcept;

    private:
        std::string m_id;
    };

}
