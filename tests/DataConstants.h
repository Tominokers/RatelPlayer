#pragma once

#include <string>

// Directories
static const std::string BigMediaDirectory = "./TestData/BigTestMediaFolder";
static const std::string DictionariesDirectory = "../resources/dictionaries/";
static const std::string MediaDirectory = "./TestData/TestMediaFolder";
static const std::string PlaylistDirectory = "TestData/TestMediaFolder/test/";
static const std::string TestDirectory = "./TestData";

// Number of files in directory
static const std::size_t FilesInMediaDirectory = 7u;
static const std::size_t FilesInTestDirectory = 25u;

static const std::size_t SubFoldersInTestDirectory = 4u;

// Test files
static const std::string Test2SecMP3File = "/TestData/BiggerFilesMediaFolder/test 2 sec.mp3";
static const std::string TestApeFile = "/TestData/TestMediaFolder/test.ape";
static const std::string TestFlacFile = "/TestData/TestMediaFolder/test.flac";
static const std::string TestM4aFile = "/TestData/TestMediaFolder/test.m4a";
static const std::string TestMP3File = "/TestData/test.mp3";
static const std::string TestOggFile = "/TestData/TestMediaFolder/test.ogg";
static const std::string TestWavFile = "/TestData/TestMediaFolder/test.wav";

static const std::string TestCueFile = "/TestData/CueTestMediaFolder/testable.cue";
static const std::string TestReferencedCueFile = "/TestData/CueTestMediaFolder/testable.flac";
static const std::string TestBadCueFile = "/TestData/CueTestMediaFolder/bad_testable.cue";
static const std::string TestCueFileWOGaps = "/TestData/CueTestMediaFolder/testable_without_pregaps.cue";
