#include "GlobalTestEnvironment.h"

namespace MediaCoreTest {

    GlobalTestEnvironment::GlobalTestEnvironment() = default;

    void GlobalTestEnvironment::SetUp() {
        m_mediaInit.init();
        m_observersInit.init();
    }

    void GlobalTestEnvironment::TearDown() {
        m_mediaInit.deinit();
        m_observersInit.deinit();
    }

}
