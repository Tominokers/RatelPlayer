#pragma once

#include "rpCore/MediaInit.h"
#include "rpObservers/ObserverIniter.h"

#include <gtest/gtest.h>

namespace MediaCoreTest {

    class GlobalTestEnvironment : public testing::Environment
    {
    public:
        GlobalTestEnvironment();

        virtual void SetUp() override;

        virtual void TearDown() override;

    private:
        rpCore::MediaInit m_mediaInit;
        rpObservers::ObserverIniter m_observersInit;
    };

}
