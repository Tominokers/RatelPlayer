#pragma once

#include "rpFacade/Concurrency/ActionListener.h"

#include <gmock/gmock.h>

namespace Mocked {

    class ActionListenerMock : public rpFacade::Concurrency::ActionListener
    {
    public:
        MOCK_METHOD0(handleStartNotification, void());
        MOCK_METHOD0(handleEndNotification, void());
    };

}
