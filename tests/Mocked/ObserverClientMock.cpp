#include "ObserverClientMock.h"

#include "rpCore/Media/MediaProxy.h"
#include "rpObservers/Observer.h"

namespace Mocked {

    ObserverClientMocked::ObserverClientMocked(rpFacade::ObserverInterface &observerInstance)
        : m_observer(observerInstance)
    {
    }

    rpFacade::ObserverInterface& ObserverClientMocked::getObserver() const noexcept {
        return m_observer;
    }

}
