#pragma once

#include "rpFacade/ObserverClient.h"

#include <gmock/gmock.h>

namespace rpFacade {
    class ObserverInterface;
}

namespace Mocked {

    class ObserverClientMocked : public rpFacade::ObserverClient
    {
    public:
        explicit ObserverClientMocked(rpFacade::ObserverInterface &observerInstance);

        virtual rpFacade::ObserverInterface& getObserver() const noexcept override;

        MOCK_QUALIFIED_METHOD1(passObtainedMedia, noexcept, void(const rpFacade::MediaProxy&));
        MOCK_QUALIFIED_METHOD1(passObtainedObservers, noexcept, void(const rpFacade::Observer&));
        MOCK_QUALIFIED_METHOD1(observerNameChanged, noexcept, void(const std::string&));
        MOCK_QUALIFIED_METHOD0(obtainingMediaStarted, noexcept, void());
        MOCK_QUALIFIED_METHOD0(obtainingMediaFinished, noexcept, void());

    private:
        rpFacade::ObserverInterface &m_observer;
    };

}
