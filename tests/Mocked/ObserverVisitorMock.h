#pragma once

#include "rpFacade/Observers/ObserverVisitor.h"

#include <gmock/gmock.h>

namespace Mocked {

    class ObserverVisitorMock : public rpFacade::Observers::ObserverVisitor
    {
    public:
        MOCK_METHOD1(visitMedia, void(const rpCore::Media::MediaProxy&));
        MOCK_METHOD1(visitObserver, void(const rpObservers::Observer&));
    };

}
