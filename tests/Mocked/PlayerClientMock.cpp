#include "PlayerClientMock.h"

namespace Mocked {

    PlayerClientMock::PlayerClientMock()
        : PlayerClient(true)
    {
    }

}

namespace rpCore {
namespace Player {

    bool operator == (const PlayingProgress &lhs, const PlayingProgress &rhs) {
        return lhs.m_position == rhs.m_position && lhs.m_duration == rhs.m_duration;
    }

    bool operator != (const PlayingProgress &lhs, const PlayingProgress &rhs) {
        return ! operator==(lhs, rhs);
    }

}
}
