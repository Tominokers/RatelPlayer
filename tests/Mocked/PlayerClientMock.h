#pragma once

#include "rpCore/Player/PlayerClient.h"

#include <gmock/gmock.h>

namespace Mocked {

    class PlayerClientMock : public rpCore::Player::PlayerClient
    {
    public:
        PlayerClientMock();

        MOCK_METHOD1(setProgress, void(const rpCore::Player::PlayingProgress&));
        MOCK_METHOD2(setTrackIndex, void(const rpCore::Player::Playlist*, rpCore::Player::TrackIndex));
        MOCK_METHOD1(handleErrorNotification, void(const std::string&));
    };

}

namespace rpCore {
namespace Player {

    bool operator == (const PlayingProgress &lhs, const PlayingProgress &rhs);
    bool operator != (const PlayingProgress &lhs, const PlayingProgress &rhs);

}
}
