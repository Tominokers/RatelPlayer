#include "PlayerFillerClientMock.h"

#include "TestUtils.h"

#include "rpFacade/FacadeManager.h"
#include "rpFacade/PlaylistAssistant.h"
#include "rpFacade/Track.h"

#include <cassert>
#include <chrono>
#include <limits>
#include <thread>

using namespace std::chrono;

namespace Mocked {

    PlayerFillerClientMock::PlayerFillerClientMock(const std::string &playlistDirectory, const std::string &count)
        : m_storage("SettingStorage")
        , m_player(rpFacade::FacadeManager::createPlayer(m_storage, this))
        , m_concurrentActionIsRunning(false)
    {
        m_storage.store("PlaylistDirectory", playlistDirectory);
        m_storage.store("PlaylistCount", count);
    }

    PlayerFillerClientMock::~PlayerFillerClientMock() {
        waitConcurrentAction();
    }

    void PlayerFillerClientMock::waitConcurrentAction() {
        if (m_concurrentActionIsRunning)
        {
            std::this_thread::sleep_for(1s); // Give some time for another thread.
            TestUtils::waitForPendingTasks(); // It should retrieve end event from loop.
            auto lockResult = m_concurrentActionLock.try_lock_for(2s); // Lock should be available here.
            assert(lockResult); // If it is not, we fail!
            assert(!m_concurrentActionIsRunning);
            m_concurrentActionLock.unlock();
        }
    }

    void PlayerFillerClientMock::setupPlayer() {
        auto playlistDirectory = m_storage.load("PlaylistDirectory");
        auto count = m_storage.load("PlaylistCount");

        if (!playlistDirectory.empty() && count != "0")
            this->getPlaylists().loadSerializedPlaylists();
        else
            this->getPlaylists().createPlaylist(*this, *this);

        this->getPlaylists().setUsedPlaylist(0);
    }

    void PlayerFillerClientMock::reloadPlayer() {
        m_player = rpFacade::FacadeManager::createPlayer(m_storage, this);

        this->setupPlayer();
    }

    rpFacade::PlayerInterface& PlayerFillerClientMock::getPlayer() noexcept {
        return *m_player;
    }

    rpFacade::PlaylistAssistant& PlayerFillerClientMock::getPlaylists() noexcept {
        return m_player->getPlaylistAssistant();
    }

    void PlayerFillerClientMock::handleStartNotification() {
        m_concurrentActionIsRunning = true;
        m_concurrentActionLock.lock();
    }
    void PlayerFillerClientMock::handleEndNotification() {
        m_concurrentActionLock.unlock();
        m_concurrentActionIsRunning = false;
    }

}

namespace rpFacade {

    bool isDoubleEqual(double lhs, double rhs) {
        double diff = 0.0;
        if (lhs > rhs)
            diff = lhs - rhs;
        else
            diff = rhs - lhs;

        return diff < std::numeric_limits<double>::epsilon();
    }

    bool operator==(const rpFacade::TrackProgress &lhs, const rpFacade::TrackProgress &rhs) {
        return isDoubleEqual(lhs.m_position, rhs.m_position) &&
            lhs.m_positionSeconds == rhs.m_positionSeconds &&
            lhs.m_positionMinutes == rhs.m_positionMinutes &&
            lhs.m_positionHours == rhs.m_positionHours &&
            lhs.m_durationSeconds == rhs.m_durationSeconds &&
            lhs.m_durationMinutes == rhs.m_durationMinutes &&
            lhs.m_durationHours == rhs.m_durationHours;
    }

}
