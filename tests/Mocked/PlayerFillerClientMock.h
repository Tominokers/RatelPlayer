#pragma once

#include "TestClients/SettingStorageClient.h"
#include "Mocked/ActionListenerMock.h"

#include "rpFacade/Concurrency/ActionListener.h"
#include "rpFacade/PlayerClient.h"
#include "rpFacade/PlayerInterface.h"
#include "rpFacade/PlaylistFiller.h"

#include <gmock/gmock.h>

#include <atomic>
#include <mutex>

using ::testing::NiceMock;

namespace Mocked {

    class PlayerFillerClientMock : public rpFacade::PlayerClient
                                 , public rpFacade::PlaylistFiller
                                 , public rpFacade::Concurrency::ActionListener
    {
    public:
        PlayerFillerClientMock(const std::string &playlistDirectory = std::string(),
                               const std::string &count = std::string());

        virtual ~PlayerFillerClientMock();

        void setupPlayer();

        void reloadPlayer();

        void waitConcurrentAction();

        virtual rpFacade::PlayerInterface& getPlayer() noexcept override;

        rpFacade::PlaylistAssistant& getPlaylists() noexcept;

        MOCK_METHOD0(requestPlaylistCreation, rpFacade::PlaylistIndex());
        MOCK_METHOD1(requestPlaylistClose, void(rpFacade::PlaylistIndex));
        MOCK_QUALIFIED_METHOD1(passChangedPlayState, noexcept, void(rpFacade::PlayerState));
        MOCK_QUALIFIED_METHOD1(passTrackProgress, noexcept, void(const rpFacade::TrackProgress&));
        MOCK_QUALIFIED_METHOD3(passCurrentTrack, noexcept, void(const rpFacade::Track&,
                                                                rpFacade::PlaylistIndex,
                                                                rpFacade::TrackIndex));
        MOCK_QUALIFIED_METHOD1(passErrorNotification, noexcept, void(const std::string&));

        MOCK_METHOD0(clearItems, void());
        MOCK_METHOD1(appendNewItem, void(const rpFacade::Track&));
        MOCK_METHOD2(insertNewItem, void(const rpFacade::Track&, rpFacade::TrackIndex));
        MOCK_METHOD1(removeItems, void(const rpFacade::TrackIndexList&));
        MOCK_METHOD1(passSuggestedName, void(const std::string &name));

        virtual void handleStartNotification() override;
        virtual void handleEndNotification() override;

    private:
        TestClients::SettingStorageClient m_storage;
        std::unique_ptr<rpFacade::PlayerInterface> m_player;

        std::atomic_bool m_concurrentActionIsRunning;
        std::timed_mutex m_concurrentActionLock;
    };

}

namespace rpFacade {

    bool operator==(const rpFacade::TrackProgress &lhs, const rpFacade::TrackProgress &rhs);

}
