#pragma once

#include "rpCore/Player/PlaylistChangesListener.h"

#include <gmock/gmock.h>

namespace Mocked {

    class PlaylistChangesListenerMock : public rpCore::Player::PlaylistChangesListener
    {
    public:
        MOCK_QUALIFIED_METHOD3(reportRemovedMedia, noexcept, void(const rpCore::Player::Playlist*,
                                                                  rpCore::Player::TrackIndex,
                                                                  rpCore::Player::TrackIndex));
        MOCK_QUALIFIED_METHOD3(reportAddedMedia, noexcept, void(const rpCore::Player::Playlist*,
                                                                rpCore::Player::TrackIndex,
                                                                rpCore::Player::TrackIndex));
        MOCK_QUALIFIED_METHOD1(reportPlaylistSorting, noexcept, void(const rpCore::Player::Playlist*));
        MOCK_QUALIFIED_METHOD1(reportPlaylistDestroying, noexcept, void(rpCore::Player::Playlist*));
    };

}
