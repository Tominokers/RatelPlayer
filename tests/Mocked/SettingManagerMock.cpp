#include "SettingManagerMock.h"

namespace Mocked {

    SettingManagerMock::SettingManagerMock(std::unique_ptr<rpSettings::SettingStorage> storage)
        : rpSettings::SettingManager(std::move(storage))
    {
    }

}
