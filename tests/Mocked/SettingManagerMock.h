#pragma once

#include "rpSettings/SettingManager.h"
#include "rpSettings/SettingStorage.h"

#include <gmock/gmock.h>

namespace Mocked {

    class SettingManagerMock : public rpSettings::SettingManager
    {
    public:
        explicit SettingManagerMock(std::unique_ptr<rpSettings::SettingStorage> storage);

        MOCK_QUALIFIED_METHOD0(getDumpStream, noexcept, std::ostream&());
        MOCK_QUALIFIED_METHOD0(getLoadStream, noexcept, std::istream&());
    };

}
