#pragma once

#include "rpSettings/SettingStorage.h"

#include <gmock/gmock.h>

namespace Mocked {

    class SettingStorageMock : public rpSettings::SettingStorage
    {
    public:
        explicit SettingStorageMock(const std::string &storageId);

        MOCK_METHOD1(flush, void (std::ostream&));
        MOCK_QUALIFIED_METHOD1(extractOneFlush, const noexcept, rpSettings::ExtractedDump(const std::string&));
        MOCK_QUALIFIED_METHOD0(clone, const noexcept, std::unique_ptr<rpSettings::SettingStorage>());
        MOCK_QUALIFIED_METHOD2(storeImpl, noexcept, void(const std::string&, const std::string&));
        MOCK_QUALIFIED_METHOD1(loadImpl, noexcept, std::string(const std::string&));
    };

}
