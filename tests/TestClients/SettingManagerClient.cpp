#include "SettingManagerClient.h"

#include "rpSettings/SettingStorage.h"

namespace TestClients {

    SettingManagerClient::SettingManagerClient(std::unique_ptr<rpSettings::SettingStorage> storage)
        : rpSettings::SettingManager(std::move(storage))
    {
    }

    std::stringstream& SettingManagerClient::getDumpStream() noexcept {
        return m_stream;
    }

    std::stringstream& SettingManagerClient::getLoadStream() noexcept {
        return m_stream;
    }

    void SettingManagerClient::setStreamContent(const std::string &content) {
        m_stream.str("");
        m_stream << content;
    }

}
