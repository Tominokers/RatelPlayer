#pragma once

#include "rpSettings/SettingManager.h"

#include <sstream>

namespace TestClients {

    class SettingManagerClient : public rpSettings::SettingManager
    {
    public:
        explicit SettingManagerClient(std::unique_ptr<rpSettings::SettingStorage> storage);

        virtual std::stringstream& getDumpStream() noexcept override;
        virtual std::stringstream& getLoadStream() noexcept override;

        void setStreamContent(const std::string &content);

    private:
        std::stringstream m_stream;
    };
}
