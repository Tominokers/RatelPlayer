#include "SettingStorageClient.h"

namespace TestClients {

    const std::string SettingStorageClient::m_separator = ":";

    SettingStorageClient::SettingStorageClient(const std::string &storageId)
        : SettingStorage(storageId)
    {
    }

    const std::string& SettingStorageClient::getSeparator() const {
        return m_separator;
    }

    std::unique_ptr<rpSettings::SettingStorage> SettingStorageClient::clone() const {
        return std::make_unique<SettingStorageClient>("");
    }

    void SettingStorageClient::storeImpl(const std::string &key, const std::string &value) noexcept {
        m_settings[key] = value;
    }

    std::string SettingStorageClient::loadImpl(const std::string &key) noexcept {
        return m_settings[key];
    }

    void SettingStorageClient::flush(std::ostream &stream) {
        for (auto it : m_settings)
        {
            stream << this->getId() << m_separator
                   << it.first << m_separator
                   << it.second << std::endl;
        }
    }

    rpSettings::ExtractedDump SettingStorageClient::extractOneFlush(const std::string &data) const noexcept {
        auto afterId = data.find(m_separator);
        auto afterKey = data.rfind(m_separator);

        std::string id = data.substr(0, afterId);
        std::string key = data.substr(afterId + 1, afterKey - afterId - 1);
        std::string value = data.substr(afterKey + 1);

        return std::make_tuple(id, key, value);
    }

}
