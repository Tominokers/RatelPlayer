#pragma once

#include "rpSettings/SettingStorage.h"

#include <map>

namespace TestClients {

    class SettingStorageClient : public rpSettings::SettingStorage
    {
    public:
        explicit SettingStorageClient(const std::string &storageId);

        const std::string& getSeparator() const;

        virtual void flush(std::ostream &stream) override;

        virtual rpSettings::ExtractedDump extractOneFlush(const std::string &data) const noexcept override;

    private:
        virtual std::unique_ptr<rpSettings::SettingStorage> clone() const override;
        virtual void storeImpl(const std::string &key, const std::string &value) noexcept override;
        virtual std::string loadImpl(const std::string &key) noexcept override;

    private:
        std::map<std::string, std::string> m_settings;

        static const std::string m_separator;
    };

}
