#include "TestUtils.h"

#include "rpCore/Media/MediaFactory.h"
#include "rpCore/Media/MediaProxy.h"

#include <cstdio>

namespace TestUtils {

    Glib::ustring getTestFile(const std::string &fileName) {
        if (!fileName.empty() && fileName.front() == '.')
            return getTestFile(fileName.substr(1));

        return Glib::get_current_dir() + fileName;

    }

    Glib::ustring getTestFileUri(const std::string &fileName) {
        return Glib::filename_to_uri(getTestFile(fileName));
    }

    rpCore::Media::MediaProxy getTestMediaFile(const std::string &fileName) {
        auto path = getTestFileUri(fileName);
        return rpCore::Media::MediaFactory::createMedia(path);
    }

    void waitForPendingTasks() {
        auto context = Glib::MainContext::get_default();
        while (context->pending())
            context->iteration(true);
    }

    RemoveFileScope::RemoveFileScope(const std::string &fileName)
        : m_file(fileName)
    {
    }

    RemoveFileScope::~RemoveFileScope() {
        std::remove(m_file.c_str());
    }

}
