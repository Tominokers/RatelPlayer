#pragma once

#include <glibmm.h>

namespace rpCore {
namespace Media {
    class MediaProxy;
}
}

namespace TestUtils {

    Glib::ustring getTestFile(const std::string &fileName);

    Glib::ustring getTestFileUri(const std::string &fileName);

    rpCore::Media::MediaProxy getTestMediaFile(const std::string &fileName);

    void waitForPendingTasks();

    class RemoveFileScope final
    {
    public:
        explicit RemoveFileScope(const std::string &fileName);

        ~RemoveFileScope();

    private:
        std::string m_file;
    };

}
