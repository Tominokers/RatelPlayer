#include "GlobalTestEnvironment.h"

#include <glibmm/main.h>
#include <gmock/gmock.h>

GTEST_API_ int main(int argv, char** argc) {
    auto loop = Glib::MainLoop::create(true);
    int returnValue = 0;

    Glib::signal_idle().connect_once([&argv, &argc, &returnValue, &loop]() -> void
                                     {
                                         testing::AddGlobalTestEnvironment(new MediaCoreTest::GlobalTestEnvironment());
                                         testing::InitGoogleMock(&argv, argc);
                                         returnValue = RUN_ALL_TESTS();
                                         loop->quit();
                                     });

    loop->run();

    return returnValue;
}
