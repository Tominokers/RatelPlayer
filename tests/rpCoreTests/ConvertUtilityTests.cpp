#include "ConvertUtilityTests.h"

#include "DataConstants.h"
#include "TestUtils.h"

#include "rpCore/Tools/ConvertUtilityFunctions.h"

using namespace rpCore::Player;
using namespace rpCore::Tools;

namespace MediaTests {

    TEST_F(ConvertUtilityTests, convert_indexes_to_ranges) {
        TrackIndexList indexes0 = { 9 };
        VectorOfRanges expectedRanges0 = { {9, 10} };

        auto ranges0 = convertIndexesToRanges(indexes0);

        ASSERT_EQ(expectedRanges0, ranges0);

        TrackIndexList indexes1 = { 0, 1, 2, 3, 6, 8, 9, 10, 11, 12, 13, 14, 15 };
        VectorOfRanges expectedRanges1 = { {0, 4}, {6, 7}, {8, 16} };

        auto ranges1 = convertIndexesToRanges(indexes1);

        ASSERT_EQ(expectedRanges1, ranges1);

        TrackIndexList indexes2 = { };
        VectorOfRanges expectedRanges2 = { };

        auto ranges2 = convertIndexesToRanges(indexes2);

        ASSERT_EQ(expectedRanges2, ranges2);
    }

    TEST_F(ConvertUtilityTests, convert_text_to_integer) {
        ASSERT_EQ(1u, convertTextToInteger("1"));
        ASSERT_EQ(1u, convertTextToInteger("1u"));
        ASSERT_EQ(1u, convertTextToInteger("1.0"));
        ASSERT_EQ(1u, convertTextToInteger("1.0f"));
        ASSERT_EQ(1u, convertTextToInteger("1.0bla"));
        ASSERT_EQ(0u, convertTextToInteger("bla1"));
    }

    TEST_F(ConvertUtilityTests, convert_uri_to_name_of_file) {
        auto testFileName = "test.mp3";
        ASSERT_EQ("", convertUriToNameOfFile(""));
        ASSERT_EQ(testFileName, convertUriToNameOfFile(TestMP3File));
        ASSERT_EQ(testFileName, convertUriToNameOfFile(TestUtils::getTestFile(TestMP3File)));
        ASSERT_EQ(testFileName, convertUriToNameOfFile(TestUtils::getTestFileUri(TestMP3File)));
    }

}
