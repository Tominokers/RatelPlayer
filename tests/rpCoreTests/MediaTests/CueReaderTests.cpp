#include "CueReaderTests.h"

#include "rpCore/Media/CueReader.h"

#include "DataConstants.h"
#include "TestUtils.h"

using namespace rpCore::Media;
using namespace std::chrono;

namespace MediaTests {

    TEST_F(CueReaderTests, read_cue_file) {
        CueReader cueFile(TestUtils::getTestFile(TestCueFile));

        CueReader::CueTrackEntries cueEntries;

        ASSERT_TRUE(cueFile.read(cueEntries));
        ASSERT_EQ(3u, cueEntries.size());
        ASSERT_EQ(std::string(TestUtils::getTestFileUri(TestReferencedCueFile)), cueFile.getReferencedFile());

        auto &entry0 = cueEntries[0];
        ASSERT_EQ("1", entry0.getTrackNumber());
        ASSERT_EQ("artist", entry0.getArtist());
        ASSERT_EQ("album", entry0.getAlbum());
        ASSERT_EQ("song title1", entry0.getSongTitle());
        ASSERT_EQ("2016", entry0.getYear());
        ASSERT_EQ(0s, entry0.getStart());
        ASSERT_EQ(14s, entry0.getEnd());
        ASSERT_EQ(14s, entry0.getDuration());

        auto &entry1 = cueEntries[1];
        ASSERT_EQ("2", entry1.getTrackNumber());
        ASSERT_EQ("artist feat guest", entry1.getArtist());
        ASSERT_EQ("album", entry1.getAlbum());
        ASSERT_EQ("song title2", entry1.getSongTitle());
        ASSERT_EQ("2016", entry1.getYear());
        ASSERT_EQ(16s, entry1.getStart());
        ASSERT_EQ(1min + 6s, entry1.getEnd());
        ASSERT_EQ(50s, entry1.getDuration());

        auto &entry2 = cueEntries[2];
        ASSERT_EQ("3", entry2.getTrackNumber());
        ASSERT_EQ("artist", entry2.getArtist());
        ASSERT_EQ("album", entry2.getAlbum());
        ASSERT_EQ("song title3", entry2.getSongTitle());
        ASSERT_EQ("2016", entry2.getYear());
        ASSERT_EQ(1min + 10s, entry2.getStart());
        ASSERT_EQ(1min + 23s, entry2.getEnd());
        ASSERT_EQ(13s, entry2.getDuration());
    }

    TEST_F(CueReaderTests, read_cue_without_pointing_to_file) {
        CueReader cueFile (TestUtils::getTestFile(TestBadCueFile));

        CueReader::CueTrackEntries cueEntries;

        ASSERT_FALSE(cueFile.read(cueEntries));
        ASSERT_EQ(0u, cueEntries.size());
    }

    TEST_F(CueReaderTests, read_cue_file_without_pregaps) {
        CueReader cueFile(TestUtils::getTestFile(TestCueFileWOGaps));

        CueReader::CueTrackEntries cueEntries;

        ASSERT_TRUE(cueFile.read(cueEntries));
        ASSERT_EQ(3u, cueEntries.size());
        ASSERT_EQ(std::string(TestUtils::getTestFileUri(TestReferencedCueFile)), cueFile.getReferencedFile());

        auto &entry0 = cueEntries[0];
        ASSERT_EQ("1", entry0.getTrackNumber());
        ASSERT_EQ(0s, entry0.getStart());
        ASSERT_EQ(16s, entry0.getEnd());
        ASSERT_EQ(16s, entry0.getDuration());

        auto &entry1 = cueEntries[1];
        ASSERT_EQ("2", entry1.getTrackNumber());
        ASSERT_EQ(16s, entry1.getStart());
        ASSERT_EQ(1min + 10s, entry1.getEnd());
        ASSERT_EQ(54s, entry1.getDuration());

        auto &entry2 = cueEntries[2];
        ASSERT_EQ("3", entry2.getTrackNumber());
        ASSERT_EQ(1min + 10s, entry2.getStart());
        ASSERT_EQ(1min + 23s, entry2.getEnd());
        ASSERT_EQ(13s, entry2.getDuration());
    }

}
