#include "MediaFactoryTest.h"

#include <DataConstants.h>
#include <TestClients/SettingStorageClient.h>
#include <TestUtils.h>

#include "rpCore/Media/MediaFactory.h"
#include "rpCore/Media/MediaFile.h"

using namespace TestClients;
using namespace rpCore::Media;
using namespace std::chrono;

namespace MediaTests {

    TEST_F(MediaFactoryTest, check_that_file_is_supported) {
        ASSERT_TRUE(MediaFactory::isSupported("media.ape"));
        ASSERT_TRUE(MediaFactory::isSupported("media.cue"));
        ASSERT_TRUE(MediaFactory::isSupported("media.flac"));
        ASSERT_TRUE(MediaFactory::isSupported("media.m4a"));
        ASSERT_TRUE(MediaFactory::isSupported("media.mp3"));
        ASSERT_TRUE(MediaFactory::isSupported("media.ogg"));
        ASSERT_TRUE(MediaFactory::isSupported("media.wav"));
        ASSERT_TRUE(MediaFactory::isSupported("media.Wav"));
        ASSERT_TRUE(MediaFactory::isSupported("media.wAv"));
        ASSERT_TRUE(MediaFactory::isSupported("media.waV"));
        ASSERT_TRUE(MediaFactory::isSupported("media.WAV"));
        ASSERT_FALSE(MediaFactory::isSupported("media.mp4"));
        ASSERT_FALSE(MediaFactory::isSupported("media.wav3"));
        ASSERT_FALSE(MediaFactory::isSupported("media.saf"));
        ASSERT_FALSE(MediaFactory::isSupported("media.txt"));
    }

    TEST_F(MediaFactoryTest, create_media_via_factory) {
        const auto uri = TestUtils::getTestFileUri(TestMP3File);
        auto mp3File1 = MediaFactory::createMedia(uri);
        auto mp3File2 = MediaFactory::createMedia(uri);

        auto mp3 = mp3File1.get();
        ASSERT_NE(mp3, mp3File2.get());
        ASSERT_EQ("test.mp3", mp3->getFileName());
        ASSERT_EQ("Sinus artist", mp3->getArtist());
        ASSERT_EQ("Sinus album", mp3->getAlbum());
        ASSERT_EQ("Sinus track", mp3->getSongTitle());
        ASSERT_EQ("2014", mp3->getYear());
        ASSERT_EQ("1", mp3->getTrackNumber());
        ASSERT_EQ("0:00", mp3->getTrackDuration());
        ASSERT_EQ(MediaFileType::Regular, mp3->getType());
    }

    TEST_F(MediaFactoryTest, create_and_extract_composable_cue_via_factory) {
        const auto uri = TestUtils::getTestFileUri(TestCueFile);
        auto cueFile = MediaFactory::createMedia(uri);
        ASSERT_EQ(MediaFileType::Composable, cueFile.get()->getType());
        ASSERT_EQ(uri, *cueFile.getUri());

        auto cue = cueFile.get();
        ASSERT_EQ("testable.cue", cue->getFileName());
        ASSERT_EQ("-", cue->getArtist());
        ASSERT_EQ("-", cue->getAlbum());
        ASSERT_EQ("-", cue->getSongTitle());
        ASSERT_EQ("-", cue->getYear());
        ASSERT_EQ("-", cue->getTrackNumber());
        ASSERT_EQ("-:-", cue->getTrackDuration());
        ASSERT_EQ(0ns, cue->getDuration());
        ASSERT_EQ(0s, cue->getMediaInfo().getStart());
        ASSERT_EQ(0s, cue->getMediaInfo().getEnd());

        auto medias = MediaFactory::createMediasFromComposable(cueFile);
        ASSERT_EQ(3u, medias.size());

        auto *track0 = medias[0].get();
        ASSERT_EQ("testable.flac", track0->getFileName());
        ASSERT_EQ("1", track0->getTrackNumber());
        ASSERT_EQ("artist", track0->getArtist());
        ASSERT_EQ("album", track0->getAlbum());
        ASSERT_EQ("song title1", track0->getSongTitle());
        ASSERT_EQ("2016", track0->getYear());
        ASSERT_EQ(0s, track0->getStart());
        ASSERT_EQ(14s, track0->getEnd());
        ASSERT_EQ(14s, track0->getDuration());

        auto *track1 = medias[1].get();
        ASSERT_EQ("2", track1->getTrackNumber());
        ASSERT_EQ("artist feat guest", track1->getArtist());
        ASSERT_EQ("album", track1->getAlbum());
        ASSERT_EQ("song title2", track1->getSongTitle());
        ASSERT_EQ("2016", track1->getYear());
        ASSERT_EQ(16s, track1->getStart());
        ASSERT_EQ(1min + 6s, track1->getEnd());
        ASSERT_EQ(50s, track1->getDuration());

        auto *track2 = medias[2].get();
        ASSERT_EQ("3", track2->getTrackNumber());
        ASSERT_EQ("artist", track2->getArtist());
        ASSERT_EQ("album", track2->getAlbum());
        ASSERT_EQ("song title3", track2->getSongTitle());
        ASSERT_EQ("2016", track2->getYear());
        ASSERT_EQ(1min + 10s, track2->getStart());
        ASSERT_EQ(1min + 23s, track2->getEnd());
        ASSERT_EQ(13s, track2->getDuration());
    }

    TEST_F(MediaFactoryTest, create_custom_media_via_factory) {
        std::string uri = TestUtils::getTestFileUri(TestFlacFile);
        const auto artistName = "CustomArtist";
        const auto albumTitle = "CustomTitle";
        const auto songTitle = "CustomSong";
        const auto year = "1990";
        const auto trackNumber = "10";
        const auto duration = 100ns;
        const auto start = 10ns;
        const auto end = 110ns;

        MediaInformation customInfo;
        customInfo.setArtist(artistName);
        customInfo.setAlbum(albumTitle);
        customInfo.setSongTitle(songTitle);
        customInfo.setYear(year);
        customInfo.setTrackNumber(trackNumber);
        customInfo.setTrackDuration(duration);
        customInfo.setStart(start);
        customInfo.setEnd(end);

        auto proxy = MediaFactory::createCustomMedia(uri, customInfo);
        const auto *media = proxy.get();
        ASSERT_NE(nullptr, media);
        ASSERT_EQ(artistName, media->getArtist());
        ASSERT_EQ(albumTitle, media->getAlbum());
        ASSERT_EQ(songTitle, media->getSongTitle());
        ASSERT_EQ(year, media->getYear());
        ASSERT_EQ(trackNumber, media->getTrackNumber());
        ASSERT_EQ(duration, media->getDuration());
        ASSERT_EQ(start, media->getStart());
        ASSERT_EQ(end, media->getEnd());
    }

}
