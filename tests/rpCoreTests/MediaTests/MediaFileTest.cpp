#include "MediaFileTest.h"

#include "DataConstants.h"
#include "TestUtils.h"

#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Media/Files/ApeFile.h"
#include "rpCore/Media/Files/FlacFile.h"
#include "rpCore/Media/Files/M4aFile.h"
#include "rpCore/Media/Files/Mp3File.h"
#include "rpCore/Media/Files/OggFile.h"
#include "rpCore/Media/Files/WavFile.h"

using namespace rpCore::Media;

namespace MediaTests {

    TEST_F(MediaFileTest, open_mp3_with_default_values) {
        Mp3File mp3;

        ASSERT_EQ(mp3.getFileName(), std::string(""));
        ASSERT_STREQ(mp3.getArtist().c_str(), "-");
        ASSERT_STREQ(mp3.getAlbum().c_str(), "-");
        ASSERT_STREQ(mp3.getSongTitle().c_str(), "-");
        ASSERT_STREQ(mp3.getYear().c_str(), "-");
        ASSERT_STREQ(mp3.getTrackNumber().c_str(), "-");
        ASSERT_STREQ(mp3.getTrackDuration().c_str(), "-:-");
        ASSERT_EQ(mp3.getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_mp3_with_tag_information) {
        auto proxy = TestUtils::getTestMediaFile(TestMP3File);
        auto mp3 = proxy.get();

        ASSERT_EQ(mp3->getFileName(), "test.mp3");
        ASSERT_STREQ(mp3->getArtist().c_str(), "Sinus artist");
        ASSERT_STREQ(mp3->getAlbum().c_str(), "Sinus album");
        ASSERT_STREQ(mp3->getSongTitle().c_str(), "Sinus track");
        ASSERT_STREQ(mp3->getYear().c_str(), "2014");
        ASSERT_STREQ(mp3->getTrackNumber().c_str(), "1");
        ASSERT_STREQ(mp3->getTrackDuration().c_str(), "0:00");
        ASSERT_NE(mp3->getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_wav_with_default_values) {
        WavFile wav;

        ASSERT_EQ(wav.getFileName(), std::string(""));
        ASSERT_STREQ(wav.getArtist().c_str(), "-");
        ASSERT_STREQ(wav.getAlbum().c_str(), "-");
        ASSERT_STREQ(wav.getSongTitle().c_str(), "-");
        ASSERT_STREQ(wav.getYear().c_str(), "-");
        ASSERT_STREQ(wav.getTrackNumber().c_str(), "-");
        ASSERT_STREQ(wav.getTrackDuration().c_str(), "-:-");
        ASSERT_EQ(wav.getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_wav_with_tag_information__not_on_travis_ci) {
        auto proxy = TestUtils::getTestMediaFile(TestWavFile);
        auto wav = proxy.get();

        ASSERT_EQ(wav->getFileName(), "test.wav");
        ASSERT_STREQ(wav->getArtist().c_str(), "Sinus artist");
        ASSERT_STREQ(wav->getAlbum().c_str(), "-");
        ASSERT_STREQ(wav->getSongTitle().c_str(), "Sinus track");
        ASSERT_STREQ(wav->getYear().c_str(), "2014");
        ASSERT_STREQ(wav->getTrackNumber().c_str(), "-");
        ASSERT_STREQ(wav->getTrackDuration().c_str(), "0:00");
        ASSERT_NE(wav->getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_flac_with_default_values) {
        FlacFile flac;

        ASSERT_EQ(flac.getFileName(), std::string(""));
        ASSERT_STREQ(flac.getArtist().c_str(), "-");
        ASSERT_STREQ(flac.getAlbum().c_str(), "-");
        ASSERT_STREQ(flac.getSongTitle().c_str(), "-");
        ASSERT_STREQ(flac.getYear().c_str(), "-");
        ASSERT_STREQ(flac.getTrackNumber().c_str(), "-");
        ASSERT_STREQ(flac.getTrackDuration().c_str(), "-:-");
        ASSERT_EQ(flac.getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_flac_with_tag_information) {
        auto proxy = TestUtils::getTestMediaFile(TestFlacFile);
        auto flac = proxy.get();

        ASSERT_EQ(flac->getFileName(), "test.flac");
        ASSERT_STREQ(flac->getArtist().c_str(), "Sinus artist");
        ASSERT_STREQ(flac->getAlbum().c_str(), "Sinus album");
        ASSERT_STREQ(flac->getSongTitle().c_str(), "Sinus track");
        ASSERT_STREQ(flac->getYear().c_str(), "2014");
        ASSERT_STREQ(flac->getTrackNumber().c_str(), "1");
        ASSERT_STREQ(flac->getTrackDuration().c_str(), "0:00");
        ASSERT_NE(flac->getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_m4a_with_default_values) {
        M4aFile m4a;

        ASSERT_EQ(m4a.getFileName(), std::string(""));
        ASSERT_STREQ(m4a.getArtist().c_str(), "-");
        ASSERT_STREQ(m4a.getAlbum().c_str(), "-");
        ASSERT_STREQ(m4a.getSongTitle().c_str(), "-");
        ASSERT_STREQ(m4a.getYear().c_str(), "-");
        ASSERT_STREQ(m4a.getTrackNumber().c_str(), "-");
        ASSERT_STREQ(m4a.getTrackDuration().c_str(), "-:-");
        ASSERT_EQ(m4a.getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_m4a_with_tag_information) {
        auto proxy = TestUtils::getTestMediaFile(TestM4aFile);
        auto m4a = proxy.get();

        ASSERT_EQ(m4a->getFileName(), "test.m4a");
        ASSERT_STREQ(m4a->getArtist().c_str(), "Sinus artist");
        ASSERT_STREQ(m4a->getAlbum().c_str(), "Sinus album");
        ASSERT_STREQ(m4a->getSongTitle().c_str(), "Sinus track");
        ASSERT_STREQ(m4a->getYear().c_str(), "2016");
        ASSERT_STREQ(m4a->getTrackNumber().c_str(), "1");
        ASSERT_STREQ(m4a->getTrackDuration().c_str(), "0:00");
        ASSERT_NE(m4a->getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_ape_with_default_values) {
        ApeFile ape;

        ASSERT_EQ(ape.getFileName(), std::string(""));
        ASSERT_STREQ(ape.getArtist().c_str(), "-");
        ASSERT_STREQ(ape.getAlbum().c_str(), "-");
        ASSERT_STREQ(ape.getSongTitle().c_str(), "-");
        ASSERT_STREQ(ape.getYear().c_str(), "-");
        ASSERT_STREQ(ape.getTrackNumber().c_str(), "-");
        ASSERT_STREQ(ape.getTrackDuration().c_str(), "-:-");
        ASSERT_EQ(ape.getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_ape_with_tag_information__not_on_travis_ci) {
        auto proxy = TestUtils::getTestMediaFile(TestApeFile);
        auto ape = proxy.get();

        ASSERT_EQ(ape->getFileName(), "test.ape");
        ASSERT_STREQ(ape->getArtist().c_str(), "Sinus artist");
        ASSERT_STREQ(ape->getAlbum().c_str(), "Sinus album");
        ASSERT_STREQ(ape->getSongTitle().c_str(), "Sinus track");
        ASSERT_STREQ(ape->getYear().c_str(), "-");
        ASSERT_STREQ(ape->getTrackNumber().c_str(), "1");
        ASSERT_STREQ(ape->getTrackDuration().c_str(), "0:00");
        ASSERT_NE(ape->getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_ogg_with_default_values) {
        OggFile ogg;

        ASSERT_EQ(ogg.getFileName(), std::string(""));
        ASSERT_STREQ(ogg.getArtist().c_str(), "-");
        ASSERT_STREQ(ogg.getAlbum().c_str(), "-");
        ASSERT_STREQ(ogg.getSongTitle().c_str(), "-");
        ASSERT_STREQ(ogg.getYear().c_str(), "-");
        ASSERT_STREQ(ogg.getTrackNumber().c_str(), "-");
        ASSERT_STREQ(ogg.getTrackDuration().c_str(), "-:-");
        ASSERT_EQ(ogg.getDuration().count(), 0);
    }

    TEST_F(MediaFileTest, open_ogg_with_tag_information) {
        auto proxy = TestUtils::getTestMediaFile(TestOggFile);
        auto ogg = proxy.get();

        ASSERT_EQ(ogg->getFileName(), "test.ogg");
        ASSERT_STREQ(ogg->getArtist().c_str(), "Sinus artist");
        ASSERT_STREQ(ogg->getAlbum().c_str(), "Sinus album");
        ASSERT_STREQ(ogg->getSongTitle().c_str(), "Sinus track");
        ASSERT_STREQ(ogg->getYear().c_str(), "2016");
        ASSERT_STREQ(ogg->getTrackNumber().c_str(), "1");
        ASSERT_STREQ(ogg->getTrackDuration().c_str(), "0:00");
        ASSERT_NE(ogg->getDuration().count(), 0);
    }

}
