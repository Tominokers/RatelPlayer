#include "SerializationTests.h"

#include <DataConstants.h>
#include <TestUtils.h>

#include "rpCore/Media/M3USerializer.h"
#include "rpCore/Media/MediaFactory.h"
#include "rpCore/Media/MediaFile.h"
#include "rpCore/Media/MediaInformation.h"

#include <chrono>
#include <sstream>

using namespace rpCore::Media;

namespace MediaTests {

    TEST_F(SerializationTests, export_media_list_to_m3u_playlist) {
        MediaList list = { TestUtils::getTestMediaFile(TestMP3File),
                           TestUtils::getTestMediaFile(Test2SecMP3File) };
        std::string etalon = *list[0].getUri() + '\n' + *list[1].getUri() + '\n';

        std::stringstream ss;
        M3USerializer::serialize(list, ss);
        auto exportedText = ss.str();

        ASSERT_EQ(etalon, exportedText);
    }

    TEST_F(SerializationTests, import_media_list_from_m3u_playlist) {
        MediaList list = { TestUtils::getTestMediaFile(TestMP3File),
                           TestUtils::getTestMediaFile(Test2SecMP3File) };

        std::stringstream ss;
        ss << *list[0].getUri() << '\n'
           << "# some comment " << '\n'
           << *list[1].getUri() << '\n'
           << '\n'
           << *list[0].getUri() << '\n'
           << '\n'
           << *list[0].getUri() << '\n'
           << "~/sdir/some.mp3" << '\n'
           << "# some comment " << '\n'
           << "/invalidpath.fl" << '\n'
           << *list[1].getUri() << '\n'
           << "# some comment " << '\n';

        MediaList deserialized = M3USerializer::deserialize(ss);

        ASSERT_EQ(6u, deserialized.size());
        ASSERT_EQ(*deserialized[0].getUri(), *list[0].getUri());
        ASSERT_EQ(*deserialized[1].getUri(), *list[1].getUri());
        ASSERT_EQ(*deserialized[2].getUri(), *list[0].getUri());
        ASSERT_EQ(*deserialized[3].getUri(), *list[0].getUri());
        ASSERT_EQ(*deserialized[4].getUri(), "~/sdir/some.mp3");
        ASSERT_EQ(*deserialized[5].getUri(), *list[1].getUri());
    }

    TEST_F(SerializationTests, serialize_media_from_cue_file_using_m3u) {
        auto cueFile = TestUtils::getTestMediaFile(TestCueFile);
        auto mediaList = MediaFactory::createMediasFromComposable(cueFile);
        ASSERT_EQ(3u, mediaList.size());

        ASSERT_NE(nullptr, mediaList[0].getUri());
        auto uri = *mediaList[0].getUri();

        std::stringstream ss;
        M3USerializer::serialize(mediaList, ss);

        std::array<char, 512> buffer;
        std::size_t counter = 0;
        ss.getline(buffer.data(), buffer.size());

        while(ss.good())
        {
            std::string line = buffer.data();
            ASSERT_EQ(MediaInformation::getSerializationTag(), line);

            ss.getline(buffer.data(), buffer.size());
            ASSERT_EQ('#', buffer.front());
            ss.getline(buffer.data(), buffer.size());
            ASSERT_EQ('#', buffer.front());
            ss.getline(buffer.data(), buffer.size());
            ASSERT_EQ('#', buffer.front());
            ss.getline(buffer.data(), buffer.size());
            ASSERT_EQ('#', buffer.front());

            ss.getline(buffer.data(), buffer.size());
            line = buffer.data();
            ASSERT_EQ(uri, line);

            ++counter;
            ss.getline(buffer.data(), buffer.size());
        }

        ASSERT_EQ(mediaList.size(), counter);
    }

    TEST_F(SerializationTests, deserialize_invented_media_files_using_m3u) {
        using namespace std::chrono;

        auto flacFile = TestUtils::getTestMediaFile(TestFlacFile);
        ASSERT_NE(nullptr, flacFile.getUri());
        auto uri = *flacFile.getUri();
        std::stringstream ss;

        std::string artist = "flac artist";
        std::string song = "flac song";
        std::string year = "-";
        auto time = 1000ns;

        ss << MediaInformation::getSerializationTag() << '\n'
           << '#' << artist << "0\n"
           << "#\n" /*no album*/
           << '#' << song << "0\n"
           << '#' << year << " 0 00:03 "
           << time.count() << ' ' << time.count() << ' ' << time.count() << '\n'
           << uri << '\n'
           << MediaInformation::getSerializationTag() << '\n'
           << '#' << artist << "1\n"
           << "#\n" /*no album*/
           << '#' << song << "1\n"
           << '#' << year << " 1 00:02 "
           << time.count() << ' ' << time.count() << ' ' << time.count() << '\n'
           << uri << '\n';

        auto mediaList = M3USerializer::deserialize(ss);

        ASSERT_EQ(2u, mediaList.size());

        const auto &mediaFile0 = *mediaList[0].get();
        ASSERT_EQ(artist + '0', mediaFile0.getArtist());
        ASSERT_TRUE(mediaFile0.getAlbum().empty());
        ASSERT_EQ(song + '0', mediaFile0.getSongTitle());
        ASSERT_EQ(year, mediaFile0.getYear());
        ASSERT_EQ("0", mediaFile0.getTrackNumber());
        ASSERT_EQ("00:03", mediaFile0.getTrackDuration());
        ASSERT_EQ(time, mediaFile0.getDuration());
        ASSERT_EQ(time, mediaFile0.getStart());
        ASSERT_EQ(time, mediaFile0.getEnd());
        ASSERT_EQ(uri, mediaFile0.getFileUri());

        const auto &mediaFile1 = *mediaList[1].get();
        ASSERT_EQ(artist + '1', mediaFile1.getArtist());
        ASSERT_TRUE(mediaFile1.getAlbum().empty());
        ASSERT_EQ(song + '1', mediaFile1.getSongTitle());
        ASSERT_EQ(year, mediaFile1.getYear());
        ASSERT_EQ("1", mediaFile1.getTrackNumber());
        ASSERT_EQ("00:02", mediaFile1.getTrackDuration());
        ASSERT_EQ(time, mediaFile1.getDuration());
        ASSERT_EQ(time, mediaFile1.getStart());
        ASSERT_EQ(time, mediaFile1.getEnd());
        ASSERT_EQ(uri, mediaFile1.getFileUri());
    }

    TEST_F(SerializationTests, deserialize_media_information_from_comments) {
        using namespace std::chrono;

        std::string artistName = "artistName";
        std::string albumTitle = "albumTitle";
        std::string songTitle = "songTitle";
        std::string yearText = "1990";
        std::string trackNumberText = "03";
        std::string durationText = "03:45";
        nanoseconds duration = 3min + 45s;
        nanoseconds startOffset = 2min + 17s;
        nanoseconds endOffset = startOffset + duration;

        std::stringstream ss;
        ss << '#' << artistName << '\n'
           << '#' << albumTitle << '\n'
           << '#' << songTitle << '\n'
           << '#' << yearText << ' '
           << trackNumberText << ' '
           << durationText << ' '
           << duration.count() << ' '
           << startOffset.count() << ' '
           << endOffset.count() << '\n';

        MediaInformation info;
        ASSERT_TRUE(info.deserialize(ss));
        ASSERT_EQ(artistName, info.getArtist());
        ASSERT_EQ(albumTitle, info.getAlbum());
        ASSERT_EQ(songTitle, info.getSongTitle());
        ASSERT_EQ(yearText, info.getYear());
        ASSERT_EQ(trackNumberText, info.getTrackNumber());
        ASSERT_EQ(durationText, info.getTrackDuration());
        ASSERT_EQ(duration, info.getDuration());
        ASSERT_EQ(startOffset, info.getStart());
        ASSERT_EQ(endOffset, info.getEnd());
    }

    TEST_F(SerializationTests, serialize_media_information_and_import_it_back) {
        auto cueFile = TestUtils::getTestMediaFile(TestCueFile);
        auto mediaList = MediaFactory::createMediasFromComposable(cueFile);
        ASSERT_LE(2u, mediaList.size());
        const auto &mediaInfo = mediaList[1].get()->getMediaInfo();

        std::stringstream ss;
        mediaInfo.serialize(ss);

        std::array<char, 512> buffer;
        ss.getline(buffer.data(), buffer.size());
        ASSERT_TRUE(ss.good());

        std::string tag = buffer.data();
        ASSERT_EQ(MediaInformation::getSerializationTag(), tag);

        MediaInformation importedInfo;
        ASSERT_TRUE(importedInfo.deserialize(ss));

        ASSERT_EQ(mediaInfo.getArtist(), importedInfo.getArtist());
        ASSERT_EQ(mediaInfo.getAlbum(), importedInfo.getAlbum());
        ASSERT_EQ(mediaInfo.getSongTitle(), importedInfo.getSongTitle());
        ASSERT_EQ(mediaInfo.getYear(), importedInfo.getYear());
        ASSERT_EQ(mediaInfo.getTrackNumber(), importedInfo.getTrackNumber());
        ASSERT_EQ(mediaInfo.getTrackDuration(), importedInfo.getTrackDuration());
        ASSERT_EQ(mediaInfo.getDuration(), importedInfo.getDuration());
        ASSERT_EQ(mediaInfo.getStart(), importedInfo.getStart());
        ASSERT_EQ(mediaInfo.getEnd(), importedInfo.getEnd());
    }

    TEST_F(SerializationTests, deserialized_bad_media_information_with_failures) {
        using namespace std::chrono;

        std::string artistName = "artistName";
        std::string albumTitle = "albumTitle";
        std::string songTitle = "songTitle";
        std::string yearText = "1990";
        std::string trackNumberText = "03";
        std::string durationText = "03:45";
        nanoseconds duration = 3min + 45s;
        nanoseconds startOffset = 2min + 17s;
        nanoseconds endOffset = startOffset + duration;

        std::stringstream withoutCommentSign;
        withoutCommentSign << '#' << artistName << '\n'
                           << albumTitle << '\n'
                           << '#' << songTitle << '\n'
                           << '#' << yearText << ' ' << trackNumberText << ' ' << durationText << ' '
                           << duration.count() << ' ' << startOffset.count() << ' ' << endOffset.count() << '\n';

        std::stringstream withoutTrackNumber;
        withoutTrackNumber << '#' << artistName << '\n'
                           << '#' << albumTitle << '\n'
                           << '#' << songTitle << '\n'
                           << '#' << yearText << ' ' << durationText << ' '
                           << duration.count() << ' '<< startOffset.count() << ' ' << endOffset.count() << '\n';

        std::stringstream withoutSongLine;
        withoutSongLine << '#' << artistName << '\n'
                        << '#' << albumTitle << '\n'
                        << '#' << yearText << ' ' << trackNumberText << ' ' << durationText << ' '
                        << duration.count() << ' '<< startOffset.count() << ' ' << endOffset.count() << '\n';

        std::stringstream withoutStartOffset;
        withoutStartOffset << '#' << artistName << '\n'
                           << '#' << albumTitle << '\n'
                           << '#' << songTitle << '\n'
                           << '#' << yearText << ' ' << trackNumberText << ' ' << durationText << ' '
                           << duration.count() << ' ' << endOffset.count() << '\n';

        std::stringstream withIncorrectSeparator;
        withIncorrectSeparator << '#' << artistName << '\n'
                               << '#' << albumTitle << '\n'
                               << '#' << songTitle << '\n'
                               << '#' << yearText << '#' << trackNumberText << '#' << durationText << '#'
                               << duration.count() << '#' << startOffset.count() << '#' << endOffset.count() << '\n';

        std::stringstream emptyStream;

        MediaInformation info;
        ASSERT_FALSE(info.deserialize(withoutCommentSign));
        ASSERT_FALSE(info.deserialize(withoutTrackNumber));
        ASSERT_FALSE(info.deserialize(withoutSongLine));
        ASSERT_FALSE(info.deserialize(withoutStartOffset));
        ASSERT_FALSE(info.deserialize(withIncorrectSeparator));
        ASSERT_FALSE(info.deserialize(emptyStream));

        ASSERT_EQ("-", info.getArtist());
        ASSERT_EQ("-", info.getAlbum());
        ASSERT_EQ("-", info.getSongTitle());
        ASSERT_EQ("-", info.getYear());
        ASSERT_EQ("-", info.getTrackNumber());
        ASSERT_EQ("-:-", info.getTrackDuration());
        ASSERT_EQ(0ns, info.getDuration());
        ASSERT_EQ(0ns, info.getStart());
        ASSERT_EQ(0ns, info.getEnd());
    }

}
