#include "MediaPlayerTest.h"

#include "DataConstants.h"
#include "Mocked/PlayerClientMock.h"
#include "TestUtils.h"

#include "rpCore/Media/Files/Mp3File.h"

#include <glibmm.h>

using namespace Mocked;
using namespace rpCore::Media;
using namespace rpCore::Player;

using namespace std::chrono_literals;

using ::testing::AtLeast;
using ::testing::Eq;
using ::testing::InSequence;
using ::testing::Ne;
using ::testing::NotNull;
using ::testing::Return;
using ::testing::_;

namespace PlayerTests {

    void MediaPlayerTest::SetUp() {
        m_playlist = std::make_unique<Playlist>();
        m_player = std::make_unique<CommonPlayer>();
        m_player->setPlaylist(m_playlist.get());
        m_file = TestUtils::getTestMediaFile(TestMP3File);
        m_file2 = TestUtils::getTestMediaFile(Test2SecMP3File);
    }

    rpCore::Tools::Gst::GstMessagePtr MediaPlayerTest::WaitTillStreamEnd() {
        return m_player->waitForBusMessage(4000000000, GST_MESSAGE_EOS);
    }

    TEST_F(MediaPlayerTest, setup_playlist_play__not_on_travis_ci) {
        m_playlist->addTrack(m_file);

        ASSERT_TRUE(m_player->play());
        auto message = this->WaitTillStreamEnd();
        ASSERT_NE(nullptr, message.get());
    }

    TEST_F(MediaPlayerTest, setup_playlist_play_pause_resume__not_on_travis_ci) {
        m_playlist->addTrack(m_file);

        ASSERT_TRUE(m_player->play());
        ASSERT_TRUE(m_player->pause());
        ASSERT_TRUE(m_player->play());
        auto message = this->WaitTillStreamEnd();
        ASSERT_NE(nullptr, message.get());
    }

    TEST_F(MediaPlayerTest, setup_playlist_play_stop_resume__not_on_travis_ci) {
        m_playlist->addTrack(m_file);

        ASSERT_TRUE(m_player->play());
        ASSERT_TRUE(m_player->stop());
        ASSERT_TRUE(m_player->play());
        auto message = this->WaitTillStreamEnd();
        ASSERT_NE(nullptr, message.get());
        ASSERT_TRUE(m_player->stop());
    }

    TEST_F(MediaPlayerTest, setup_empty_playlist_and_commands_fail) {
        ASSERT_FALSE(m_player->play());
        ASSERT_TRUE(m_player->stop());
        ASSERT_FALSE(m_player->pause());
        ASSERT_FALSE(m_player->nextTrack());
        ASSERT_FALSE(m_player->previousTrack());
    }

    TEST_F(MediaPlayerTest, setup_playlist_play_and_commands_work__not_on_travis_ci) {
        m_playlist->addTrack(m_file);
        m_playlist->addTrack(m_file);

        ASSERT_TRUE(m_player->play());
        ASSERT_TRUE(m_player->stop());
        ASSERT_TRUE(m_player->nextTrack());
        ASSERT_TRUE(m_player->pause());
        ASSERT_TRUE(m_player->previousTrack());
        ASSERT_FALSE(m_player->previousTrack());
        ASSERT_TRUE(m_player->nextTrack());
        ASSERT_TRUE(m_player->nextTrack());
        ASSERT_FALSE(m_player->nextTrack());
    }

    TEST_F(MediaPlayerTest, setup_player_client_and_get_progress_of_track__not_on_travis_ci) {
        auto context = Glib::MainContext::get_default();

        m_playlist->addTrack(m_file2);

        PlayerClientMock client;
        EXPECT_CALL(client, setProgress(Ne(PlayingProgress {0ns, 0ns})))
            .WillOnce(Return());
        EXPECT_CALL(client, setTrackIndex(NotNull(), 0))
            .Times(3);

        m_player->setClient(&client);

        ASSERT_TRUE(client.isListening());
        ASSERT_TRUE(m_player->play());
        auto message = this->WaitTillStreamEnd();

        ASSERT_NE(nullptr, message.get());
        ASSERT_EQ(GST_MESSAGE_EOS, GST_MESSAGE_TYPE(message.get()));
        ASSERT_TRUE(context->pending());
        ASSERT_TRUE(context->iteration(true));

        ASSERT_TRUE(m_player->stop());
        ASSERT_FALSE(client.isListening());
        ASSERT_TRUE(m_player->play());
        ASSERT_TRUE(client.isListening());
        ASSERT_TRUE(m_player->pause());
        ASSERT_FALSE(client.isListening());
        ASSERT_TRUE(m_player->play());
        ASSERT_TRUE(client.isListening());
        ASSERT_TRUE(m_player->stop());
        ASSERT_FALSE(client.isListening());
    }

    TEST_F(MediaPlayerTest, setup_player_client_and_get_current_track_index__not_on_travis_ci) {
        m_playlist->addTrack(m_file);
        m_playlist->addTrack(m_file);
        m_playlist->addTrack(m_file);

        PlayerClientMock client;
        m_player->setClient(&client);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 0));
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 1));
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 2));

        ASSERT_TRUE(client.isListening());
        ASSERT_TRUE(m_player->play());
        auto message = this->WaitTillStreamEnd();
        ASSERT_NE(nullptr, message.get());
        ASSERT_EQ(GST_MESSAGE_EOS, GST_MESSAGE_TYPE(message.get()));

        // DANGER: Send stolen message to player bus handler by own hands!
        ASSERT_EQ(TRUE, rpCore::Player::handleBusMessage(nullptr, message.get(), m_player.get()));

        auto message2 = this->WaitTillStreamEnd();
        ASSERT_NE(nullptr, message2.get());
        ASSERT_EQ(GST_MESSAGE_EOS, GST_MESSAGE_TYPE(message2.get()));
        ASSERT_EQ(TRUE, rpCore::Player::handleBusMessage(nullptr, message.get(), m_player.get()));
        this->WaitTillStreamEnd();
    }

    TEST_F(MediaPlayerTest, setup_player_client_and_set_current_position__not_on_travis_ci) {
        m_playlist->addTrack(m_file);

        PlayerClientMock client;
        m_player->setClient(&client);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 0));
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {0ns        , 156734693ns})))
            .Times(2);
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {78367346ns , 156734693ns})));
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {156734693ns, 156734693ns})))
            .Times(2);

        ASSERT_TRUE(m_player->play());
        ASSERT_TRUE(m_player->pause());

        m_player->setPosition(0.0);
        m_player->setPosition(0.5);
        m_player->setPosition(1.0);
        m_player->setPosition(-1.0);
        m_player->setPosition(2.0);
    }

    TEST_F(MediaPlayerTest, setup_playlist_and_current_track) {
        m_playlist->addTrack(m_file);
        m_playlist->addTrack(m_file);
        m_playlist->addTrack(m_file2);
        m_playlist->addTrack(m_file2);

        InSequence dummy;
        PlayerClientMock client;
        m_player->setClient(&client);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 1));
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 3));
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 2));
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), rpCore::Player::NoIndex)).Times(2);

        m_player->setCurrentTrackIndex(1);
        m_player->setCurrentTrackIndex(3);
        m_player->setCurrentTrackIndex(2);
        m_player->setCurrentTrackIndex(rpCore::Player::NoIndex);
        m_player->setCurrentTrackIndex(5);
    }

    TEST_F(MediaPlayerTest, pausing_player_doesnt_break_flow_manager__not_on_travis_ci) {
        m_playlist->addTrack(m_file2);
        m_playlist->addTrack(m_file);

        PlayerClientMock client;
        m_player->setClient(&client);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 0))
            .Times(2);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 1));

        ASSERT_TRUE(m_player->play());
        ASSERT_TRUE(m_player->pause());
        ASSERT_TRUE(m_player->play());

        auto message = this->WaitTillStreamEnd();
        // DANGER: Send stolen message to player bus handler by own hands!
        ASSERT_EQ(TRUE, rpCore::Player::handleBusMessage(nullptr, message.get(), m_player.get()));
    }

    TEST_F(MediaPlayerTest, play_playlist_with_cue_file__not_on_travis_ci) {
        m_playlist->addTrack(TestUtils::getTestMediaFile(TestCueFile));

        PlayerClientMock client;
        m_player->setClient(&client);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 0));
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {0s , 1min + 23s})));
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 1));
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {16s , 1min + 23s})));
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 2));
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {1min + 10s , 1min + 23s})));

        ASSERT_TRUE(m_player->play());
        ASSERT_TRUE(m_player->nextTrack());
        ASSERT_TRUE(m_player->nextTrack());
        ASSERT_FALSE(m_player->nextTrack());
    }

    // This test verifies regression which is crash on access to current played track,
    // when it was actually removed.
    TEST_F(MediaPlayerTest, remove_currently_played_track_from_cue__not_on_travis_ci) {
        auto context = Glib::MainContext::get_default();
        m_playlist->addTrack(TestUtils::getTestMediaFile(TestCueFile));

        PlayerClientMock client;
        m_player->setClient(&client);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 1)).Times(2);
        EXPECT_CALL(client, setProgress(_));
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {16s , 1min + 23s})));
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), rpCore::Player::NoIndex));

        m_player->setCurrentTrackIndex(1);
        ASSERT_TRUE(m_player->play());
        m_playlist->removeTracks({1});

        sleep(1); // Wait 1 second so time out should be fired and handled by next lines.

        ASSERT_TRUE(context->pending());
        ASSERT_TRUE(context->iteration(true)); // Shouldn't crash here.
        ASSERT_TRUE(m_player->stop());
    }

    TEST_F(MediaPlayerTest, set_position_in_cue_file_causes_next_previous_track__not_on_travis_ci) {
        m_playlist->addTrack(TestUtils::getTestMediaFile(TestCueFile));

        PlayerClientMock client;
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 0)).Times(2);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 1)).Times(2);
        EXPECT_CALL(client, setTrackIndex(m_playlist.get(), 2));
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {0s , 1min + 23s}))).Times(2);
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {16s , 1min + 23s}))).Times(2);
        EXPECT_CALL(client, setProgress(Eq(PlayingProgress {1min + 10s , 1min + 23s})));
        m_player->setClient(&client);

        ASSERT_TRUE(m_player->play());
        m_player->setPosition(0.5);
        m_player->setPosition(0.96);
        m_player->setPosition(0.05);
        m_player->setPosition(0.05);
        ASSERT_TRUE(m_player->stop());
    }

    TEST_F(MediaPlayerTest, play_not_existed_file_and_get_error_about_it) {
        m_playlist->addTrack(TestUtils::getTestMediaFile("not_existed.mp3"));

        PlayerClientMock client;
        EXPECT_CALL(client, handleErrorNotification(_));
        m_player->setClient(&client);

        ASSERT_FALSE(m_player->play());
    }

}
