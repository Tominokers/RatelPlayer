#pragma once

#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Player/CommonPlayer.h"
#include "rpCore/Player/Playlist.h"

#include <gtest/gtest.h>

#include <memory>

namespace PlayerTests {

    class MediaPlayerTest : public testing::Test
    {
    public:
        void SetUp();

        rpCore::Tools::Gst::GstMessagePtr WaitTillStreamEnd();

    public:
        std::unique_ptr<rpCore::Player::CommonPlayer> m_player;
        std::unique_ptr<rpCore::Player::Playlist> m_playlist;
        rpCore::Media::MediaProxy m_file;
        rpCore::Media::MediaProxy m_file2;

    };

}
