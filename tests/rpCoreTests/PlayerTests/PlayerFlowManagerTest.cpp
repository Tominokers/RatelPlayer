#include "PlayerFlowManagerTest.h"

#include "Mocked/PlayerClientMock.h"
#include "DataConstants.h"
#include "TestUtils.h"

#include "rpCore/Media/Files/Mp3File.h"
#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Player/PlayerFlowManager.h"
#include "rpCore/Player/Playlist.h"
#include "rpCore/Player/PlaylistSortType.h"

using ::testing::InSequence;

using rpCore::Player::FirstIndex;
using rpCore::Player::NoIndex;
using rpCore::Player::TrackIndex;
using rpCore::Player::TrackIndexList;

namespace PlayerTests {

    TEST_F(PlayerFlowManagerTests, create_default_player_flow_manager) {
        rpCore::Player::PlayerFlowManager manager;

        ASSERT_EQ(rpCore::Player::RepeatMode::None, manager.getRepeatMode());
        ASSERT_EQ(rpCore::Player::ShuffleMode::None, manager.getShuffleMode());
        ASSERT_EQ(nullptr, manager.getPlaylist());
    }

    TEST_F(PlayerFlowManagerTests, create_with_playlist_and_use_next_previous) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);
        rpCore::Player::Playlist playlist;

        rpCore::Player::PlayerFlowManager manager(&playlist);

        playlist.addTrack(file);
        playlist.addTrack(file);

        ASSERT_EQ(FirstIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        ASSERT_TRUE(manager.next());
        ASSERT_EQ(1u, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        ASSERT_TRUE(manager.previous());
        ASSERT_EQ(FirstIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        ASSERT_FALSE(manager.previous());
        ASSERT_EQ(NoIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_TRUE(manager.getCurrentTrack().get() == nullptr);

        ASSERT_TRUE(manager.next());
        ASSERT_EQ(FirstIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        ASSERT_TRUE(manager.next());
        ASSERT_EQ(1u, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        ASSERT_FALSE(manager.next());
        ASSERT_EQ(NoIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
    }

    TEST_F(PlayerFlowManagerTests, set_current_index_in_playlist) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);
        auto file2 = TestUtils::getTestMediaFile(Test2SecMP3File);

        rpCore::Player::Playlist playlist;
        rpCore::Player::PlayerFlowManager manager(&playlist);

        playlist.addTrack(file);
        playlist.addTrack(file);
        playlist.addTrack(file2);
        playlist.addTrack(file2);

        ASSERT_EQ(FirstIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        manager.setCurrentIndex(1u);
        ASSERT_EQ(1u, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        manager.setCurrentIndex(3u);
        ASSERT_EQ(3u, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file2.get(), manager.getCurrentTrack().get());

        manager.setCurrentIndex(2u);
        ASSERT_EQ(2u, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file2.get(), manager.getCurrentTrack().get());

        manager.setCurrentIndex(NoIndex);
        ASSERT_EQ(NoIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_TRUE(manager.getCurrentTrack().get() == nullptr);

        manager.setCurrentIndex(4u);
        ASSERT_EQ(NoIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());

        manager.setCurrentIndex(FirstIndex);
        ASSERT_EQ(FirstIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());
    }

    TEST_F(PlayerFlowManagerTests, perform_repeat_mode_playilst) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);
        auto file2 = TestUtils::getTestMediaFile(Test2SecMP3File);

        rpCore::Player::Playlist playlist;
        playlist.addTrack(file);
        playlist.addTrack(file2);

        rpCore::Player::PlayerFlowManager manager(&playlist);
        manager.setRepeatMode(rpCore::Player::RepeatMode::WholePlaylist);

        ASSERT_EQ(FirstIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        ASSERT_TRUE(manager.next());
        ASSERT_EQ(1u, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file2.get(), manager.getCurrentTrack().get());

        ASSERT_TRUE(manager.next());
        ASSERT_EQ(FirstIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        ASSERT_TRUE(manager.previous());
        ASSERT_EQ(1u, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file2.get(), manager.getCurrentTrack().get());

        manager.setCurrentIndex(NoIndex);
        ASSERT_TRUE(manager.next());
        ASSERT_EQ(FirstIndex, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file.get(), manager.getCurrentTrack().get());

        manager.setCurrentIndex(NoIndex);
        ASSERT_TRUE(manager.previous());
        ASSERT_EQ(1u, manager.getCurrentIndex());
        ASSERT_NO_THROW(manager.getCurrentTrack());
        ASSERT_EQ(file2.get(), manager.getCurrentTrack().get());
    }

    TEST_F(PlayerFlowManagerTests, DISABLED_perform_shuffle_by_track_repeat_mode_playlist) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);

        rpCore::Player::Playlist playlist;
        for(TrackIndex i = 0; i < 4; ++i)
            playlist.addTrack(file);

        rpCore::Player::PlayerFlowManager manager(&playlist);
        manager.setRepeatMode(rpCore::Player::RepeatMode::None);
        manager.setShuffleMode(rpCore::Player::ShuffleMode::ByTrack);

        TrackIndexList playlistOrder;

        ASSERT_TRUE(manager.next());
        auto newIndex = manager.getCurrentIndex();
        auto index = newIndex;
        playlistOrder.push_back(newIndex);

        ASSERT_TRUE(manager.next());
        newIndex = manager.getCurrentIndex();
        ASSERT_NE(index, newIndex);
        index = newIndex;
        playlistOrder.push_back(newIndex);

        ASSERT_TRUE(manager.next());
        newIndex = manager.getCurrentIndex();
        ASSERT_NE(index, newIndex);
        index = newIndex;
        playlistOrder.push_back(newIndex);

        ASSERT_TRUE(manager.next());
        newIndex = manager.getCurrentIndex();
        ASSERT_NE(index, newIndex);
        playlistOrder.push_back(newIndex);

        ASSERT_FALSE(manager.next());
        newIndex = manager.getCurrentIndex();
        ASSERT_EQ(NoIndex, newIndex);
        ASSERT_FALSE(std::is_sorted(std::begin(playlistOrder),
                                    std::end(playlistOrder))
                     && "It will be a bad shuffle if this check fails!");
    }

    TEST_F(PlayerFlowManagerTests, DISABLED_check_playlist_resuffling_on_going_backward) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);

        rpCore::Player::Playlist playlist;
        for(TrackIndex i = 0; i < 20; ++i)
            playlist.addTrack(file);

        rpCore::Player::PlayerFlowManager manager(&playlist);
        manager.setRepeatMode(rpCore::Player::RepeatMode::None);
        manager.setShuffleMode(rpCore::Player::ShuffleMode::ByTrack);

        TrackIndexList playlistOrder;

        ASSERT_TRUE(manager.previous());
        auto newIndex = manager.getCurrentIndex();
        auto index = newIndex;

        ASSERT_FALSE(manager.previous());
        ASSERT_EQ(NoIndex, manager.getCurrentIndex());

        ASSERT_TRUE(manager.next());
        newIndex = manager.getCurrentIndex();
        ASSERT_NE(index, newIndex);
        playlistOrder.push_back(newIndex);
        index = newIndex;

        ASSERT_TRUE(manager.next());
        playlistOrder.push_back(manager.getCurrentIndex());
        ASSERT_TRUE(manager.next());

        ASSERT_TRUE(manager.previous());
        ASSERT_EQ(playlistOrder[1], manager.getCurrentIndex());

        ASSERT_TRUE(manager.previous());
        ASSERT_EQ(playlistOrder[0], manager.getCurrentIndex());

        ASSERT_FALSE(manager.previous());
        ASSERT_EQ(NoIndex, manager.getCurrentIndex());

        ASSERT_TRUE(manager.next());
        ASSERT_NE(playlistOrder[0], manager.getCurrentIndex());
    }

    TEST_F(PlayerFlowManagerTests, DISABLED_check_playlist_resuffling_on_removing_adding_new_tracks) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);

        rpCore::Player::Playlist playlist;
        for(TrackIndex i = 0; i < 20; ++i)
            playlist.addTrack(file);

        rpCore::Player::PlayerFlowManager manager(&playlist);
        manager.setRepeatMode(rpCore::Player::RepeatMode::None);
        manager.setShuffleMode(rpCore::Player::ShuffleMode::ByTrack);

        ASSERT_TRUE(manager.next());
        ASSERT_TRUE(manager.next());

        playlist.addTrack(file);

        ASSERT_TRUE(manager.previous());
        ASSERT_TRUE(manager.next());

        playlist.removeTracks({9});

        ASSERT_TRUE(manager.previous());
        ASSERT_TRUE(manager.next());
    }

    TEST_F(PlayerFlowManagerTests, check_repeat_of_playlist_in_shuffle_mode) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);

        rpCore::Player::Playlist playlist;
        const std::size_t trackCount = 5;
        for(TrackIndex i = 0; i < trackCount; ++i)
            playlist.addTrack(file);

        rpCore::Player::PlayerFlowManager manager(&playlist);
        manager.setRepeatMode(rpCore::Player::RepeatMode::WholePlaylist);
        manager.setShuffleMode(rpCore::Player::ShuffleMode::ByTrack);

        TrackIndexList playlistOrder0;
        for (TrackIndex i = 0; i < trackCount; ++i)
        {
            ASSERT_TRUE(manager.next());
            playlistOrder0.push_back(manager.getCurrentIndex());
        }

        TrackIndexList playlistOrder1;
        for (TrackIndex i = 0; i < trackCount; ++i)
        {
            ASSERT_TRUE(manager.next());
            playlistOrder1.push_back(manager.getCurrentIndex());
        }

        ASSERT_NE(playlistOrder0, playlistOrder1);
    }

    TEST_F(PlayerFlowManagerTests, check_synchronization_of_track_on_playlist_manipulation) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);
        auto file2 = TestUtils::getTestMediaFile(Test2SecMP3File);

        rpCore::Player::Playlist playlist;
        for(TrackIndex i = 0; i < 10; ++i)
            playlist.addTrack(file);

        InSequence dummy;
        Mocked::PlayerClientMock client;
        EXPECT_CALL(client, setTrackIndex(&playlist, 5u)); // set index
        EXPECT_CALL(client, setTrackIndex(&playlist, 8u)); // insert
        EXPECT_CALL(client, setTrackIndex(&playlist, 7u)); // remove 1 part 1
        EXPECT_CALL(client, setTrackIndex(&playlist, 6u)); // remove 1 part 2
        EXPECT_CALL(client, setTrackIndex(&playlist, NoIndex)); // remove 2
        EXPECT_CALL(client, setTrackIndex(&playlist, 3u)); // set index
        EXPECT_CALL(client, setTrackIndex(&playlist, 6u)); // index after sorting
        EXPECT_CALL(client, setTrackIndex(&playlist, NoIndex)); // clear

        rpCore::Player::PlayerFlowManager manager(&playlist, &client);
        manager.setCurrentIndex(5u);

        playlist.insertTracks(3u, {file, file, file});
        playlist.removeTracks({0, 2, 9, 10});
        playlist.removeTracks({5, 6, 7});
        playlist.insertTracks(3u, {file2});

        manager.setCurrentIndex(3u);

        playlist.sort(rpCore::Player::PlaylistSortType::Duration);
        playlist.clearTracks();
    }

    TEST_F(PlayerFlowManagerTests, perform_repeat_track_mode) {
        auto file = TestUtils::getTestMediaFile(TestMP3File);
        auto file2 = TestUtils::getTestMediaFile(Test2SecMP3File);

        rpCore::Player::Playlist playlist;
        playlist.addTrack(file);
        playlist.addTrack(file2);

        rpCore::Player::PlayerFlowManager manager(&playlist);
        manager.setRepeatMode(rpCore::Player::RepeatMode::Track);
        manager.setShuffleMode(rpCore::Player::ShuffleMode::None);

        TrackIndex index = 0;
        manager.setCurrentIndex(index);
        for (auto i = 0; i < 5; ++i)
        {
            ASSERT_TRUE(manager.next());
            ASSERT_EQ(index, manager.getCurrentIndex());
        }
        index = 1;
        manager.setCurrentIndex(index);
        for (auto i = 0; i < 5; ++i)
        {
            ASSERT_TRUE(manager.previous());
            ASSERT_EQ(index, manager.getCurrentIndex());
        }

        index = 0;
        manager.setCurrentIndex(index);
        manager.setShuffleMode(rpCore::Player::ShuffleMode::ByTrack);
        for (auto i = 0; i < 5; ++i)
        {
            ASSERT_TRUE(manager.next());
            ASSERT_EQ(index, manager.getCurrentIndex());
        }
        index = 1;
        manager.setCurrentIndex(index);
        for (auto i = 0; i < 5; ++i)
        {
            ASSERT_TRUE(manager.previous());
            ASSERT_EQ(index, manager.getCurrentIndex());
        }

    }

}
