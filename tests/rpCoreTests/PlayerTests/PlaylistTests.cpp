#include "PlaylistTests.h"

#include "DataConstants.h"
#include "Mocked/PlaylistChangesListenerMock.h"
#include "TestUtils.h"

#include "rpCore/Player/PlaylistSortType.h"
#include "rpCore/Media/MediaFile.h"

#include <stdexcept>

using ::testing::Invoke;
using ::testing::Ne;
using ::testing::_;

using namespace rpCore::Player;

namespace {

    auto createMockedPlaylistListener(Playlist &playlist) {
        auto deleteListener = [&playlist](auto *instance)
            {
                playlist.unregistrateListener(instance);
                delete instance;
            };

        using ListenerMockPtr = std::unique_ptr<Mocked::PlaylistChangesListenerMock, decltype(deleteListener)>;
        auto listener = ListenerMockPtr(new Mocked::PlaylistChangesListenerMock(), deleteListener);

        playlist.registrateListener(listener.get());
        return listener;
    }
}

namespace PlayerTests {

    PlaylistTests::PlaylistTests()
    {
        auto directory = BigMediaDirectory.substr(1) + "/";
        m_initList = { TestUtils::getTestMediaFile(directory + "test 1.mp3"),   // 0
                       TestUtils::getTestMediaFile(directory + "test 4.mp3"),   // 1
                       TestUtils::getTestMediaFile(directory + "test 3.mp3"),   // 2
                       TestUtils::getTestMediaFile(directory + "test 8.mp3"),   // 3
                       TestUtils::getTestMediaFile(directory + "test 9.mp3"),   // 4
                       TestUtils::getTestMediaFile(directory + "test 5.mp3"),   // 5
                       TestUtils::getTestMediaFile(directory + "test 2.mp3"),   // 6
                       TestUtils::getTestMediaFile(directory + "test 7.mp3"),   // 7
                       TestUtils::getTestMediaFile(directory + "test 11.mp3"),  // 8
                       TestUtils::getTestMediaFile(directory + "test 6.mp3"),   // 9
                       TestUtils::getTestMediaFile(directory + "test 12.mp3"),  // 10
                       TestUtils::getTestMediaFile(directory + "test 10.mp3") };// 11

        for (auto &it : m_initList)
            m_initUriList.push_back(*(it.getUri()));

        m_compare = [](const auto &lhs, const auto &rhs) -> bool
        {
            for (std::size_t i = 0; i < lhs.size(); ++i)
                if (*lhs.at(i).getUri() != rhs[i])
                    return false;
            return true;
        };
    }

    TEST_F(PlaylistTests, create_empty_playlist) {
        Playlist playlist;

        ASSERT_EQ(0u, playlist.size());
        ASSERT_THROW(playlist.at(0), std::out_of_range);
    }

    TEST_F(PlaylistTests, create_playlist_with_fake_files) {
        Playlist playlist;

        std::size_t count = 10;
        rpCore::Media::MediaList fakeFiles;
        for (std::size_t i = 0; i < count; ++i)
        {
            rpCore::Media::MediaProxy fakeFile;

            // Adds same files in playlist and some vector.
            fakeFiles.push_back(fakeFile);
            playlist.addTrack(fakeFile);
        }

        EXPECT_EQ(count, playlist.size());
        EXPECT_NO_THROW(playlist.at(0));
        EXPECT_NO_THROW(playlist.at(count-1));

        // Checks order of files in playlist and vector is equal.
        for (size_t i = 0; i < count; ++i)
            EXPECT_EQ(playlist.at(i).get(), fakeFiles[i].get());
    }

    TEST_F(PlaylistTests, create_playlist_with_few_files) {
        MediaList list = { TestUtils::getTestMediaFile(TestMP3File),
                           TestUtils::getTestMediaFile(TestWavFile),
                           TestUtils::getTestMediaFile(TestFlacFile) };
        std::vector<std::string> nameList = { TestUtils::getTestFileUri(TestMP3File),
                                              TestUtils::getTestFileUri(TestWavFile),
                                              TestUtils::getTestFileUri(TestFlacFile) };
        auto names = nameList;

        Playlist playlist;
        ASSERT_EQ(0u, playlist.size());

        playlist.insertTracks(playlist.size(), list);
        ASSERT_EQ(3u, playlist.size());
        ASSERT_TRUE(m_compare(playlist, names));

        names.insert(std::end(names), std::begin(nameList), std::end(nameList));
        playlist.insertTracks(playlist.size() + 10, list);
        ASSERT_EQ(6u, playlist.size());
        ASSERT_TRUE(m_compare(playlist, names));

        names.insert(std::begin(names) + 2, std::begin(nameList), std::end(nameList));
        playlist.insertTracks(2, list);
        ASSERT_EQ(9u, playlist.size());
        ASSERT_TRUE(m_compare(playlist, names));
    }

    TEST_F(PlaylistTests, sort_playlist_by_track_number) {
        // 1 2 3 4 5 6 7 8 9 10 11 12
        MediaList sortedList = { m_initList[0], m_initList[6], m_initList[2], m_initList[1],
                                 m_initList[5], m_initList[9], m_initList[7], m_initList[3],
                                 m_initList[4], m_initList[11],m_initList[8], m_initList[10] };

        std::vector<std::string> sortedUriListForward;
        for (auto &it : sortedList)
            sortedUriListForward.push_back(*(it.getUri()));

        std::vector<std::string> sortedUriListBackward;
        std::copy(std::rbegin(sortedUriListForward),
                  std::rend(sortedUriListForward),
                  std::back_inserter(sortedUriListBackward));

        Playlist playlist;
        playlist.insertTracks(playlist.size(), m_initList);

        ASSERT_EQ(playlist.size(), m_initUriList.size());
        ASSERT_TRUE(m_compare(playlist, m_initUriList));
        playlist.sort(PlaylistSortType::TrackNumber);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
        playlist.sort(PlaylistSortType::TrackNumber);
        ASSERT_TRUE(m_compare(playlist, sortedUriListBackward));
        playlist.sort(PlaylistSortType::TrackNumber);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
    }

    TEST_F(PlaylistTests, sort_playlist_by_album) {
        // 1 3 6 2 4 5 7 8 9 10 11 12
        MediaList sortedForwardList = { m_initList[0], m_initList[2], m_initList[9], m_initList[6],
                                        m_initList[1], m_initList[5], m_initList[7], m_initList[3],
                                        m_initList[4], m_initList[11],m_initList[8], m_initList[10] };

        // 10 11 12 5 7 8 9 2 4 1 3 6
        MediaList sortedBackwardList = { m_initList[11],m_initList[8], m_initList[10],m_initList[5],
                                         m_initList[7], m_initList[3], m_initList[4], m_initList[6],
                                         m_initList[1], m_initList[0], m_initList[2], m_initList[9] };

        std::vector<std::string> sortedUriListForward;
        for (auto &it : sortedForwardList)
            sortedUriListForward.push_back(*(it.getUri()));

        std::vector<std::string> sortedUriListBackward;
        for (auto &it : sortedBackwardList)
            sortedUriListBackward.push_back(*(it.getUri()));

        Playlist playlist;
        playlist.insertTracks(playlist.size(), m_initList);

        ASSERT_EQ(playlist.size(), m_initUriList.size());
        ASSERT_TRUE(m_compare(playlist, m_initUriList));
        playlist.sort(PlaylistSortType::Album);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
        playlist.sort(PlaylistSortType::Album);
        ASSERT_TRUE(m_compare(playlist, sortedUriListBackward));
        playlist.sort(PlaylistSortType::Album);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
    }

    TEST_F(PlaylistTests, sort_playlist_by_track_name) {
        // 7 6 9 2 11 8 1 10 3 4 5 12
        MediaList sortedList = { m_initList[7], m_initList[9], m_initList[4], m_initList[6],
                                 m_initList[8], m_initList[3], m_initList[0], m_initList[11],
                                 m_initList[2], m_initList[1], m_initList[5], m_initList[10] };

        std::vector<std::string> sortedUriListForward;
        for (auto &it : sortedList)
            sortedUriListForward.push_back(*(it.getUri()));

        std::vector<std::string> sortedUriListBackward;
        std::copy(std::rbegin(sortedUriListForward),
                  std::rend(sortedUriListForward),
                  std::back_inserter(sortedUriListBackward));

        Playlist playlist;
        playlist.insertTracks(playlist.size(), m_initList);

        ASSERT_EQ(playlist.size(), m_initUriList.size());
        ASSERT_TRUE(m_compare(playlist, m_initUriList));
        playlist.sort(PlaylistSortType::TrackName);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
        playlist.sort(PlaylistSortType::TrackName);
        ASSERT_TRUE(m_compare(playlist, sortedUriListBackward));
        playlist.sort(PlaylistSortType::TrackName);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
    }

    TEST_F(PlaylistTests, sort_playlist_by_year) {
        // 9 12 10 11 67 3 8 1 4 2 5
        MediaList sortedForwardList = { m_initList[4], m_initList[10],m_initList[11],m_initList[8],
                                        m_initList[9], m_initList[7], m_initList[2], m_initList[3],
                                        m_initList[0], m_initList[1], m_initList[6], m_initList[5] };

        // 2 5 1 4 3 8 6 7 10 11 9 12
        MediaList sortedBackwardList = { m_initList[6], m_initList[5], m_initList[0], m_initList[1],
                                         m_initList[2], m_initList[3], m_initList[9], m_initList[7],
                                         m_initList[11],m_initList[8], m_initList[4], m_initList[10] };

        std::vector<std::string> sortedUriListForward;
        for (auto &it : sortedForwardList)
            sortedUriListForward.push_back(*(it.getUri()));

        std::vector<std::string> sortedUriListBackward;
        for (auto &it : sortedBackwardList)
            sortedUriListBackward.push_back(*(it.getUri()));

        Playlist playlist;
        playlist.insertTracks(playlist.size(), m_initList);

        ASSERT_EQ(playlist.size(), m_initUriList.size());
        ASSERT_TRUE(m_compare(playlist, m_initUriList));
        playlist.sort(PlaylistSortType::Year);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
        playlist.sort(PlaylistSortType::Year);
        ASSERT_TRUE(m_compare(playlist, sortedUriListBackward));
        playlist.sort(PlaylistSortType::Year);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
    }

    TEST_F(PlaylistTests, sort_playlist_by_artist) {
        // 1 8 10 6 5 7 9 11 12 3 4 2
        MediaList sortedForwardList = { m_initList[0], m_initList[3], m_initList[11],m_initList[9],
                                        m_initList[5], m_initList[7], m_initList[4], m_initList[8],
                                        m_initList[10],m_initList[2], m_initList[1], m_initList[6] };

        // 2 3 4 9 11 12 6 5 7 1 8 10
        MediaList sortedBackwardList = { m_initList[6], m_initList[2], m_initList[1], m_initList[4],
                                         m_initList[8], m_initList[10],m_initList[9], m_initList[5],
                                         m_initList[7], m_initList[0], m_initList[3], m_initList[11]};

        std::vector<std::string> sortedUriListForward;
        for (auto &it : sortedForwardList)
            sortedUriListForward.push_back(*(it.getUri()));

        std::vector<std::string> sortedUriListBackward;
        for (auto &it : sortedBackwardList)
            sortedUriListBackward.push_back(*(it.getUri()));

        Playlist playlist;
        playlist.insertTracks(playlist.size(), m_initList);

        ASSERT_EQ(playlist.size(), m_initUriList.size());
        ASSERT_TRUE(m_compare(playlist, m_initUriList));
        playlist.sort(PlaylistSortType::Artist);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
        playlist.sort(PlaylistSortType::Artist);
        ASSERT_TRUE(m_compare(playlist, sortedUriListBackward));
        playlist.sort(PlaylistSortType::Artist);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
    }

    TEST_F(PlaylistTests, sort_playlist_by_duration) {
        MediaList initDurationList = { TestUtils::getTestMediaFile(Test2SecMP3File), m_initList[0] };
        MediaList sortedForwardList = { m_initList[0], TestUtils::getTestMediaFile(Test2SecMP3File) };
        MediaList sortedBackwardList = { TestUtils::getTestMediaFile(Test2SecMP3File), m_initList[0] };

        std::vector<std::string> sortedUriListForward;
        for (auto &it : sortedForwardList)
            sortedUriListForward.push_back(*(it.getUri()));

        std::vector<std::string> sortedUriListBackward;
        for (auto &it : sortedBackwardList)
            sortedUriListBackward.push_back(*(it.getUri()));

        std::vector<std::string> initDurationUriList;
        for (auto &it : initDurationList)
            initDurationUriList.push_back(*(it.getUri()));

        Playlist playlist;
        playlist.insertTracks(playlist.size(), initDurationList);

        ASSERT_EQ(playlist.size(), initDurationUriList.size());
        ASSERT_TRUE(m_compare(playlist, initDurationUriList));
        playlist.sort(PlaylistSortType::Duration);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
        playlist.sort(PlaylistSortType::Duration);
        ASSERT_TRUE(m_compare(playlist, sortedUriListBackward));
        playlist.sort(PlaylistSortType::Duration);
        ASSERT_TRUE(m_compare(playlist, sortedUriListForward));
    }

    TEST_F(PlaylistTests, remove_tracks_from_playlist) {
        Playlist playlist;

        playlist.insertTracks(playlist.size(), m_initList);
        playlist.removeTracks({0, 3, 5, 7, 8 , 9, 11});

        ASSERT_EQ(5u, playlist.size());
        ASSERT_EQ(*m_initList[1].getUri(), *playlist.at(0).getUri());
        ASSERT_EQ(*m_initList[2].getUri(), *playlist.at(1).getUri());
        ASSERT_EQ(*m_initList[4].getUri(), *playlist.at(2).getUri());
        ASSERT_EQ(*m_initList[6].getUri(), *playlist.at(3).getUri());
        ASSERT_EQ(*m_initList[10].getUri(), *playlist.at(4).getUri());
    }

    TEST_F(PlaylistTests, catch_reporting_of_adding_removing) {
        Playlist playlist;

        auto listener = createMockedPlaylistListener(playlist);
        EXPECT_CALL(*listener, reportAddedMedia(&playlist, 0, m_initList.size()));
        EXPECT_CALL(*listener, reportAddedMedia(&playlist, 5, 6));
        EXPECT_CALL(*listener, reportAddedMedia(&playlist, 1, 4));

        EXPECT_CALL(*listener, reportRemovedMedia(&playlist, 11, 12));
        EXPECT_CALL(*listener, reportRemovedMedia(&playlist, 7, 10));
        EXPECT_CALL(*listener, reportRemovedMedia(&playlist, 5, 6));
        EXPECT_CALL(*listener, reportRemovedMedia(&playlist, 3, 4));
        EXPECT_CALL(*listener, reportRemovedMedia(&playlist, 0, 1)).Times(2);
        EXPECT_CALL(*listener, reportRemovedMedia(&playlist, 6, 9));
        EXPECT_CALL(*listener, reportRemovedMedia(&playlist, 0, 5));

        playlist.insertTracks(playlist.size(), m_initList);
        playlist.removeTracks({0, 3, 5, 7, 8, 9, 11});
        playlist.addTrack(m_initList[0]);
        playlist.insertTracks(1, { m_initList[0], m_initList[1], m_initList[2] });
        playlist.removeTracks({6, 7, 8});
        playlist.removeTracks({0});
        playlist.removeTracks({});
        playlist.clearTracks();
    }

    TEST_F(PlaylistTests, catch_reporting_on_sorting) {
        Playlist playlist;

        auto listener = createMockedPlaylistListener(playlist);
        EXPECT_CALL(*listener, reportPlaylistSorting(&playlist)).Times(6);

        playlist.sort(PlaylistSortType::Album);
        playlist.sort(PlaylistSortType::Artist);
        playlist.sort(PlaylistSortType::TrackName);
        playlist.sort(PlaylistSortType::TrackNumber);
        playlist.sort(PlaylistSortType::Year);
        playlist.sort(PlaylistSortType::Duration);
    }

    TEST_F(PlaylistTests, catch_reporting_about_playlist_destroying) {
        Mocked::PlaylistChangesListenerMock listener;
        EXPECT_CALL(listener, reportPlaylistDestroying(Ne(nullptr)))
            .WillOnce(Invoke([&listener](Playlist *playlist)
                             {
                                 playlist->unregistrateListener(&listener);
                             }));

        {
            Playlist playlist;
            playlist.registrateListener(&listener);
        }
    }

    TEST_F(PlaylistTests, adding_one_cue_file_causes_importing_more_files) {
        Playlist playlist;
        auto listener = createMockedPlaylistListener(playlist);
        EXPECT_CALL(*listener, reportAddedMedia(&playlist, 0, 3));

        auto cueFile = TestUtils::getTestMediaFile(TestCueFile);
        playlist.addTrack(cueFile);
        ASSERT_EQ(3u, playlist.size());

        ASSERT_EQ("testable.flac", playlist.at(0).get()->getFileName());
        ASSERT_EQ("1", playlist.at(0).get()->getTrackNumber());
        ASSERT_EQ("testable.flac", playlist.at(1).get()->getFileName());
        ASSERT_EQ("2", playlist.at(1).get()->getTrackNumber());
        ASSERT_EQ("testable.flac", playlist.at(2).get()->getFileName());
        ASSERT_EQ("3", playlist.at(2).get()->getTrackNumber());
    }

    TEST_F(PlaylistTests, interting_one_cue_file_causes_importing_more_files) {
        MediaList list = { TestUtils::getTestMediaFile(TestMP3File),
                           TestUtils::getTestMediaFile(TestCueFile),
                           TestUtils::getTestMediaFile(TestCueFile),
                           TestUtils::getTestMediaFile(TestWavFile),
                           TestUtils::getTestMediaFile(TestCueFile),
                           TestUtils::getTestMediaFile(TestFlacFile) };

        Playlist playlist;
        auto listener = createMockedPlaylistListener(playlist);
        EXPECT_CALL(*listener, reportAddedMedia(&playlist, 0, 12));

        playlist.insertTracks(0u, list);
        ASSERT_EQ(12u, playlist.size());

        std::vector<std::array<std::size_t, 3>> cueTrackIndexes = { {{1,2,3}}, {{4,5,6}}, {{8,9,10}} };
        for (const auto &indexes : cueTrackIndexes)
        {
            ASSERT_EQ("testable.flac", playlist.at(indexes[0]).get()->getFileName());
            ASSERT_EQ("1", playlist.at(indexes[0]).get()->getTrackNumber());
            ASSERT_EQ("testable.flac", playlist.at(indexes[1]).get()->getFileName());
            ASSERT_EQ("2", playlist.at(indexes[1]).get()->getTrackNumber());
            ASSERT_EQ("testable.flac", playlist.at(indexes[2]).get()->getFileName());
            ASSERT_EQ("3", playlist.at(indexes[2]).get()->getTrackNumber());
        }
    }

}
