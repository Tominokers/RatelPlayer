#pragma once

#include "rpCore/Media/MediaProxy.h"
#include "rpCore/Player/Playlist.h"

#include <gtest/gtest.h>

#include <functional>
#include <vector>

namespace PlayerTests {

    using MediaList = rpCore::Media::MediaList;
    using Playlist = rpCore::Player::Playlist;

    class PlaylistTests : public testing::Test
    {
    public:
        PlaylistTests();

    public:
        MediaList m_initList;
        std::vector<std::string> m_initUriList;
        std::function<bool(const Playlist&, const std::vector<std::string>)> m_compare;
    };

}
