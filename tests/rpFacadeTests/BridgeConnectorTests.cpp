#include "BridgeConnectorTests.h"

#include "DataConstants.h"
#include "Mocked/ActionListenerMock.h"
#include "Mocked/PlayerFillerClientMock.h"
#include "TestUtils.h"

#include "rpFacade/Bridge/BridgeConnector.h"
#include "rpFacade/FacadeManager.h"
#include "rpFacade/Observers/ObserverMediator.h"
#include "rpFacade/Player/PlayerMediator.h"
#include "rpFacade/Track.h"
#include "rpObservers/States/DirectoryState.h"

#include <chrono>
#include <mutex>
#include <numeric>

using namespace Mocked;
using namespace rpFacade;
using namespace std::chrono;

using ::testing::Invoke;
using ::testing::NiceMock;
using ::testing::_;

namespace rpFacadeTests {

    const rpObservers::States::DirectoryState state(MediaDirectory);

    BridgeConnectorTests::BridgeConnectorTests()
        : m_storage("SettingStorage")
        , m_player(rpFacade::FacadeManager::createPlayer(m_storage, nullptr))
        , m_observer(rpFacade::FacadeManager::createObserver(state, nullptr))
    {
    }

    TEST_F(BridgeConnectorTests, append_media_files_to_current_playlist) {
        auto mp3 = TestUtils::getTestMediaFile(TestMP3File);
        NiceMock<Mocked::ActionListenerMock> actionListener;

        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, appendNewItem(_)).Times(4);
        EXPECT_CALL(client, clearItems());

        Bridge::BridgeConnector bridge(*m_observer, *m_player);

        m_player->getPlaylistAssistant().createPlaylist(client, actionListener);

        bridge.appendToCurrentPlaylist(std::move(mp3));

        rpFacade::MediaList list = { mp3, mp3, mp3 };
        bridge.appendToCurrentPlaylist(list);
    }

    TEST_F(BridgeConnectorTests, append_delayed_media_get_and_clear_them) {
        Bridge::BridgeConnector bridge(*m_observer, *m_player);

        auto file = TestUtils::getTestMediaFile(TestMP3File);

        bridge.appendDelayedMedia(file);
        ASSERT_EQ(1u, bridge.getDelayedMedia().size());
        bridge.clearDelayedMedia();
        ASSERT_EQ(0u, bridge.getDelayedMedia().size());

        MediaList container = { TestUtils::getTestMediaFile(TestMP3File),
                                TestUtils::getTestMediaFile(TestWavFile),
                                TestUtils::getTestMediaFile(TestFlacFile) };
        auto size = container.size();

        bridge.appendDelayedMedia(std::move(container));
        ASSERT_EQ(size, bridge.getDelayedMedia().size());
        bridge.clearDelayedMedia();
        ASSERT_EQ(0u, bridge.getDelayedMedia().size());
    }

    TEST_F(BridgeConnectorTests, delay_and_use_media_items_via_bridgeconnector_internally) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, appendNewItem(_))
            .Times(FilesInMediaDirectory);
        EXPECT_CALL(client, clearItems());

        std::timed_mutex playlistLock;
        Mocked::ActionListenerMock actionListener;
        EXPECT_CALL(actionListener, handleStartNotification())
            .WillRepeatedly(Invoke([&playlistLock]()
                                   {
                                       playlistLock.lock();
                                   }));
        EXPECT_CALL(actionListener, handleEndNotification())
            .WillRepeatedly(Invoke([&playlistLock]()
                                   {
                                       playlistLock.unlock();
                                   }));

        Bridge::BridgeConnector bridge(*m_observer, *m_player);

        // Generate items to delay with indexes:3...3+FilesInTestDirectory and above.
        // 0-2 are skipped because it is subfolders.
        std::vector<ContainerIndex> delayedItems = {};
        for (ContainerIndex i = 3; i < FilesInTestDirectory + 4; ++i)
            delayedItems.push_back(i);

        auto token = m_observer->Update();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();
        TestUtils::waitForPendingTasks();

        m_observer->delayItems(delayedItems);
        ASSERT_EQ(FilesInMediaDirectory, bridge.getDelayedMedia().size());

        static_cast<rpFacade::Player::PlayerMediator&>(*m_player).setClient(&client);
        m_player->getPlaylistAssistant().createPlaylist(client, actionListener);
        m_player->getPlaylistAssistant().insertDelayedMedia(0u, 0u);

        std::this_thread::sleep_for(1s); // Give some time for another thread.
        TestUtils::waitForPendingTasks(); // It should retrieve end event from loop.
        auto lockResult = playlistLock.try_lock_for(1s); // Lock should be available here.
        ASSERT_TRUE(lockResult); // If it is not, we fail!
        playlistLock.unlock();

        ASSERT_EQ(0u, bridge.getDelayedMedia().size());
    }

    TEST_F(BridgeConnectorTests, delay_media_items_from_subobservers) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, appendNewItem(_))
            .Times(FilesInTestDirectory+2);
        EXPECT_CALL(client, clearItems());

        std::timed_mutex playlistLock;
        ActionListenerMock actionListener;
        EXPECT_CALL(actionListener, handleStartNotification())
            .WillRepeatedly(Invoke([&playlistLock]()
                                   {
                                       playlistLock.lock();
                                   }));
        EXPECT_CALL(actionListener, handleEndNotification())
            .WillRepeatedly(Invoke([&playlistLock]()
                                   {
                                       playlistLock.unlock();
                                   }));


        const rpObservers::States::DirectoryState testState(TestDirectory);
        auto subobserversMediator = rpFacade::FacadeManager::createObserver(testState, nullptr);
        Bridge::BridgeConnector bridge(*subobserversMediator, *m_player);

        std::vector<ContainerIndex> delayedItems(SubFoldersInTestDirectory, 0);
        std::iota(std::begin(delayedItems), std::end(delayedItems), 0);

        auto token = subobserversMediator->Update();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();

        subobserversMediator->delayItems(delayedItems);
        ASSERT_EQ(FilesInTestDirectory-1, bridge.getDelayedMedia().size());

        static_cast<rpFacade::Player::PlayerMediator&>(*m_player).setClient(&client);
        m_player->getPlaylistAssistant().createPlaylist(client, actionListener);
        m_player->getPlaylistAssistant().insertDelayedMedia(0u, 0u);

        std::this_thread::sleep_for(1s); // Give some time for another thread.
        TestUtils::waitForPendingTasks(); // It should retrieve end event from loop.
        auto lockResult = playlistLock.try_lock_for(1s); // Lock should be available here.
        ASSERT_TRUE(lockResult); // If it is not, we fail!
        playlistLock.unlock();

        ASSERT_EQ(0u, bridge.getDelayedMedia().size());
    }

    TEST_F(BridgeConnectorTests, create_same_connection_twice) {
        Bridge::BridgeConnector bridge0(*m_observer, *m_player);
        {
            Bridge::BridgeConnector bridge1(*m_observer, *m_player);

            ASSERT_EQ(2u, m_observer->getBridges(). size());
            ASSERT_EQ(2u, m_player->getBridges().size());
            ASSERT_EQ(&bridge0, m_observer->getBridges()[0]);
            ASSERT_EQ(&bridge0, m_player->getBridges()[0]);
            ASSERT_EQ(&bridge1, m_observer->getBridges()[1]);
            ASSERT_EQ(&bridge1, m_player->getBridges()[1]);
        }

        ASSERT_EQ(1u, m_observer->getBridges(). size());
        ASSERT_EQ(1u, m_player->getBridges().size());
        ASSERT_EQ(&bridge0, m_observer->getBridges()[0]);
        ASSERT_EQ(&bridge0, m_player->getBridges()[0]);
    }

    TEST_F(BridgeConnectorTests, append_delayed_media_from_few_observers) {
        Bridge::BridgeConnector bridge0(*m_observer, *m_player);

        const rpObservers::States::DirectoryState testState(TestDirectory);
        auto subobserversMediator = rpFacade::FacadeManager::createObserver(testState, nullptr);
        Bridge::BridgeConnector bridge1(*subobserversMediator, *m_player);

        ASSERT_EQ(1u, m_observer->getBridges().size());
        ASSERT_EQ(1u, subobserversMediator->getBridges().size());
        ASSERT_EQ(2u, m_player->getBridges().size());

        MediaList container = { TestUtils::getTestMediaFile(TestMP3File),
                                TestUtils::getTestMediaFile(TestWavFile),
                                TestUtils::getTestMediaFile(TestFlacFile) };
        std::vector<ContainerIndex> delayedItems(SubFoldersInTestDirectory, 0);
        std::iota(std::begin(delayedItems), std::end(delayedItems), 0);

        auto size = container.size();
        auto subobserverItemCount = FilesInTestDirectory-1;

        bridge0.appendDelayedMedia(std::move(container));
        ASSERT_EQ(size, bridge0.getDelayedMedia().size());

        auto token = subobserversMediator->Update();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();

        subobserversMediator->delayItems(delayedItems);
        ASSERT_EQ(subobserverItemCount, bridge1.getDelayedMedia().size());

        std::size_t count = 0;
        for (auto &item : m_player->getBridges())
            count += item->getDelayedMedia().size();
        ASSERT_EQ(size + subobserverItemCount, count);

        bridge0.clearDelayedMedia();
        bridge1.clearDelayedMedia();
        ASSERT_EQ(0u, bridge0.getDelayedMedia().size());
        ASSERT_EQ(0u, bridge0.getDelayedMedia().size());

        count = 0;
        for (auto &item : m_player->getBridges())
            count += item->getDelayedMedia().size();
        ASSERT_EQ(0u, count);
    }

    TEST_F(BridgeConnectorTests, insert_delayed_media_from_big_observer) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, appendNewItem(_)).Times(3);
        EXPECT_CALL(client, insertNewItem(_, 0));
        EXPECT_CALL(client, insertNewItem(_, 1)).Times(2);
        EXPECT_CALL(client, insertNewItem(_, 2)).Times(2);
        EXPECT_CALL(client, insertNewItem(_, 3)).Times(2);
        EXPECT_CALL(client, insertNewItem(_, 4));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 2));
        client.setupPlayer();

        const rpObservers::States::DirectoryState bigState(BigMediaDirectory);
        auto bigObserver = rpFacade::FacadeManager::createObserver(bigState, nullptr);
        auto token = bigObserver->Update();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();
        TestUtils::waitForPendingTasks();

        Bridge::BridgeConnector bridge(*bigObserver, client.getPlayer());

        bigObserver->delayItems(std::vector<ContainerIndex> { 9, 10, 11 });
        client.getPlayer().getPlaylistAssistant().insertDelayedMedia(0u, 0u);
        client.waitConcurrentAction();

        bigObserver->delayItems(std::vector<ContainerIndex> { 12, 13, 14 });
        client.getPlayer().getPlaylistAssistant().insertDelayedMedia(0u, 2u);
        client.waitConcurrentAction();

        bigObserver->delayItems(std::vector<ContainerIndex> { 15, 16, 17 });
        client.getPlayer().getPlaylistAssistant().insertDelayedMedia(0u, 1u);
        client.waitConcurrentAction();

        bigObserver->delayItems(std::vector<ContainerIndex> { 19, 20, 21 });
        client.getPlayer().getPlaylistAssistant().insertDelayedMedia(0u, 0u);
        client.waitConcurrentAction();
    }

    TEST_F(BridgeConnectorTests, delay_and_process_items_via_observer_async) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, appendNewItem(_)).Times(2);
        EXPECT_CALL(client, clearItems());

        std::timed_mutex playlistLock;
        Mocked::ActionListenerMock actionListener;
        EXPECT_CALL(actionListener, handleStartNotification())
            .Times(2)
            .WillRepeatedly(Invoke([&playlistLock]()
                                   {
                                       playlistLock.lock();
                                   }));
        EXPECT_CALL(actionListener, handleEndNotification())
            .Times(2)
            .WillRepeatedly(Invoke([&playlistLock]()
                                   {
                                       playlistLock.unlock();
                                   }));

        Bridge::BridgeConnector bridge(*m_observer, *m_player);
        static_cast<rpFacade::Player::PlayerMediator&>(*m_player).setClient(&client);
        m_player->getPlaylistAssistant().createPlaylist(client, actionListener);

        auto token = m_observer->Update();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();
        TestUtils::waitForPendingTasks();

        // 0-2 are skipped because it is subfolders.
        std::vector<ContainerIndex> delayedItems = { 3, 4 };
        for (auto &item : delayedItems)
        {
            m_observer->delayItems( { item });
            ASSERT_EQ(1u, bridge.getDelayedMedia().size());
            m_observer->processDelayedItems();

            std::this_thread::sleep_for(600ns); // Give some time for another thread.
            TestUtils::waitForPendingTasks(); // It should retrieve end event from loop.
            auto lockResult = playlistLock.try_lock_for(1s); // Lock should be available here.
            ASSERT_TRUE(lockResult); // If it is not, we fail!
            playlistLock.unlock();
            ASSERT_EQ(0u, bridge.getDelayedMedia().size());
        }
    }

}
