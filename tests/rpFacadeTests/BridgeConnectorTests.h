#pragma once

#include "TestClients/SettingStorageClient.h"

#include "rpFacade/ObserverInterface.h"
#include "rpFacade/PlayerInterface.h"

#include <gtest/gtest.h>

namespace rpFacadeTests {

    class BridgeConnectorTests : public testing::Test
    {
    public:
        BridgeConnectorTests();

    public:
        TestClients::SettingStorageClient m_storage;
        std::unique_ptr<rpFacade::PlayerInterface> m_player;
        std::unique_ptr<rpFacade::ObserverInterface> m_observer;
    };

}
