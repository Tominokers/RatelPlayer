#include "MediaContainerTests.h"

#include "DataConstants.h"
#include "TestClients/SettingStorageClient.h"

#include "rpCore/Media/MediaFile.h"
#include "rpFacade/ObserverItemInformation.h"
#include "rpFacade/Observers/MediaContainer.h"
#include "rpObservers/ObserverCreator.h"

using namespace rpFacade;
using namespace rpFacade::Observers;

namespace rpFacadeTests {

    MediaContainerTests::MediaContainerTests()
        : m_state(TestDirectory)
        , m_observer(rpObservers::ObserverCreator::create(m_state))
    {
        if (m_observer.get() != nullptr)
            m_observer->Update();
    }

    TEST_F(MediaContainerTests, access_to_container_items_via_direct_getters) {
        ASSERT_NE(nullptr, m_observer.get());

        MediaContainer container(*m_observer);
        ASSERT_EQ(1u, container.getMediaFilesCount());
        ASSERT_EQ(1u, container.getMedias().size());
        for (ContainerIndex i = 0; i < container.getMediaFilesCount(); ++i)
        {
            const auto &item = container.getMediaFileAt(0);
            ASSERT_NE(nullptr, item.get());
            ASSERT_STREQ("Sinus artist", item.get()->getArtist().c_str());
            ASSERT_STREQ("Sinus track", item.get()->getSongTitle().c_str());
            ASSERT_STREQ("2014", item.get()->getYear().c_str());
        }
        for (const auto &media : container.getMedias())
        {
            ASSERT_STREQ("Sinus artist", media.get()->getArtist().c_str());
            ASSERT_STREQ("Sinus track", media.get()->getSongTitle().c_str());
            ASSERT_STREQ("2014", media.get()->getYear().c_str());
        }

        ASSERT_EQ(SubFoldersInTestDirectory, container.getSubobserversCount());
        ASSERT_EQ(SubFoldersInTestDirectory, container.getSubobservers().size());
        for (ContainerIndex i = 0; i < container.getSubobserversCount(); ++i)
        {
            const auto &subobserver = container.getSubobserverAt(i);
            ASSERT_NE(subobserver->getName().find("MediaFolder"), std::string::npos);
            ASSERT_EQ(6u, subobserver->getDate().find("201"));
        }

        for (const auto &subobserver : container.getSubobservers())
        {
            ASSERT_NE(subobserver->getName().find("MediaFolder"), std::string::npos);
            ASSERT_EQ(6u, subobserver->getDate().find("201"));
        }
    }

    TEST_F(MediaContainerTests, access_to_container_items_via_special_index_handlers) {
        ASSERT_NE(nullptr, m_observer.get());

        auto mediaHandler = [](const auto &media)
        {
            ASSERT_STREQ("Sinus artist", media.get()->getArtist().c_str());
            ASSERT_STREQ("Sinus track", media.get()->getSongTitle().c_str());
            ASSERT_STREQ("2014", media.get()->getYear().c_str());
        };

        auto observerHandler = [](const auto &subobserver)
        {
            ASSERT_NE(subobserver->getName().find("MediaFolder"), std::string::npos);
            ASSERT_EQ(6u, subobserver->getDate().find("201"));
        };

        auto shouldNotBeCalled = [](const auto&)
        {
            ASSERT_TRUE(false && "this handler shouldn't be called!");
        };

        MediaContainer container(*m_observer);

        for (std::size_t i = 0; i < SubFoldersInTestDirectory; ++i)
            container.forItem(i, observerHandler, shouldNotBeCalled);

        container.forItem(SubFoldersInTestDirectory + 1, shouldNotBeCalled, mediaHandler);
        container.forItem(SubFoldersInTestDirectory + 2, shouldNotBeCalled, shouldNotBeCalled);
    }

    TEST_F(MediaContainerTests, get_observer_item_informations) {
        ASSERT_NE(nullptr, m_observer.get());
        MediaContainer container(*m_observer);

        for (std::size_t i = 0; i < SubFoldersInTestDirectory; ++i)
        {
            auto info = container.getItemInfo(i);
            ASSERT_EQ(ItemType::Observer, info.getType());
            ASSERT_NE(nullptr, info.getObserverInfo());
        }

        auto info4 = container.getItemInfo(4);
        ASSERT_EQ(ItemType::Media, info4.getType());
        ASSERT_NE(nullptr, info4.getMediaInfo());

        auto info5 = container.getItemInfo(5);
        ASSERT_EQ(ItemType::Invalid, info5.getType());
        ASSERT_EQ(nullptr, info5.getObserverInfo());
        ASSERT_EQ(nullptr, info5.getMediaInfo());

    }

}
