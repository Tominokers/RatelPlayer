#pragma once

#include "rpObservers/Observer.h"
#include "rpObservers/States/DirectoryState.h"

#include <gtest/gtest.h>

#include <memory>

namespace rpFacadeTests {

    class MediaContainerTests : public testing::Test
    {
    public:
        MediaContainerTests();

    public:
        rpObservers::States::DirectoryState m_state;

        std::unique_ptr<rpObservers::Observer> m_observer;
    };

}
