#include "ObserverDeserializerTests.h"

#include "DataConstants.h"
#include "TestUtils.h"

#include "rpFacade/Observers/ObserverMediaDeserializer.h"

#include <sstream>

namespace rpFacadeTests {

    TEST_F(ObserverDeserializerTests, get_deserialized_uri_files_and_directories) {
        std::stringstream stream;
        stream << TestUtils::getTestFileUri(TestMP3File) << "\r\n"
               << TestUtils::getTestFileUri(TestDirectory) << "\r\n"
               << TestUtils::getTestFileUri(Test2SecMP3File) << "\r\n";

        auto list = rpFacade::Observers::ObserverMediaDeserializer::deserializeObserverData(stream.str());

        ASSERT_EQ(FilesInTestDirectory + 2u, list.size());
    }

}
