#include "ObserverInspectorTests.h"

#include "DataConstants.h"
#include "Mocked/ObserverVisitorMock.h"

#include "rpFacade/Observers/ObserverInspector.h"
#include "rpFacade/Observers/Visitors/RecursiveMediaGetter.h"
#include "rpObservers/ObserverCreator.h"
#include "rpObservers/States/DirectoryState.h"

using namespace Mocked;
using namespace rpFacade::Observers;
using namespace rpObservers::States;
using namespace rpObservers;

using ::testing::_;

namespace rpFacadeTests {

    TEST_F(ObserverInspectorTests, inspect_observerver_flat_visiting) {
        ObserverVisitorMock visitor;
        EXPECT_CALL(visitor, visitMedia(_)).Times(1);
        EXPECT_CALL(visitor, visitObserver(_)).Times(SubFoldersInTestDirectory);

        ObserverInspector inspector(visitor);

        DirectoryState state(TestDirectory);
        auto observer = ObserverCreator::create(state);

        auto container = inspector.inspect(*observer, ObserverInspector::TypeOfVisiting::Flat);

        ASSERT_NE(nullptr, container.get());
        ASSERT_EQ(1u, container->getMediaFilesCount());
        ASSERT_EQ(SubFoldersInTestDirectory, container->getSubobserversCount());
    }

    TEST_F(ObserverInspectorTests, inspect_observerver_recursive_visiting) {
        ObserverVisitorMock visitor;
        EXPECT_CALL(visitor, visitMedia(_)).Times(FilesInTestDirectory);
        EXPECT_CALL(visitor, visitObserver(_)).Times(0);

        ObserverInspector inspector(visitor);

        DirectoryState state(TestDirectory);
        auto observer = ObserverCreator::create(state);

        auto container = inspector.inspect(*observer, ObserverInspector::TypeOfVisiting::RecursiveMedia);

        ASSERT_NE(nullptr, container.get());
        ASSERT_EQ(1u, container->getMediaFilesCount());
        ASSERT_EQ(SubFoldersInTestDirectory, container->getSubobserversCount());
   }

    TEST_F(ObserverInspectorTests, get_all_observer_medias_via_wrapper_visitor) {
        DirectoryState state(TestDirectory);
        auto observer = ObserverCreator::create(state);

        { // ctor with state.
            Visitors::RecursiveMediaGetter mediaGetter(state);
            const auto &list = mediaGetter.get();
            ASSERT_EQ(FilesInTestDirectory, list.size());
        }

        { // ctor with observer.
            Visitors::RecursiveMediaGetter mediaGetter(*observer);
            const auto &list = mediaGetter.get();
            ASSERT_EQ(FilesInTestDirectory, list.size());
        }
    }

}
