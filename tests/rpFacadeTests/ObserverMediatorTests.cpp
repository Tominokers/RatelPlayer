#include "ObserverMediatorTests.h"

#include "DataConstants.h"
#include "Mocked/ObserverClientMock.h"
#include "Mocked/PlayerFillerClientMock.h"
#include "TestUtils.h"

#include "rpCore/Media/MediaProxy.h"
#include "rpFacade/Bridge/BridgeConnector.h"
#include "rpFacade/FacadeManager.h"
#include "rpFacade/ObserverItemInformation.h"
#include "rpFacade/Track.h"
#include "rpObservers/Observer.h"
#include "rpObservers/States/DirectoryState.h"

#include <glibmm.h>

using namespace Mocked;
using namespace rpFacade;
using namespace rpObservers::States;

using ::testing::EndsWith;
using ::testing::NiceMock;
using ::testing::Sequence;
using ::testing::_;

const DirectoryState state(MediaDirectory);
const DirectoryState bigState(BigMediaDirectory);

namespace rpFacadeTests {

    ObserverMediatorTests::ObserverMediatorTests()
        : m_mediator(rpFacade::FacadeManager::createObserver(state, nullptr))
        , m_bigMediator(rpFacade::FacadeManager::createObserver(bigState, nullptr))
    {
        auto token = m_mediator->Update();
        if (token.valid())
        {
            TestUtils::waitForPendingTasks();
            token.get();
        }
        TestUtils::waitForPendingTasks();

        token = m_bigMediator->Update();
        if (token.valid())
        {
            TestUtils::waitForPendingTasks();
            token.get();
        }
        TestUtils::waitForPendingTasks();
    }

    TEST_F(ObserverMediatorTests, run_mediator_with_client_and_check_it) {
        Sequence ui, filling;
        ObserverClientMocked client(*m_bigMediator);
        EXPECT_CALL(client, obtainingMediaStarted())
            .InSequence(ui);
        EXPECT_CALL(client, observerNameChanged(_))
            .InSequence(filling);
        EXPECT_CALL(client, passObtainedObservers(_))
            .Times(9)
            .InSequence(filling);
        EXPECT_CALL(client, passObtainedMedia(_))
            .Times(12)
            .InSequence(filling);
        EXPECT_CALL(client, obtainingMediaFinished())
            .InSequence(ui);
        ASSERT_NO_THROW(m_bigMediator->setClient(&client));

        auto token = m_bigMediator->Update();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();
        // Wait for obtainingMediaFinished callback.
        TestUtils::waitForPendingTasks();
    }

    TEST_F(ObserverMediatorTests, run_mediator_and_get_its_parent) {
        Sequence nameChanges;
        ObserverClientMocked client(*m_mediator);
        EXPECT_CALL(client, obtainingMediaStarted())
            .Times(2);
        EXPECT_CALL(client, passObtainedObservers(_))
            .Times(7);
        EXPECT_CALL(client, passObtainedMedia(_))
            .Times(8);
        EXPECT_CALL(client, obtainingMediaFinished())
            .Times(2);

        EXPECT_CALL(client, observerNameChanged(EndsWith(MediaDirectory.substr(1))))
            .InSequence(nameChanges);
        EXPECT_CALL(client, observerNameChanged(EndsWith(TestDirectory.substr(1))))
            .InSequence(nameChanges);

        ASSERT_NO_THROW(m_mediator->setClient(&client));

        auto token = m_mediator->Update();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();

        token = m_mediator->switchToParentObserver();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();
        // Wait for obtainingMediaFinished callback.
        TestUtils::waitForPendingTasks();
    }

    TEST_F(ObserverMediatorTests, switch_mediator_to_different_states) {
        Sequence nameChanges;
        auto observer = rpFacade::FacadeManager::createObserver(state, nullptr);

        ObserverClientMocked client(*m_mediator);
        EXPECT_CALL(client, obtainingMediaStarted())
            .Times(3);
        EXPECT_CALL(client, passObtainedObservers(_))
            .Times(16);
        EXPECT_CALL(client, passObtainedMedia(_))
            .Times(20);
        EXPECT_CALL(client, obtainingMediaFinished())
            .Times(3);

        EXPECT_CALL(client, observerNameChanged(EndsWith(MediaDirectory.substr(1))))
            .InSequence(nameChanges);
        EXPECT_CALL(client, observerNameChanged(EndsWith(BigMediaDirectory.substr(1))))
            .InSequence(nameChanges);
        EXPECT_CALL(client, observerNameChanged(EndsWith(TestDirectory.substr(1))))
            .InSequence(nameChanges);

        ASSERT_NO_THROW(m_mediator->setClient(&client));

        auto token0 = m_mediator->Update();
        ASSERT_TRUE(token0.valid());
        TestUtils::waitForPendingTasks();
        token0.get();

        auto token1 = m_mediator->switchToState(bigState);
        ASSERT_TRUE(token1.valid());
        TestUtils::waitForPendingTasks();
        token1.get();

        auto token2 = m_mediator->switchToState(DirectoryState(TestDirectory));
        ASSERT_TRUE(token2.valid());
        TestUtils::waitForPendingTasks();
        token2.get();
        TestUtils::waitForPendingTasks();
    }

    TEST_F(ObserverMediatorTests, switch_media_to_incorrect_states) {
        auto observer = rpFacade::FacadeManager::createObserver(state, nullptr);

        ObserverClientMocked client(*m_mediator);
        EXPECT_CALL(client, obtainingMediaStarted());
        EXPECT_CALL(client, observerNameChanged(EndsWith(MediaDirectory.substr(1))));
        EXPECT_CALL(client, passObtainedObservers(_)).Times(3);
        EXPECT_CALL(client, passObtainedMedia(_)).Times(7);
        EXPECT_CALL(client, obtainingMediaFinished());

        ASSERT_NO_THROW(m_mediator->setClient(&client));

        auto token0 = m_mediator->switchToState(DirectoryState(TestDirectory + "blablabla"));
        ASSERT_FALSE(token0.valid());
        TestUtils::waitForPendingTasks();
        ASSERT_THROW(token0.get(), std::future_error);

        auto token1 = m_mediator->switchToState(DirectoryState(TestDirectory + "/try_another_wrong_dir"));
        ASSERT_FALSE(token1.valid());
        TestUtils::waitForPendingTasks();
        ASSERT_THROW(token0.get(), std::future_error);

        auto token2 = m_mediator->Update();
        ASSERT_TRUE(token2.valid());
        TestUtils::waitForPendingTasks();
        ASSERT_NO_THROW(token2.get());
        TestUtils::waitForPendingTasks();
    }

    TEST_F(ObserverMediatorTests, add_media_files_from_observer_one_by_one__not_on_travis_ci) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, appendNewItem(_)).Times(4);
        EXPECT_CALL(client, passChangedPlayState(rpFacade::PlayerState::Playing))
            .Times(4);
        EXPECT_CALL(client, passChangedPlayState(rpFacade::PlayerState::Stop));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0)).Times(2);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 1));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 2));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 3));

        client.setupPlayer();

        Bridge::BridgeConnector bridge(*m_mediator, client.getPlayer());

        m_mediator->addItem(3);
        m_mediator->addItem(4);
        m_mediator->addItem(5);
        m_mediator->addItem(6);

        client.getPlayer().play();
        client.getPlayer().next();
        client.getPlayer().next();
        client.getPlayer().next();
        client.getPlayer().stop();

        TestUtils::waitForPendingTasks();
    }

    TEST_F(ObserverMediatorTests, add_all_medias_from_subobserver__not_on_travis_ci) {
        const DirectoryState state(TestDirectory);
        auto subobserversMediator = rpFacade::FacadeManager::createObserver(state, nullptr);
        ObserverClientMocked subClient(*subobserversMediator);
        EXPECT_CALL(subClient, obtainingMediaStarted());
        EXPECT_CALL(subClient, observerNameChanged(EndsWith(TestDirectory.substr(1))));
        EXPECT_CALL(subClient, passObtainedObservers(_)).Times(SubFoldersInTestDirectory);
        EXPECT_CALL(subClient, passObtainedMedia(_));
        EXPECT_CALL(subClient, obtainingMediaFinished());
        subobserversMediator->setClient(&subClient);

        auto token = subobserversMediator->Update();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();
        TestUtils::waitForPendingTasks();

        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, appendNewItem(_))
            .Times(FilesInMediaDirectory + 2);
        EXPECT_CALL(client, passChangedPlayState(rpFacade::PlayerState::Playing))
            .Times(8);
        EXPECT_CALL(client, passChangedPlayState(rpFacade::PlayerState::Stop))
            .Times(3);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0))
            .Times(4);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 1));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 2));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 3));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 4));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 5));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 6));

        client.setupPlayer();

        Bridge::BridgeConnector bridge(*subobserversMediator, client.getPlayer());

        subobserversMediator->addItem(1);
        client.getPlayer().play();
        client.getPlayer().next();
        client.getPlayer().stop();

        subobserversMediator->addItem(3);
        subobserversMediator->addItem(4);
        client.getPlayer().setTrack(0);
        client.getPlayer().play();
        for(std::size_t trackCounter = 3, trackNumber = 0;
            trackCounter < 9;
            ++trackCounter, ++trackNumber, client.getPlayer().next());

        client.getPlayer().stop();
    }

    TEST_F(ObserverMediatorTests, get_directory_items_info) {
        // Performs requestItemInfo on bigMediator.

        for (ContainerIndex i = 0; i < 9; ++i)
        {
            auto info = m_bigMediator->requestItemInfo(i);
            ASSERT_EQ(rpFacade::ItemType::Observer, info.getType());
            ASSERT_NE(nullptr, info.getObserverInfo());
            ASSERT_EQ(nullptr, info.getMediaInfo());
        }

        for (ContainerIndex i = 9; i < 21; ++i)
        {
            auto info = m_bigMediator->requestItemInfo(i);
            ASSERT_EQ(rpFacade::ItemType::Media, info.getType());
            ASSERT_NE(nullptr, info.getMediaInfo());
            ASSERT_EQ(nullptr, info.getObserverInfo());
        }

        auto info = m_bigMediator->requestItemInfo(21);
        ASSERT_EQ(rpFacade::ItemType::Invalid, info.getType());
        ASSERT_EQ(nullptr, info.getMediaInfo());
        ASSERT_EQ(nullptr, info.getObserverInfo());
    }


}
