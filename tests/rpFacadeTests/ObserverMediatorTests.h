#pragma once

#include "rpFacade/ObserverInterface.h"

#include <gtest/gtest.h>

#include <memory>

namespace rpFacadeTests {

    class ObserverMediatorTests : public testing::Test
    {
    public:
        ObserverMediatorTests();

    public:
        std::unique_ptr<rpFacade::ObserverInterface> m_mediator;

        std::unique_ptr<rpFacade::ObserverInterface> m_bigMediator;
    };

}
