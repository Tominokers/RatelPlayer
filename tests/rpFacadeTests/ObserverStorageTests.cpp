#include "ObserverStorageTests.h"

#include "Mocked/SettingStorageMock.h"

using namespace Mocked;
using namespace rpFacade::Observers;

using ::testing::_;
using ::testing::Return;
using ::testing::Sequence;

namespace rpFacadeTests {

    TEST_F(ObserverStorageTests, store_load_from_observers_storage) {
        std::string data0("serialized content 0");
        std::string data1("serialized content 1");
        std::string one("1");
        std::string two("2");

        SerializableObserverMock observerMock0;
        EXPECT_CALL(observerMock0, serialize()).WillOnce(Return(data0));
        SerializableObserverMock observerMock1;
        EXPECT_CALL(observerMock1, serialize()).WillOnce(Return(data1));
        std::vector<SerializableObserver*> observers({&observerMock0, &observerMock1});

        ObserverLoaderMock loader;
        EXPECT_CALL(loader, load(data0));
        EXPECT_CALL(loader, load(data1));

        Sequence seq;
        SettingStorageMock settingsStorage("observer_settings");
        EXPECT_CALL(settingsStorage, storeImpl(_, two)).InSequence(seq);
        EXPECT_CALL(settingsStorage, storeImpl(_, data0)).InSequence(seq);
        EXPECT_CALL(settingsStorage, storeImpl(_, data1)).InSequence(seq);
        EXPECT_CALL(settingsStorage, storeImpl(_, one)).InSequence(seq);
        EXPECT_CALL(settingsStorage, loadImpl(_)).WillOnce(Return(two))
                                                 .WillOnce(Return(data0))
                                                 .WillOnce(Return(data1))
                                                 .WillOnce(Return(one));

        ObserverStorage storage(settingsStorage);

        storage.store(observers);
        storage.load(loader);
        storage.setLastActiveObserver(1u);
        ASSERT_EQ(1u, storage.getLastActiveObserver());
    }

}
