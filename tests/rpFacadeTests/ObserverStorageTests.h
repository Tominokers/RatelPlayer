#pragma once

#include "rpFacade/Observers/ObserverStorage.h"

#include <gmock/gmock.h>

namespace rpFacadeTests {

    class SerializableObserverMock : public rpFacade::Observers::SerializableObserver
    {
    public:
        MOCK_CONST_METHOD0(serialize, std::string());
    };

    class ObserverLoaderMock : public rpFacade::Observers::ObserverLoader
    {
    public:
        MOCK_METHOD1(load, void(const std::string&));
    };

    class ObserverStorageTests : public testing::Test
    {
    };

}
