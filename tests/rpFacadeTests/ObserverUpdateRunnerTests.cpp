#include "ObserverUpdateRunnerTests.h"

#include "Mocked/ObserverClientMock.h"
#include "DataConstants.h"
#include "TestUtils.h"

#include "rpFacade/FacadeManager.h"
#include "rpFacade/Observers/MediaContainer.h"
#include "rpFacade/Observers/ObserverMediator.h"
#include "rpFacade/Observers/ObserverUpdateRunner.h"
#include "rpObservers/ObserverCreator.h"
#include "rpObservers/States/DirectoryState.h"

#include <memory>

using namespace Mocked;

using ::testing::Sequence;
using ::testing::_;

namespace rpFacadeTests {

    TEST_F(ObserverUpdateRunnerTests, run_observer_update_runner) {
        Sequence ui, filling;

        rpObservers::States::DirectoryState state(BigMediaDirectory);
        auto observer = rpObservers::ObserverCreator::create(state);
        std::unique_ptr<rpFacade::Observers::MediaContainer> container;
        // Just to make client happy.
        auto mediator = rpFacade::FacadeManager::createObserver(state, nullptr);
        ObserverClientMocked client(*mediator);
        EXPECT_CALL(client, obtainingMediaStarted())
            .InSequence(ui);
        EXPECT_CALL(client, observerNameChanged(_))
            .InSequence(filling);
        EXPECT_CALL(client, passObtainedObservers(_))
            .Times(9)
            .InSequence(filling);
        EXPECT_CALL(client, passObtainedMedia(_))
            .Times(12)
            .InSequence(filling);
        EXPECT_CALL(client, obtainingMediaFinished())
            .InSequence(ui);

        rpFacade::Observers::ObserverUpdateRunner runner(&client, observer.get(), container);

        auto token = runner.run();
        ASSERT_TRUE(token.valid());
        TestUtils::waitForPendingTasks();
        token.get();
        TestUtils::waitForPendingTasks();

        ASSERT_NE(nullptr, container.get());
        ASSERT_EQ(9u, container->getSubobserversCount());
        ASSERT_EQ(12u, container->getMediaFilesCount());
    }

}
