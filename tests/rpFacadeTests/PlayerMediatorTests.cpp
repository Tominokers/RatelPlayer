#include "PlayerMediatorTests.h"

#include "DataConstants.h"
#include "Mocked/ActionListenerMock.h"
#include "Mocked/PlayerFillerClientMock.h"
#include "TestUtils.h"

#include "rpFacade/Track.h"
#include "rpFacade/PlaylistAssistant.h"

using namespace Mocked;
using namespace rpFacade::Player;
using namespace rpFacade;

using ::testing::Invoke;
using ::testing::NiceMock;
using ::testing::_;

namespace rpFacadeTests {

    PlayerMediatorTests::PlayerMediatorTests()
        : m_file(TestUtils::getTestMediaFile(TestMP3File))
        , m_file2(TestUtils::getTestMediaFile(Test2SecMP3File))
    {
    }

    TEST_F(PlayerMediatorTests, catch_events_on_changing_player_state__not_on_travis_ci) {
        NiceMock<PlayerFillerClientMock> client;

        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, appendNewItem(_));
        EXPECT_CALL(client, passChangedPlayState(rpFacade::PlayerState::Playing))
            .Times(2);
        EXPECT_CALL(client, passChangedPlayState(rpFacade::PlayerState::Pause));
        EXPECT_CALL(client, passChangedPlayState(rpFacade::PlayerState::Stop))
            .Times(3);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0))
            .Times(3);

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file);

        client.getPlayer().play();
        client.getPlayer().play();
        client.getPlayer().play();
        client.getPlayer().stop();
        client.getPlayer().next();
        client.getPlayer().previous();
     }

    TEST_F(PlayerMediatorTests, catch_events_on_progress_of_playing__not_on_travis_ci) {
        auto context = Glib::MainContext::get_default();

        NiceMock<PlayerFillerClientMock> client;

        EXPECT_CALL(client, passTrackProgress(rpFacade::TrackProgress{ 1.0, "2", "0", "0", "2", "0", "0" }));

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file2);
        client.getPlayer().play();
        sleep(3);

        ASSERT_TRUE(context->pending());
        ASSERT_TRUE(context->iteration(true));
    }

    TEST_F(PlayerMediatorTests, catch_event_on_set_current_track__not_on_travis_ci) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, appendNewItem(_)).Times(3);
        EXPECT_CALL(client, passChangedPlayState(_)).Times(4);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0)).Times(2);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 1));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 2));

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file);
        client.getPlaylists().appendToOpenedPlaylist(m_file);
        client.getPlaylists().appendToOpenedPlaylist(m_file);

        client.getPlayer().play();
        client.getPlayer().next();
        client.getPlayer().next();
        client.getPlayer().stop();
    }

    TEST_F(PlayerMediatorTests, catch_event_on_set_current_position_in_track__not_on_travis_ci) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, appendNewItem(_));
        EXPECT_CALL(client, passChangedPlayState(_)).Times(2);
        EXPECT_CALL(client, passCurrentTrack(_, _, _)).Times(2);
        EXPECT_CALL(client, passTrackProgress(rpFacade::TrackProgress{ 0.0, "0", "0", "0", "2", "0", "0" }))
            .Times(2);
        EXPECT_CALL(client, passTrackProgress(rpFacade::TrackProgress{ 0.5, "1", "0", "0", "2", "0", "0" }));
        EXPECT_CALL(client, passTrackProgress(rpFacade::TrackProgress{ 1.0, "2", "0", "0", "2", "0", "0" }))
            .Times(2);

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file2);

        client.getPlayer().play();
        client.getPlayer().play();

        client.getPlayer().setPosition(0.0);
        client.getPlayer().setPosition(0.5);
        client.getPlayer().setPosition(1.0);
        client.getPlayer().setPosition(-1.0);
        client.getPlayer().setPosition(2.0);
    }

    TEST_F(PlayerMediatorTests, catch_events_on_set_current_track__not_on_travis_ci) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, appendNewItem(_)).Times(4);
        EXPECT_CALL(client, passChangedPlayState(_)).Times(2);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0)).Times(2);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 2));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 1));

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file);
        client.getPlaylists().appendToOpenedPlaylist(m_file);
        client.getPlaylists().appendToOpenedPlaylist(m_file2);
        client.getPlaylists().appendToOpenedPlaylist(m_file2);

        client.getPlayer().play();
        client.getPlayer().stop();
        client.getPlayer().setTrack(2);
        ASSERT_NO_THROW(client.getPlayer().setTrack(4));
        client.getPlayer().setTrack(1);
    }

    TEST_F(PlayerMediatorTests, set_properties_of_player_and_check_whether_they_are_taken_from_settings) {
        NiceMock<Mocked::ActionListenerMock> actionListener;

        TestUtils::RemoveFileScope removeFile0(PlaylistDirectory + "Playlist2.m3u");
        TestUtils::RemoveFileScope removeFile1(PlaylistDirectory + "Playlist3.m3u");

        NiceMock<PlayerFillerClientMock> client(PlaylistDirectory, "2");
        EXPECT_CALL(client, requestPlaylistCreation())
            .WillRepeatedly(Invoke([&client, &actionListener]()
                                   {
                                       return client
                                           .getPlaylists()
                                           .createPlaylist(client, actionListener);
                                   }));

        client.setupPlayer();
        auto &player = client.getPlayer();

        ASSERT_EQ(RepeatMode::None, player.getRepeatMode());
        player.setRepeatMode(RepeatMode::WholePlaylist);
        ASSERT_EQ(RepeatMode::WholePlaylist, player.getRepeatMode());

        ASSERT_EQ(ShuffleMode::None, player.getShuffleMode());
        player.setShuffleMode(ShuffleMode::ByTrack);
        ASSERT_EQ(ShuffleMode::ByTrack, player.getShuffleMode());

        ASSERT_EQ(0u, client.getPlaylists().getUsedPlaylistIndex());
        client.getPlaylists().setUsedPlaylist(1u);

        void *initialPlayerAddress = std::addressof(player);

        client.reloadPlayer();

        auto &player2 = client.getPlayer();
        void *newPlayerAddress = std::addressof(player2);

        // Check whether we got another instance of player
        // such player should be loaded with stored settings.
        // initialPlayerAddress has dangling address.
        ASSERT_NE(initialPlayerAddress, newPlayerAddress);
        ASSERT_EQ(RepeatMode::WholePlaylist, player2.getRepeatMode());
        ASSERT_EQ(ShuffleMode::ByTrack, player2.getShuffleMode());

        client.getPlaylists().loadSerializedPlaylists();
        ASSERT_EQ(1u, client.getPlaylists().getUsedPlaylistIndex());
    }

}
