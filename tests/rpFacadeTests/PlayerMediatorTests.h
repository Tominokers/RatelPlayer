#pragma once

#include "rpCore/Media/MediaProxy.h"

#include <gtest/gtest.h>

#include <memory>

namespace rpFacadeTests {

    class PlayerMediatorTests : public testing::Test
    {
    public:
        PlayerMediatorTests();

    public:
        rpCore::Media::MediaProxy m_file;
        rpCore::Media::MediaProxy m_file2;
    };

}
