#include "PlaylistAssistantTests.h"

#include "DataConstants.h"
#include "Mocked/ActionListenerMock.h"
#include "Mocked/PlayerFillerClientMock.h"
#include "TestUtils.h"

#include "rpCore/Media/M3USerializer.h"
#include "rpFacade/PlaylistAssistant.h"
#include "rpFacade/Track.h"

#include <fstream>

using namespace Mocked;
using namespace rpFacade;
using namespace rpFacade::Player;

using ::testing::_;
using ::testing::Invoke;
using ::testing::NiceMock;
using ::testing::Return;

namespace rpFacadeTests {

    PlaylistAssistantTests::PlaylistAssistantTests()
        : m_file(TestUtils::getTestMediaFile(TestMP3File))
        , m_file2(TestUtils::getTestMediaFile(Test2SecMP3File))
    {
        auto directory = BigMediaDirectory.substr(1) + "/";
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 7.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 11.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 10.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 5.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 9.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 1.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 3.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 6.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 12.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 4.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 8.mp3"));
        m_unsortedMediaList.emplace_back(TestUtils::getTestMediaFile(directory + "test 2.mp3"));
    }

    TEST_F(PlaylistAssistantTests, catch_events_on_adding_track_twice) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, appendNewItem(_)).Times(2);

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file);

        auto files = std::vector<std::string> { TestUtils::getTestFileUri(TestMP3File) };
        client.getPlaylists().appendToOpenedPlaylist(files);
    }

    TEST_F(PlaylistAssistantTests, catch_event_on_setting_different_playlists__not_on_travis_ci) {
        NiceMock<ActionListenerMock> actionListener;
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems()).Times(2);
        EXPECT_CALL(client, appendNewItem(_)).Times(2);
        EXPECT_CALL(client, passChangedPlayState(_)).Times(2);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0)).Times(2);

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file);

        auto opened = client.getPlaylists().createPlaylist(client, actionListener);
        ASSERT_EQ(1u, opened);
        client.getPlaylists().setOpenedPlaylist(opened);
        ASSERT_EQ(opened, client.getPlaylists().getOpenedPlaylistIndex());
        client.getPlaylists().appendToOpenedPlaylist(m_file2);

        client.getPlayer().play();
        client.getPlayer().stop();
    }

    TEST_F(PlaylistAssistantTests, close_new_created_playlists) {
        NiceMock<ActionListenerMock> actionListener;
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems()).Times(9);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0));
        EXPECT_CALL(client, appendNewItem(_));
        EXPECT_CALL(client, requestPlaylistClose(3));
        EXPECT_CALL(client, requestPlaylistClose(2));
        EXPECT_CALL(client, requestPlaylistClose(1)).Times(2);
        EXPECT_CALL(client, requestPlaylistClose(0)).Times(2);

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file);
        auto index1 = client.getPlaylists().createPlaylist(client, actionListener);
        auto index2 = client.getPlaylists().createPlaylist(client, actionListener);
        auto index3 = client.getPlaylists().createPlaylist(client, actionListener);

        client.getPlaylists().closePlaylist(index3);
        client.getPlaylists().closePlaylist(index2);
        client.getPlaylists().closePlaylist(index1);
        client.getPlaylists().closePlaylist(0);

        index1 = client.getPlaylists().createPlaylist(client, actionListener);
        index2 = client.getPlaylists().createPlaylist(client, actionListener);
        index3 = client.getPlaylists().createPlaylist(client, actionListener);

        client.getPlaylists().closePlaylist(0);
        client.getPlaylists().closePlaylist(index1);
        client.getPlaylists().closePlaylist(index2);
        client.getPlaylists().closePlaylist(index3);

        client.getPlaylists().createPlaylist(client, actionListener);
        client.getPlaylists().closePlaylist(0);
    }

    TEST_F(PlaylistAssistantTests, close_invalid_or_incorrect_playlists) {
        NiceMock<ActionListenerMock> actionListener;
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems()).Times(3);
        EXPECT_CALL(client, appendNewItem(_));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0));

        client.setupPlayer();
        client.getPlaylists().appendToOpenedPlaylist(m_file);

        client.getPlaylists().closePlaylist(0);

        auto index = client.getPlaylists().createPlaylist(client, actionListener);
        client.getPlaylists().closePlaylist(index + 1);
        client.getPlaylists().closePlaylist(index + 100);
        client.getPlaylists().closePlaylist(index - 100);
    }

    TEST_F(PlaylistAssistantTests, close_playlist_and_next_track_is_played_from_previous_playlist__not_on_travis_ci) {
        NiceMock<ActionListenerMock> actionListener;
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems()).Times(2);
        EXPECT_CALL(client, appendNewItem(_)).Times(3);
        EXPECT_CALL(client, passChangedPlayState(_)).Times(3);
        EXPECT_CALL(client, passCurrentTrack(_, 1, 0)).Times(2);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 1));
        EXPECT_CALL(client, requestPlaylistClose(1));

        client.setupPlayer();
        // Set up the first playlist with two tracks.
        client.getPlaylists().appendToOpenedPlaylist(m_file);
        client.getPlaylists().appendToOpenedPlaylist(m_file);
        // Set up the second playlist with one track.
        auto index1 = client.getPlaylists().createPlaylist(client, actionListener);
        client.getPlaylists().setOpenedPlaylist(index1);
        ASSERT_EQ(index1, client.getPlaylists().getOpenedPlaylistIndex());
        client.getPlaylists().setUsedPlaylist(index1);
        client.getPlaylists().appendToOpenedPlaylist(m_file);
        // Play track from the second playlist and close it.
        client.getPlayer().play();
        client.getPlaylists().closePlaylist(index1);

        // Player should switch to the second track in the first playlist.
        client.getPlayer().next();
        client.getPlayer().stop();
    }

    TEST_F(PlaylistAssistantTests, request_sorting_of_playlist_by_track_number) {
        NiceMock<ActionListenerMock> actionListener;
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems()).Times(2);
        EXPECT_CALL(client, appendNewItem(_)).Times(24);
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 6));

        client.setupPlayer();
        auto &playlists = client.getPlaylists();
        for (auto &item : m_unsortedMediaList)
            playlists.appendToOpenedPlaylist(item);

        playlists.sortPlaylist(0, rpFacade::PlaylistSortType::TrackNumber);
    }

    TEST_F(PlaylistAssistantTests, check_loading_serialized_playlists) {
        NiceMock<ActionListenerMock> actionListener;
        NiceMock<PlayerFillerClientMock> client(PlaylistDirectory, "2");
        EXPECT_CALL(client, clearItems()).Times(4);
        EXPECT_CALL(client, requestPlaylistCreation())
            .Times(2)
            .WillRepeatedly(Invoke([&client, &actionListener]()
                                   {
                                       return client.getPlaylists()
                                           .createPlaylist(client, actionListener);
                                   }));
        EXPECT_CALL(client, appendNewItem(_)).Times(4);

        client.setupPlayer();
    }

    TEST_F(PlaylistAssistantTests, check_serialization_of_playlist_after_creation) {
        NiceMock<ActionListenerMock> actionListener;

        auto playlistDirectory = TestDirectory.substr(2) + "/"; // TestData directory
        auto fileName0 = playlistDirectory + "Playlist0.m3u";
        auto fileName1 = playlistDirectory + "Playlist1.m3u";
        auto fileName2 = playlistDirectory + "Playlist2.m3u";
        TestUtils::RemoveFileScope removeFile0(fileName0);
        TestUtils::RemoveFileScope removeFile1(fileName1);
        TestUtils::RemoveFileScope removeFile2(fileName2);

        {
            NiceMock<PlayerFillerClientMock> client(playlistDirectory, "0");
            EXPECT_CALL(client, appendNewItem(_)).Times(4);

            client.setupPlayer();
            client.getPlaylists().appendToOpenedPlaylist(m_file);
            auto index1 = client.getPlaylists().createPlaylist(client, actionListener);
            ASSERT_EQ(1u, index1);
            client.getPlaylists().setOpenedPlaylist(index1);
            ASSERT_EQ(index1, client.getPlaylists().getOpenedPlaylistIndex());
            client.getPlaylists().appendToOpenedPlaylist(m_file2);

            auto index2 = client.getPlaylists().createPlaylist(client, actionListener);
            ASSERT_EQ(2u, index2);
            client.getPlaylists().setOpenedPlaylist(index2);
            ASSERT_EQ(index2, client.getPlaylists().getOpenedPlaylistIndex());
            client.getPlaylists().appendToOpenedPlaylist(m_file);
            client.getPlaylists().appendToOpenedPlaylist(m_file2);
        }

        std::fstream stream0(fileName0);
        ASSERT_TRUE(stream0.is_open());

        auto list0 = rpCore::Media::M3USerializer::deserialize(stream0);
        ASSERT_EQ(1u, list0.size());
        ASSERT_NE(nullptr, list0[0].getUri());
        ASSERT_EQ(*m_file.getUri(), *list0[0].getUri());

        std::fstream stream1(fileName1);
        ASSERT_TRUE(stream1.is_open());

        auto list1 = rpCore::Media::M3USerializer::deserialize(stream1);
        ASSERT_EQ(1u, list1.size());
        ASSERT_NE(nullptr, list1[0].getUri());
        ASSERT_EQ(*m_file2.getUri(), *list1[0].getUri());

        std::fstream stream2(fileName2);
        ASSERT_TRUE(stream2.is_open());

        auto list2 = rpCore::Media::M3USerializer::deserialize(stream2);
        ASSERT_EQ(2u, list2.size());
        ASSERT_NE(nullptr, list2[0].getUri());
        ASSERT_NE(nullptr, list2[1].getUri());
        ASSERT_EQ(*m_file.getUri(), *list2[0].getUri());
        ASSERT_EQ(*m_file2.getUri(), *list2[1].getUri());
    }

    TEST_F(PlaylistAssistantTests, catch_event_on_removing_tracks) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, appendNewItem(_)).Times(12);
        EXPECT_CALL(client, removeItems(TrackIndexList({ 1 })));
        EXPECT_CALL(client, removeItems(TrackIndexList({ 5 })));
        EXPECT_CALL(client, removeItems(TrackIndexList({ 7, 8 })));
        EXPECT_CALL(client, removeItems(TrackIndexList({ 10, 11 })));
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0));

        client.setupPlayer();
        auto &playlists = client.getPlaylists();
        for (auto &item : m_unsortedMediaList)
            playlists.appendToOpenedPlaylist(item);

        TrackIndexList removedItems = {1, 5, 7, 8, 10, 11};
        playlists.removeMedia(0, removedItems);
    }

    TEST_F(PlaylistAssistantTests, catch_clear_event_on_closing_one_existing_playlist) {
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems()).Times(4);
        EXPECT_CALL(client, requestPlaylistClose(_)).Times(0);

        client.setupPlayer();
        client.getPlaylists().closePlaylist(0);
        client.getPlaylists().closePlaylist(0);
        client.getPlaylists().closePlaylist(0);
    }

    TEST_F(PlaylistAssistantTests, catch_event_on_copying_tracks) {
        NiceMock<ActionListenerMock> actionListener;
        NiceMock<PlayerFillerClientMock> client;
        NiceMock<PlayerFillerClientMock> client2;

        EXPECT_CALL(client, passCurrentTrack(_, 0, 0));

        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client2, clearItems());

        EXPECT_CALL(client, appendNewItem(_)).Times(12);
        EXPECT_CALL(client2, appendNewItem(_)).Times(6);

        EXPECT_CALL(client2, insertNewItem(_, 0));
        EXPECT_CALL(client2, insertNewItem(_, 1));
        EXPECT_CALL(client2, insertNewItem(_, 2));
        EXPECT_CALL(client2, insertNewItem(_, 3));
        EXPECT_CALL(client2, insertNewItem(_, 4));
        EXPECT_CALL(client2, insertNewItem(_, 5));

        client.setupPlayer();
        auto &playlists = client.getPlaylists();
        for (auto &item : m_unsortedMediaList)
            playlists.appendToOpenedPlaylist(item);

        playlists.createPlaylist(client2, actionListener);

        TrackIndexList copyItems = { 1, 5, 7, 8, 10, 11 };
        playlists.insertByCopying(0, 1, copyItems, 0);

        copyItems = { 0, 1, 2, 3, 4, 5 };
        playlists.insertByCopying(1, 1, copyItems, 0);

        copyItems = { 100, 500 };
        ASSERT_THROW(playlists.insertByCopying(1, 1, copyItems, 0), std::out_of_range);
    }

    TEST_F(PlaylistAssistantTests, insert_media_items_from_serialized_text_data) {
        std::stringstream stream;
        stream << TestUtils::getTestFileUri(TestMP3File) << "\r\n"
               << TestUtils::getTestFileUri(TestDirectory) << "\r\n"
               << TestUtils::getTestFileUri(Test2SecMP3File) << "\r\n";
        auto data = stream.str();

        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, clearItems());
        EXPECT_CALL(client, passCurrentTrack(_, 0, 0));
        EXPECT_CALL(client, appendNewItem(_)).Times(FilesInTestDirectory + 5u);
        EXPECT_CALL(client, insertNewItem(_, _)).Times(FilesInTestDirectory + 5u);

        client.setupPlayer();

        client.getPlaylists().insertSerializedData(data, 0);
        client.getPlaylists().insertSerializedData(data, 1);
    }

    TEST_F(PlaylistAssistantTests, get_name_suggestions_for_playlists) {
        NiceMock<ActionListenerMock> actionListener;
        NiceMock<PlayerFillerClientMock> client;
        EXPECT_CALL(client, passSuggestedName(std::string("Z-Sinus artist")));
        EXPECT_CALL(client, passSuggestedName(std::string("Unknown Sinus")));
        EXPECT_CALL(client, passSuggestedName(std::string("Sobaka barabaka")));
        EXPECT_CALL(client, passSuggestedName(std::string("Sinus artist")));
        client.setupPlayer();

        auto index1 = client.getPlaylists().createPlaylist(client, actionListener);
        client.getPlaylists().setOpenedPlaylist(index1);
        client.getPlaylists().appendToOpenedPlaylist(m_unsortedMediaList[11]);

        auto index2 = client.getPlaylists().createPlaylist(client, actionListener);
        client.getPlaylists().setOpenedPlaylist(index2);
        client.getPlaylists().appendToOpenedPlaylist(m_unsortedMediaList[6]);

        auto index3 = client.getPlaylists().createPlaylist(client, actionListener);
        client.getPlaylists().setOpenedPlaylist(index3);
        client.getPlaylists().appendToOpenedPlaylist(m_unsortedMediaList[4]);

        auto index4 = client.getPlaylists().createPlaylist(client, actionListener);
        client.getPlaylists().setOpenedPlaylist(index4);
        client.getPlaylists().appendToOpenedPlaylist(m_unsortedMediaList[5]);
    }

}
