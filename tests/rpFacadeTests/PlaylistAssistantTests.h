#pragma once

#include "rpCore/Media/MediaProxy.h"

#include <gtest/gtest.h>

#include <memory>

namespace rpFacadeTests {

    class PlaylistAssistantTests : public testing::Test
    {
    public:
        PlaylistAssistantTests();

    public:
        rpCore::Media::MediaProxy m_file;
        rpCore::Media::MediaProxy m_file2;

        rpCore::Media::MediaList m_unsortedMediaList;
    };

}
