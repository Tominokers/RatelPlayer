#include "PlaylistNameSuggesterTests.h"

#include "rpCore/Player/Playlist.h"
#include "rpFacade/Player/PlaylistNameSuggester.h"

#include "DataConstants.h"
#include "TestUtils.h"

#include <memory>

using namespace rpCore::Player;
using namespace rpFacade::Player;

namespace rpFacadeTests {

    TEST_F(PlaylistNameSuggesterTests, get_empty_suggested_name) {
        PlaylistNameSuggester suggester;
        Playlist playlist;
        playlist.registrateListener(&suggester);

        ASSERT_EQ(std::string("Empty playlist"), suggester.getSuggestedName());
    }

    TEST_F(PlaylistNameSuggesterTests, get_suggestion_after_single_add) {
        PlaylistNameSuggester suggester;
        Playlist playlist;
        playlist.registrateListener(&suggester);

        playlist.addTrack(TestUtils::getTestMediaFile(Test2SecMP3File));

        ASSERT_EQ(std::string("Sinus artist"), suggester.getSuggestedName());
    }

    TEST_F(PlaylistNameSuggesterTests, get_suggestions_after_few_adding) {
        PlaylistNameSuggester suggester;
        Playlist playlist;
        playlist.registrateListener(&suggester);

        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 2.mp3"));
        ASSERT_EQ(std::string("Z-Sinus artist"), suggester.getSuggestedName());

        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 3.mp3"));
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 4.mp3"));
        ASSERT_EQ(std::string("Unknown Sinus"), suggester.getSuggestedName());

        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 9.mp3"));
        ASSERT_EQ(std::string("Unknown Sinus"), suggester.getSuggestedName());
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 11.mp3"));
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 12.mp3"));
        ASSERT_EQ(std::string("Sobaka barabaka"), suggester.getSuggestedName());

        playlist.insertTracks(0, { TestUtils::getTestMediaFile(Test2SecMP3File),
                                   TestUtils::getTestMediaFile(BigMediaDirectory + "/test 1.mp3") });
        ASSERT_EQ(std::string("Sobaka barabaka"), suggester.getSuggestedName());
        playlist.insertTracks(1, { TestUtils::getTestMediaFile(BigMediaDirectory + "/test 8.mp3"),
                                   TestUtils::getTestMediaFile(BigMediaDirectory + "/test 10.mp3") });
        ASSERT_EQ(std::string("Sinus artist"), suggester.getSuggestedName());
    }

    TEST_F(PlaylistNameSuggesterTests, get_suggestions_after_few_removing) {
        PlaylistNameSuggester suggester;
        Playlist playlist;
        playlist.registrateListener(&suggester);

        playlist.addTrack(TestUtils::getTestMediaFile(Test2SecMP3File));                   // 0
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 1.mp3")); // 1
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 2.mp3")); // 2
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 3.mp3")); // 3
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 4.mp3")); // 4
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 8.mp3")); // 5
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 9.mp3")); // 6
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 10.mp3"));// 7
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 11.mp3"));// 8
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 12.mp3"));// 9
        ASSERT_EQ(std::string("Sinus artist"), suggester.getSuggestedName());

        playlist.removeTracks({1, 5, 7});
        ASSERT_EQ(std::string("Sobaka barabaka"), suggester.getSuggestedName());

        playlist.removeTracks({5, 6});
        ASSERT_EQ(std::string("Unknown Sinus"), suggester.getSuggestedName());

        playlist.clearTracks();
        ASSERT_EQ(std::string("Empty playlist"), suggester.getSuggestedName());
    }

    TEST_F(PlaylistNameSuggesterTests, check_that_sort_does_not_change_suggestion) {
        PlaylistNameSuggester suggester;
        Playlist playlist;
        playlist.registrateListener(&suggester);

        playlist.addTrack(TestUtils::getTestMediaFile(Test2SecMP3File));                   // 0
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 1.mp3")); // 1
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 2.mp3")); // 2
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 3.mp3")); // 3
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 4.mp3")); // 4
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 8.mp3")); // 5
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 9.mp3")); // 6
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 10.mp3"));// 7
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 11.mp3"));// 8
        playlist.addTrack(TestUtils::getTestMediaFile(BigMediaDirectory + "/test 12.mp3"));// 9

        ASSERT_EQ(std::string("Sinus artist"), suggester.getSuggestedName());
        playlist.sort(PlaylistSortType::Album);
        ASSERT_EQ(std::string("Sinus artist"), suggester.getSuggestedName());
    }

}
