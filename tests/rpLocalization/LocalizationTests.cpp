#include "LocalizationTests.h"

#include "DataConstants.h"

#include "rpLocalization/LocalizationKeys.h"

using namespace rpLocalization;

namespace rpLocalizationTests {

    LocalizationTests::LocalizationTests()
        : m_loc("")
    {
    }

    TEST_F(LocalizationTests, check_english_string_parsing) {
        auto quitResult = parseLine(L"Quit=quit");
        ASSERT_EQ(LocalizerKeys::Quit, quitResult.first);
        ASSERT_EQ(std::wstring(L"quit"), quitResult.second);

        auto iconResult = parseLine(L"Icon=");
        ASSERT_EQ(LocalizerKeys::Icon, iconResult.first);
        ASSERT_EQ(std::wstring(), iconResult.second);

        auto trackResult = parseLine(L"TrackNumber=#");
        ASSERT_EQ(LocalizerKeys::TrackNumber, trackResult.first);
        ASSERT_EQ(std::wstring(L"#"), trackResult.second);

        auto dateResult = parseLine(L"TrackDate=YY=MM=DD");
        ASSERT_EQ(LocalizerKeys::TrackDate, dateResult.first);
        ASSERT_EQ(std::wstring(L"YY=MM=DD"), dateResult.second);
    }

    TEST_F(LocalizationTests, check_ukrainian_string_parsing) {
        auto dateResult = parseLine(L"TrackDate=Рік");
        ASSERT_EQ(LocalizerKeys::TrackDate, dateResult.first);
        ASSERT_EQ(std::wstring(L"Рік"), dateResult.second);

        auto artistResult = parseLine(L"ArtistTitle=Іі=Її=Єє");
        ASSERT_EQ(LocalizerKeys::ArtistTitle, artistResult.first);
        ASSERT_EQ(std::wstring(L"Іі=Її=Єє"), artistResult.second);
    }

    TEST_F(LocalizationTests, check_russian_string_parsing) {
        auto tapResult = parseLine(L"DefaultPlaylistTap=Новый плейлист");
        ASSERT_EQ(LocalizerKeys::DefaultPlaylistTap, tapResult.first);
        ASSERT_EQ(std::wstring(L"Новый плейлист"), tapResult.second);

        auto fileNameResult = parseLine(L"FileName=Ыы=Ъъ=Ээ");
        ASSERT_EQ(LocalizerKeys::FileName, fileNameResult.first);
        ASSERT_EQ(std::wstring(L"Ыы=Ъъ=Ээ"), fileNameResult.second);
    }

    TEST_F(LocalizationTests, check_edge_parsing_cases) {
        auto emptyResult = parseLine(L"");
        ASSERT_EQ(LocalizerKeys::NO_VALUE, emptyResult.first);
        ASSERT_EQ(std::wstring(), emptyResult.second);

        auto iconResult = parseLine(L"Icon");
        ASSERT_EQ(LocalizerKeys::NO_VALUE, iconResult.first);
        ASSERT_EQ(std::wstring(), iconResult.second);

        auto wrongResult = parseLine(L"Трололо=РР=ММ=ДД");
        ASSERT_EQ(LocalizerKeys::NO_VALUE, wrongResult.first);
        ASSERT_EQ(std::wstring(), wrongResult.second);
    }

    TEST_F(LocalizationTests, configure_localizer_with_invalid_dic_location) {
        Localizer::setDefaultLanguage(Language::AUTO);
        ASSERT_NE(Language::AUTO, Localizer::getLanguage());
        Localizer::setDictionariesLocation("/some/incorrect/directory/");
        // setDictionariesLocation will cause update and current language will be changed.
        ASSERT_NE(Language::AUTO, Localizer::getLanguage());
        ASSERT_EQ(std::wstring(), Localizer::get(LocalizerKeys::ArtistTitle));
    }

    TEST_F(LocalizationTests, load_english_strings_from_dictionary) {
        Localizer::setDefaultLanguage(Language::EN);
        ASSERT_EQ(Language::EN, Localizer::getLanguage());
        Localizer::setDictionariesLocation(DictionariesDirectory);
        ASSERT_EQ(Language::EN, Localizer::getLanguage());

        ASSERT_EQ(std::wstring(L"Artist"), Localizer::get(LocalizerKeys::ArtistTitle));
        ASSERT_EQ(std::wstring(L"Quit"), Localizer::get(LocalizerKeys::Quit));
        ASSERT_EQ(std::wstring(L""), Localizer::get(LocalizerKeys::Icon));
        ASSERT_EQ(std::wstring(L"#"), Localizer::get(LocalizerKeys::TrackNumber));
        ASSERT_EQ(std::wstring(L"New playlist"), Localizer::get(LocalizerKeys::DefaultPlaylistTap));
    }

    TEST_F(LocalizationTests, load_ukrainian_strings_from_dictionary) {
        Localizer::setDefaultLanguage(Language::UA);
        ASSERT_EQ(Language::UA, Localizer::getLanguage());
        Localizer::setDictionariesLocation(DictionariesDirectory);
        ASSERT_EQ(Language::UA, Localizer::getLanguage());

        ASSERT_EQ(std::wstring(L"Виконавець"), Localizer::get(LocalizerKeys::ArtistTitle));
        ASSERT_EQ(std::wstring(L"Вихід"), Localizer::get(LocalizerKeys::Quit));
        ASSERT_EQ(std::wstring(L""), Localizer::get(LocalizerKeys::Icon));
        ASSERT_EQ(std::wstring(L"#"), Localizer::get(LocalizerKeys::TrackNumber));
        ASSERT_EQ(std::wstring(L"Новий плейліст"), Localizer::get(LocalizerKeys::DefaultPlaylistTap));
    }

    TEST_F(LocalizationTests, load_russian_strings_from_dictionary) {
        Localizer::setDefaultLanguage(Language::RU);
        ASSERT_EQ(Language::RU, Localizer::getLanguage());
        Localizer::setDictionariesLocation(DictionariesDirectory);
        ASSERT_EQ(Language::RU, Localizer::getLanguage());

        ASSERT_EQ(std::wstring(L"Исполнитель"), Localizer::get(LocalizerKeys::ArtistTitle));
        ASSERT_EQ(std::wstring(L"Выход"), Localizer::get(LocalizerKeys::Quit));
        ASSERT_EQ(std::wstring(L""), Localizer::get(LocalizerKeys::Icon));
        ASSERT_EQ(std::wstring(L"#"), Localizer::get(LocalizerKeys::TrackNumber));
        ASSERT_EQ(std::wstring(L"Новый плейлист"), Localizer::get(LocalizerKeys::DefaultPlaylistTap));
    }

    TEST_F(LocalizationTests, check_that_second_loc_instance_throw_exception) {
        ASSERT_THROW(Localizer loc(""), std::domain_error);
    }

}
