#pragma once

#include "rpLocalization/Localizer.h"

#include <gtest/gtest.h>

namespace rpLocalizationTests {

    class LocalizationTests : public testing::Test
    {
    public:
        LocalizationTests();

    private:
        rpLocalization::Localizer m_loc;
    };

}
