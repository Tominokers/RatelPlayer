#include "DirectoryObserverTest.h"

#include "DataConstants.h"

#include "rpCore/Media/MediaFile.h"
#include "rpCore/Media/MediaProxy.h"
#include "rpObservers/ObserverCreator.h"
#include "rpObservers/Observers/DirectoryObserver.h"
#include "rpObservers/States/DirectoryState.h"

#include <string>

using namespace rpCore::Media;
using namespace rpObservers;
using namespace rpObservers::States;
using namespace rpObservers::Observers;

namespace rpObserversTests {

    TEST_F(DirectoryObserverTest, open_home_directory_from_path) {
        auto homePath = DirectoryObserver::getHomePath();

        ASSERT_FALSE(homePath.empty());
        ASSERT_TRUE(homePath.find("/home/") == 0);

        auto dir = DirectoryObserver::createFromPath(homePath);

        ASSERT_NE(nullptr, dir.get());
        dir->Update();
        ASSERT_FALSE(dir->empty());
    }

    TEST_F(DirectoryObserverTest, get_media_files_from_directory) {
        auto dir = DirectoryObserver::createFromPath(MediaDirectory);

        ASSERT_NE(nullptr, dir.get());
        dir->Update();
        ASSERT_EQ(FilesInMediaDirectory, dir->getMediaCount());

        for (size_t i = 0; i < dir->getMediaCount(); ++i)
        {
            ASSERT_NO_THROW(dir->extractMediaAt(i));
            ASSERT_NE(nullptr, dir->extractMediaAt(i).get());
        }
    }

    TEST_F(DirectoryObserverTest, get_subdirectories_from_directory) {
        auto dir = DirectoryObserver::createFromPath(MediaDirectory);

        ASSERT_NE(nullptr, dir.get());
        dir->Update();
        ASSERT_EQ(3u, dir->getSubobserversCount());

        size_t notEmptyCount = 0;
        for (size_t i = 0; i < dir->getSubobserversCount(); ++i)
        {
            std::unique_ptr<DirectoryObserver> subdir;
            ASSERT_NO_THROW(subdir.reset(reinterpret_cast<DirectoryObserver*>(dir->extractObserverAt(i).release())));

            ASSERT_NE(nullptr, subdir.get());
            subdir->Update();

            if (!subdir->empty())
            {
                notEmptyCount++;
            }
        }

        ASSERT_EQ(1u, notEmptyCount);
    }

    TEST_F(DirectoryObserverTest, get_directory_observer_from_state) {
        DirectoryState state(DirectoryObserver::getHomePath());
        auto dir = ObserverCreator::create(state);

        ASSERT_NE(nullptr, dir.get());
        dir->Update();
        ASSERT_NE(0u, dir->getSubobserversCount());
    }

    TEST_F(DirectoryObserverTest, get_directory_items_info) {
        auto homePath = DirectoryObserver::getHomePath();
        DirectoryState state(homePath);
        auto dir0 = ObserverCreator::create(state);
        auto dir1 = DirectoryObserver::createFromPath(BigMediaDirectory);
        auto dir2 = DirectoryObserver::createFromPath(MediaDirectory);

        const auto &info0 = dir0->getInfo();
        const auto &info1 = dir1->getInfo();
        const auto &info2 = dir2->getInfo();

        ASSERT_EQ(homePath, info0.getFullName());
        ASSERT_NE(std::string(), info0.getName());
        ASSERT_NE(std::string(), info0.getDate());

        ASSERT_EQ("BigTestMediaFolder", info1.getName());
        ASSERT_NE(std::string(), info1.getFullName());
        ASSERT_NE(std::string(), info1.getDate());

        ASSERT_EQ("TestMediaFolder", info2.getName());
        ASSERT_NE(std::string(), info2.getFullName());
        ASSERT_NE(std::string(), info2.getDate());
    }

}
