#include "SettingManagerTests.h"

#include "Mocked/SettingStorageMock.h"
#include "Mocked/SettingManagerMock.h"
#include "TestClients/SettingManagerClient.h"
#include "TestClients/SettingStorageClient.h"

#include "rpObservers/ObserverCreator.h"
#include "rpObservers/Observers/DirectoryObserver.h"
#include "rpObservers/States/DirectoryState.h"

#include <sstream>

using namespace Mocked;
using namespace TestClients;
using namespace rpObservers::Observers;
using namespace rpSettings;

using ::testing::Invoke;
using ::testing::Ref;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::Sequence;

namespace rpSettingsTests {

    static const auto home = DirectoryObserver::getHomePath();
    static const std::string DirectoryObserverStateKey = "DirectoryObserverState";
    static const std::string prefix0 = "prefix0";
    static const std::string prefix1 = "prefix1";
    static const std::string prefix2 = "prefix2";

    static const std::string CustomSetting0 = "CustomSetting0";
    static const std::string CustomSetting1 = "CustomSetting1";
    static const std::string SomeValue0 = "SomeValue0";
    static const std::string SomeValue1 = "SomeValue1";
    static char separator = ':';

    static const std::string flush0 = prefix0 + separator
                                    + DirectoryObserverStateKey + separator
                                    + home;
    static const std::string flush1 = prefix1 + separator
                                    + CustomSetting0 + separator
                                    + SomeValue0;
    static const std::string flush2 = prefix1 + separator
                                    + DirectoryObserverStateKey + separator
                                    + home;
    static const std::string flush3 = prefix0 + separator
                                    + CustomSetting1 + separator
                                    + SomeValue1;

    std::stringstream getTestFilledStream() {
        std::stringstream ss;
        ss << "[StorageIds]\n"
           << prefix0 << "\n" << prefix1 << "\n"
           << "[Settings]\n"
           << flush0 << "\n" << flush1 << "\n"
           << flush2 << "\n" << flush3 << "\n";
        ss.seekg(0);

        return ss;
    }


    TEST_F(SettingManagerTests, create_storage_via_setting_manager) {
        auto prototype = std::make_unique<SettingStorageMock>("");
        auto storagePtr = new SettingStorageMock(prefix0);

        EXPECT_CALL(*prototype, clone())
            .WillOnce(Invoke([storagePtr](){ return std::unique_ptr<SettingStorageMock>(storagePtr); }));
        EXPECT_CALL(*storagePtr, storeImpl(DirectoryObserverStateKey, home));

        SettingManagerClient manager(std::move(prototype));
        auto &storage0 = static_cast<SettingStorageMock&>(manager.getStorage(prefix0));

        ASSERT_EQ(storagePtr->getId(), storage0.getId());

        storage0.store(DirectoryObserverStateKey, home);
    }

    TEST_F(SettingManagerTests, dump_all_settings_to_stream) {
        auto prototype = std::make_unique<SettingStorageClient>("");
        std::string etalon0 = prefix0 + separator + DirectoryObserverStateKey + separator + home + "\n";
        std::string etalon1 = prefix1 + separator + DirectoryObserverStateKey + separator + home + "\n";
        std::string etalon2 = prefix2 + separator + DirectoryObserverStateKey + separator + home + "\n";

        SettingManagerClient manager(std::move(prototype));
        manager.getStorage(prefix0).store(DirectoryObserverStateKey, home);
        manager.getStorage(prefix1).store(DirectoryObserverStateKey, home);
        manager.getStorage(prefix2).store(DirectoryObserverStateKey, home);

        manager.dump();

        auto result = manager.getDumpStream().str();

        ASSERT_TRUE(result.find(etalon0) != std::string::npos);
        ASSERT_TRUE(result.find(etalon1) != std::string::npos);
        ASSERT_TRUE(result.find(etalon2) != std::string::npos);
    }

    TEST_F(SettingManagerTests, check_calls_during_settings_dumping) {
        std::stringstream ss;
        auto prototype = std::make_unique<SettingStorageMock>("");
        auto prototypePtr = prototype.get();
        auto storagePtr0 = new SettingStorageMock("");
        auto storagePtr1 = new SettingStorageMock("");
        auto storagePtr2 = new SettingStorageMock("");

        SettingManagerMock manager(std::move(prototype));
        EXPECT_CALL(*prototypePtr, clone())
            .WillOnce(Invoke([storagePtr0](){ return std::unique_ptr<SettingStorageMock>(storagePtr0); }))
            .WillOnce(Invoke([storagePtr1](){ return std::unique_ptr<SettingStorageMock>(storagePtr1); }))
            .WillOnce(Invoke([storagePtr2](){ return std::unique_ptr<SettingStorageMock>(storagePtr2); }));

        manager.getStorage(prefix0);
        manager.getStorage(prefix1);
        manager.getStorage(prefix2);

        EXPECT_CALL(manager, getDumpStream())
            .WillOnce(ReturnRef(ss));

        EXPECT_CALL(*storagePtr0, flush(Ref(ss)));
        EXPECT_CALL(*storagePtr1, flush(Ref(ss)));
        EXPECT_CALL(*storagePtr2, flush(Ref(ss)));

        manager.dump();
    }

    TEST_F(SettingManagerTests, load_all_settings_from_stream) {
        std::stringstream ss = getTestFilledStream();

        auto prototype = std::make_unique<SettingStorageClient>("");
        SettingManagerClient manager(std::move(prototype));
        manager.setStreamContent(ss.str());
        manager.load();

        auto &storage0 = manager.getStorage(prefix0);
        auto &storage1 = manager.getStorage(prefix1);

        ASSERT_EQ(home, storage0.load(DirectoryObserverStateKey));
        ASSERT_EQ(SomeValue1, storage0.load(CustomSetting1));
        ASSERT_EQ(home, storage1.load(DirectoryObserverStateKey));
        ASSERT_EQ(SomeValue0, storage1.load(CustomSetting0));
    }

    TEST_F(SettingManagerTests, check_calls_during_settings_loading) {
        std::stringstream ss = getTestFilledStream();

        auto prototype = std::make_unique<SettingStorageMock>("");
        auto prototypePtr = prototype.get();
        auto storagePtr0 = new SettingStorageMock("");
        auto storagePtr1 = new SettingStorageMock("");

        SettingManagerMock manager(std::move(prototype));

        Sequence loading;
        EXPECT_CALL(manager, getLoadStream())
            .InSequence(loading)
            .WillOnce(ReturnRef(ss));

        EXPECT_CALL(*prototypePtr, clone())
            .InSequence(loading)
            .WillOnce(Invoke([storagePtr0](){ return std::unique_ptr<SettingStorageMock>(storagePtr0); }))
            .WillOnce(Invoke([storagePtr1](){ return std::unique_ptr<SettingStorageMock>(storagePtr1); }));

        EXPECT_CALL(*prototypePtr, extractOneFlush(flush0))
            .InSequence(loading)
            .WillOnce(Return(std::make_tuple(prefix0, DirectoryObserverStateKey, home)));

        EXPECT_CALL(*storagePtr0, storeImpl(DirectoryObserverStateKey, home))
            .InSequence(loading);

        EXPECT_CALL(*prototypePtr, extractOneFlush(flush1))
            .InSequence(loading)
            .WillOnce(Return(std::make_tuple(prefix1, CustomSetting0, SomeValue0)));
        EXPECT_CALL(*storagePtr1, storeImpl(CustomSetting0, SomeValue0))
            .InSequence(loading);

        EXPECT_CALL(*prototypePtr, extractOneFlush(flush2))
            .InSequence(loading)
            .WillOnce(Return(std::make_tuple(prefix1, DirectoryObserverStateKey, home)));
        EXPECT_CALL(*storagePtr1, storeImpl(DirectoryObserverStateKey, home))
            .InSequence(loading);

        EXPECT_CALL(*prototypePtr, extractOneFlush(flush3))
            .InSequence(loading)
            .WillOnce(Return(std::make_tuple(prefix0, CustomSetting1, SomeValue1)));
        EXPECT_CALL(*storagePtr0, storeImpl(CustomSetting1, SomeValue1))
            .InSequence(loading);

        manager.load();
    }

}
