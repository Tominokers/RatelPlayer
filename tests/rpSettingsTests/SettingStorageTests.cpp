#include "SettingStorageTests.h"

#include "DataConstants.h"
#include "Mocked/SettingStorageMock.h"
#include "TestClients/SettingStorageClient.h"

#include "rpObservers/ObserverCreator.h"
#include "rpObservers/Observers/DirectoryObserver.h"

using namespace TestClients;
using namespace rpSettings;
using namespace rpObservers::Observers;
using namespace Mocked;

using ::testing::Eq;
using ::testing::Return;

namespace rpSettingsTests {

    static const auto home = DirectoryObserver::getHomePath();
    static const std::string DirectoryObserverStateKey = "DirectoryObserverState";
    static const std::string prefix0 = "prefix0";
    static const std::string prefix1 = "prefix1";

    TEST_F(SettingStorageTests, save_and_load_settings_in_custom_storage) {
        std::string value = "some_state";
        auto key = DirectoryObserverStateKey;

        SettingStorageMock storage("");
        EXPECT_CALL(storage, storeImpl(Eq(key), Eq(value)));
        EXPECT_CALL(storage, loadImpl(Eq(key)))
            .WillOnce(Return(value));

        storage.store(key, value);
        ASSERT_EQ(value, storage.load(key));
    }

    TEST_F(SettingStorageTests, flush_stored_settings_to_stream) {
        SettingStorageClient storage0(prefix0);
        storage0.store(DirectoryObserverStateKey, home);

        SettingStorageClient storage1(prefix1);
        storage1.store(DirectoryObserverStateKey, home);

        std::stringstream ss;
        storage0.flush(ss);
        storage1.flush(ss);

        std::string etalon = prefix0 + storage0.getSeparator()
            + DirectoryObserverStateKey + storage0.getSeparator()
            + home + "\n"
            + prefix1 + storage1.getSeparator()
            + DirectoryObserverStateKey + storage1.getSeparator()
            + home + "\n";
        auto result = ss.str();

        ASSERT_EQ(etalon, result);
    }

    TEST_F(SettingStorageTests, extract_flushed_settings_from_date) {
        SettingStorageClient storage("");
        auto data = prefix0 + storage.getSeparator() +
                    DirectoryObserverStateKey + storage.getSeparator() +
                    home;

        auto extracted = storage.extractOneFlush(data);

        ASSERT_EQ(prefix0, std::get<0>(extracted));
        ASSERT_EQ(DirectoryObserverStateKey, std::get<1>(extracted));
        ASSERT_EQ(home, std::get<2>(extracted));
    }

}
